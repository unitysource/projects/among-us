﻿using UnityEngine;

namespace _Scripts
{
    public class NpcBlock : MonoBehaviour
    {
        private GameObject _main;

        private void Awake() => 
            _main = transform.parent.gameObject;

        private void Update()
        {
            if (!_main) Destroy(gameObject);
        }
    }
}