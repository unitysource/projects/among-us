﻿using _Scripts.MYSELF;
using UnityEngine;

namespace _Scripts
{
    public class Npc : MonoBehaviour
    { 
        public GameObject _triangle;
        
        private float _countdown;
        private Vector3 _targ;
        private GameObject _character;
        private bool _act;
        private GameObject _newMain;

        private void Start()
        {
            _character = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>().hero;
            _countdown = Random.Range(-0.74f, 1.43f);
            var a = Instantiate(_triangle, transform.position, _triangle.transform.rotation);
            foreach (Transform child in a.transform) child.GetComponent<Triangles>().npc = gameObject;
        }

        private void Update()
        {
            if (!_character) return;
        
            _countdown += Time.deltaTime;

            if (_countdown > 2.3f)
            {
                var dist = Vector3.Distance(transform.position, _character.transform.position);
                if (dist > 26)
                {
                    if (_act)
                    {
                        if (GameObject.Find(gameObject.name + "Main"))
                        {
                            _newMain = GameObject.Find(gameObject.name + "Main");
                            foreach (Transform tr in _newMain.transform)
                            {
                                tr.gameObject.active = false;
                            }
                        }
                        tag = "Untagged";
                    }
                    _act = false;
                }

                if (dist < 24 && _act == false)
                {
                    tag = "MainMinion";
                    gameObject.GetComponent<Controller>().BeginAddNpc();
                    _act = true;
                }
            }
        }
    }
}