﻿
namespace _Scripts.Audio {

    namespace Audio {

        public enum AudioType {
            None,
            Sfx01,
            Sfx02,
            Sfx03,
            Sfx04,
            Sfx05,
            Sfx06,
            Sfx07,
            Sfx08,
            Sfx09,
            Sfx10,
            Sfx11,
            Music,
            GameOver,
            GameOver2,
            LevelFinished,
            YeahSfx,
            Kill,
            CameraSfx,
            ClickUi,
        }
    }
}