﻿using _Scripts.MYSELF;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Scripts
{
    public class MoveCam : MonoBehaviour
    {
        public float tempo;
        
        private GameObject _character;

        private void Start() => 
            _character = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>().hero;

        private void LateUpdate()
        {
            if (_character)
                transform.position = Vector3.Lerp(transform.position, _character.transform.position + new Vector3(0, 18f, -15.5f),
                    Time.deltaTime * tempo);
        }
    }
}