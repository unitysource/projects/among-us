﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts
{
    public class RowController : MonoBehaviour
    {
        [SerializeField] private float yOffset = 30f;

        public GameObject hat;
        public GameObject[] heroesUi;
        public GameObject[] heroes;

        private readonly GameObject[] _heroes = new GameObject[10];
        private readonly int[] _heroesResult = new int[10];

        private float _countdown;
        private int _curHero;
        private int _curScore;
        private bool _isColSet;
        private float _startYOffset;

        private void Start()
        {
            _isColSet = false;
            _startYOffset = heroesUi[0].GetComponent<RectTransform>().anchoredPosition.y;
        }

        private void Update()
        {
            _countdown += Time.deltaTime;
            if (_countdown > 1)
            {
                _countdown = 0;
                if (_isColSet == false)
                {
                    _isColSet = true;
                    SetColor();
                }

                GetFacts();
            }
        }

        private void FixedUpdate()
        {
            if (_heroes[0])
                hat.transform.position = _heroes[0].transform.position;
        }

        private void SetColor()
        {
            for (var i = 0; i < heroesUi.Length; i++)
            {
                var sss = heroes[i].GetComponent<Controller>().myString;
                if (sss.Length > 10)
                    sss = sss.Substring(0, 11);

                heroesUi[i].GetComponentInChildren<TextMeshProUGUI>().text = sss;
                heroesUi[i].transform.GetChild(1).GetComponent<Image>().color =
                    heroes[i].GetComponent<Controller>().col;
            }
        }

        private void GetFacts()
        {
            for (var i = 0; i < heroesUi.Length; i++)
            {
                _heroesResult[i] = 0;
                if (heroes[i])
                    _heroesResult[i] = heroes[i].GetComponent<Controller>().strokeAmount;
                else if (heroesUi[i])
                    Destroy(heroesUi[i]);
            }

            _curScore = -1;
            for (var i = 0; i < heroesUi.Length; i++)
                if (_curScore < _heroesResult[i])
                {
                    _curScore = _heroesResult[i];
                    _heroes[0] = heroes[i];
                    _curHero = i;
                }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, _startYOffset, 0);

                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[1] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";


                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[2] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 2, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";


                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[3] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 3, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";


                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[4] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 4, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3] && heroes[i] != _heroes[4])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[5] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 5, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3] && heroes[i] != _heroes[4] &&
                        heroes[i] != _heroes[5])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[6] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 6, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3] && heroes[i] != _heroes[4] &&
                        heroes[i] != _heroes[5] && heroes[i] != _heroes[6])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[7] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 7, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3] && heroes[i] != _heroes[4] &&
                        heroes[i] != _heroes[5] && heroes[i] != _heroes[6] && heroes[i] != _heroes[7])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[8] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 8, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";

                _curScore = -1;
                for (var i = 0; i < 10; i++)
                    if (_curScore < _heroesResult[i] && heroes[i] != _heroes[0] && heroes[i] != _heroes[1] &&
                        heroes[i] != _heroes[2] && heroes[i] != _heroes[3] && heroes[i] != _heroes[4] &&
                        heroes[i] != _heroes[5] && heroes[i] != _heroes[6] && heroes[i] != _heroes[7] &&
                        heroes[i] != _heroes[8])
                    {
                        _curScore = _heroesResult[i];
                        _heroes[9] = heroes[i];
                        _curHero = i;
                    }
            }

            if (heroesUi[_curHero])
            {
                heroesUi[_curHero].GetComponent<RectTransform>().anchoredPosition =
                    new Vector3(0, _startYOffset - yOffset * 9, 0);
                heroesUi[_curHero].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text =
                    _curScore + "";
            }
        }
    }
}