﻿using System;
using System.Collections.Generic;
using _Scripts.Audio.Audio;
using _Scripts.MYSELF;
using _Scripts.other;
using CnControls;
using UnityEngine;
using AudioType = _Scripts.Audio.Audio.AudioType;
using Random = UnityEngine.Random;

namespace _Scripts
{
    public class Controller : MonoBehaviour
    {
        public int rank;
        public GameObject minionPrefab;
        public int countOfNpc;
        public string myString;
        public bool npc;
        public int result;
        public int resultAll;
        public GameObject joystick;
        public GameObject wild;
        public float tempo;
        public float myTempo;
        public float myTarget;
        public int strokeAmount;
        public GameObject strokeAmountBlock;
        public GameObject resultBlock;
        public GameObject txt;
        public GameObject myStringName;
        public GameObject improveGameObject;
        public GameObject targetEff;
        public Color col;
        public bool isStart;
        public bool isClone;

        private List<GameObject> _allChildren;
        private int _chanceX2;
        private GameObject _character;
        private HeroContainer _container;
        private float _countdown;
        private int _foodBoxAmount;
        private bool _isCreateChild;
        private int _minChildren;
        private GameObject _npcLoad;
        private int _sCount;
        private GameObject _skin;
        private float _soundTimer;
        private float _t;
        private Vector3 _targ;
        private float _tempoBonus;
        private int _tempStroke;


        private void Awake()
        {
            if (isClone == false)
            {
                _allChildren = new List<GameObject>();

                _tempoBonus = 1;
                if (!npc) myString = PlayerPrefs.GetString("Nickname");

                var resp = GameObject.FindGameObjectsWithTag("Respawn");
                if (resp.Length > 0)
                {
                    var rnd = Random.Range(0, resp.Length);
                    transform.position = resp[rnd].transform.position;
                    resp[rnd].tag = "Untagged";
                    Destroy(resp[rnd], 1);
                }
            }
        }

        private void Start()
        {
            _character = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>().hero;
            _container = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>();

            if (isClone == false)
            {
                if (npc)
                {
                    isStart = true;
                    var skn = Instantiate(
                        _container.allSkins.allSkins[Random.Range(0, _container.allSkins.allSkins.Length)],
                        transform.position, transform.rotation);
                    skn.transform.parent = transform;
                    _skin = skn;
                }
                else
                {
                    var skn = Instantiate(
                        _container.allSkins
                            .allSkins[PlayerPrefs.GetInt("curSkin")],
                        transform.position, transform.rotation);
                    skn.transform.parent = transform;
                    _skin = skn;
                }

                _t = Random.Range(0.01f, 1.78f);
                if (!GameObject.Find(gameObject.name + "Main"))
                {
                    var d = new GameObject {name = gameObject.name + "Main"};
                }


                if (!npc)
                {
                    tempo = tempo + PlayerPrefs.GetInt("skill1") * 0.06f;
                    strokeAmount = 1 + PlayerPrefs.GetInt("skill2") * 1 + PlayerPrefs.GetInt("crowdBonus");
                    BeginAddNpc();
                }
                else
                {
                    _chanceX2 = 4 + PlayerPrefs.GetInt("skill3") * 3;
                    tempo = tempo + PlayerPrefs.GetInt("skill1") * 0.02f;
                    strokeAmount = Random.Range(2, 19);
                }

                strokeAmountBlock.transform.parent = _container.boxesWithPetsContainer;
                txt.GetComponent<TextMesh>().text = strokeAmount + "";


                AddNewNpc();

                var i = 1;
                while (i < countOfNpc)
                {
                    AddNpc(i);
                    i++;
                }

                myStringName.GetComponent<TextMesh>().text = myString + "";


                foreach (Transform tr in strokeAmountBlock.transform)
                {
                    if (tr.CompareTag("iconBoost")) improveGameObject = tr.gameObject;
                    if (tr.CompareTag("effBronia")) targetEff = tr.gameObject;
                }

                improveGameObject.GetComponent<SpriteRenderer>().sprite = null;
                targetEff.active = false;
            }
        }

        private void Update()
        {
            myTempo -= Time.deltaTime;
            myTarget -= Time.deltaTime;

            if (myTempo < 0)
            {
                if (_tempoBonus != 1) improveGameObject.GetComponent<SpriteRenderer>().sprite = null;
                _tempoBonus = 1;
            }
            else
            {
                if (_tempoBonus != 1.5f)
                    improveGameObject.GetComponent<SpriteRenderer>().sprite = _container.coreSystem.speedups[0];
                _tempoBonus = 1.5f;
            }

            if (myTarget > 9)
            {
                targetEff.active = true;
            }
            else
            {
                if (myTarget < 0 && myTarget > -10)
                {
                    myTarget = -999;
                    targetEff.active = false;
                }
            }

            if (_isCreateChild)
            {
                CreateNpc();
                _tempStroke += 1;
                if (_tempStroke >= strokeAmount) _isCreateChild = false;
            }

            if (!npc)
                if (CnInputManager.GetAxis("Horizontal") != 0 || CnInputManager.GetAxis("Vertical") != 0)
                    Motion();

            if (npc)
            {
                _soundTimer -= Time.deltaTime;
                _t += Time.deltaTime;

                if (_t > 0.7f)
                {
                    _t = Random.Range(-0.30f, 0.11f);

                    float dist = 0;
                    if (_character) dist = Vector3.Distance(transform.position, _character.transform.position);
                    if (dist < 7.1f && _character)
                    {
                        if (_soundTimer < 0)
                        {
                            var randInt = Random.Range(1, 12);
                            AudioController.Instance.PlayAudio(GetEnumValue<AudioType>(randInt), true);
                            _soundTimer = 7;
                        }


                        if (_character.GetComponent<Controller>().strokeAmount > strokeAmount)
                            _targ = (transform.position - _character.transform.position) * 5;
                        else
                            _targ = _character.transform.position;

                        _foodBoxAmount = 0;
                    }
                    else
                    {
                        if (dist > 26) SettingsNpc();

                        if (_foodBoxAmount == 0)
                            _targ = _container.gradkas[Random.Range(0, _container.gradkas.Length)].transform.position;

                        _foodBoxAmount += 1;
                        if (_foodBoxAmount > 15) _foodBoxAmount = 0;
                    }

                    var raycastDistance = 2.5f;
                    RaycastHit hit;
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit,
                        raycastDistance))
                        if (!hit.collider.CompareTag("Minion") || !hit.collider.CompareTag("MainMinion") ||
                            !hit.collider.CompareTag("Food"))
                        {
                            var angle = transform.eulerAngles;
                            var rnd = Random.Range(0, 2);
                            if (rnd == 0) rnd = 45;
                            if (rnd == 1) rnd = -45;
                            var trueAngle = rnd;
                            float distRay = 99999;
                            var WayFinded = false;

                            transform.Rotate(Vector3.up * rnd);

                            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),
                                out hit,
                                raycastDistance * 1.4f))
                                distRay = hit.distance;
                            else
                                WayFinded = true;


                            transform.eulerAngles = angle;
                            transform.Rotate(Vector3.up * -rnd);

                            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),
                                out hit,
                                raycastDistance * 1.4f))
                            {
                                if (distRay > hit.distance && !WayFinded) trueAngle = rnd * -1;
                            }
                            else
                            {
                                if (!WayFinded) trueAngle = rnd * -1;
                                WayFinded = true;
                            }


                            if (!WayFinded)
                            {
                                transform.eulerAngles = angle;

                                transform.Rotate(Vector3.up * (trueAngle * 2));
                                if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),
                                    out hit,
                                    raycastDistance))
                                {
                                    WayFinded = true;
                                    trueAngle *= 2;
                                }

                                transform.Rotate(Vector3.up * trueAngle);
                                if (!WayFinded && !Physics.Raycast(transform.position,
                                    transform.TransformDirection(Vector3.forward), out hit,
                                    raycastDistance))
                                {
                                    WayFinded = true;
                                    trueAngle *= 3;
                                }

                                transform.Rotate(Vector3.up * trueAngle);
                                if (!WayFinded && !Physics.Raycast(transform.position,
                                    transform.TransformDirection(Vector3.forward), out hit,
                                    raycastDistance))
                                    trueAngle *= 4;
                            }


                            transform.eulerAngles = angle;
                            transform.Rotate(Vector3.up * trueAngle);

                            _targ = transform.TransformDirection(Vector3.forward * 100);
                            transform.eulerAngles = angle;
                        }
                }

                XzFocus(_targ, 22);
            }

            if (isStart) transform.Translate(Vector3.forward * (tempo * Time.deltaTime * _tempoBonus));
        }

        private void LateUpdate()
        {
            strokeAmountBlock.transform.position = transform.position + new Vector3(0, 2, 0);
        }


        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Minion"))
                if (collision.gameObject.GetComponent<NpcChild>().HasDead())
                {
                    var cg = collision.gameObject.GetComponent<NpcChild>().character;

                    if (cg != gameObject && cg != null)
                    {
                        _container.killEffect.transform.position =
                            collision.gameObject.transform.position + new Vector3(0, 0.5f, 0);
                        _container.killEffect.GetComponent<OutTransform>().Activate(0.5f);
                        collision.gameObject.GetComponent<NpcChild>().DeleteNpc();

                        cg.GetComponent<Controller>().DecreaseChild();
                        SetResult(20);
                    }
                }

            if (collision.gameObject.CompareTag("MainMinion"))
                if (strokeAmount > collision.gameObject.GetComponent<Controller>().strokeAmount)
                    if (collision.gameObject.GetComponent<Controller>().myTarget < 0)
                    {
                        if (!npc) AudioController.Instance.PlayAudio(AudioType.Kill, false, .3f);

                        _container.killEffect.transform.position =
                            collision.gameObject.transform.position + new Vector3(0, 0.5f, 0);
                        _container.killEffect.GetComponent<OutTransform>().Activate(0.5f);

                        if (collision.gameObject == _character)
                        {
                            _container.coreSystem.EndGame(false);

                            // AudioController.Instance.PlayAudio(AudioType.GameOver2,true);
                            // AudioController.Instance.PlayAudio(AudioType.LevelFinished);
                            AudioController.Instance.StopAudio(AudioType.Music, true);
                        }

                        Destroy(collision.gameObject);

                        var pet = GameObject.FindWithTag("PetCount");
                        var t = Instantiate(pet, transform.position + new Vector3(0, 1.5f, 0),
                            pet.transform.rotation);

                        var cnt = 5 + PlayerPrefs.GetInt("skill4") * 5;
                        t.GetComponent<ChildAmountText>().BeginBook(cnt.ToString());

                        for (var i = 0; i < cnt; i++) SetResult(20);
                    }
        }

        private void AddNpc(int i)
        {
            var main = GameObject.Find(gameObject.name + "Main");


            var gg = Instantiate(_npcLoad, transform.position, transform.rotation);


            gg.GetComponent<NpcChild>().SetCharacter(gameObject);
            gg.name = gameObject.name + "Minion";

            if (i < 15)
            {
                gg.GetComponent<NpcChild>().root.GetComponent<Animation>().enabled = true;
                gg.GetComponent<NpcChild>().pack.GetComponent<Animation>().enabled = true;
            }


            if (!main)
            {
                var d = new GameObject();
                d.name = gameObject.name + "Main";
                gg.transform.parent = d.transform;
            }
            else
            {
                gg.transform.parent = main.transform;
            }

            if (gg)
                if (_container.newPetEffect)
                {
                    _container.newPetEffect.transform.position = gg.transform.position;
                    _container.newPetEffect.transform.transform.parent = gg.transform;
                    _container.newPetEffect.GetComponent<OutTransform>().Activate(0.75f);
                }

            gg.transform.position = new Vector3(Random.Range(4444, 9999), 0, Random.Range(4444, 9999));
            _allChildren.Add(gg);
            gg.active = false;
        }

        public void SetShadeChildren(Color col)
        {
            this.col = col + new Color(0, 0, 0, 1);
            strokeAmountBlock.GetComponent<SpriteRenderer>().color = col;
            resultBlock.GetComponent<SpriteRenderer>().color = col;
        }

        public void BeginAddNpc()
        {
            _tempStroke = 0;
            _isCreateChild = true;
        }

        private void Motion()
        {
            var vec = wild.transform.position - joystick.transform.position;
            vec = new Vector3(vec.x, 0, vec.y);
            XzFocus(vec + transform.position, 35);
        }

        private void XzFocus(Vector3 point, float speed)
        {
            var direction = (point - transform.position).normalized;
            direction.y = 0f;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction),
                speed * Time.deltaTime * 10);
        }

        private void AddNewNpc()
        {
            var gg = Instantiate(minionPrefab, transform.position, minionPrefab.transform.rotation);
            gg.transform.Translate(Vector3.up * Random.Range(50, 2000));

            foreach (Transform tr in _skin.transform)
                if (tr.name == "Cube")
                {
                    var mats = gg.GetComponent<NpcChild>().root.GetComponent<MeshRenderer>().materials;
                    mats[0] = tr.GetComponent<SkinnedMeshRenderer>().materials[0];
                    mats[1] = tr.GetComponent<SkinnedMeshRenderer>().materials[2];
                    gg.GetComponent<NpcChild>().root.GetComponent<MeshRenderer>().materials = mats;
                    gg.GetComponent<NpcChild>().pack.GetComponent<MeshRenderer>().material =
                        tr.GetComponent<SkinnedMeshRenderer>().materials[1];
                }

            if (!npc) gg.layer = 9;
            gg.GetComponent<Rigidbody>().mass = 0.1f;
            _npcLoad = gg;
        }

        private void CreateNpc()
        {
            var skip = 100;
            if (_minChildren > countOfNpc - 2) _minChildren = 0;
            if (_minChildren < 0) _minChildren = 0;

            while (skip > 0)
            {
                skip = -99;
                if (_allChildren[_minChildren].active == false)
                {
                    _allChildren[_minChildren].transform.position = transform.position;
                    _allChildren[_minChildren].active = true;
                    _allChildren[_minChildren].GetComponent<NpcChild>().NpcReturn();
                    if (_allChildren[_minChildren])
                        if (_container.newPetEffect)
                        {
                            _container.newPetEffect.transform.position = _allChildren[_minChildren].transform.position;
                            _container.newPetEffect.transform.transform.parent = _allChildren[_minChildren].transform;
                            _container.newPetEffect.GetComponent<OutTransform>().Activate(0.75f);
                        }
                }

                skip -= 1;
                _minChildren += 1;
                if (_minChildren > countOfNpc - 2) _minChildren = 0;
            }
        }

        public void DecreaseChild()
        {
            strokeAmount -= 1;
            if (strokeAmount < 0) strokeAmount = 0;
            txt.GetComponent<TextMesh>().text = strokeAmount + "";
        }

        private void SettingsNpc()
        {
            var chp = _character.GetComponent<Controller>().strokeAmount;
            var rnd = Random.Range(-2, rank + 1);

            if (strokeAmount > 45 && strokeAmount > chp + 11)
            {
                strokeAmount -= Random.Range(1, 4);
            }
            else
            {
                var rndd = 1 + chp / 20;
                if (rndd > 12) rndd = 12;
                if (rnd == 0) strokeAmount += Random.Range(rndd / 4, 3 + chp / 10);
            }

            txt.GetComponent<TextMesh>().text = $"{strokeAmount.ToString()}";
        }

        public void SetResult(int ss)
        {
            if (gameObject == _character)
            {
                _sCount += 1;
                if (_sCount > 6) _sCount = Random.Range(-7, 1);
            }

            if (ss > 0) ss = 50;

            result += ss;
            resultAll += ss;

            if (result > 100)
            {
                result += 1;
                result -= 100;
                strokeAmount += 1;
                CreateNpc();

                var rn = Random.Range(0, 101);
                if (rn < _chanceX2)
                {
                    strokeAmount += 1;
                    CreateNpc();
                }
            }

            if (result < 0)
            {
                _container.coreSystem.GetComponent<PieceContainer>()
                    .SetPos(_allChildren[_minChildren].transform.position + new Vector3(0, 0.5f, 0));
                _container.coreSystem.GetComponent<PieceContainer>().ClonePart(1);

                _allChildren[_minChildren].active = false;
                strokeAmount -= 1;
                if (strokeAmount > 0)
                    result += 100;
            }

            txt.GetComponent<TextMesh>().text = strokeAmount + "";
        }

        public T GetEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum) throw new Exception("T must be an Enumeration type.");

            var val = ((T[]) Enum.GetValues(typeof(T)))[0];

            foreach (var enumValue in (T[]) Enum.GetValues(typeof(T)))
                if (Convert.ToInt32(enumValue).Equals(intValue))
                {
                    val = enumValue;
                    break;
                }

            return val;
        }
    }
}