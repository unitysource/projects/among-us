﻿using UnityEngine;

namespace _Scripts
{
    public class ChildAmountText : MonoBehaviour
    {
        private bool _isText;
        private float _countdown;

        private void Update()
        {
            if (_isText)
            {
                _countdown -= Time.deltaTime;
                if (_countdown < 0)
                    Destroy(gameObject);
                else if (_countdown < 1) 
                    GetComponent<TextMesh>().color = new Color(1f, 0.7f, 0, _countdown);
            }
        }

        public void BeginBook(string t)
        {
            GetComponent<TextMesh>().text = t;
            _isText = true;
            _countdown = 1.3f;
        }
    }
}