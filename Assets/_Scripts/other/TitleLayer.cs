﻿using UnityEngine;

namespace _Scripts.other
{
    public class TitleLayer : MonoBehaviour
    {
        public int layer;

        private void Start()
        {
            if (layer == 0) layer = 600;
            gameObject.GetComponent<MeshRenderer>().sortingOrder = layer;
        }
    }
}