﻿using _Scripts.Audio.Audio;
using UnityEngine;
using AudioType = _Scripts.Audio.Audio.AudioType;

namespace _Scripts.other
{
    public class Speedup : MonoBehaviour
    {
        public string speedupTitle;
        public GameObject upshot;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Minion"))
            {
                switch (speedupTitle)
                {
                    case "speedUp":
                        other.GetComponent<NpcChild>().character.GetComponent<Controller>().myTarget = -1f;
                        other.GetComponent<NpcChild>().character.GetComponent<Controller>().myTempo = 10f;
                        break;
                    case "shield":
                        other.GetComponent<NpcChild>().character.GetComponent<Controller>().myTempo = -1f;
                        other.GetComponent<NpcChild>().character.GetComponent<Controller>().myTarget = 10f;
                        break;
                    case "crowd":
                    {
                        var cnt = 10 * 5;
                        for (var i = 0; i < cnt; i++)
                            other.GetComponent<NpcChild>().character.GetComponent<Controller>().SetResult(20);
                        break;
                    }
                }

                var transform1 = transform;
                Instantiate(upshot, transform1.position, transform1.rotation);
                transform.Translate(Vector3.up * -999f);
                Destroy(gameObject, 2);
                if (!other.GetComponent<NpcChild>().character.GetComponent<Controller>().npc)
                    AudioController.Instance.PlayAudio(AudioType.Kill, true);
            }

            if (other.CompareTag("MainMinion"))
            {
                switch (speedupTitle)
                {
                    case "speedUp":
                        other.GetComponent<Controller>().myTempo = 10f;
                        break;
                    case "shield":
                        other.GetComponent<Controller>().myTarget = 10f;
                        break;
                    case "crowd":
                    {
                        var cnt = 5 * 5;
                        for (var i = 0; i < cnt; i++) other.GetComponent<Controller>().SetResult(20);
                        break;
                    }
                }

                var transform1 = transform;
                Instantiate(upshot, transform1.position, transform1.rotation);
                transform.Translate(Vector3.up * -999f);
                Destroy(gameObject, 2);
                if (!other.GetComponent<Controller>().npc) AudioController.Instance.PlayAudio(AudioType.Kill, true);
            }
        }
    }
}