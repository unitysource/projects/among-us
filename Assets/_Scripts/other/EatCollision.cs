﻿using _Scripts.Audio.Audio;
using UnityEngine;
using AudioType = _Scripts.Audio.Audio.AudioType;

namespace _Scripts.other
{
    public class EatCollision : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Minion") || other.CompareTag("MainMinion"))
            {
                var pet = GameObject.FindWithTag("PetCount");
                var t = Instantiate(pet, transform.position + new Vector3(0, 1.5f, 0),
                    pet.transform.rotation);
                t.GetComponent<ChildAmountText>().BeginBook("+1");

                AudioController.Instance.PlayAudio(AudioType.CameraSfx,true);

                if (gameObject) Destroy(gameObject);
            }

            if (other.CompareTag("MainMinion")) other.GetComponent<Controller>().SetResult(20);

            if (other.CompareTag("Minion")) other.GetComponent<NpcChild>().FoodEating();
        }
    }
}