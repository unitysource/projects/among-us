﻿using UnityEngine;

namespace _Scripts.other
{
    public class OutTransform : MonoBehaviour
    {
        private float _countdown;

        private void Awake()
        {
            _countdown = 0.5f;
        }

        private void Update()
        {
            _countdown -= Time.deltaTime;

            if (_countdown < 0)
            {
                var transform1 = transform;
                transform1.position = new Vector3(999, 999, 999);
                transform1.parent = null;
                enabled = false;
            }
        }

        public void Activate(float ti)
        {
            _countdown = ti;
            enabled = true;
        }
    }
}