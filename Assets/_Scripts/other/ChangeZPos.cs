﻿using UnityEngine;

namespace _Scripts.other
{
    public class ChangeZPos : MonoBehaviour
    {
        public float plate;
        public float zoomTempo;
        public float beginTempo;
        
        private float _beginS;
        private int _pace;
        private float _s;
        private float _tempo;
        private int _trigInt;

        private void Start()
        {
            _s = transform.localScale.x;
            _beginS = _s;

            _tempo = beginTempo;

            if (_tempo == 0) _tempo = 0.3f;
            if (plate == 0) plate = 1.2f;

            _pace = 1;
            _s = 0.01f;
            transform.localScale = Vector3.zero;
        }

        private void Update()
        {
            if (_pace == 0)
            {
                _s += Time.unscaledDeltaTime * _tempo;
                transform.localScale = new Vector3(_s, _s, 1);

                if (_s > _beginS * plate) _pace = 1;
            }
            else
            {
                _s -= Time.unscaledDeltaTime * _tempo;
                transform.localScale = new Vector3(_s, _s, 1);

                if (_s < _beginS)
                {
                    _pace = 0;
                    _trigInt++;
                    if (_trigInt > 1) _tempo = zoomTempo;
                }
            }
        }
    }
}