﻿using UnityEngine;

namespace _Scripts
{
    public class NpcChild : MonoBehaviour
    {
        public GameObject character;
        public GameObject root;
        public GameObject pack;
        public GameObject shieldEff;
        public GameObject deathEff;
        public GameObject respEff;
        private CoreSystem _coreSystem;

        private float _countdown;
        private bool _isHaveTag;
        private float _t;
        private float _tempo;
        private float _tempoBonus;

        private void Awake()
        {
            _coreSystem = GameObject.FindWithTag("Core").GetComponent<CoreSystem>();
        }

        private void Update()
        {
            var delt = Time.deltaTime;
            if (character)
            {
                _t -= delt;

                if (_t < 0)
                {
                    _t = 0.7f;
                    shieldEff.active = character.GetComponent<Controller>().myTarget > 0;
                    _tempoBonus = character.GetComponent<Controller>().myTempo > 0 ? 1.75f : 1f;
                }


                transform.Translate(Vector3.forward * (_tempo * 0.5f * delt * _tempoBonus));
                transform.position =
                    Vector3.Lerp(transform.position, character.transform.position, delt * _tempo * 0.2f);
                transform.rotation = character.transform.rotation;
                if (_countdown < 1)
                {
                    _countdown += delt;
                }
                else
                {
                    if (_isHaveTag == false)
                    {
                        gameObject.tag = "Minion";
                        _isHaveTag = true;
                    }
                }
            }
            else
            {
                _countdown += delt;
                if (_countdown >= 1)
                {
                    _coreSystem.GetComponent<PieceContainer>()
                        .SetPos(transform.position + new Vector3(0, 0.5f, 0));
                    _coreSystem.GetComponent<PieceContainer>().ClonePart(1);
                    DeleteNpc();
                }
            }
        }

        public bool HasDead()
        {
            var kill = shieldEff.active == false;
            return kill;
        }

        public void SetCharacter(GameObject h)
        {
            character = h;
            _tempo = character.GetComponent<Controller>().tempo;
            shieldEff.active = false;
            enabled = true;
            _tempoBonus = 1f;

            deathEff = _coreSystem.upshots[0];
            respEff = _coreSystem.upshots[1];
        }

        public void NpcReturn()
        {
            var re = Instantiate(respEff, transform.position, respEff.transform.rotation);
            re.transform.parent = transform;
            re.transform.localPosition = Vector3.zero;
        }

        public void DeleteNpc()
        {
            Instantiate(deathEff, transform.position, deathEff.transform.rotation);
            transform.position = Vector3.up * 999f;
            gameObject.active = false;
        }

        public void FoodEating()
        {
            character.GetComponent<Controller>().SetResult(20);

            var pet = GameObject.FindWithTag("PetCount");
            var t = Instantiate(pet, transform.position + new Vector3(0, 1.5f, 0),
                pet.transform.rotation);
            t.GetComponent<ChildAmountText>().BeginBook("+1");
        }
    }
}