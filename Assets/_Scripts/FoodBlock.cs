﻿using UnityEngine;

namespace _Scripts
{
    public class FoodBlock : MonoBehaviour
    {
        public GameObject[] food;

        private bool _isClear = true;
        private GameObject _block;
        private bool _isCountdown;
        private float _countdown;

        private void Start()
        {
            foreach (Transform child in transform)
                _block = child.gameObject;

            _block.GetComponent<MeshRenderer>().enabled = false;
        }

        private void Update()
        {
            if (_isCountdown)
            {
                _countdown += Time.deltaTime;
                if (_countdown > 10)
                {
                    _isClear = true;
                    foreach (Transform child in _block.transform) 
                        Destroy(child.gameObject);

                    _isCountdown = false;
                }
            }
        }

        private void OnBecameInvisible()
        {
            if (_isClear == false) _isCountdown = true;
        }

        private void OnBecameVisible()
        {
            _isCountdown = false;
            _countdown = 0;

            if (_isClear)
            {
                var rnd = Random.Range(0, 3);
                var fd = Instantiate(food[rnd], transform.position, food[rnd].transform.rotation);
                fd.transform.parent = _block.transform;
                rnd = Random.Range(0, 4);
                fd.transform.Rotate(Vector3.up * (rnd * 90));
                _isClear = false;
            }
        }
    }
}