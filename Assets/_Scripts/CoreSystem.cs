﻿using _Scripts.MYSELF;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
    public class CoreSystem : MonoBehaviour
    {
        [SerializeField] [Scene] private string sceneToLoad;
        [SerializeField] private TextMeshProUGUI countdown;
        [SerializeField] private TextMeshProUGUI yourCoins;
        [SerializeField] private TextMeshProUGUI yourCoinsFinish;
        [SerializeField] private GameObject tutorial;
        [SerializeField] private TextMeshProUGUI halfTimeTMP;
        [SerializeField] private GameObject jsCanvas;

        public GameObject partBlock;
        public GameObject continuePanel;
        public GameObject endPanel;
        public GameObject finishPanel;
        public Sprite[] speedups;
        public GameObject[] speedupObjs;
        public GameObject[] upshots;
        public float mainTime;
        public float curTime;

        private int _fGame;
        private int _gameLev;
        private Controller _heroControl;
        private int _ht2Int = 2;
        private int _htInt;
        private float _partTime;
        private int _recCoin;
        private float _speedupTime;
        private float _tMetr;
        private int _tutBool;

        private void Start()
        {
            _heroControl = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>().hero
                .GetComponent<Controller>();

            PlayerPrefs.SetInt("numLvlPlayer", PlayerPrefs.GetInt("numLvlPlayer") + 1);

            _partTime = mainTime * 0.5f;
            curTime = 1;
            _fGame = PlayerPrefs.GetInt("firstGame");
            if (_fGame < 4)
                PlayerPrefs.SetInt("firstGame", PlayerPrefs.GetInt("firstGame") + 1);
        }


        private void Update()
        {
            if (Input.GetMouseButtonDown(0) && _tutBool == 0 &&
                _heroControl)
            {
                _heroControl.isStart = true;
                Destroy(tutorial, 1.5f);
                _tutBool = 1;
            }

            _tMetr += _tMetr;
            _speedupTime -= Time.deltaTime;
            if (_speedupTime < 0)
            {
                var resp = GameObject.FindGameObjectsWithTag("ItemRespawn")[Random.Range(0, 34)];
                var oj = Instantiate(speedupObjs[Random.Range(0, speedupObjs.Length)], resp.transform.position,
                    transform.rotation);
                oj.transform.position += new Vector3(Random.Range(-2.21f, 2.21f), 0, Random.Range(-2.21f, 2.21f));
                _speedupTime = Random.Range(3, 6);
            }

            if (transform.position.y == 5)
            {
                transform.position = new Vector3(0, 0, 0);
                Pay2X();
            }

            mainTime -= Time.deltaTime;
            CountdownChanger();

            if (_gameLev == 0 && mainTime < _partTime + 5)
            {
                if (_htInt == 0)
                {
                    partBlock.SetActive(true);
                    _htInt = 1;
                }

                var min = (int) Mathf.Floor(mainTime / 60);
                if (_htInt != 2)
                    halfTimeTMP.text =
                        (int) Mathf.Floor(mainTime) - min * 60 + " sec";
            }

            if (mainTime < _partTime && _gameLev == 0)
            {
                Time.timeScale = 0;
                if (_htInt == 1)
                {
                    continuePanel.SetActive(true);
                    partBlock.SetActive(false);
                    jsCanvas.SetActive(false);

                    _htInt = 2;
                }

                if (Input.GetMouseButtonDown(0) && _ht2Int == 2)
                {
                    Time.timeScale = 1;
                    _gameLev = 1;
                    curTime = 5.113f;

                    _ht2Int = 1;
                    jsCanvas.SetActive(true);
                    continuePanel.SetActive(false);
                }
            }

            if (mainTime < 0 && _gameLev > 0)
            {
                curTime -= Time.unscaledDeltaTime;
                if (curTime < 0 || _gameLev == 2)
                {
                    if (_gameLev == 2) Time.timeScale = 0;
                    EndGame(true);
                }
            }
        }

        private void CountdownChanger()
        {
            if (mainTime >= 0)
            {
                var min = (int) Mathf.Floor(mainTime / 60);
                var sec = (int) Mathf.Floor(mainTime) - min * 60;
                var zero = "";
                if (sec < 10) zero = "0";
                countdown.text = $"{min}:{zero}{sec}";
            }
            else
            {
                countdown.text = "0:00";
            }
        }

        public void ToMenu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(sceneToLoad);
        }

        public void EndGame(bool isVictory)
        {
            _recCoin = (int) (_heroControl.resultAll * 0.25f);

            if (isVictory)
            {
                finishPanel.SetActive(true);
                yourCoinsFinish.text = $"{_recCoin.ToString()}";
            }
            else
            {
                endPanel.SetActive(true);
                yourCoins.text = $"{_recCoin.ToString()}";
            }

            jsCanvas.SetActive(false);
            Time.timeScale = 0;


            PlayerPrefs.SetInt("gold", PlayerPrefs.GetInt("gold") + _recCoin);

            if (_heroControl.strokeAmount > PlayerPrefs.GetInt("maxPet"))
                PlayerPrefs.SetInt("maxPet", _heroControl.strokeAmount);

            countdown.transform.Translate(Vector3.up * 999);
            _gameLev = 3;
            curTime = 9999f;
        }

        private void Pay2X()
        {
            Time.timeScale = 1;
            yourCoins.text = $"{(_recCoin * 2).ToString()}";

            PlayerPrefs.SetInt("gold", PlayerPrefs.GetInt("gold") + _recCoin);

            SceneManager.LoadScene(sceneToLoad);
        }
    }
}