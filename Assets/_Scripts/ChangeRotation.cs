﻿using UnityEngine;

namespace _Scripts
{
    public class ChangeRotation : MonoBehaviour
    {
        public Vector3 rotVector3;

        private void Update()
        {
            transform.Rotate(rotVector3 * Time.deltaTime);
        }
    }
}