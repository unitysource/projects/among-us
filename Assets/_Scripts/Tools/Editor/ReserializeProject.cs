using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts.Tools.Editor
{
    public static class ReserializeProject
    {
        [MenuItem("Tools/Reserialize All", false, 10000000)]
        public static void ReserializeAllScenes()
        {
            Debug.Log("Project is reserialized!");

            var sceneNumber = SceneManager.sceneCountInBuildSettings;
            var scenes = new string[sceneNumber];
            for (int i = 0; i < sceneNumber; i++)
                scenes[i] = Path.GetFullPath(SceneUtility.GetScenePathByBuildIndex(i));

            foreach (var sceneStr in scenes)
            {
                EditorSceneManager.OpenScene(sceneStr);

                var transforms = Resources.FindObjectsOfTypeAll<Transform>();
                foreach (var transform in transforms)
                {
                    var components = transform.GetComponents<MonoBehaviour>();
                    foreach (var t in components)
                        if (t != null)
                            EditorUtility.SetDirty(t);

                    EditorUtility.SetDirty(transform.gameObject);
                }

                var scene = SceneManager.GetActiveScene();
                EditorSceneManager.MarkSceneDirty(scene);
                AssetDatabase.SaveAssets();
                EditorSceneManager.SaveScene(scene, "", false);
            }
        }

        public static void ReserializeActiveScene()
        {
            var transforms = Resources.FindObjectsOfTypeAll<Transform>();
            foreach (var transform in transforms)
            {
                var components = transform.GetComponents<MonoBehaviour>();
                foreach (var t in components)
                    if (t != null)
                        EditorUtility.SetDirty(t);

                EditorUtility.SetDirty(transform.gameObject);
            }

            var scene = SceneManager.GetActiveScene();
            EditorSceneManager.MarkSceneDirty(scene);
            AssetDatabase.SaveAssets();
            EditorSceneManager.SaveScene(scene, "", false);
        }
    }
}