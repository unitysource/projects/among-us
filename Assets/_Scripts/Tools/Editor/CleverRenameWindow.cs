using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts.Tools.Editor
{
    public class CleverRenameWindow : EditorWindow
    {
        private bool _changeInAllScenes = true;
        private bool _changeAllRandom;
        
        private string _newName = "New";
        private string _oldName = "Old";

        private bool _stringGroupEnabled = true;

        private void OnGUI()
        {
            GUILayout.Label("GENERAL RENAME SETTINGS", EditorStyles.boldLabel);
            GUILayout.Space(10f);

            _changeInAllScenes = EditorGUILayout.Toggle("Change In All Scenes", _changeInAllScenes);
            _changeAllRandom = EditorGUILayout.Toggle("Change All objects randomly", _changeAllRandom);

            _stringGroupEnabled = EditorGUILayout.BeginToggleGroup("Change By Name", _stringGroupEnabled);
            _oldName = EditorGUILayout.TextField("Old Name", _oldName);
            EditorGUILayout.EndToggleGroup();

            _newName = EditorGUILayout.TextField("New Name", _newName);

            if (GUILayout.Button("Rename"))
            {
                if (_stringGroupEnabled)
                {
                    if (_changeInAllScenes)
                    {
                        var activeSceneName = SceneManager.GetActiveScene().path;
                        var sceneNumber = SceneManager.sceneCountInBuildSettings;
                        var scenes = new string[sceneNumber];

                        for (var i = 0; i < sceneNumber; i++)
                            scenes[i] = Path.GetFullPath(SceneUtility.GetScenePathByBuildIndex(i));

                        foreach (var sceneStr in scenes)
                        {
                            EditorSceneManager.OpenScene(sceneStr);
                            ChangeNamesInInspector();
                            var scene = SceneManager.GetActiveScene();
                            EditorSceneManager.MarkSceneDirty(scene);
                            AssetDatabase.SaveAssets();
                            EditorSceneManager.SaveScene(scene, "", false);
                        }

                        EditorSceneManager.OpenScene(activeSceneName);
                    }
                    else
                    {
                        ChangeNamesInInspector();
                    }
                }
            }
            else if (GUILayout.Button("Reverse Names"))
            {
                var oldName = _oldName;
                _oldName = EditorGUILayout.TextField("Old Name", _newName);
                _newName = EditorGUILayout.TextField("New Name", oldName);
            }
        }
        // private bool _changeInCode = true;

        [MenuItem("Tools/Clever Rename", false, 1)]
        public static void OpenCleverRename()
        {
            GetWindow<CleverRenameWindow>("Clever Rename");
        }

        private void ChangeNamesInInspector()
        {
            var objects = _changeAllRandom
                ? Resources.FindObjectsOfTypeAll<GameObject>().ToArray()
                : Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == _oldName).ToArray();

            if (objects.Length > 0)
            {
                Debug.Log($"Confirm. All GameObjects with name: [{_oldName}] changing to name: [{_newName}]");

                foreach (var gameObject in objects)
                    if (_changeAllRandom)
                    {
                        int randLength = Random.Range(4, 11);
                        gameObject.name = GenerateName(randLength);
                        Debug.Log(gameObject.name);
                    }
                    else
                        gameObject.name = _newName;
            }
            else
            {
                Debug.LogError($"GameObject with name: [{_oldName}] in this Scene isn't find!");
            }
        }

        private static string GenerateName(int len)
        {
            var r = new System.Random();
            var consonants = new[] {
                "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", 
                "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x"
            };
            var vowels = new[] {"a", "e", "i", "o", "u", "ae", "y"};
            var newName = "";
            newName = $"{newName}{consonants[r.Next(consonants.Length)].ToUpper()}";
            newName = $"{newName}{vowels[r.Next(vowels.Length)]}";
            var b = 2; //b tells how many times a new letter has been added.
                       //It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                newName = $"{newName}{consonants[r.Next(consonants.Length)]}";
                b++;
                newName = $"{newName}{vowels[r.Next(vowels.Length)]}";
                b++;
            }

            return newName;
        }
    }
}