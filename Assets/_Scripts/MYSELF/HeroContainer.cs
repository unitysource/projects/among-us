using UnityEngine;
using UnityEngine.Serialization;

namespace _Scripts.MYSELF
{
    public class HeroContainer : MonoBehaviour
    {
        public GameObject hero;
        
        public GameObject killEffect;
        public GameObject newPetEffect;
        [FormerlySerializedAs("mainSystem")] public CoreSystem coreSystem;
        [FormerlySerializedAs("skinCollector")] public AllSkins allSkins;
        public GameObject[] gradkas;
        public Transform boxesWithPetsContainer;
    }
}