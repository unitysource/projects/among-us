using System;
using _Scripts.Menu;
using _Scripts.MYSELF.Ui;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Scripts.MYSELF
{
    public class MainMenuInMainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject startPanel;
        [SerializeField] private GameObject[] allPanels;
        [SerializeField] private TextMeshProUGUI coinsTMP;
        [SerializeField] private GameObject dayButton;
        [SerializeField] private GameObject skinEffect;
        [SerializeField] private GameObject dailyPanel;
        [SerializeField] private Transform pointToInstantiate;

        public TextMeshProUGUI crowdTMP;
        public GameObject hero;
        public AllSkins skins;

        private GameObject _goldEffect;

        private void OnEnable() =>
            AchievementElement.OnSetGold += SetGold;

        private void OnDisable() =>
            AchievementElement.OnSetGold -= SetGold;

        private void Awake()
        {
            int curDay = DateTime.Now.Day;
            int oldDay = PlayerPrefs.GetInt("newDay");

            //PlayerPrefs.SetInt("firstStart", 0);
            if (curDay != oldDay && PlayerPrefs.GetInt("weeklyStart") == 1)
            {
                dailyPanel.SetActive(true);
            }

            if (PlayerPrefs.GetInt("firstStart") == 0)
            {
                StartSetting();
            }

            // informerUpdate();

            GetComponent<UpgradeUi>().SetStartValues();

            crowdTMP.text = $"{PlayerPrefs.GetInt("maxPet").ToString()}";
            SetGold(0);
            StartSkin();
        }

        private void Start()
        {
            foreach (var panel in allPanels)
                panel.SetActive(false);
            startPanel.SetActive(true);

            _goldEffect = GetComponent<PlayUi>().goldEffect;

            // CheckUnlocked();
        }

        public void SetGold(int gd)
        {
            PlayerPrefs.SetInt("gold", PlayerPrefs.GetInt("gold") + gd);
            coinsTMP.text = $"{PlayerPrefs.GetInt("gold").ToString()}";
        }

        private void StartSetting()
        {
            PlayerPrefs.SetInt("giftDay", 999);

            dayButton.SetActive(true);
            PlayerPrefs.SetString("Nickname", "Player" + Random.Range(1321, 7543));

            PlayerPrefs.SetInt("newDay", 999);
            PlayerPrefs.SetInt("weekDay", 1);

            PlayerPrefs.SetInt("firstStart", 1);
            PlayerPrefs.SetInt("gold", 3950);
            PlayerPrefs.SetInt("skill1", 1);
            PlayerPrefs.SetInt("skill2", 1);
            PlayerPrefs.SetInt("skill3", 1);
            PlayerPrefs.SetInt("skill4", 1);

            PlayerPrefs.SetInt("skin0", 1);

            PlayerPrefs.SetInt("instBtn", 0);
        }

        public void StartSkin()
        {
            foreach (Transform child in hero.transform)
                Destroy(child.gameObject);

            var gs = skins.allSkins;
            var ns = Instantiate(gs[PlayerPrefs.GetInt("curSkin")], hero.transform.position, hero.transform.rotation);
            ns.transform.parent = hero.transform;
            ns.transform.localScale *= 1.3154f;
        }

        public void OnDaily(int btnNumber)
        {
            PlayerPrefs.SetInt("weekDay", PlayerPrefs.GetInt("weekDay") + 1);
            PlayerPrefs.SetInt("newDay", DateTime.Now.Day);
            GetComponent<SkinUi>().OnSelect();

            switch (btnNumber)
            {
                case 0:
                    DailySkin(Random.Range(5, 9));
                    break;
                case 1:
                    DailyCoin(2000);
                    break;
                case 2:
                    DailySkin(Random.Range(1, skins.allSkins.Length));
                    break;
                case 3:
                    DailyCoin(5000);
                    break;
                case 4:
                    DailyCoin(7000);
                    break;
                case 5:
                    DailySkin(Random.Range(1, skins.allSkins.Length));
                    break;
                case 6:
                    DailyCoin(70000);
                    break;
            }

            dailyPanel.SetActive(false);
        }

        private void DailySkin(int rndSkn)
        {
            GetComponent<SkinUi>().showSkin = PlayerPrefs.GetInt("showSkin");
            PlayerPrefs.SetInt("showSkin", rndSkn);
            PlayerPrefs.SetInt($"skin{rndSkn.ToString()}", 1);
            Instantiate(skinEffect, pointToInstantiate.position,
                skinEffect.transform.rotation);
        }

        private void DailyCoin(int count)
        {
            SetGold(count);
            Instantiate(_goldEffect, pointToInstantiate.position, _goldEffect.transform.rotation);
        }

        // void CheckUnlocked()
        // {
        //     bool allBuyed = true;
        //     int next = 0;
        //     int maxSkins = skins.skins.Length;
        //
        //     while (next < maxSkins)
        //     {
        //         if (PlayerPrefs.GetInt("skin" + next) != 1) allBuyed = false;
        //         next++;
        //     }
        //
        //     if (allBuyed)
        //     {
        //         if (GameObject.Find("b_buyskin")) GameObject.Find("b_buyskin").transform.position = Vector3.up * 9999;
        //         if (GameObject.Find("b_disableADS"))
        //         {
        //             var vect = GameObject.Find("b_disableADS").transform.localPosition;
        //             GameObject.Find("b_disableADS").transform.localPosition = new Vector3(0, vect.y, vect.z);
        //         }
        //     }
        // }
    }
}