using _Scripts.Audio.Audio;
using UnityEngine;
using UnityEngine.EventSystems;
using AudioType = _Scripts.Audio.Audio.AudioType;

namespace _Scripts.MYSELF.Ui
{
    public class ButtonSfx : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            AudioController.Instance.PlayAudio(AudioType.ClickUi, false, .1f);
        }
    }
}