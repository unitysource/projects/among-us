using UnityEngine;

namespace _Scripts.MYSELF.Ui
{
    public class UpgradeUi : MonoBehaviour
    {
        [SerializeField] private SetupUpgrade[] setupUpgrades;
        private int _cost;

        public void ChangePrice(int updateNumber)
        {
            _cost = int.Parse(setupUpgrades[updateNumber].coins.text);

            if (_cost <= PlayerPrefs.GetInt("gold"))
            {
                GetComponent<MainMenuInMainMenu>().SetGold(-_cost);
                SkillSet(updateNumber);

                int pr;
                pr = 1200;
                for (int i = 0; i < PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}") - 1; i++)
                    if (PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}") > 1)
                        pr *= 2;
                setupUpgrades[updateNumber].coins.text = pr.ToString();
            }
        }

        public void SkillSet(int updateNumber)
        {
            PlayerPrefs.SetInt($"skill{(updateNumber + 1).ToString()}",
                PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}") + 1);
            switch (updateNumber)
            {
                case 0:
                case 1:
                    setupUpgrades[updateNumber].count.text =
                        $"{PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}").ToString()}";
                    break;
                case 2:
                    setupUpgrades[updateNumber].count.text =
                        $"{(PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}")*3).ToString()}";
                    break;
                case 3:
                    setupUpgrades[updateNumber].count.text =
                        $"{(PlayerPrefs.GetInt($"skill{(updateNumber + 1).ToString()}")*5).ToString()}";
                    break;
            }
        }

        public void SetStartValues()
        {
            int j = 0;
            for (int i = 0; i < 4; i++)
            {
                j++;
                switch (i)
                {
                    case 0:
                    case 1:
                        setupUpgrades[i].count.text =
                            $"{PlayerPrefs.GetInt($"skill{j.ToString()}").ToString()}";
                        break;
                    case 2:
                        setupUpgrades[i].count.text =
                            $"{(PlayerPrefs.GetInt($"skill{j.ToString()}")*3).ToString()}";
                        break;
                    case 3:
                        setupUpgrades[i].count.text =
                            $"{(PlayerPrefs.GetInt($"skill{j.ToString()}")*5).ToString()}";
                        break;
                }
            }
        }
    }
}