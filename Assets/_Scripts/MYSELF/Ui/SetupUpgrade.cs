using System;
using TMPro;
using UnityEngine;

namespace _Scripts.MYSELF.Ui
{
    public class SetupUpgrade : MonoBehaviour
    {
        public TextMeshProUGUI count;
        public TextMeshProUGUI coins;
        [SerializeField] private string prefsName;

        private void Start()
        {
            var pr = 1200;

            for (int i = 0; i < PlayerPrefs.GetInt(prefsName) - 1; i++)
                if (PlayerPrefs.GetInt(prefsName) > 1)
                {
                    // Debug.Log(gameObject.name);
                    pr *= 2;
                }


            coins.text = $"{pr}";
        }
    }
}