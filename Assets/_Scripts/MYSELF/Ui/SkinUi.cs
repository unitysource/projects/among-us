using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace _Scripts.MYSELF.Ui
{
    public class SkinUi : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI skinNumber;
        [SerializeField] private TextMeshProUGUI skinCost;
        [SerializeField] private GameObject chooseBlock;
        [SerializeField] private GameObject buyBlock;
        [SerializeField] private Button buyButton;

        [HideInInspector] public int showSkin;
        private MainMenuInMainMenu _main;

        private void Start()
        {
            _main = GetComponent<MainMenuInMainMenu>();
            showSkin = PlayerPrefs.GetInt("showSkin");
            skinNumber.text = (showSkin+1).ToString();
        }

        public void OnArrowSkin(int sign)
        {
            SkinShow(sign);
        }

        public void OnSelect()
        {
            PlayerPrefs.SetInt("curSkin", showSkin);
            PlayerPrefs.SetInt("showSkin", PlayerPrefs.GetInt("curSkin"));

            _main.StartSkin();
        }

        public void OnBuy()
        {
            int cos = int.Parse(_main.crowdTMP.text);
            if (cos <= PlayerPrefs.GetInt("gold"))
            {
                PlayerPrefs.SetInt("skinBuyed", PlayerPrefs.GetInt("skinBuyed") + 1);

                _main.SetGold(-cos);
                PlayerPrefs.SetInt($"skin{showSkin.ToString()}", 1);
                PlayerPrefs.SetInt("curSkin", showSkin);
                PlayerPrefs.SetInt("showSkin", PlayerPrefs.GetInt("curSkin"));
                SkinShow(0);
                _main.StartSkin();
            }
        }

        private void SkinShow(int skn)
        {
            showSkin += skn;

            foreach (Transform child in _main.hero.transform)
                Destroy(child.gameObject);

            var gs = _main.skins.allSkins;

            if (showSkin >= gs.Length) showSkin = 0;
            if (showSkin < 0) showSkin = gs.Length - 1;

            var ns = Instantiate(gs[showSkin], _main.hero.transform.position,
                _main.hero.transform.rotation);
            ns.transform.parent = _main.hero.transform;
            // ns.transform.localScale *= 0.66f;

            skinNumber.text = (showSkin+1).ToString();
            ns.transform.localScale *= 1.3154f;

            // GameObject.Find("bgSkin").transform.localPosition = new Vector3(0, 0, 0);

            // Debug.Log($"showSkin: {_showSkin.ToString()}");
            // Debug.Log(PlayerPrefs.GetInt($"skin{_showSkin.ToString()}").ToString());
            
            if (PlayerPrefs.GetInt($"skin{showSkin.ToString()}") == 1)
            {
                // GameObject.Find("BUY").transform.localPosition = new Vector3(0, 999, 0);
                buyBlock.SetActive(false);
                chooseBlock.SetActive(true);
            }
            else
            {
                // GameObject.Find("buyIt").transform.localPosition = new Vector3(0, 0, 0);
                // GameObject.Find("BUY").transform.localPosition = new Vector3(0, 0, 0);

                buyBlock.SetActive(true);
                chooseBlock.SetActive(false);

                int cost = 0;

                if (showSkin > 0) cost = 30000;
                if (showSkin >= 5) cost = 50000;
                if (showSkin >= 10) cost = 100000;

                skinCost.text = $"{cost}";

                buyButton.interactable = cost <= PlayerPrefs.GetInt("gold");
            }
        }
    }
}