using System;
using _Scripts.Menu;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace _Scripts.MYSELF.Ui
{
    public class PlayUi : MonoBehaviour
    {
        [SerializeField] [Scene] private string sceneToLoad;
        [SerializeField] private Image musicImg;
        [SerializeField] private Sprite mus;
        [SerializeField] private Sprite noMus;
        [SerializeField] private Button bonusBtn;
        public GameObject goldEffect;

        [FormerlySerializedAs("achievementButtons")] [SerializeField]
        private GameObject[] achievementElements;

        public void ChangeMusic()
        {
            var volume = 1 - AudioListener.volume;
            AudioListener.volume = volume;

            musicImg.sprite = Math.Abs(volume - 1) < .001f ? mus : noMus;
        }

        public void ChangeCharacterName(string heroName) =>
            PlayerPrefs.SetString("Nickname", heroName);

        public void StartGame()
        {
            PlayerPrefs.SetInt("crowdBonus", 0);
            PlayerPrefs.SetInt("weeklyStart", 1);
            SceneManager.LoadScene(sceneToLoad);
        }

        public void Bonus()
        {
            PlayerPrefs.SetInt("giftDay", DateTime.Now.Day);
            GetComponent<MainMenuInMainMenu>().SetGold(7000);
            var eff = Instantiate(goldEffect, bonusBtn.transform.parent);
            eff.transform.position = bonusBtn.transform.position;
        }

        public void OnAchievement(int i)
        {
            achievementElements[i].GetComponent<AchievementElement>().SetAchievement();
            var eff = Instantiate(goldEffect, achievementElements[i].transform);
            eff.transform.localPosition = achievementElements[i].transform.localPosition;
        }
    }
}