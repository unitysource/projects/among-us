using UnityEngine;

namespace _Scripts.MYSELF
{
    public class ChangePos : MonoBehaviour
    {
        [SerializeField] private RectTransform[] _transforms;


        private void Start()
        {
            _transforms[1].anchoredPosition =
                new Vector2(_transforms[1].anchoredPosition.x, _transforms[2].anchoredPosition.y);
        }
    }
}
