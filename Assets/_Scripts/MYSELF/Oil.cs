using UnityEngine;

namespace _Scripts.MYSELF
{
    public class Oil : MonoBehaviour
    {
        private float _timer;

        private void FixedUpdate()
        {
            _timer += Time.unscaledDeltaTime;
            if (_timer >= .2f)
            {
                gameObject.SetActive(false);
                Destroy(this);
            }
        }
    }
}