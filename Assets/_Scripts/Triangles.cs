﻿using _Scripts.MYSELF;
using UnityEngine;

namespace _Scripts
{
    public class Triangles : MonoBehaviour
    {
        public GameObject npc;
        public GameObject label;
        public GameObject mySpr;
        private GameObject _character;

        private float _countdown;

        private void Start()
        {
            _countdown = Random.Range(-0.99f, 1.12f);
            _character = GameObject.FindWithTag("MinionController").GetComponent<HeroContainer>().hero;
            label.transform.position = _character.transform.position;
            label.transform.parent = _character.transform;

            transform.parent.position = Camera.main.transform.position;
            transform.parent.transform.parent = Camera.main.transform;
            transform.parent.transform.localEulerAngles = new Vector3(0, 0, 0);

            var c1 = Random.Range(0.01f, 0.79f);
            var c2 = Random.Range(0.01f, 0.79f);
            var c3 = Random.Range(0.01f, 0.79f);
            mySpr.GetComponent<SpriteRenderer>().color = new Color(c1, c2, c3, 0.82f);

            if (npc.TryGetComponent<Controller>(out var control))
                control.SetShadeChildren(new Color(c1, c2, c3, 0.7f));
        }

        private void Update()
        {
            if (!label) return;

            if (npc) label.transform.LookAt(npc.transform);
            else Destroy(transform.parent.gameObject);
            transform.localEulerAngles = new Vector3(0, 0, -label.transform.eulerAngles.y);

            _countdown += Time.deltaTime;
            if (_countdown > 2.2f)
                if (npc && _character)
                    transform.parent.transform.localPosition =
                        Vector3.Distance(_character.transform.position, npc.transform.position) < 6.5f
                            ? new Vector3(0, 999, 0)
                            : new Vector3(0, 0, 0);
        }
    }
}