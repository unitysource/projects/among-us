﻿using UnityEngine;

namespace _Scripts
{
    public class PieceContainer : MonoBehaviour
    {
        public GameObject[] effects;
        public GameObject par;

        private Vector3 _pos;

        public void SetPos(Vector3 p)
        {
            _pos = p;
        }

        public void SetParent(GameObject p)
        {
            par = p;
        }

        public void ClonePart(int v)
        {
            var gg = Instantiate(effects[v], _pos, effects[v].transform.rotation);

            if (par != null)
                gg.transform.parent = par.transform;

            par = null;
        }
    }
}