﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Menu
{
    public class AchievementElement : MonoBehaviour
    {
        [SerializeField] private Button plusButton;
        [SerializeField] private GameObject myLock;
        [SerializeField] private int num;

        public static event Action<int> OnSetGold;

        private void Start()
        {
            var petCount = num switch
            {
                1 => 10,
                2 => 25,
                3 => 50,
                4 => 75,
                5 => 100,
                6 => 150,
                7 => 200,
                8 => 250,
                9 => 300,
                _ => 1
            };

            if (PlayerPrefs.GetInt("maxPet") >= petCount)
            {
                Destroy(myLock);

                if (PlayerPrefs.GetInt($"achi{num.ToString()}") == 1)
                    CheckAchievement();
            }
        }

        public void SetAchievement()
        {
            PlayerPrefs.SetInt($"achi{num.ToString()}", 1);

            switch (num)
            {
                case 1:
                    OnSetGold?.Invoke(300);
                    break;
                case 2:
                    OnSetGold?.Invoke(500);
                    break;
                case 3:
                    OnSetGold?.Invoke(1000);
                    break;
                case 4:
                    OnSetGold?.Invoke(1500);
                    break;
                case 5:
                    OnSetGold?.Invoke(2500);
                    break;
                case 6:
                    OnSetGold?.Invoke(4000);
                    break;
                case 7:
                    OnSetGold?.Invoke(5000);
                    break;
                case 8:
                    OnSetGold?.Invoke(7000);
                    break;
                case 9:
                    OnSetGold?.Invoke(10000);
                    break;
            }

            CheckAchievement();
        }

        private void CheckAchievement()
        {
            plusButton.interactable = false;
            GetComponent<Image>().color = Color.green;
        }
    }
}