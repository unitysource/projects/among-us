﻿using UnityEngine;

namespace _Scripts.Menu
{
    public class ChangeCost : MonoBehaviour
    {
        public string nameOfPref;

        private void Start()
        {
            int pr;
            pr = 1200;

            for (var i = 0; i < PlayerPrefs.GetInt(nameOfPref) - 1; i++)
                if (PlayerPrefs.GetInt(nameOfPref) > 1)
                    pr *= 2;

            GetComponent<TextMesh>().text = $"{pr}";
        }
    }
}