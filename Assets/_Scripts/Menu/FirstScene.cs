﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts.Menu
{
    public class FirstScene : MonoBehaviour
    {
        [Scene] [SerializeField] private string loadScene;

        private float _timer;

        private void Start()
        {
            _timer = PlayerPrefs.GetInt("firstStart") == 0 ? 2.5f : 1.5F;
        }

        private void Update()
        {
            _timer -= Time.deltaTime;

            if (_timer < 0)
            {
                SceneManager.LoadScene(loadScene);
                _timer = 9999f;
            }
        }
    }
}