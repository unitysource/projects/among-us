﻿using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Menu
{
    public class WeeklyPay : MonoBehaviour
    {
        [SerializeField] private int rewardNumber;
        
        private void Start()
        {
            if (rewardNumber == PlayerPrefs.GetInt("weekDay"))
            {
                GetComponent<Image>().color = new Color(0, 1, 0, 1f);
                GetComponent<Animator>().enabled = true;
            }
            else
            {
                GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);
                if (rewardNumber < PlayerPrefs.GetInt("weekDay"))
                    GetComponent<Image>().color = new Color(0, 0, 0, 0.3f);
                GetComponent<Animator>().enabled = false;
            }
        }
    }
}