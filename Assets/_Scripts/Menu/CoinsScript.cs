﻿using UnityEngine;

namespace _Scripts.Menu
{
    public class CoinsScript : MonoBehaviour
    {
        private GameObject _coinsObg;

        private float _countdown;

        private void Awake()
        {
            _coinsObg = GameObject.FindWithTag("CoinsCollector");
            transform.position = new Vector3(Random.Range(-40f, 40f), Random.Range(-20f, 20f), 0);
            _countdown = Random.Range(0, 0.5f);
        }

        private void Update()
        {
            _countdown -= Time.deltaTime;

            if (_countdown < 0)
                transform.position =
                    Vector3.Lerp(transform.position, _coinsObg.transform.position, Time.deltaTime * 4);

            if (_countdown < -1.2f) Destroy(gameObject);
        }
    }
}