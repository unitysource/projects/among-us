﻿using System;
using UnityEngine;

namespace _Scripts.Menu
{
    public class RemovePay : MonoBehaviour
    {
        private void Start()
        {
            var curDay = DateTime.Now.Day;
            var oldDay = PlayerPrefs.GetInt("giftDay");

            if (curDay == oldDay) gameObject.SetActive(false);
        }
    }
}