# Among Us Impostor (Hyper-Casual Game)

![Game Logo](/images/logo.png)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)

## About the Game

Select character and play fake multiplayer game IO in format

## Gameplay

Joystick to move, tap to jump. Avoid obstacles and reach the finish line. Collect coins to unlock new characters. Collect children from other players

![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:
- Fake multiplayer game IO in format
- 3D graphics

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/among-us.git`
2. Navigate to the game directory: `cd among-us`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Joystick to move

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
