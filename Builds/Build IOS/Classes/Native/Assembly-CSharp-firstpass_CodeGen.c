﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ThidPersonExampleController::OnEnable()
extern void ThidPersonExampleController_OnEnable_mB2992A2594EDC4250F065E13583A0C8521340334 (void);
// 0x00000002 System.Void ThidPersonExampleController::Update()
extern void ThidPersonExampleController_Update_mEB0EB920E5FD84D02E3289893C024E6B23A68805 (void);
// 0x00000003 System.Void ThidPersonExampleController::.ctor()
extern void ThidPersonExampleController__ctor_m6B7054DE640F21560C86BF017E8368F9EA232101 (void);
// 0x00000004 System.Void CommonOnScreenControl::.ctor()
extern void CommonOnScreenControl__ctor_m0EDAD36310415FB96E7C848A3824BE488964EF43 (void);
// 0x00000005 CnControls.CnInputManager CnControls.CnInputManager::get_Instance()
extern void CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E (void);
// 0x00000006 System.Void CnControls.CnInputManager::.ctor()
extern void CnInputManager__ctor_m4FF766F7ACC318C6B77AF8CF8EB7A8E60F87F6F8 (void);
// 0x00000007 System.Int32 CnControls.CnInputManager::get_TouchCount()
extern void CnInputManager_get_TouchCount_mA59758D8D293E1F02ECF559A596A7040CC0E1782 (void);
// 0x00000008 UnityEngine.Touch CnControls.CnInputManager::GetTouch(System.Int32)
extern void CnInputManager_GetTouch_m8B17A871490DAC8716F88916E9B513BF18D67842 (void);
// 0x00000009 System.Single CnControls.CnInputManager::GetAxis(System.String)
extern void CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B (void);
// 0x0000000A System.Single CnControls.CnInputManager::GetAxisRaw(System.String)
extern void CnInputManager_GetAxisRaw_mC341ECB544726B2F36DA83F51F718A2E93380C2E (void);
// 0x0000000B System.Single CnControls.CnInputManager::GetAxis(System.String,System.Boolean)
extern void CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C (void);
// 0x0000000C System.Boolean CnControls.CnInputManager::GetButton(System.String)
extern void CnInputManager_GetButton_mC823D5A0CC24751793A0CAD17C97AA35A05490F4 (void);
// 0x0000000D System.Boolean CnControls.CnInputManager::GetButtonDown(System.String)
extern void CnInputManager_GetButtonDown_m465EE8E01A82D8BA2D0AED4017B26A7991BF9DF7 (void);
// 0x0000000E System.Boolean CnControls.CnInputManager::GetButtonUp(System.String)
extern void CnInputManager_GetButtonUp_m8809729C18402C8F7B142F6C255FDCE42659543E (void);
// 0x0000000F System.Boolean CnControls.CnInputManager::AxisExists(System.String)
extern void CnInputManager_AxisExists_m0ED578BD6C0803B9FB25E5551BA91776583776C3 (void);
// 0x00000010 System.Boolean CnControls.CnInputManager::ButtonExists(System.String)
extern void CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64 (void);
// 0x00000011 System.Void CnControls.CnInputManager::RegisterVirtualAxis(CnControls.VirtualAxis)
extern void CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2 (void);
// 0x00000012 System.Void CnControls.CnInputManager::UnregisterVirtualAxis(CnControls.VirtualAxis)
extern void CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46 (void);
// 0x00000013 System.Void CnControls.CnInputManager::RegisterVirtualButton(CnControls.VirtualButton)
extern void CnInputManager_RegisterVirtualButton_mE337F2AE2069EFC98A433950662B2AC92CB107F6 (void);
// 0x00000014 System.Void CnControls.CnInputManager::UnregisterVirtualButton(CnControls.VirtualButton)
extern void CnInputManager_UnregisterVirtualButton_mFE6F77977405E14E3E67716871D87C62A2D2FB7B (void);
// 0x00000015 System.Single CnControls.CnInputManager::GetVirtualAxisValue(System.Collections.Generic.List`1<CnControls.VirtualAxis>,System.String,System.Boolean)
extern void CnInputManager_GetVirtualAxisValue_m3A649765E16DD2CBFF4C013525956B1F5EFACC9C (void);
// 0x00000016 System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonDown(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern void CnInputManager_GetAnyVirtualButtonDown_m9A0048CA59A5964B864C20E17DFC2535A22C4138 (void);
// 0x00000017 System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonUp(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern void CnInputManager_GetAnyVirtualButtonUp_m2A85153733F81918326833CDF305111DEB5D9990 (void);
// 0x00000018 System.Boolean CnControls.CnInputManager::GetAnyVirtualButton(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern void CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F (void);
// 0x00000019 System.String CnControls.VirtualAxis::get_Name()
extern void VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777 (void);
// 0x0000001A System.Void CnControls.VirtualAxis::set_Name(System.String)
extern void VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB (void);
// 0x0000001B System.Single CnControls.VirtualAxis::get_Value()
extern void VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130 (void);
// 0x0000001C System.Void CnControls.VirtualAxis::set_Value(System.Single)
extern void VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD (void);
// 0x0000001D System.Void CnControls.VirtualAxis::.ctor(System.String)
extern void VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED (void);
// 0x0000001E System.String CnControls.VirtualButton::get_Name()
extern void VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA (void);
// 0x0000001F System.Void CnControls.VirtualButton::set_Name(System.String)
extern void VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6 (void);
// 0x00000020 System.Boolean CnControls.VirtualButton::get_IsPressed()
extern void VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD (void);
// 0x00000021 System.Void CnControls.VirtualButton::set_IsPressed(System.Boolean)
extern void VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB (void);
// 0x00000022 System.Void CnControls.VirtualButton::.ctor(System.String)
extern void VirtualButton__ctor_m33D71A415DA9EE486D63A4A6BF2F712F69EAC603 (void);
// 0x00000023 System.Void CnControls.VirtualButton::Press()
extern void VirtualButton_Press_m1944E262243474DF95B526DE21A08CAB0E39A2E9 (void);
// 0x00000024 System.Void CnControls.VirtualButton::Release()
extern void VirtualButton_Release_m5C7E5B88EC972BDCA47DDE42169C19B774CEBC68 (void);
// 0x00000025 System.Boolean CnControls.VirtualButton::get_GetButton()
extern void VirtualButton_get_GetButton_m5A784A059961193CE5B16ADB59B0D2C362DFBAF3 (void);
// 0x00000026 System.Boolean CnControls.VirtualButton::get_GetButtonDown()
extern void VirtualButton_get_GetButtonDown_m1DAE11354C811678B39111E146200B23C6803E12 (void);
// 0x00000027 System.Boolean CnControls.VirtualButton::get_GetButtonUp()
extern void VirtualButton_get_GetButtonUp_mA53CCC28120C136AD5691AE8C98973DF460E6C86 (void);
// 0x00000028 UnityEngine.Camera CnControls.Dpad::get_CurrentEventCamera()
extern void Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956 (void);
// 0x00000029 System.Void CnControls.Dpad::set_CurrentEventCamera(UnityEngine.Camera)
extern void Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379 (void);
// 0x0000002A System.Void CnControls.Dpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Dpad_OnPointerDown_mF82EB7B90302B22E359009913D5BA754FCE42C35 (void);
// 0x0000002B System.Void CnControls.Dpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Dpad_OnPointerUp_m29099F9D826939398203D027C9951BB7C241F535 (void);
// 0x0000002C System.Void CnControls.Dpad::.ctor()
extern void Dpad__ctor_m04228D51365F267F1B691E37B763E8C724AD663C (void);
// 0x0000002D UnityEngine.RectTransform CnControls.DpadAxis::get_RectTransform()
extern void DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2 (void);
// 0x0000002E System.Void CnControls.DpadAxis::set_RectTransform(UnityEngine.RectTransform)
extern void DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163 (void);
// 0x0000002F System.Int32 CnControls.DpadAxis::get_LastFingerId()
extern void DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4 (void);
// 0x00000030 System.Void CnControls.DpadAxis::set_LastFingerId(System.Int32)
extern void DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C (void);
// 0x00000031 System.Void CnControls.DpadAxis::Awake()
extern void DpadAxis_Awake_m08F451D554C54B2DE8EAD3859C209EE21616AB11 (void);
// 0x00000032 System.Void CnControls.DpadAxis::OnEnable()
extern void DpadAxis_OnEnable_m64D8939B835CD0E93488DD219E7EE90D684DDCC8 (void);
// 0x00000033 System.Void CnControls.DpadAxis::OnDisable()
extern void DpadAxis_OnDisable_mFF33785DDED5388FE8264131DE795079DB0FB546 (void);
// 0x00000034 System.Void CnControls.DpadAxis::Press(UnityEngine.Vector2,UnityEngine.Camera,System.Int32)
extern void DpadAxis_Press_mD9BCDA57FCBADC57DDB5BB4A4439647EFBCBAD90 (void);
// 0x00000035 System.Void CnControls.DpadAxis::TryRelease(System.Int32)
extern void DpadAxis_TryRelease_mD494D94FAA2D11866BF18348679651CB57FDE322 (void);
// 0x00000036 System.Void CnControls.DpadAxis::.ctor()
extern void DpadAxis__ctor_m7DE7D60F8FF168B2131EB768A69CC64C0F7118DE (void);
// 0x00000037 System.Void CnControls.SensitiveJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void SensitiveJoystick_OnDrag_mEF3AFC37A7F67D138F9E5A03C823B899CEAEB9EB (void);
// 0x00000038 System.Void CnControls.SensitiveJoystick::.ctor()
extern void SensitiveJoystick__ctor_m04B2E748AA080D3C64EB2BF5105E5C8E350289E5 (void);
// 0x00000039 System.Void CnControls.SimpleButton::OnEnable()
extern void SimpleButton_OnEnable_m57FF5CB5CBDE58B890755AF99140E4EE225813E7 (void);
// 0x0000003A System.Void CnControls.SimpleButton::OnDisable()
extern void SimpleButton_OnDisable_m92FF8AE5F15DEA4AE042456EC6BCEBA761902C65 (void);
// 0x0000003B System.Void CnControls.SimpleButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SimpleButton_OnPointerUp_mB01978646F7323AE9F95A2015BABC4D722B097A1 (void);
// 0x0000003C System.Void CnControls.SimpleButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void SimpleButton_OnPointerDown_mCAED6359F7B3E616334761061255876421E5879F (void);
// 0x0000003D System.Void CnControls.SimpleButton::.ctor()
extern void SimpleButton__ctor_mDF932CB6F180F0900F92D4B3241A4D41ED402178 (void);
// 0x0000003E UnityEngine.Camera CnControls.SimpleJoystick::get_CurrentEventCamera()
extern void SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1 (void);
// 0x0000003F System.Void CnControls.SimpleJoystick::set_CurrentEventCamera(UnityEngine.Camera)
extern void SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C (void);
// 0x00000040 System.Void CnControls.SimpleJoystick::Awake()
extern void SimpleJoystick_Awake_m4A668E3B6AA0C1779D5B5D1B2637B9559B08CC72 (void);
// 0x00000041 System.Void CnControls.SimpleJoystick::OnEnable()
extern void SimpleJoystick_OnEnable_mF783F82863C089C43C34585A262414AE20E9E250 (void);
// 0x00000042 System.Void CnControls.SimpleJoystick::OnDisable()
extern void SimpleJoystick_OnDisable_m585C6E6A10806341B0B2D865B869BABBB0115C4B (void);
// 0x00000043 System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void SimpleJoystick_OnDrag_m315B6DA9C170A7106078F76A18F95C2398A94816 (void);
// 0x00000044 System.Void CnControls.SimpleJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SimpleJoystick_OnPointerUp_m1BBC47BB0B9F02B873794D073BAD8BD0BF1BD7D1 (void);
// 0x00000045 System.Void CnControls.SimpleJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void SimpleJoystick_OnPointerDown_m228FDDFF072D62F4F531BE48AFE562E5D591EF13 (void);
// 0x00000046 System.Void CnControls.SimpleJoystick::Hide(System.Boolean)
extern void SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9 (void);
// 0x00000047 System.Void CnControls.SimpleJoystick::.ctor()
extern void SimpleJoystick__ctor_mDDF1DAF26C35A567748C85CB911DD8B2A429311D (void);
// 0x00000048 UnityEngine.Camera CnControls.Touchpad::get_CurrentEventCamera()
extern void Touchpad_get_CurrentEventCamera_m77CEFA791EFA8662F3F8C5B6E6FEC11E1A3139E4 (void);
// 0x00000049 System.Void CnControls.Touchpad::set_CurrentEventCamera(UnityEngine.Camera)
extern void Touchpad_set_CurrentEventCamera_m37E4B76190F2765D52ED9F71CF67EBE6CE5BAA92 (void);
// 0x0000004A System.Void CnControls.Touchpad::OnEnable()
extern void Touchpad_OnEnable_m434A35BC9F950BF4EDC68E6DB4F93F013D5BBD3B (void);
// 0x0000004B System.Void CnControls.Touchpad::OnDisable()
extern void Touchpad_OnDisable_mB50AA2BED48EA529DB36AF516D143FAD1E5F0F90 (void);
// 0x0000004C System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Touchpad_OnDrag_m668AFF004B7734F4C376FC7C31D928DC84AD72C7 (void);
// 0x0000004D System.Void CnControls.Touchpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Touchpad_OnPointerUp_mC05E90519E111D361ED3C8E09D92269A0CFDC989 (void);
// 0x0000004E System.Void CnControls.Touchpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Touchpad_OnPointerDown_m1BEBD7E307303F93B24D1B79A7F78354BF11717F (void);
// 0x0000004F System.Void CnControls.Touchpad::Update()
extern void Touchpad_Update_mF7762CE57B2BAB8EED9E379E152F372A0D65523C (void);
// 0x00000050 System.Void CnControls.Touchpad::.ctor()
extern void Touchpad__ctor_mCADCA3DB1B6248C43CDD46EE1A5DDB2C08C77537 (void);
// 0x00000051 System.Void CnControls.EmptyGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void EmptyGraphic_OnPopulateMesh_m641C20ACAB49C8E8D264ED1AF6A8448DA06E91BD (void);
// 0x00000052 System.Void CnControls.EmptyGraphic::.ctor()
extern void EmptyGraphic__ctor_mAE371F9CE4D9C24BB2D82C0585EFDECEB3440F56 (void);
// 0x00000053 System.Void Examples.Scenes.TouchpadCamera.RotateCamera::Update()
extern void RotateCamera_Update_m4645E8E651D7268AA58DECC2FDDCD22A48B92724 (void);
// 0x00000054 System.Void Examples.Scenes.TouchpadCamera.RotateCamera::.ctor()
extern void RotateCamera__ctor_mDEE2D7C98ED57BF92A36CAC9A54A7791117B120F (void);
// 0x00000055 System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::Awake()
extern void RotationConstraint_Awake_m5FFE5E61328AB477064AC23BAA0A26BF35F9AA8A (void);
// 0x00000056 System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::LateUpdate()
extern void RotationConstraint_LateUpdate_mD64E7AB56C7F672D22D3E96D72D5FC9A5BF9BCCB (void);
// 0x00000057 System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::.ctor()
extern void RotationConstraint__ctor_m72B29DC3691BD50A46BF00F86F45A99B58D26781 (void);
// 0x00000058 System.Void CustomJoystick.FourWayController::Awake()
extern void FourWayController_Awake_m2111C25BCDC49D679BF07F91D90255389C220666 (void);
// 0x00000059 System.Void CustomJoystick.FourWayController::Update()
extern void FourWayController_Update_m12783631ECF15B350164B57C0417F869B5777D41 (void);
// 0x0000005A System.Void CustomJoystick.FourWayController::.ctor()
extern void FourWayController__ctor_m116F69B4CAF6BA3E5B5FA479A1AF701977C6C7BB (void);
// 0x0000005B System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern void AbstractTargetFollower_Start_m3C2AFB5E22059519851F667C84F2B012A21195F7 (void);
// 0x0000005C System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern void AbstractTargetFollower_FixedUpdate_m31CDC56D90DE22CC9B91B9D6DB8BD4E45043DEFE (void);
// 0x0000005D System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern void AbstractTargetFollower_LateUpdate_m3B6E61E56F64E3782FA5C105D6176E547895BC4E (void);
// 0x0000005E System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern void AbstractTargetFollower_ManualUpdate_m27219E307B25A44F405FD7C092527B2605C72BB5 (void);
// 0x0000005F System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// 0x00000060 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern void AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB (void);
// 0x00000061 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern void AbstractTargetFollower_SetTarget_m09075B1BF3B7EADF99BC4E338A43C4E7781C7E82 (void);
// 0x00000062 UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern void AbstractTargetFollower_get_Target_mC4B781CBCC331D9CB9B6411D1E1B59AD9455BEDB (void);
// 0x00000063 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern void AbstractTargetFollower__ctor_m729146B299728E3D434C1CEE3832FA6940994EA3 (void);
// 0x00000064 System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern void AutoCam_FollowTarget_mEFC2AFFB812961D49CE8EA44ED5711F55EE41F63 (void);
// 0x00000065 System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern void AutoCam__ctor_m573B4C3091E5E299C636B0AA28EB9C2BB3F9CF53 (void);
// 0x00000066 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern void PivotBasedCameraRig_Awake_m26BD1EC124D79E38642DAC0C7A61AF260CBD247C (void);
// 0x00000067 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern void PivotBasedCameraRig__ctor_m3B61A8F501BD284A9B7C1EB30A5B585EBDBD904A (void);
// 0x00000068 System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Awake()
extern void Platformer2DUserControl_Awake_mABD0B580E89C5468D99D2CD54D7FFF4FC0B61487 (void);
// 0x00000069 System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Update()
extern void Platformer2DUserControl_Update_mB04800ADEDB9749C86C36286AF963264F2B6D6FE (void);
// 0x0000006A System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::FixedUpdate()
extern void Platformer2DUserControl_FixedUpdate_mAD35FD5F54D275CAAC7E3D0C76E17A99A223ED90 (void);
// 0x0000006B System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::.ctor()
extern void Platformer2DUserControl__ctor_m2CFB7DEA8CC9EE1E9CD72EABA68F144512AB3E9F (void);
// 0x0000006C System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Awake()
extern void PlatformerCharacter2D_Awake_m2C234BD7760D8A1BA3E4509F10C01E15F77C2D7C (void);
// 0x0000006D System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::FixedUpdate()
extern void PlatformerCharacter2D_FixedUpdate_m36E4B99B14AF11152920DC79BC413A0C7BBA8211 (void);
// 0x0000006E System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean)
extern void PlatformerCharacter2D_Move_mD91A6DDDA7DB352E4905785BFF0A7C75D800CD21 (void);
// 0x0000006F System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Flip()
extern void PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62 (void);
// 0x00000070 System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::.ctor()
extern void PlatformerCharacter2D__ctor_mD4485148574D5910D0694CA518875CE200C1B592 (void);
// 0x00000071 System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern void Camera2DFollow_Start_m3227D33A9D35130293D1617A8D42EFCD9A2C3B0D (void);
// 0x00000072 System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern void Camera2DFollow_Update_m5CD7C2B523C3B074DFCA9FD9963439BEA5695D66 (void);
// 0x00000073 System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern void Camera2DFollow__ctor_m8A7C93E24F2CD212F136FCBDD3C32D5645E00CBE (void);
// 0x00000074 System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Restarter_OnTriggerEnter2D_m8BB6E3D53D95807B777A981678EE7BCCDFD35220 (void);
// 0x00000075 System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern void Restarter__ctor_mB1248092F111834B403FBD6AD3FA097D01B7DB20 (void);
static Il2CppMethodPointer s_methodPointers[117] = 
{
	ThidPersonExampleController_OnEnable_mB2992A2594EDC4250F065E13583A0C8521340334,
	ThidPersonExampleController_Update_mEB0EB920E5FD84D02E3289893C024E6B23A68805,
	ThidPersonExampleController__ctor_m6B7054DE640F21560C86BF017E8368F9EA232101,
	CommonOnScreenControl__ctor_m0EDAD36310415FB96E7C848A3824BE488964EF43,
	CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E,
	CnInputManager__ctor_m4FF766F7ACC318C6B77AF8CF8EB7A8E60F87F6F8,
	CnInputManager_get_TouchCount_mA59758D8D293E1F02ECF559A596A7040CC0E1782,
	CnInputManager_GetTouch_m8B17A871490DAC8716F88916E9B513BF18D67842,
	CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B,
	CnInputManager_GetAxisRaw_mC341ECB544726B2F36DA83F51F718A2E93380C2E,
	CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C,
	CnInputManager_GetButton_mC823D5A0CC24751793A0CAD17C97AA35A05490F4,
	CnInputManager_GetButtonDown_m465EE8E01A82D8BA2D0AED4017B26A7991BF9DF7,
	CnInputManager_GetButtonUp_m8809729C18402C8F7B142F6C255FDCE42659543E,
	CnInputManager_AxisExists_m0ED578BD6C0803B9FB25E5551BA91776583776C3,
	CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64,
	CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2,
	CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46,
	CnInputManager_RegisterVirtualButton_mE337F2AE2069EFC98A433950662B2AC92CB107F6,
	CnInputManager_UnregisterVirtualButton_mFE6F77977405E14E3E67716871D87C62A2D2FB7B,
	CnInputManager_GetVirtualAxisValue_m3A649765E16DD2CBFF4C013525956B1F5EFACC9C,
	CnInputManager_GetAnyVirtualButtonDown_m9A0048CA59A5964B864C20E17DFC2535A22C4138,
	CnInputManager_GetAnyVirtualButtonUp_m2A85153733F81918326833CDF305111DEB5D9990,
	CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F,
	VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777,
	VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB,
	VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130,
	VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD,
	VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED,
	VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA,
	VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6,
	VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD,
	VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB,
	VirtualButton__ctor_m33D71A415DA9EE486D63A4A6BF2F712F69EAC603,
	VirtualButton_Press_m1944E262243474DF95B526DE21A08CAB0E39A2E9,
	VirtualButton_Release_m5C7E5B88EC972BDCA47DDE42169C19B774CEBC68,
	VirtualButton_get_GetButton_m5A784A059961193CE5B16ADB59B0D2C362DFBAF3,
	VirtualButton_get_GetButtonDown_m1DAE11354C811678B39111E146200B23C6803E12,
	VirtualButton_get_GetButtonUp_mA53CCC28120C136AD5691AE8C98973DF460E6C86,
	Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956,
	Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379,
	Dpad_OnPointerDown_mF82EB7B90302B22E359009913D5BA754FCE42C35,
	Dpad_OnPointerUp_m29099F9D826939398203D027C9951BB7C241F535,
	Dpad__ctor_m04228D51365F267F1B691E37B763E8C724AD663C,
	DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2,
	DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163,
	DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4,
	DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C,
	DpadAxis_Awake_m08F451D554C54B2DE8EAD3859C209EE21616AB11,
	DpadAxis_OnEnable_m64D8939B835CD0E93488DD219E7EE90D684DDCC8,
	DpadAxis_OnDisable_mFF33785DDED5388FE8264131DE795079DB0FB546,
	DpadAxis_Press_mD9BCDA57FCBADC57DDB5BB4A4439647EFBCBAD90,
	DpadAxis_TryRelease_mD494D94FAA2D11866BF18348679651CB57FDE322,
	DpadAxis__ctor_m7DE7D60F8FF168B2131EB768A69CC64C0F7118DE,
	SensitiveJoystick_OnDrag_mEF3AFC37A7F67D138F9E5A03C823B899CEAEB9EB,
	SensitiveJoystick__ctor_m04B2E748AA080D3C64EB2BF5105E5C8E350289E5,
	SimpleButton_OnEnable_m57FF5CB5CBDE58B890755AF99140E4EE225813E7,
	SimpleButton_OnDisable_m92FF8AE5F15DEA4AE042456EC6BCEBA761902C65,
	SimpleButton_OnPointerUp_mB01978646F7323AE9F95A2015BABC4D722B097A1,
	SimpleButton_OnPointerDown_mCAED6359F7B3E616334761061255876421E5879F,
	SimpleButton__ctor_mDF932CB6F180F0900F92D4B3241A4D41ED402178,
	SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1,
	SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C,
	SimpleJoystick_Awake_m4A668E3B6AA0C1779D5B5D1B2637B9559B08CC72,
	SimpleJoystick_OnEnable_mF783F82863C089C43C34585A262414AE20E9E250,
	SimpleJoystick_OnDisable_m585C6E6A10806341B0B2D865B869BABBB0115C4B,
	SimpleJoystick_OnDrag_m315B6DA9C170A7106078F76A18F95C2398A94816,
	SimpleJoystick_OnPointerUp_m1BBC47BB0B9F02B873794D073BAD8BD0BF1BD7D1,
	SimpleJoystick_OnPointerDown_m228FDDFF072D62F4F531BE48AFE562E5D591EF13,
	SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9,
	SimpleJoystick__ctor_mDDF1DAF26C35A567748C85CB911DD8B2A429311D,
	Touchpad_get_CurrentEventCamera_m77CEFA791EFA8662F3F8C5B6E6FEC11E1A3139E4,
	Touchpad_set_CurrentEventCamera_m37E4B76190F2765D52ED9F71CF67EBE6CE5BAA92,
	Touchpad_OnEnable_m434A35BC9F950BF4EDC68E6DB4F93F013D5BBD3B,
	Touchpad_OnDisable_mB50AA2BED48EA529DB36AF516D143FAD1E5F0F90,
	Touchpad_OnDrag_m668AFF004B7734F4C376FC7C31D928DC84AD72C7,
	Touchpad_OnPointerUp_mC05E90519E111D361ED3C8E09D92269A0CFDC989,
	Touchpad_OnPointerDown_m1BEBD7E307303F93B24D1B79A7F78354BF11717F,
	Touchpad_Update_mF7762CE57B2BAB8EED9E379E152F372A0D65523C,
	Touchpad__ctor_mCADCA3DB1B6248C43CDD46EE1A5DDB2C08C77537,
	EmptyGraphic_OnPopulateMesh_m641C20ACAB49C8E8D264ED1AF6A8448DA06E91BD,
	EmptyGraphic__ctor_mAE371F9CE4D9C24BB2D82C0585EFDECEB3440F56,
	RotateCamera_Update_m4645E8E651D7268AA58DECC2FDDCD22A48B92724,
	RotateCamera__ctor_mDEE2D7C98ED57BF92A36CAC9A54A7791117B120F,
	RotationConstraint_Awake_m5FFE5E61328AB477064AC23BAA0A26BF35F9AA8A,
	RotationConstraint_LateUpdate_mD64E7AB56C7F672D22D3E96D72D5FC9A5BF9BCCB,
	RotationConstraint__ctor_m72B29DC3691BD50A46BF00F86F45A99B58D26781,
	FourWayController_Awake_m2111C25BCDC49D679BF07F91D90255389C220666,
	FourWayController_Update_m12783631ECF15B350164B57C0417F869B5777D41,
	FourWayController__ctor_m116F69B4CAF6BA3E5B5FA479A1AF701977C6C7BB,
	AbstractTargetFollower_Start_m3C2AFB5E22059519851F667C84F2B012A21195F7,
	AbstractTargetFollower_FixedUpdate_m31CDC56D90DE22CC9B91B9D6DB8BD4E45043DEFE,
	AbstractTargetFollower_LateUpdate_m3B6E61E56F64E3782FA5C105D6176E547895BC4E,
	AbstractTargetFollower_ManualUpdate_m27219E307B25A44F405FD7C092527B2605C72BB5,
	NULL,
	AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB,
	AbstractTargetFollower_SetTarget_m09075B1BF3B7EADF99BC4E338A43C4E7781C7E82,
	AbstractTargetFollower_get_Target_mC4B781CBCC331D9CB9B6411D1E1B59AD9455BEDB,
	AbstractTargetFollower__ctor_m729146B299728E3D434C1CEE3832FA6940994EA3,
	AutoCam_FollowTarget_mEFC2AFFB812961D49CE8EA44ED5711F55EE41F63,
	AutoCam__ctor_m573B4C3091E5E299C636B0AA28EB9C2BB3F9CF53,
	PivotBasedCameraRig_Awake_m26BD1EC124D79E38642DAC0C7A61AF260CBD247C,
	PivotBasedCameraRig__ctor_m3B61A8F501BD284A9B7C1EB30A5B585EBDBD904A,
	Platformer2DUserControl_Awake_mABD0B580E89C5468D99D2CD54D7FFF4FC0B61487,
	Platformer2DUserControl_Update_mB04800ADEDB9749C86C36286AF963264F2B6D6FE,
	Platformer2DUserControl_FixedUpdate_mAD35FD5F54D275CAAC7E3D0C76E17A99A223ED90,
	Platformer2DUserControl__ctor_m2CFB7DEA8CC9EE1E9CD72EABA68F144512AB3E9F,
	PlatformerCharacter2D_Awake_m2C234BD7760D8A1BA3E4509F10C01E15F77C2D7C,
	PlatformerCharacter2D_FixedUpdate_m36E4B99B14AF11152920DC79BC413A0C7BBA8211,
	PlatformerCharacter2D_Move_mD91A6DDDA7DB352E4905785BFF0A7C75D800CD21,
	PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62,
	PlatformerCharacter2D__ctor_mD4485148574D5910D0694CA518875CE200C1B592,
	Camera2DFollow_Start_m3227D33A9D35130293D1617A8D42EFCD9A2C3B0D,
	Camera2DFollow_Update_m5CD7C2B523C3B074DFCA9FD9963439BEA5695D66,
	Camera2DFollow__ctor_m8A7C93E24F2CD212F136FCBDD3C32D5645E00CBE,
	Restarter_OnTriggerEnter2D_m8BB6E3D53D95807B777A981678EE7BCCDFD35220,
	Restarter__ctor_mB1248092F111834B403FBD6AD3FA097D01B7DB20,
};
static const int32_t s_InvokerIndices[117] = 
{
	1499,
	1499,
	1499,
	1499,
	2421,
	1499,
	2416,
	2371,
	2365,
	2365,
	2180,
	2352,
	2352,
	2352,
	2352,
	2352,
	2388,
	2388,
	2388,
	2388,
	1988,
	2352,
	2352,
	2352,
	1466,
	1284,
	1488,
	1304,
	1284,
	1466,
	1284,
	1486,
	1302,
	1284,
	1499,
	1499,
	1486,
	1486,
	1486,
	1466,
	1284,
	1284,
	1284,
	1499,
	1466,
	1284,
	1455,
	1275,
	1499,
	1499,
	1499,
	584,
	1275,
	1499,
	1284,
	1499,
	1499,
	1499,
	1284,
	1284,
	1499,
	1466,
	1284,
	1499,
	1499,
	1499,
	1284,
	1284,
	1284,
	1302,
	1499,
	1466,
	1284,
	1499,
	1499,
	1284,
	1284,
	1284,
	1499,
	1499,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1304,
	1499,
	1284,
	1466,
	1499,
	1304,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	843,
	1499,
	1499,
	1499,
	1499,
	1499,
	1284,
	1499,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	117,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
