﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CFX_AutoStopLoopedEffect::OnEnable()
extern void CFX_AutoStopLoopedEffect_OnEnable_m81F6EA071D17234F48C16EEF91F108C85C6BC34D (void);
// 0x00000002 System.Void CFX_AutoStopLoopedEffect::Update()
extern void CFX_AutoStopLoopedEffect_Update_m8455CD3ECE5A5608169D2B9FEBB9EA84C81A7060 (void);
// 0x00000003 System.Void CFX_AutoStopLoopedEffect::.ctor()
extern void CFX_AutoStopLoopedEffect__ctor_mA4C85BFF549A5FADA49B99EDC5A024514046752C (void);
// 0x00000004 System.Void CFX_Demo_RandomDir::Start()
extern void CFX_Demo_RandomDir_Start_m52999C49F2868BFB0BB6A9FCB6E10AB35D6B904F (void);
// 0x00000005 System.Void CFX_Demo_RandomDir::.ctor()
extern void CFX_Demo_RandomDir__ctor_mEEE60AD5BB24565A71CF1CA6AC68636D42359070 (void);
// 0x00000006 System.Void CFX_Demo_RandomDirectionTranslate::Start()
extern void CFX_Demo_RandomDirectionTranslate_Start_mE1CBF47B450BE1B0BF60FB9B2E75F786DBEAB217 (void);
// 0x00000007 System.Void CFX_Demo_RandomDirectionTranslate::Update()
extern void CFX_Demo_RandomDirectionTranslate_Update_mA650A1F440EB777B4B338EBD98B172F8D5EB7733 (void);
// 0x00000008 System.Void CFX_Demo_RandomDirectionTranslate::.ctor()
extern void CFX_Demo_RandomDirectionTranslate__ctor_m47FBF30D0E5B8A1516F80E50464827FD73EB752D (void);
// 0x00000009 System.Void CFX_Demo_RotateCamera::Update()
extern void CFX_Demo_RotateCamera_Update_mC18CE551FFAECEF6548ADA6D449767F91DBDD65E (void);
// 0x0000000A System.Void CFX_Demo_RotateCamera::.ctor()
extern void CFX_Demo_RotateCamera__ctor_m68157A63E4E338DE609D5287B468DE885C4026F7 (void);
// 0x0000000B System.Void CFX_Demo_RotateCamera::.cctor()
extern void CFX_Demo_RotateCamera__cctor_mCECECE60780835BC37FD35A206AC0441FAE011BB (void);
// 0x0000000C System.Void CFX_Demo_Translate::Start()
extern void CFX_Demo_Translate_Start_m21EF393CC56CF932D6EE07FF36D92382F688976A (void);
// 0x0000000D System.Void CFX_Demo_Translate::Update()
extern void CFX_Demo_Translate_Update_m3F23CA37301D5744C6B48FC317F9DC39C43E4482 (void);
// 0x0000000E System.Void CFX_Demo_Translate::.ctor()
extern void CFX_Demo_Translate__ctor_m37D2AE194A4700B77F1A6B8F1CAFBE153E76A1E3 (void);
// 0x0000000F System.Void CFX_AutoDestructShuriken::OnEnable()
extern void CFX_AutoDestructShuriken_OnEnable_mB713D9880D9F693E4331464691F14DAF2AFCA024 (void);
// 0x00000010 System.Collections.IEnumerator CFX_AutoDestructShuriken::CheckIfAlive()
extern void CFX_AutoDestructShuriken_CheckIfAlive_mAA5284710E7D50FD9E2D9A411832421FC5CCF887 (void);
// 0x00000011 System.Void CFX_AutoDestructShuriken::.ctor()
extern void CFX_AutoDestructShuriken__ctor_mBCC50D11EBC8FA1D2485E5CC22EFE99203E2326E (void);
// 0x00000012 System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::.ctor(System.Int32)
extern void U3CCheckIfAliveU3Ed__2__ctor_m9CFF1B8CDF82583983832BA103A4BA18C983D3E2 (void);
// 0x00000013 System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.IDisposable.Dispose()
extern void U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_m07DEC90B24059ACF5EB13FFD45A081B3F7366B53 (void);
// 0x00000014 System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>d__2::MoveNext()
extern void U3CCheckIfAliveU3Ed__2_MoveNext_mFF6D5E6297855647BEB70161FE8043152EBFE4BC (void);
// 0x00000015 System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43BF71BDE8530EE51B7418043E96F489B3B8C38B (void);
// 0x00000016 System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mEF16B074432DE98F427DAA1007C84C6ADD3B6E6F (void);
// 0x00000017 System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mF25D234AC7B0F5FCA156FA8210CBBB703217EB2D (void);
// 0x00000018 System.Void CFX_AutoRotate::Update()
extern void CFX_AutoRotate_Update_m32F1C6853ADCFF1F79A841B962CE43E3D173E1C9 (void);
// 0x00000019 System.Void CFX_AutoRotate::.ctor()
extern void CFX_AutoRotate__ctor_m8AC2481F43E3D374D9B6B07B5D33C512D8ED7A1D (void);
// 0x0000001A System.Void CFX_LightFlicker::Awake()
extern void CFX_LightFlicker_Awake_m66771E6E85675B9C56EA864327F6D4C910236E0F (void);
// 0x0000001B System.Void CFX_LightFlicker::OnEnable()
extern void CFX_LightFlicker_OnEnable_mA47BDC0A7A70FC9436520F71607B013F17A8C36E (void);
// 0x0000001C System.Void CFX_LightFlicker::Update()
extern void CFX_LightFlicker_Update_m3BAF027F4230DC340971980EF4F2FEE73911A5FA (void);
// 0x0000001D System.Void CFX_LightFlicker::.ctor()
extern void CFX_LightFlicker__ctor_mA27A91508C1F8D2FFB9F9C1A03EC219B81144CC1 (void);
// 0x0000001E System.Void CFX_LightIntensityFade::Start()
extern void CFX_LightIntensityFade_Start_m6D34477E424A03459333CBC6B1BB10FD0AE214F9 (void);
// 0x0000001F System.Void CFX_LightIntensityFade::OnEnable()
extern void CFX_LightIntensityFade_OnEnable_m43847A2A7E8FE80B760CD2CF2CED47D0AFAE5F22 (void);
// 0x00000020 System.Void CFX_LightIntensityFade::Update()
extern void CFX_LightIntensityFade_Update_m5B134469959272677C90F5B7105D8E2EFAF2B7C0 (void);
// 0x00000021 System.Void CFX_LightIntensityFade::.ctor()
extern void CFX_LightIntensityFade__ctor_mEF1612F406C762E592D76077146D32C641FCB77E (void);
// 0x00000022 System.Void ArrowGUI::Start()
extern void ArrowGUI_Start_m4723135F8D83A378649C3E338FDD967878115D2C (void);
// 0x00000023 System.Void ArrowGUI::Update()
extern void ArrowGUI_Update_m4B46E45ED9AC08E6E044F97CB3467E4A9678B287 (void);
// 0x00000024 System.Void ArrowGUI::.ctor()
extern void ArrowGUI__ctor_m1BA09445284A3D52C615ABCCD9BA96BBCD200E9C (void);
// 0x00000025 System.Void Bot::Start()
extern void Bot_Start_m8B3DD5E6B12510FA48A99627325DF1BD964AB18B (void);
// 0x00000026 System.Void Bot::Update()
extern void Bot_Update_m5A1B342E63B1EB246596C372481AA5029BE8F57D (void);
// 0x00000027 System.Void Bot::.ctor()
extern void Bot__ctor_m101A4EF319447B0A74361032950AFC49D5E008F3 (void);
// 0x00000028 System.Void CameraMovement::Start()
extern void CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41 (void);
// 0x00000029 System.Void CameraMovement::LateUpdate()
extern void CameraMovement_LateUpdate_mC4AEE0CC410E83478E93D46B6080A7E12646E926 (void);
// 0x0000002A System.Void CameraMovement::FieldView(System.Single)
extern void CameraMovement_FieldView_m9FC38F3DCC1D438AC3537DB1380C36574F304C44 (void);
// 0x0000002B System.Void CameraMovement::.ctor()
extern void CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA (void);
// 0x0000002C System.Void Controll::CreateMinionMassive(System.Int32)
extern void Controll_CreateMinionMassive_m4FD7F15270E40FC77D15BF2990E56460914F0638 (void);
// 0x0000002D System.Void Controll::Awake()
extern void Controll_Awake_m23CF75E96C5D5649E3AB27286241FEE42B738C4A (void);
// 0x0000002E System.Void Controll::SetColorPets(UnityEngine.Color)
extern void Controll_SetColorPets_m45005A2C38E4EA7795C70118E9FA0E73954663D9 (void);
// 0x0000002F System.Void Controll::Start()
extern void Controll_Start_m088DEFF140A7CB447F168B6E6FE8DA508536427E (void);
// 0x00000030 System.Void Controll::StartCreateMinion()
extern void Controll_StartCreateMinion_m14E20D1737341B896BE7ED9EC6272620461ACA09 (void);
// 0x00000031 System.Void Controll::Update()
extern void Controll_Update_m4D0CB5EF1B3A28B6879B27CE309E7978BE27A7F5 (void);
// 0x00000032 System.Void Controll::LateUpdate()
extern void Controll_LateUpdate_m39B387198AB9C68AAAE29AE0DB9F6421ABFA3E97 (void);
// 0x00000033 System.Void Controll::Movement()
extern void Controll_Movement_mB158994FDAA877A7E10D054FE584C1FC595CF3E2 (void);
// 0x00000034 System.Void Controll::LookAtXZ(UnityEngine.Vector3,System.Single)
extern void Controll_LookAtXZ_mF53766C911E39C7B7FFC87E0AE3E296C8986129C (void);
// 0x00000035 System.Void Controll::GetFood(UnityEngine.GameObject)
extern void Controll_GetFood_m4F52990DEAD02636F56E833B0894D5C908209307 (void);
// 0x00000036 System.Void Controll::CreateStartedMinion()
extern void Controll_CreateStartedMinion_m80641F8ABE45E811EACE031FF4BF173E9288306C (void);
// 0x00000037 System.Void Controll::CreateMinion()
extern void Controll_CreateMinion_mBC4DB45BD718C42801B77891182473BD83685374 (void);
// 0x00000038 System.Void Controll::MinusPet()
extern void Controll_MinusPet_m3F0C467DB0ADA4A2E1275F66A45A6A59875F6DFB (void);
// 0x00000039 System.Void Controll::BotCountSetting()
extern void Controll_BotCountSetting_mAB2468D96DF4A77D90349E3AD66AD363458DA2FA (void);
// 0x0000003A System.Void Controll::SetScore(System.Int32)
extern void Controll_SetScore_m426C67DFB81DDE038E0C7EE862FE064829E9B5AF (void);
// 0x0000003B System.Void Controll::OnCollisionEnter(UnityEngine.Collision)
extern void Controll_OnCollisionEnter_m117541DB2F3CC388F878071D6B8C75E52F5DE1FF (void);
// 0x0000003C System.Void Controll::.ctor()
extern void Controll__ctor_m54697B279640350CEEB4C2195CC21A1E3A5994D6 (void);
// 0x0000003D System.Void CountPetsBox::Awake()
extern void CountPetsBox_Awake_mEBDF14E7A00FD31343ACFB3A7229AA969CEC40A2 (void);
// 0x0000003E System.Void CountPetsBox::Start()
extern void CountPetsBox_Start_mB68669E7FFBF2A00A239D408E0091424075DB34F (void);
// 0x0000003F System.Void CountPetsBox::Update()
extern void CountPetsBox_Update_m482794C0974281910100D4D4C65C37387D9FAEEE (void);
// 0x00000040 System.Void CountPetsBox::.ctor()
extern void CountPetsBox__ctor_mB3C48F368D77EF53CB47B0876550787453365B35 (void);
// 0x00000041 System.Void DestroyFar::Start()
extern void DestroyFar_Start_m8381E95EBCBFCA9D31662C9517C33AC597FC9124 (void);
// 0x00000042 System.Void DestroyFar::Update()
extern void DestroyFar_Update_mAB76E211B025DC412F7DEE495F5ECDF6135E4136 (void);
// 0x00000043 System.Void DestroyFar::.ctor()
extern void DestroyFar__ctor_m48E672189682828E7658935DAD47DEC16944DB6F (void);
// 0x00000044 System.Void Farmer::OnBecameInvisible()
extern void Farmer_OnBecameInvisible_mC8D4FCA7435F7ED560D65C4E1F6385C27641724C (void);
// 0x00000045 System.Void Farmer::OnBecameVisible()
extern void Farmer_OnBecameVisible_m391ECEA4F6C31E67AEAC5BA31CE06C0C5D2950CF (void);
// 0x00000046 System.Void Farmer::Awake()
extern void Farmer_Awake_mB54877CC56294C4E8F86AFC7470A90683EC6FD97 (void);
// 0x00000047 System.Void Farmer::Update()
extern void Farmer_Update_m7AD75492EC1213C0CA3E8A6C56F9D3A4AF8BF025 (void);
// 0x00000048 System.Void Farmer::LookAtXZ(UnityEngine.Vector3,System.Single)
extern void Farmer_LookAtXZ_m2909A5A102B5754B1BEF931717DF3ED4D676F0FC (void);
// 0x00000049 System.Void Farmer::OnCollisionEnter(UnityEngine.Collision)
extern void Farmer_OnCollisionEnter_m89160A6FCFA6FD6EB55F886475550F9D5C25F9BF (void);
// 0x0000004A System.Void Farmer::.ctor()
extern void Farmer__ctor_m8701EE554873745CF9AF3780967427DFD0C84FA3 (void);
// 0x0000004B System.Void GenerateEvnironment::Start()
extern void GenerateEvnironment_Start_m31EBEF286575FE8FE4F2071B4F041EC3E9C1D707 (void);
// 0x0000004C System.Void GenerateEvnironment::Update()
extern void GenerateEvnironment_Update_m990AE327EAFE8A59844996B563736A7E80EE9B53 (void);
// 0x0000004D System.Void GenerateEvnironment::OnTriggerStay(UnityEngine.Collider)
extern void GenerateEvnironment_OnTriggerStay_m46CAED39D1ECC4CFF9825E42D65BFCCEC1BBB37F (void);
// 0x0000004E System.Void GenerateEvnironment::Generate()
extern void GenerateEvnironment_Generate_m966BB7FF8226573127D44E610849612552C82B4C (void);
// 0x0000004F System.Void GenerateEvnironment::.ctor()
extern void GenerateEvnironment__ctor_m2C47AB9B1B6A96558337851BB4520285F4DA1A0A (void);
// 0x00000050 System.Void Gradka::Start()
extern void Gradka_Start_m8E55DD6F78A1B46ADFB21FFF45ACE5BE7684125B (void);
// 0x00000051 System.Void Gradka::Update()
extern void Gradka_Update_m92E34377EB37BD47A332D22C9D9329715EF388EF (void);
// 0x00000052 System.Void Gradka::OnBecameInvisible()
extern void Gradka_OnBecameInvisible_m8D5B1F970D8592BD1081941E7647C6DA95973722 (void);
// 0x00000053 System.Void Gradka::OnBecameVisible()
extern void Gradka_OnBecameVisible_m94FB45A82CB18182FEA4CD7EFE56718A6CB154C8 (void);
// 0x00000054 System.Void Gradka::.ctor()
extern void Gradka__ctor_m0405C303B4CFD2D48A4260899ECA0C994173C5FF (void);
// 0x00000055 System.Void GrassDirtScale::Start()
extern void GrassDirtScale_Start_mACF59EE3D66C3D4CF6B067782F3EEBAF9C9ECFFB (void);
// 0x00000056 System.Void GrassDirtScale::Update()
extern void GrassDirtScale_Update_mFC66ABD47482B2048E9109CB8D024603F00F712A (void);
// 0x00000057 System.Void GrassDirtScale::.ctor()
extern void GrassDirtScale__ctor_mDE1DDEA1DB1C78A4B708B7E99166A218D4EA7992 (void);
// 0x00000058 System.Void MainSystem::Start()
extern void MainSystem_Start_m8231FA56BCEBAFC8E68D23091E3052C56B27EC42 (void);
// 0x00000059 System.Void MainSystem::TimeConvert()
extern void MainSystem_TimeConvert_mCF9CCCC584EB96A53333F182C0A6201EDC0F3D38 (void);
// 0x0000005A System.Void MainSystem::Update()
extern void MainSystem_Update_mEF39AEDE05C63EF70D8C751297484661A1FBC022 (void);
// 0x0000005B System.Void MainSystem::GetRewardCrowd()
extern void MainSystem_GetRewardCrowd_m315A1565EADA009E6B8CB8E1AE4D4ED7F711BF82 (void);
// 0x0000005C System.Void MainSystem::GetFood(UnityEngine.GameObject)
extern void MainSystem_GetFood_m0863FD7EFC87B0E7589DFF965D13063318A5E10A (void);
// 0x0000005D System.Void MainSystem::GameFinished()
extern void MainSystem_GameFinished_mE40D1B1659B7782F145F5D33CA01BA816D3B5F9B (void);
// 0x0000005E System.Void MainSystem::Reward2x()
extern void MainSystem_Reward2x_m61DCEEDDCDFB947D9F91A4C17C71B8EACC6F527D (void);
// 0x0000005F System.Void MainSystem::.ctor()
extern void MainSystem__ctor_m5DF07F8CBBFF2ABDB44998EB8F071FC620D44DFD (void);
// 0x00000060 System.Void AchiveItem::Start()
extern void AchiveItem_Start_m2E7BAC29648247982DE823976FC1421351162579 (void);
// 0x00000061 System.Void AchiveItem::SetAchiveActive()
extern void AchiveItem_SetAchiveActive_m7B52768C56503E681104CDC0A3CEE3727D13E155 (void);
// 0x00000062 System.Void AchiveItem::CheckAchi()
extern void AchiveItem_CheckAchi_m5D9D3315B74754A62273EB9DE8ACED664FFCD37E (void);
// 0x00000063 System.Void AchiveItem::.ctor()
extern void AchiveItem__ctor_mB09B58692C88C6D7B2EF81E10287B5D4E965081A (void);
// 0x00000064 System.Void CoinsEffect::Start()
extern void CoinsEffect_Start_mACFDC451A898BCDC11B69FD1902CD2B1AA720246 (void);
// 0x00000065 System.Void CoinsEffect::Update()
extern void CoinsEffect_Update_m70E283162B797C370B591B72B4608BB3D6344A83 (void);
// 0x00000066 System.Void CoinsEffect::.ctor()
extern void CoinsEffect__ctor_mDDBB6D32FE0004F3607799C2BDC4FEE1B585436F (void);
// 0x00000067 System.Void DailyRewardDestroy::Start()
extern void DailyRewardDestroy_Start_mFF6398F2A619768017ABF7A493F369EEC66F7971 (void);
// 0x00000068 System.Void DailyRewardDestroy::.ctor()
extern void DailyRewardDestroy__ctor_m0790D087B3595DD50E4B9803C787C796540CCEB7 (void);
// 0x00000069 System.Void FirstScene::Start()
extern void FirstScene_Start_mE5A95AB0ECCF4B23E5C7C274728C688931D2E9DF (void);
// 0x0000006A System.Void FirstScene::Update()
extern void FirstScene_Update_mAFE11C1FBBF163415312587F489439C8421291BB (void);
// 0x0000006B System.Void FirstScene::.ctor()
extern void FirstScene__ctor_mCB6B73B487CEE719A960CD65747D256A438D8275 (void);
// 0x0000006C System.Void MainMenu::Awake()
extern void MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE (void);
// 0x0000006D System.Void MainMenu::Start()
extern void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (void);
// 0x0000006E System.Void MainMenu::StartSetting()
extern void MainMenu_StartSetting_mA990A609210D323F1BBF69DBF78EB1641013B8F8 (void);
// 0x0000006F System.Void MainMenu::informerUpdate()
extern void MainMenu_informerUpdate_mFBE9C433918CF9C1310277E9FA73860B9D85BA22 (void);
// 0x00000070 System.Void MainMenu::Update()
extern void MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76 (void);
// 0x00000071 System.Void MainMenu::ButtonClicked()
extern void MainMenu_ButtonClicked_m4BA4B5DA9573810F6FA1813CBC36CF740238A56D (void);
// 0x00000072 System.Void MainMenu::CheckUnlocked()
extern void MainMenu_CheckUnlocked_m8E1EC5AE63E1F3279F223CC226FF736DABB7140E (void);
// 0x00000073 System.Void MainMenu::StartSkin()
extern void MainMenu_StartSkin_mBC722F6E042F1EBC5FFA68E5694E3C2394262B6B (void);
// 0x00000074 System.Void MainMenu::SkinShow(System.Int32)
extern void MainMenu_SkinShow_mEFF5EA1BEF235E9E89296C6978D5B0A3D4AFB70E (void);
// 0x00000075 System.Void MainMenu::DownPanelReset()
extern void MainMenu_DownPanelReset_mF88181A208B72D203210FBE91266C0E9457AD45A (void);
// 0x00000076 System.Void MainMenu::SkillSet(System.Int32)
extern void MainMenu_SkillSet_mE36C9875E6F35F749A65F6B033F4B2B165928AE4 (void);
// 0x00000077 System.Void MainMenu::SetGold(System.Int32)
extern void MainMenu_SetGold_m7C98CFE6C3E3C0F78AD6FAB1452F406CBB98BB08 (void);
// 0x00000078 System.Void MainMenu::GoldEffect()
extern void MainMenu_GoldEffect_m9DFD5BC304B34EB1B876B8B4348577612CF9C5B3 (void);
// 0x00000079 System.Void MainMenu::RandomSkin(System.Int32)
extern void MainMenu_RandomSkin_mB580FAE5CC272FF2B3C8CA00B9C1A8C825A403B0 (void);
// 0x0000007A System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x0000007B System.Void UpgradePrice::Start()
extern void UpgradePrice_Start_mCF58BD0B9804E35AACD8C11E5C1E951EBB728AED (void);
// 0x0000007C System.Void UpgradePrice::Update()
extern void UpgradePrice_Update_mEB4135A78509C3F45DAC05FD964735C7B5317B2C (void);
// 0x0000007D System.Void UpgradePrice::.ctor()
extern void UpgradePrice__ctor_mC1DDBB48094E06C4F804C7CEE9C340E47E9164FB (void);
// 0x0000007E System.Void WeekReward::Start()
extern void WeekReward_Start_m97316E0634CEF5BF4D2F82DADE625D9414E8C2FB (void);
// 0x0000007F System.Void WeekReward::Update()
extern void WeekReward_Update_mA6EF8049FFA73A62524785C4912C2A771A88A23D (void);
// 0x00000080 System.Void WeekReward::.ctor()
extern void WeekReward__ctor_mD44273D9E21AED7C1513A9B6D8B28C81275374B5 (void);
// 0x00000081 System.Void wwwBlock::Start()
extern void wwwBlock_Start_m8757300AF42ED16FD1C7280530231534CA179CC5 (void);
// 0x00000082 System.Void wwwBlock::Update()
extern void wwwBlock_Update_m22CA0459B98BA43B4C6103608E6820525F903045 (void);
// 0x00000083 System.Void wwwBlock::.ctor()
extern void wwwBlock__ctor_m3E2FF37683CBF51ACE0C4A6B3EB18CFB0D5A396B (void);
// 0x00000084 System.Boolean Minion::CheckKill()
extern void Minion_CheckKill_m81152F84B4972395FB02C3418A7DC891D97A5545 (void);
// 0x00000085 System.Void Minion::SetHero(UnityEngine.GameObject)
extern void Minion_SetHero_m854F3065A8BFEFD688CEDBEA68EEFB17D9BB60C2 (void);
// 0x00000086 System.Void Minion::MinionReturn()
extern void Minion_MinionReturn_m99AF8AEEB09019CAB466C4EBA4D9A94F3F9F1F38 (void);
// 0x00000087 System.Void Minion::DestroyMinion()
extern void Minion_DestroyMinion_mA3103A3B1154D11010A7CCA3457414796988DD7F (void);
// 0x00000088 System.Void Minion::Update()
extern void Minion_Update_m337B792AB2E7ECA12AC2CDC47902A8C73407D7F0 (void);
// 0x00000089 System.Void Minion::minusPet()
extern void Minion_minusPet_m714A91A585BEB08C9785A737721A564FF5A58834 (void);
// 0x0000008A System.Void Minion::EatFood()
extern void Minion_EatFood_m0106169FE9E9C78A51F3B1BB38C6DFE3AF1DC4EC (void);
// 0x0000008B System.Void Minion::.ctor()
extern void Minion__ctor_m1429D6FF2A028715E7F8330A8ACE3BC34CEBF284 (void);
// 0x0000008C System.Void ParticlesBase::Start()
extern void ParticlesBase_Start_m5654D41E720DBD1D377501ADF028F2CC22540BF5 (void);
// 0x0000008D System.Void ParticlesBase::SetPos(UnityEngine.Vector3)
extern void ParticlesBase_SetPos_mF86860F03EDC7FD68B932FE486391928D61D19B1 (void);
// 0x0000008E System.Void ParticlesBase::SetParent(UnityEngine.GameObject)
extern void ParticlesBase_SetParent_mF083317FF3617951BB5117E332B93A3837E9D01B (void);
// 0x0000008F System.Void ParticlesBase::ClonePart(System.Int32)
extern void ParticlesBase_ClonePart_mD8FC8031DF075AC05E66171EC7F0110D193245A3 (void);
// 0x00000090 System.Void ParticlesBase::.ctor()
extern void ParticlesBase__ctor_m5AE7611AB30E112A5D5114EBC97FF3C7B1966255 (void);
// 0x00000091 System.Void Rotate::Start()
extern void Rotate_Start_m260F3DA22D8BEE3571FB61596065ECC238B60968 (void);
// 0x00000092 System.Void Rotate::Update()
extern void Rotate_Update_m66D07F3686A017A63E869D8294C08E68F4A2FBAB (void);
// 0x00000093 System.Void Rotate::.ctor()
extern void Rotate__ctor_mAB2884DA9234D7A6485C5662D97205C92CA9B9C4 (void);
// 0x00000094 System.Void Shkala::SetScore(System.Int32)
extern void Shkala_SetScore_mAF61E2CEAD6676FB739C2C5E907DBD1202446A52 (void);
// 0x00000095 System.Void Shkala::Update()
extern void Shkala_Update_m238314D88236D379D17637641B3893B42AFD1E26 (void);
// 0x00000096 System.Void Shkala::.ctor()
extern void Shkala__ctor_m6B103A6508D0A28B8800DBE55914EC6E099AF16E (void);
// 0x00000097 System.Void SkinCollector::Start()
extern void SkinCollector_Start_m1FFAA068442F683F6C0AED646159AFD52D1A1415 (void);
// 0x00000098 System.Void SkinCollector::Update()
extern void SkinCollector_Update_mE89154F3BDAEDF5763F8E0C9CC9908BF280B8FA7 (void);
// 0x00000099 System.Void SkinCollector::.ctor()
extern void SkinCollector__ctor_m5D3F23077A752A37702C6400D3B8BDE4902EEFEB (void);
// 0x0000009A System.Void guiRankSystem::Start()
extern void guiRankSystem_Start_mC851CCB3C96B4C69BFD1581B7DE1BB1D0705EDB1 (void);
// 0x0000009B System.Void guiRankSystem::SetColor()
extern void guiRankSystem_SetColor_mB823FA5562A471E47BFED28419C3F67A01A48FBC (void);
// 0x0000009C System.Void guiRankSystem::FixedUpdate()
extern void guiRankSystem_FixedUpdate_m164FE1B89D696B0C9CB3CC2A9CD962CF62E13764 (void);
// 0x0000009D System.Void guiRankSystem::Update()
extern void guiRankSystem_Update_m860BBA2C2ED0F08A745AB405BE1EE56D741D6D53 (void);
// 0x0000009E System.Void guiRankSystem::GetData()
extern void guiRankSystem_GetData_m56CE67AF7D07080B8AE97225C670278F59937BA9 (void);
// 0x0000009F System.Void guiRankSystem::.ctor()
extern void guiRankSystem__ctor_mA51632BE792772E8519B90F90364F025222ED2BE (void);
// 0x000000A0 System.Void ButtonActiveTimer::Awake()
extern void ButtonActiveTimer_Awake_mFD2FF8CCA446B264EF2E3DF93ED50CBD5B5B7ECE (void);
// 0x000000A1 System.Void ButtonActiveTimer::Update()
extern void ButtonActiveTimer_Update_m7D2490E47B664FCD4870FD8F6ABD0D82EC2D05FD (void);
// 0x000000A2 System.Void ButtonActiveTimer::.ctor()
extern void ButtonActiveTimer__ctor_m1C4DFA770B1FEBA352A0E051A40D00201978FCDC (void);
// 0x000000A3 System.Void ButtonClickEffect::Start()
extern void ButtonClickEffect_Start_mE07B72947A806D32E1B9F7A89A28EBE18D2ACCD9 (void);
// 0x000000A4 System.Void ButtonClickEffect::StartPos()
extern void ButtonClickEffect_StartPos_m6E1F0618A930E0ECE163E2031C20161C1A50FFC1 (void);
// 0x000000A5 System.Void ButtonClickEffect::Update()
extern void ButtonClickEffect_Update_mA5C10EB60ADA5417A940A26A1DD1189E5FC4DFD0 (void);
// 0x000000A6 System.Void ButtonClickEffect::.ctor()
extern void ButtonClickEffect__ctor_m1780378CD91828E436C31541EB1D6A1E0C06A3DE (void);
// 0x000000A7 System.Void ButtonZ::Start()
extern void ButtonZ_Start_m9902217A37B5E881EDBD40EC4123CA51FAB760BE (void);
// 0x000000A8 System.Void ButtonZ::Update()
extern void ButtonZ_Update_m6CE3EA57F7E554FF26C711700CDE3B77C89D06BB (void);
// 0x000000A9 System.Void ButtonZ::.ctor()
extern void ButtonZ__ctor_m298A6141295BF90ED07B036BF21F99DC70603694 (void);
// 0x000000AA System.Void ButtonZfix::Start()
extern void ButtonZfix_Start_m59F0D46DB0AF6DC9004191BD9A9F4445201EA02B (void);
// 0x000000AB System.Void ButtonZfix::Update()
extern void ButtonZfix_Update_m45768A2CD537C6246193D0845A684594673F211A (void);
// 0x000000AC System.Void ButtonZfix::.ctor()
extern void ButtonZfix__ctor_m389EB5B92860B6CBC70F78D82D8014A3473922C3 (void);
// 0x000000AD System.Void ButtonsAfterLevel::Start()
extern void ButtonsAfterLevel_Start_mD8835AF81FE81ED756A90FAD835EA48603213A66 (void);
// 0x000000AE System.Void ButtonsAfterLevel::Update()
extern void ButtonsAfterLevel_Update_m9AF4E9560F656A8DFDAD4B1341C17C10244D8879 (void);
// 0x000000AF System.Void ButtonsAfterLevel::.ctor()
extern void ButtonsAfterLevel__ctor_m7D686807DF070AC3DE58A2CF3C7EE2C56504398C (void);
// 0x000000B0 System.String CheckInternet::GetHtmlFromUri(System.String)
extern void CheckInternet_GetHtmlFromUri_mEB60E5CA48D24CD761D3ACD6FB201CAFCB14D019 (void);
// 0x000000B1 System.Void CheckInternet::Start()
extern void CheckInternet_Start_mDA999EFF9DC0D699A6A95608178D258AAD1190E9 (void);
// 0x000000B2 System.Void CheckInternet::Update()
extern void CheckInternet_Update_mBDCCC808E1345FC384BE8270541AE364A7582B9F (void);
// 0x000000B3 System.Void CheckInternet::.ctor()
extern void CheckInternet__ctor_m8D6C23967EBF0476E7AF6BDFD63CF5C83F5B2F8E (void);
// 0x000000B4 System.Void DelButtonBuySkin::Start()
extern void DelButtonBuySkin_Start_mB72882D33A968DAD9E941830EEA07B74D6D62784 (void);
// 0x000000B5 System.Void DelButtonBuySkin::Update()
extern void DelButtonBuySkin_Update_mDC9493B204F9CA9A0B74ED3B538DEC6E00C8FF88 (void);
// 0x000000B6 System.Void DelButtonBuySkin::.ctor()
extern void DelButtonBuySkin__ctor_mB391AD8F30C89DA424ED6D4D7C0680F0F81E48FB (void);
// 0x000000B7 System.Void DelButtonNoAds::Start()
extern void DelButtonNoAds_Start_m2312D0FE98156AD8D7BE34F2C41B1F9149222E92 (void);
// 0x000000B8 System.Void DelButtonNoAds::Update()
extern void DelButtonNoAds_Update_m62B7EEECE4F4B303FFE7E5B1E216347D5746EDE2 (void);
// 0x000000B9 System.Void DelButtonNoAds::.ctor()
extern void DelButtonNoAds__ctor_m958599C17CB88AA8E3AE8519169B6FF499348625 (void);
// 0x000000BA System.Void EndLevel::Start()
extern void EndLevel_Start_m045C1432E5E20A26F523AE2E0DB1680BE01FF64C (void);
// 0x000000BB System.Void EndLevel::Update()
extern void EndLevel_Update_mCAB6EA15F073C9B0EAD7298063ECE0E244F490AB (void);
// 0x000000BC System.Void EndLevel::.ctor()
extern void EndLevel__ctor_m8B8076137402B2804A386C67AB7350A53B1FFB6A (void);
// 0x000000BD System.Void ExitButtonShow::Start()
extern void ExitButtonShow_Start_m406294879B32AFED9995E391D1DE16715FE357EF (void);
// 0x000000BE System.Void ExitButtonShow::Update()
extern void ExitButtonShow_Update_mFEFEEBAC119E5D0408776688DEAA8C6D7A677489 (void);
// 0x000000BF System.Void ExitButtonShow::.ctor()
extern void ExitButtonShow__ctor_mA9162A7BFF5ABB7FA1C4E1CEC5CE2E41908DF8E8 (void);
// 0x000000C0 System.Void ExtraTime::Start()
extern void ExtraTime_Start_mF9E5DC444F377D1CBBE219E9954E76CFD73DEEBB (void);
// 0x000000C1 System.Void ExtraTime::Update()
extern void ExtraTime_Update_m7E2E2713234B6A4BC0CB9E0EC92B0418ADF9B367 (void);
// 0x000000C2 System.Void ExtraTime::.ctor()
extern void ExtraTime__ctor_mADC1E5B64BE1F81F8DFBB5665407EBEB93CF8523 (void);
// 0x000000C3 System.Void GoAwayCoordinate999::Awake()
extern void GoAwayCoordinate999_Awake_m3FEA7EDBE9CB6177CFF8F19D8B1D7EE97F738A34 (void);
// 0x000000C4 System.Void GoAwayCoordinate999::GoActive(System.Single)
extern void GoAwayCoordinate999_GoActive_m6E5A8D319624F815A39B7E262907F512FD0ADDCB (void);
// 0x000000C5 System.Void GoAwayCoordinate999::Update()
extern void GoAwayCoordinate999_Update_m5188D1BC376DBE27BBC95994CA32E14DEC55C8FB (void);
// 0x000000C6 System.Void GoAwayCoordinate999::.ctor()
extern void GoAwayCoordinate999__ctor_m03F6692B596456186F4025BCF3A8F828FE76811C (void);
// 0x000000C7 System.Void Names::Awake()
extern void Names_Awake_mE9D9A54ABEECC714EA012DD56186FF4BBC9C6108 (void);
// 0x000000C8 System.Void Names::.ctor()
extern void Names__ctor_m5331A463E2EDDF4932436C09AEE6B8DE6839AB5D (void);
// 0x000000C9 System.Void OnBoost::OnTriggerEnter(UnityEngine.Collider)
extern void OnBoost_OnTriggerEnter_m7072B44693FB11DB293F1D52A2E8739A37FD9EF5 (void);
// 0x000000CA System.Void OnBoost::.ctor()
extern void OnBoost__ctor_m37415AEC77CC07633FBC085D82FC96C34B7A08DC (void);
// 0x000000CB System.Void OnFood::OnTriggerEnter(UnityEngine.Collider)
extern void OnFood_OnTriggerEnter_m6BB66A7F0F2487D6DF810D7796DDCFEEE2A10165 (void);
// 0x000000CC System.Void OnFood::.ctor()
extern void OnFood__ctor_m7BDC4BDF600A64071CECCBE677F830EB4B1F8E9C (void);
// 0x000000CD System.Void OnTextLayer::Start()
extern void OnTextLayer_Start_mF7F2FB5B8FF1EAC58BA2D3D9AB1B09972E9B841A (void);
// 0x000000CE System.Void OnTextLayer::.ctor()
extern void OnTextLayer__ctor_mF03A2A242AFA619EDCB1023C8485A32F23F2D622 (void);
// 0x000000CF System.Void ScaleZfromZero::Start()
extern void ScaleZfromZero_Start_m94FD92CA50BC7F929373F7847EAB4FA924CA8C7E (void);
// 0x000000D0 System.Void ScaleZfromZero::Update()
extern void ScaleZfromZero_Update_mFA24A4D046131464927D650A2C85419327188E4B (void);
// 0x000000D1 System.Void ScaleZfromZero::.ctor()
extern void ScaleZfromZero__ctor_mD7FAFBFDBE482A430F986C2F0484B6C7D6AC3365 (void);
// 0x000000D2 System.Void YourPlace::Awake()
extern void YourPlace_Awake_m96E4A9E1757509F60DDD030AE9E5AB89956D32E3 (void);
// 0x000000D3 System.Void YourPlace::Update()
extern void YourPlace_Update_m321AC7BE750DB85C875132054CA34F98FC01B2B0 (void);
// 0x000000D4 System.Void YourPlace::.ctor()
extern void YourPlace__ctor_mE2938F71984B009DE0678C3126F1D84E10F09CA9 (void);
// 0x000000D5 System.Void translateLanguages::Awake()
extern void translateLanguages_Awake_mE9B9BDF764535E4C43865C5EF9D56EFDC2B012B1 (void);
// 0x000000D6 System.Void translateLanguages::.ctor()
extern void translateLanguages__ctor_mB6854D5A6353DB164F09F51D9F0E7AE14ED4D104 (void);
// 0x000000D7 System.Void textCountPet::Start()
extern void textCountPet_Start_m65E746456D2B48D724B9189AC747969BB28F2DE6 (void);
// 0x000000D8 System.Void textCountPet::startText(System.String)
extern void textCountPet_startText_m6519369B424DAFC7AF3C4E64C442B3DAC5333250 (void);
// 0x000000D9 System.Void textCountPet::Update()
extern void textCountPet_Update_m9D3CA3B9DA878FF1A398656A1A71ADACD11E4097 (void);
// 0x000000DA System.Void textCountPet::.ctor()
extern void textCountPet__ctor_m4FE7C5E3CEBADF0A7908261DA894D12E1EB85F36 (void);
// 0x000000DB System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[219] = 
{
	CFX_AutoStopLoopedEffect_OnEnable_m81F6EA071D17234F48C16EEF91F108C85C6BC34D,
	CFX_AutoStopLoopedEffect_Update_m8455CD3ECE5A5608169D2B9FEBB9EA84C81A7060,
	CFX_AutoStopLoopedEffect__ctor_mA4C85BFF549A5FADA49B99EDC5A024514046752C,
	CFX_Demo_RandomDir_Start_m52999C49F2868BFB0BB6A9FCB6E10AB35D6B904F,
	CFX_Demo_RandomDir__ctor_mEEE60AD5BB24565A71CF1CA6AC68636D42359070,
	CFX_Demo_RandomDirectionTranslate_Start_mE1CBF47B450BE1B0BF60FB9B2E75F786DBEAB217,
	CFX_Demo_RandomDirectionTranslate_Update_mA650A1F440EB777B4B338EBD98B172F8D5EB7733,
	CFX_Demo_RandomDirectionTranslate__ctor_m47FBF30D0E5B8A1516F80E50464827FD73EB752D,
	CFX_Demo_RotateCamera_Update_mC18CE551FFAECEF6548ADA6D449767F91DBDD65E,
	CFX_Demo_RotateCamera__ctor_m68157A63E4E338DE609D5287B468DE885C4026F7,
	CFX_Demo_RotateCamera__cctor_mCECECE60780835BC37FD35A206AC0441FAE011BB,
	CFX_Demo_Translate_Start_m21EF393CC56CF932D6EE07FF36D92382F688976A,
	CFX_Demo_Translate_Update_m3F23CA37301D5744C6B48FC317F9DC39C43E4482,
	CFX_Demo_Translate__ctor_m37D2AE194A4700B77F1A6B8F1CAFBE153E76A1E3,
	CFX_AutoDestructShuriken_OnEnable_mB713D9880D9F693E4331464691F14DAF2AFCA024,
	CFX_AutoDestructShuriken_CheckIfAlive_mAA5284710E7D50FD9E2D9A411832421FC5CCF887,
	CFX_AutoDestructShuriken__ctor_mBCC50D11EBC8FA1D2485E5CC22EFE99203E2326E,
	U3CCheckIfAliveU3Ed__2__ctor_m9CFF1B8CDF82583983832BA103A4BA18C983D3E2,
	U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_m07DEC90B24059ACF5EB13FFD45A081B3F7366B53,
	U3CCheckIfAliveU3Ed__2_MoveNext_mFF6D5E6297855647BEB70161FE8043152EBFE4BC,
	U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43BF71BDE8530EE51B7418043E96F489B3B8C38B,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mEF16B074432DE98F427DAA1007C84C6ADD3B6E6F,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mF25D234AC7B0F5FCA156FA8210CBBB703217EB2D,
	CFX_AutoRotate_Update_m32F1C6853ADCFF1F79A841B962CE43E3D173E1C9,
	CFX_AutoRotate__ctor_m8AC2481F43E3D374D9B6B07B5D33C512D8ED7A1D,
	CFX_LightFlicker_Awake_m66771E6E85675B9C56EA864327F6D4C910236E0F,
	CFX_LightFlicker_OnEnable_mA47BDC0A7A70FC9436520F71607B013F17A8C36E,
	CFX_LightFlicker_Update_m3BAF027F4230DC340971980EF4F2FEE73911A5FA,
	CFX_LightFlicker__ctor_mA27A91508C1F8D2FFB9F9C1A03EC219B81144CC1,
	CFX_LightIntensityFade_Start_m6D34477E424A03459333CBC6B1BB10FD0AE214F9,
	CFX_LightIntensityFade_OnEnable_m43847A2A7E8FE80B760CD2CF2CED47D0AFAE5F22,
	CFX_LightIntensityFade_Update_m5B134469959272677C90F5B7105D8E2EFAF2B7C0,
	CFX_LightIntensityFade__ctor_mEF1612F406C762E592D76077146D32C641FCB77E,
	ArrowGUI_Start_m4723135F8D83A378649C3E338FDD967878115D2C,
	ArrowGUI_Update_m4B46E45ED9AC08E6E044F97CB3467E4A9678B287,
	ArrowGUI__ctor_m1BA09445284A3D52C615ABCCD9BA96BBCD200E9C,
	Bot_Start_m8B3DD5E6B12510FA48A99627325DF1BD964AB18B,
	Bot_Update_m5A1B342E63B1EB246596C372481AA5029BE8F57D,
	Bot__ctor_m101A4EF319447B0A74361032950AFC49D5E008F3,
	CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41,
	CameraMovement_LateUpdate_mC4AEE0CC410E83478E93D46B6080A7E12646E926,
	CameraMovement_FieldView_m9FC38F3DCC1D438AC3537DB1380C36574F304C44,
	CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA,
	Controll_CreateMinionMassive_m4FD7F15270E40FC77D15BF2990E56460914F0638,
	Controll_Awake_m23CF75E96C5D5649E3AB27286241FEE42B738C4A,
	Controll_SetColorPets_m45005A2C38E4EA7795C70118E9FA0E73954663D9,
	Controll_Start_m088DEFF140A7CB447F168B6E6FE8DA508536427E,
	Controll_StartCreateMinion_m14E20D1737341B896BE7ED9EC6272620461ACA09,
	Controll_Update_m4D0CB5EF1B3A28B6879B27CE309E7978BE27A7F5,
	Controll_LateUpdate_m39B387198AB9C68AAAE29AE0DB9F6421ABFA3E97,
	Controll_Movement_mB158994FDAA877A7E10D054FE584C1FC595CF3E2,
	Controll_LookAtXZ_mF53766C911E39C7B7FFC87E0AE3E296C8986129C,
	Controll_GetFood_m4F52990DEAD02636F56E833B0894D5C908209307,
	Controll_CreateStartedMinion_m80641F8ABE45E811EACE031FF4BF173E9288306C,
	Controll_CreateMinion_mBC4DB45BD718C42801B77891182473BD83685374,
	Controll_MinusPet_m3F0C467DB0ADA4A2E1275F66A45A6A59875F6DFB,
	Controll_BotCountSetting_mAB2468D96DF4A77D90349E3AD66AD363458DA2FA,
	Controll_SetScore_m426C67DFB81DDE038E0C7EE862FE064829E9B5AF,
	Controll_OnCollisionEnter_m117541DB2F3CC388F878071D6B8C75E52F5DE1FF,
	Controll__ctor_m54697B279640350CEEB4C2195CC21A1E3A5994D6,
	CountPetsBox_Awake_mEBDF14E7A00FD31343ACFB3A7229AA969CEC40A2,
	CountPetsBox_Start_mB68669E7FFBF2A00A239D408E0091424075DB34F,
	CountPetsBox_Update_m482794C0974281910100D4D4C65C37387D9FAEEE,
	CountPetsBox__ctor_mB3C48F368D77EF53CB47B0876550787453365B35,
	DestroyFar_Start_m8381E95EBCBFCA9D31662C9517C33AC597FC9124,
	DestroyFar_Update_mAB76E211B025DC412F7DEE495F5ECDF6135E4136,
	DestroyFar__ctor_m48E672189682828E7658935DAD47DEC16944DB6F,
	Farmer_OnBecameInvisible_mC8D4FCA7435F7ED560D65C4E1F6385C27641724C,
	Farmer_OnBecameVisible_m391ECEA4F6C31E67AEAC5BA31CE06C0C5D2950CF,
	Farmer_Awake_mB54877CC56294C4E8F86AFC7470A90683EC6FD97,
	Farmer_Update_m7AD75492EC1213C0CA3E8A6C56F9D3A4AF8BF025,
	Farmer_LookAtXZ_m2909A5A102B5754B1BEF931717DF3ED4D676F0FC,
	Farmer_OnCollisionEnter_m89160A6FCFA6FD6EB55F886475550F9D5C25F9BF,
	Farmer__ctor_m8701EE554873745CF9AF3780967427DFD0C84FA3,
	GenerateEvnironment_Start_m31EBEF286575FE8FE4F2071B4F041EC3E9C1D707,
	GenerateEvnironment_Update_m990AE327EAFE8A59844996B563736A7E80EE9B53,
	GenerateEvnironment_OnTriggerStay_m46CAED39D1ECC4CFF9825E42D65BFCCEC1BBB37F,
	GenerateEvnironment_Generate_m966BB7FF8226573127D44E610849612552C82B4C,
	GenerateEvnironment__ctor_m2C47AB9B1B6A96558337851BB4520285F4DA1A0A,
	Gradka_Start_m8E55DD6F78A1B46ADFB21FFF45ACE5BE7684125B,
	Gradka_Update_m92E34377EB37BD47A332D22C9D9329715EF388EF,
	Gradka_OnBecameInvisible_m8D5B1F970D8592BD1081941E7647C6DA95973722,
	Gradka_OnBecameVisible_m94FB45A82CB18182FEA4CD7EFE56718A6CB154C8,
	Gradka__ctor_m0405C303B4CFD2D48A4260899ECA0C994173C5FF,
	GrassDirtScale_Start_mACF59EE3D66C3D4CF6B067782F3EEBAF9C9ECFFB,
	GrassDirtScale_Update_mFC66ABD47482B2048E9109CB8D024603F00F712A,
	GrassDirtScale__ctor_mDE1DDEA1DB1C78A4B708B7E99166A218D4EA7992,
	MainSystem_Start_m8231FA56BCEBAFC8E68D23091E3052C56B27EC42,
	MainSystem_TimeConvert_mCF9CCCC584EB96A53333F182C0A6201EDC0F3D38,
	MainSystem_Update_mEF39AEDE05C63EF70D8C751297484661A1FBC022,
	MainSystem_GetRewardCrowd_m315A1565EADA009E6B8CB8E1AE4D4ED7F711BF82,
	MainSystem_GetFood_m0863FD7EFC87B0E7589DFF965D13063318A5E10A,
	MainSystem_GameFinished_mE40D1B1659B7782F145F5D33CA01BA816D3B5F9B,
	MainSystem_Reward2x_m61DCEEDDCDFB947D9F91A4C17C71B8EACC6F527D,
	MainSystem__ctor_m5DF07F8CBBFF2ABDB44998EB8F071FC620D44DFD,
	AchiveItem_Start_m2E7BAC29648247982DE823976FC1421351162579,
	AchiveItem_SetAchiveActive_m7B52768C56503E681104CDC0A3CEE3727D13E155,
	AchiveItem_CheckAchi_m5D9D3315B74754A62273EB9DE8ACED664FFCD37E,
	AchiveItem__ctor_mB09B58692C88C6D7B2EF81E10287B5D4E965081A,
	CoinsEffect_Start_mACFDC451A898BCDC11B69FD1902CD2B1AA720246,
	CoinsEffect_Update_m70E283162B797C370B591B72B4608BB3D6344A83,
	CoinsEffect__ctor_mDDBB6D32FE0004F3607799C2BDC4FEE1B585436F,
	DailyRewardDestroy_Start_mFF6398F2A619768017ABF7A493F369EEC66F7971,
	DailyRewardDestroy__ctor_m0790D087B3595DD50E4B9803C787C796540CCEB7,
	FirstScene_Start_mE5A95AB0ECCF4B23E5C7C274728C688931D2E9DF,
	FirstScene_Update_mAFE11C1FBBF163415312587F489439C8421291BB,
	FirstScene__ctor_mCB6B73B487CEE719A960CD65747D256A438D8275,
	MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE,
	MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621,
	MainMenu_StartSetting_mA990A609210D323F1BBF69DBF78EB1641013B8F8,
	MainMenu_informerUpdate_mFBE9C433918CF9C1310277E9FA73860B9D85BA22,
	MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76,
	MainMenu_ButtonClicked_m4BA4B5DA9573810F6FA1813CBC36CF740238A56D,
	MainMenu_CheckUnlocked_m8E1EC5AE63E1F3279F223CC226FF736DABB7140E,
	MainMenu_StartSkin_mBC722F6E042F1EBC5FFA68E5694E3C2394262B6B,
	MainMenu_SkinShow_mEFF5EA1BEF235E9E89296C6978D5B0A3D4AFB70E,
	MainMenu_DownPanelReset_mF88181A208B72D203210FBE91266C0E9457AD45A,
	MainMenu_SkillSet_mE36C9875E6F35F749A65F6B033F4B2B165928AE4,
	MainMenu_SetGold_m7C98CFE6C3E3C0F78AD6FAB1452F406CBB98BB08,
	MainMenu_GoldEffect_m9DFD5BC304B34EB1B876B8B4348577612CF9C5B3,
	MainMenu_RandomSkin_mB580FAE5CC272FF2B3C8CA00B9C1A8C825A403B0,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	UpgradePrice_Start_mCF58BD0B9804E35AACD8C11E5C1E951EBB728AED,
	UpgradePrice_Update_mEB4135A78509C3F45DAC05FD964735C7B5317B2C,
	UpgradePrice__ctor_mC1DDBB48094E06C4F804C7CEE9C340E47E9164FB,
	WeekReward_Start_m97316E0634CEF5BF4D2F82DADE625D9414E8C2FB,
	WeekReward_Update_mA6EF8049FFA73A62524785C4912C2A771A88A23D,
	WeekReward__ctor_mD44273D9E21AED7C1513A9B6D8B28C81275374B5,
	wwwBlock_Start_m8757300AF42ED16FD1C7280530231534CA179CC5,
	wwwBlock_Update_m22CA0459B98BA43B4C6103608E6820525F903045,
	wwwBlock__ctor_m3E2FF37683CBF51ACE0C4A6B3EB18CFB0D5A396B,
	Minion_CheckKill_m81152F84B4972395FB02C3418A7DC891D97A5545,
	Minion_SetHero_m854F3065A8BFEFD688CEDBEA68EEFB17D9BB60C2,
	Minion_MinionReturn_m99AF8AEEB09019CAB466C4EBA4D9A94F3F9F1F38,
	Minion_DestroyMinion_mA3103A3B1154D11010A7CCA3457414796988DD7F,
	Minion_Update_m337B792AB2E7ECA12AC2CDC47902A8C73407D7F0,
	Minion_minusPet_m714A91A585BEB08C9785A737721A564FF5A58834,
	Minion_EatFood_m0106169FE9E9C78A51F3B1BB38C6DFE3AF1DC4EC,
	Minion__ctor_m1429D6FF2A028715E7F8330A8ACE3BC34CEBF284,
	ParticlesBase_Start_m5654D41E720DBD1D377501ADF028F2CC22540BF5,
	ParticlesBase_SetPos_mF86860F03EDC7FD68B932FE486391928D61D19B1,
	ParticlesBase_SetParent_mF083317FF3617951BB5117E332B93A3837E9D01B,
	ParticlesBase_ClonePart_mD8FC8031DF075AC05E66171EC7F0110D193245A3,
	ParticlesBase__ctor_m5AE7611AB30E112A5D5114EBC97FF3C7B1966255,
	Rotate_Start_m260F3DA22D8BEE3571FB61596065ECC238B60968,
	Rotate_Update_m66D07F3686A017A63E869D8294C08E68F4A2FBAB,
	Rotate__ctor_mAB2884DA9234D7A6485C5662D97205C92CA9B9C4,
	Shkala_SetScore_mAF61E2CEAD6676FB739C2C5E907DBD1202446A52,
	Shkala_Update_m238314D88236D379D17637641B3893B42AFD1E26,
	Shkala__ctor_m6B103A6508D0A28B8800DBE55914EC6E099AF16E,
	SkinCollector_Start_m1FFAA068442F683F6C0AED646159AFD52D1A1415,
	SkinCollector_Update_mE89154F3BDAEDF5763F8E0C9CC9908BF280B8FA7,
	SkinCollector__ctor_m5D3F23077A752A37702C6400D3B8BDE4902EEFEB,
	guiRankSystem_Start_mC851CCB3C96B4C69BFD1581B7DE1BB1D0705EDB1,
	guiRankSystem_SetColor_mB823FA5562A471E47BFED28419C3F67A01A48FBC,
	guiRankSystem_FixedUpdate_m164FE1B89D696B0C9CB3CC2A9CD962CF62E13764,
	guiRankSystem_Update_m860BBA2C2ED0F08A745AB405BE1EE56D741D6D53,
	guiRankSystem_GetData_m56CE67AF7D07080B8AE97225C670278F59937BA9,
	guiRankSystem__ctor_mA51632BE792772E8519B90F90364F025222ED2BE,
	ButtonActiveTimer_Awake_mFD2FF8CCA446B264EF2E3DF93ED50CBD5B5B7ECE,
	ButtonActiveTimer_Update_m7D2490E47B664FCD4870FD8F6ABD0D82EC2D05FD,
	ButtonActiveTimer__ctor_m1C4DFA770B1FEBA352A0E051A40D00201978FCDC,
	ButtonClickEffect_Start_mE07B72947A806D32E1B9F7A89A28EBE18D2ACCD9,
	ButtonClickEffect_StartPos_m6E1F0618A930E0ECE163E2031C20161C1A50FFC1,
	ButtonClickEffect_Update_mA5C10EB60ADA5417A940A26A1DD1189E5FC4DFD0,
	ButtonClickEffect__ctor_m1780378CD91828E436C31541EB1D6A1E0C06A3DE,
	ButtonZ_Start_m9902217A37B5E881EDBD40EC4123CA51FAB760BE,
	ButtonZ_Update_m6CE3EA57F7E554FF26C711700CDE3B77C89D06BB,
	ButtonZ__ctor_m298A6141295BF90ED07B036BF21F99DC70603694,
	ButtonZfix_Start_m59F0D46DB0AF6DC9004191BD9A9F4445201EA02B,
	ButtonZfix_Update_m45768A2CD537C6246193D0845A684594673F211A,
	ButtonZfix__ctor_m389EB5B92860B6CBC70F78D82D8014A3473922C3,
	ButtonsAfterLevel_Start_mD8835AF81FE81ED756A90FAD835EA48603213A66,
	ButtonsAfterLevel_Update_m9AF4E9560F656A8DFDAD4B1341C17C10244D8879,
	ButtonsAfterLevel__ctor_m7D686807DF070AC3DE58A2CF3C7EE2C56504398C,
	CheckInternet_GetHtmlFromUri_mEB60E5CA48D24CD761D3ACD6FB201CAFCB14D019,
	CheckInternet_Start_mDA999EFF9DC0D699A6A95608178D258AAD1190E9,
	CheckInternet_Update_mBDCCC808E1345FC384BE8270541AE364A7582B9F,
	CheckInternet__ctor_m8D6C23967EBF0476E7AF6BDFD63CF5C83F5B2F8E,
	DelButtonBuySkin_Start_mB72882D33A968DAD9E941830EEA07B74D6D62784,
	DelButtonBuySkin_Update_mDC9493B204F9CA9A0B74ED3B538DEC6E00C8FF88,
	DelButtonBuySkin__ctor_mB391AD8F30C89DA424ED6D4D7C0680F0F81E48FB,
	DelButtonNoAds_Start_m2312D0FE98156AD8D7BE34F2C41B1F9149222E92,
	DelButtonNoAds_Update_m62B7EEECE4F4B303FFE7E5B1E216347D5746EDE2,
	DelButtonNoAds__ctor_m958599C17CB88AA8E3AE8519169B6FF499348625,
	EndLevel_Start_m045C1432E5E20A26F523AE2E0DB1680BE01FF64C,
	EndLevel_Update_mCAB6EA15F073C9B0EAD7298063ECE0E244F490AB,
	EndLevel__ctor_m8B8076137402B2804A386C67AB7350A53B1FFB6A,
	ExitButtonShow_Start_m406294879B32AFED9995E391D1DE16715FE357EF,
	ExitButtonShow_Update_mFEFEEBAC119E5D0408776688DEAA8C6D7A677489,
	ExitButtonShow__ctor_mA9162A7BFF5ABB7FA1C4E1CEC5CE2E41908DF8E8,
	ExtraTime_Start_mF9E5DC444F377D1CBBE219E9954E76CFD73DEEBB,
	ExtraTime_Update_m7E2E2713234B6A4BC0CB9E0EC92B0418ADF9B367,
	ExtraTime__ctor_mADC1E5B64BE1F81F8DFBB5665407EBEB93CF8523,
	GoAwayCoordinate999_Awake_m3FEA7EDBE9CB6177CFF8F19D8B1D7EE97F738A34,
	GoAwayCoordinate999_GoActive_m6E5A8D319624F815A39B7E262907F512FD0ADDCB,
	GoAwayCoordinate999_Update_m5188D1BC376DBE27BBC95994CA32E14DEC55C8FB,
	GoAwayCoordinate999__ctor_m03F6692B596456186F4025BCF3A8F828FE76811C,
	Names_Awake_mE9D9A54ABEECC714EA012DD56186FF4BBC9C6108,
	Names__ctor_m5331A463E2EDDF4932436C09AEE6B8DE6839AB5D,
	OnBoost_OnTriggerEnter_m7072B44693FB11DB293F1D52A2E8739A37FD9EF5,
	OnBoost__ctor_m37415AEC77CC07633FBC085D82FC96C34B7A08DC,
	OnFood_OnTriggerEnter_m6BB66A7F0F2487D6DF810D7796DDCFEEE2A10165,
	OnFood__ctor_m7BDC4BDF600A64071CECCBE677F830EB4B1F8E9C,
	OnTextLayer_Start_mF7F2FB5B8FF1EAC58BA2D3D9AB1B09972E9B841A,
	OnTextLayer__ctor_mF03A2A242AFA619EDCB1023C8485A32F23F2D622,
	ScaleZfromZero_Start_m94FD92CA50BC7F929373F7847EAB4FA924CA8C7E,
	ScaleZfromZero_Update_mFA24A4D046131464927D650A2C85419327188E4B,
	ScaleZfromZero__ctor_mD7FAFBFDBE482A430F986C2F0484B6C7D6AC3365,
	YourPlace_Awake_m96E4A9E1757509F60DDD030AE9E5AB89956D32E3,
	YourPlace_Update_m321AC7BE750DB85C875132054CA34F98FC01B2B0,
	YourPlace__ctor_mE2938F71984B009DE0678C3126F1D84E10F09CA9,
	translateLanguages_Awake_mE9B9BDF764535E4C43865C5EF9D56EFDC2B012B1,
	translateLanguages__ctor_mB6854D5A6353DB164F09F51D9F0E7AE14ED4D104,
	textCountPet_Start_m65E746456D2B48D724B9189AC747969BB28F2DE6,
	textCountPet_startText_m6519369B424DAFC7AF3C4E64C442B3DAC5333250,
	textCountPet_Update_m9D3CA3B9DA878FF1A398656A1A71ADACD11E4097,
	textCountPet__ctor_m4FE7C5E3CEBADF0A7908261DA894D12E1EB85F36,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
static const int32_t s_InvokerIndices[219] = 
{
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	2436,
	1499,
	1499,
	1499,
	1499,
	1466,
	1499,
	1275,
	1499,
	1486,
	1466,
	1499,
	1466,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1304,
	1499,
	1275,
	1499,
	1251,
	1499,
	1499,
	1499,
	1499,
	1499,
	851,
	1284,
	1499,
	1499,
	1499,
	1499,
	1275,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	851,
	1284,
	1499,
	1499,
	1499,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1275,
	1499,
	1275,
	1275,
	1499,
	1275,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1486,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1313,
	1284,
	1275,
	1499,
	1499,
	1499,
	1499,
	1275,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1048,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1304,
	1499,
	1499,
	1499,
	1499,
	1284,
	1499,
	1284,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1499,
	1284,
	1499,
	1499,
	2288,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	219,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
