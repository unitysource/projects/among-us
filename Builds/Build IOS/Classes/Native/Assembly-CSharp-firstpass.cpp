﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>
struct Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>
struct Dictionary_2_t02D647864343068994385888E81A0778D3247896;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>
struct KeyCollection_t331A5A5A5287B17D2626A56DB7B88621D6E0EEAF;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>
struct KeyCollection_t690C973E23B644E2A8A3C436A914E67E1B658F90;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Collections.Generic.List`1<CnControls.VirtualAxis>
struct List_1_tE5006B750AFF755696A7A223714798055FB561C7;
// System.Collections.Generic.List`1<CnControls.VirtualButton>
struct List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>
struct ValueCollection_t291060183AA593E939B6CB25C9B3447D90BC2179;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>
struct ValueCollection_tC18FB206EE1AF58FA7378788156B77E94F79AEBC;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>[]
struct EntryU5BU5D_t106A254E66C4FEA104F22CF6671642CA851D8658;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>[]
struct EntryU5BU5D_tB88AA273256A20DB37C2F0BE2C5C56179D4BF527;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1;
// CnControls.DpadAxis[]
struct DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// CnControls.VirtualAxis[]
struct VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB;
// CnControls.VirtualButton[]
struct VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155;
// UnityStandardAssets.Cameras.AbstractTargetFollower
struct AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityStandardAssets.Cameras.AutoCam
struct AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityStandardAssets._2D.Camera2DFollow
struct Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E;
// CnControls.CnInputManager
struct CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// CommonOnScreenControl
struct CommonOnScreenControl_t0C28AB2E71F8BA8DB737F5DB21BCE523FEC4AECD;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// CnControls.Dpad
struct Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75;
// CnControls.DpadAxis
struct DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B;
// CnControls.EmptyGraphic
struct EmptyGraphic_tEA7B1F28199F9AC58CB745C9706147B30E7A02A4;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// CustomJoystick.FourWayController
struct FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51;
// UnityStandardAssets.Copy._2D.Platformer2DUserControl
struct Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C;
// UnityStandardAssets.Copy._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityStandardAssets._2D.Restarter
struct Restarter_tD2B91A87072A0549C4D60034C25AFC16223FA9ED;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// Examples.Scenes.TouchpadCamera.RotateCamera
struct RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158;
// Examples.Scenes.TouchpadCamera.RotationConstraint
struct RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04;
// CnControls.SensitiveJoystick
struct SensitiveJoystick_t6BD7A696BA08155183A560BF113D5A06CDC99FFD;
// CnControls.SimpleButton
struct SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF;
// CnControls.SimpleJoystick
struct SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// ThidPersonExampleController
struct ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40;
// CnControls.Touchpad
struct Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// CnControls.VirtualAxis
struct VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD;
// CnControls.VirtualButton
struct VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t02D647864343068994385888E81A0778D3247896_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE5006B750AFF755696A7A223714798055FB561C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Physics2D_t1C1ECE6BA2F958C5C1440DDB9E9A5DAAA8F86D92_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral41C37D6140239C1EEC97F656CE8CF405635536E8;
IL2CPP_EXTERN_C String_t* _stringLiteral4FF6F93CCEBCA7FFB7AD642F8D2A1568D2339BE5;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32;
IL2CPP_EXTERN_C String_t* _stringLiteral65E0026258A51B468CEA87545210A330EECBA556;
IL2CPP_EXTERN_C String_t* _stringLiteral6976943C2BFBCFE85CB5EBBE8F087A90B277059C;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A;
IL2CPP_EXTERN_C String_t* _stringLiteral7E39FF2E25E7C626101199E5389D85A5D4DF1D47;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral96F1E4C380B7193666F533294DF99B093CF11038;
IL2CPP_EXTERN_C String_t* _stringLiteralB1CC7ED8198324B9A3ACFCF175E153FC25D05999;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mC2EEB227949FF6517A085ECC9E0FC1F8897A5546_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE_m07F4B2D7BABB71917777835CEADEA7C40DA07183_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m0531B1FE77B918C82A60F92F15467B240AC1CC8D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA49EC9C8CBA40F297C88B5FAD6B88BF8AAD7B3A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m116B6F471180B27F7AD111ACE39DEEF0559D7FF1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m341F36867DC9FCF1CD5931753FF4BD4C9A516464_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m844F394B59F3D535F3493877B6D21EE1B04C943D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m9C831679EEDF9F01998A14EA907429653D1BAE80_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m6AE3AC5DE2CDA479434C910C9B598E74A186F15D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m84FC0E138EF115F91CC134E1DDD9C879DDFED9B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB7AB75528AD2F0CFC1A1DFCB24A168895566661F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8147BB302F6B4E2CFFB9AA338472C7789FDBFA8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mAB7376FB6CC746A39504B77B71BA7572B16572EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m72EB5FED66B0343A8FC5F20EFACA90F0AD39E92F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var;

struct Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1;
struct DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t00DDB26693B7BCCD40544DBD546A67E6CCF39740 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>
struct  Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t106A254E66C4FEA104F22CF6671642CA851D8658* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t331A5A5A5287B17D2626A56DB7B88621D6E0EEAF * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t291060183AA593E939B6CB25C9B3447D90BC2179 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___entries_1)); }
	inline EntryU5BU5D_t106A254E66C4FEA104F22CF6671642CA851D8658* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t106A254E66C4FEA104F22CF6671642CA851D8658** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t106A254E66C4FEA104F22CF6671642CA851D8658* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___keys_7)); }
	inline KeyCollection_t331A5A5A5287B17D2626A56DB7B88621D6E0EEAF * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t331A5A5A5287B17D2626A56DB7B88621D6E0EEAF ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t331A5A5A5287B17D2626A56DB7B88621D6E0EEAF * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ___values_8)); }
	inline ValueCollection_t291060183AA593E939B6CB25C9B3447D90BC2179 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t291060183AA593E939B6CB25C9B3447D90BC2179 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t291060183AA593E939B6CB25C9B3447D90BC2179 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>
struct  Dictionary_2_t02D647864343068994385888E81A0778D3247896  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tB88AA273256A20DB37C2F0BE2C5C56179D4BF527* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t690C973E23B644E2A8A3C436A914E67E1B658F90 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tC18FB206EE1AF58FA7378788156B77E94F79AEBC * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___entries_1)); }
	inline EntryU5BU5D_tB88AA273256A20DB37C2F0BE2C5C56179D4BF527* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tB88AA273256A20DB37C2F0BE2C5C56179D4BF527** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tB88AA273256A20DB37C2F0BE2C5C56179D4BF527* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___keys_7)); }
	inline KeyCollection_t690C973E23B644E2A8A3C436A914E67E1B658F90 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t690C973E23B644E2A8A3C436A914E67E1B658F90 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t690C973E23B644E2A8A3C436A914E67E1B658F90 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ___values_8)); }
	inline ValueCollection_tC18FB206EE1AF58FA7378788156B77E94F79AEBC * get_values_8() const { return ___values_8; }
	inline ValueCollection_tC18FB206EE1AF58FA7378788156B77E94F79AEBC ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tC18FB206EE1AF58FA7378788156B77E94F79AEBC * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t02D647864343068994385888E81A0778D3247896, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<CnControls.VirtualAxis>
struct  List_1_tE5006B750AFF755696A7A223714798055FB561C7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE5006B750AFF755696A7A223714798055FB561C7, ____items_1)); }
	inline VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* get__items_1() const { return ____items_1; }
	inline VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE5006B750AFF755696A7A223714798055FB561C7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE5006B750AFF755696A7A223714798055FB561C7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE5006B750AFF755696A7A223714798055FB561C7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE5006B750AFF755696A7A223714798055FB561C7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE5006B750AFF755696A7A223714798055FB561C7_StaticFields, ____emptyArray_5)); }
	inline VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* get__emptyArray_5() const { return ____emptyArray_5; }
	inline VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(VirtualAxisU5BU5D_tDA4771F1B2223E2918A26C60AFB442BE28696DBB* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<CnControls.VirtualButton>
struct  List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59, ____items_1)); }
	inline VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* get__items_1() const { return ____items_1; }
	inline VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59_StaticFields, ____emptyArray_5)); }
	inline VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* get__emptyArray_5() const { return ____emptyArray_5; }
	inline VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(VirtualButtonU5BU5D_tF48009855A07C6FCB4C6977674870B432C5EA155* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// CnControls.CnInputManager
struct  CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>> CnControls.CnInputManager::_virtualAxisDictionary
	Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * ____virtualAxisDictionary_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>> CnControls.CnInputManager::_virtualButtonsDictionary
	Dictionary_2_t02D647864343068994385888E81A0778D3247896 * ____virtualButtonsDictionary_2;

public:
	inline static int32_t get_offset_of__virtualAxisDictionary_1() { return static_cast<int32_t>(offsetof(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A, ____virtualAxisDictionary_1)); }
	inline Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * get__virtualAxisDictionary_1() const { return ____virtualAxisDictionary_1; }
	inline Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 ** get_address_of__virtualAxisDictionary_1() { return &____virtualAxisDictionary_1; }
	inline void set__virtualAxisDictionary_1(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * value)
	{
		____virtualAxisDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____virtualAxisDictionary_1), (void*)value);
	}

	inline static int32_t get_offset_of__virtualButtonsDictionary_2() { return static_cast<int32_t>(offsetof(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A, ____virtualButtonsDictionary_2)); }
	inline Dictionary_2_t02D647864343068994385888E81A0778D3247896 * get__virtualButtonsDictionary_2() const { return ____virtualButtonsDictionary_2; }
	inline Dictionary_2_t02D647864343068994385888E81A0778D3247896 ** get_address_of__virtualButtonsDictionary_2() { return &____virtualButtonsDictionary_2; }
	inline void set__virtualButtonsDictionary_2(Dictionary_2_t02D647864343068994385888E81A0778D3247896 * value)
	{
		____virtualButtonsDictionary_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____virtualButtonsDictionary_2), (void*)value);
	}
};

struct CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_StaticFields
{
public:
	// CnControls.CnInputManager CnControls.CnInputManager::_instance
	CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_StaticFields, ____instance_0)); }
	inline CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * get__instance_0() const { return ____instance_0; }
	inline CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// CnControls.VirtualAxis
struct  VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD  : public RuntimeObject
{
public:
	// System.String CnControls.VirtualAxis::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Single CnControls.VirtualAxis::<Value>k__BackingField
	float ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD, ___U3CValueU3Ek__BackingField_1)); }
	inline float get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline float* get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(float value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}
};


// CnControls.VirtualButton
struct  VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F  : public RuntimeObject
{
public:
	// System.String CnControls.VirtualButton::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean CnControls.VirtualButton::<IsPressed>k__BackingField
	bool ___U3CIsPressedU3Ek__BackingField_1;
	// System.Int32 CnControls.VirtualButton::_lastPressedFrame
	int32_t ____lastPressedFrame_2;
	// System.Int32 CnControls.VirtualButton::_lastReleasedFrame
	int32_t ____lastReleasedFrame_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsPressedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F, ___U3CIsPressedU3Ek__BackingField_1)); }
	inline bool get_U3CIsPressedU3Ek__BackingField_1() const { return ___U3CIsPressedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsPressedU3Ek__BackingField_1() { return &___U3CIsPressedU3Ek__BackingField_1; }
	inline void set_U3CIsPressedU3Ek__BackingField_1(bool value)
	{
		___U3CIsPressedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__lastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F, ____lastPressedFrame_2)); }
	inline int32_t get__lastPressedFrame_2() const { return ____lastPressedFrame_2; }
	inline int32_t* get_address_of__lastPressedFrame_2() { return &____lastPressedFrame_2; }
	inline void set__lastPressedFrame_2(int32_t value)
	{
		____lastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of__lastReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F, ____lastReleasedFrame_3)); }
	inline int32_t get__lastReleasedFrame_3() const { return ____lastReleasedFrame_3; }
	inline int32_t* get_address_of__lastReleasedFrame_3() { return &____lastReleasedFrame_3; }
	inline void set__lastReleasedFrame_3(int32_t value)
	{
		____lastReleasedFrame_3 = value;
	}
};


// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E  : public AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E, ___m_EventSystem_1)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Keyframe
struct  Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// UnityEngine.LayerMask
struct  LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct  Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AnimationCurve
struct  AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.CollisionFlags
struct  CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CnControls.ControlMovementDirection
struct  ControlMovementDirection_t7BEF5D079051983B5A450A1BCDDA04EAF3C7A94A 
{
public:
	// System.Int32 CnControls.ControlMovementDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlMovementDirection_t7BEF5D079051983B5A450A1BCDDA04EAF3C7A94A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___m_GameObject_0)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___module_1)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldPosition_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldNormal_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___screenPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___screenPosition_9 = value;
	}

	inline static int32_t get_offset_of_displayIndex_10() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___displayIndex_10)); }
	inline int32_t get_displayIndex_10() const { return ___displayIndex_10; }
	inline int32_t* get_address_of_displayIndex_10() { return &___displayIndex_10; }
	inline void set_displayIndex_10(int32_t value)
	{
		___displayIndex_10 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// UnityEngine.TouchPhase
struct  TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchType
struct  TouchType_t2EF726465ABD45681A6686BAC426814AA087C20F 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t2EF726465ABD45681A6686BAC426814AA087C20F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.VertexHelper
struct  VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Colors_1)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv0S_2)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv1S_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv2S_4)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv3S_5)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Normals_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Tangents_7)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Indices_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
struct  UpdateType_tF0F45CA77514D26866987A8480CBFE8FA9EC0E44 
{
public:
	// System.Int32 UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_tF0F45CA77514D26866987A8480CBFE8FA9EC0E44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954  : public BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___m_PointerPress_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerClickU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerClickU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerClickU3Ek__BackingField_7() const { return ___U3CpointerClickU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerClickU3Ek__BackingField_7() { return &___U3CpointerClickU3Ek__BackingField_7; }
	inline void set_U3CpointerClickU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerClickU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerClickU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerCurrentRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerCurrentRaycastU3Ek__BackingField_8() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_8(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerPressRaycastU3Ek__BackingField_9)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerPressRaycastU3Ek__BackingField_9() const { return ___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return &___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_9(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___hovered_10)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_hovered_10() const { return ___hovered_10; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_hovered_10() { return &___hovered_10; }
	inline void set_hovered_10(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___hovered_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CeligibleForClickU3Ek__BackingField_11)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_11() const { return ___U3CeligibleForClickU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_11() { return &___U3CeligibleForClickU3Ek__BackingField_11; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_11(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_12() const { return ___U3CpointerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_12() { return &___U3CpointerIdU3Ek__BackingField_12; }
	inline void set_U3CpointerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpositionU3Ek__BackingField_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpositionU3Ek__BackingField_13() const { return ___U3CpositionU3Ek__BackingField_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpositionU3Ek__BackingField_13() { return &___U3CpositionU3Ek__BackingField_13; }
	inline void set_U3CpositionU3Ek__BackingField_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpositionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdeltaU3Ek__BackingField_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaU3Ek__BackingField_14() const { return ___U3CdeltaU3Ek__BackingField_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaU3Ek__BackingField_14() { return &___U3CdeltaU3Ek__BackingField_14; }
	inline void set_U3CdeltaU3Ek__BackingField_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpressPositionU3Ek__BackingField_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressPositionU3Ek__BackingField_15() const { return ___U3CpressPositionU3Ek__BackingField_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressPositionU3Ek__BackingField_15() { return &___U3CpressPositionU3Ek__BackingField_15; }
	inline void set_U3CpressPositionU3Ek__BackingField_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldPositionU3Ek__BackingField_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldPositionU3Ek__BackingField_16() const { return ___U3CworldPositionU3Ek__BackingField_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldPositionU3Ek__BackingField_16() { return &___U3CworldPositionU3Ek__BackingField_16; }
	inline void set_U3CworldPositionU3Ek__BackingField_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldPositionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldNormalU3Ek__BackingField_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldNormalU3Ek__BackingField_17() const { return ___U3CworldNormalU3Ek__BackingField_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldNormalU3Ek__BackingField_17() { return &___U3CworldNormalU3Ek__BackingField_17; }
	inline void set_U3CworldNormalU3Ek__BackingField_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldNormalU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickTimeU3Ek__BackingField_18)); }
	inline float get_U3CclickTimeU3Ek__BackingField_18() const { return ___U3CclickTimeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_18() { return &___U3CclickTimeU3Ek__BackingField_18; }
	inline void set_U3CclickTimeU3Ek__BackingField_18(float value)
	{
		___U3CclickTimeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickCountU3Ek__BackingField_19)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_19() const { return ___U3CclickCountU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_19() { return &___U3CclickCountU3Ek__BackingField_19; }
	inline void set_U3CclickCountU3Ek__BackingField_19(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CscrollDeltaU3Ek__BackingField_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CscrollDeltaU3Ek__BackingField_20() const { return ___U3CscrollDeltaU3Ek__BackingField_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CscrollDeltaU3Ek__BackingField_20() { return &___U3CscrollDeltaU3Ek__BackingField_20; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CuseDragThresholdU3Ek__BackingField_21)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_21() const { return ___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_21() { return &___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_21(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdraggingU3Ek__BackingField_22)); }
	inline bool get_U3CdraggingU3Ek__BackingField_22() const { return ___U3CdraggingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_22() { return &___U3CdraggingU3Ek__BackingField_22; }
	inline void set_U3CdraggingU3Ek__BackingField_22(bool value)
	{
		___U3CdraggingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CbuttonU3Ek__BackingField_23)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_23() const { return ___U3CbuttonU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_23() { return &___U3CbuttonU3Ek__BackingField_23; }
	inline void set_U3CbuttonU3Ek__BackingField_23(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_23 = value;
	}
};


// UnityEngine.Touch
struct  Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Position_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_RawPosition_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_PositionDelta_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.CharacterController
struct  CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E  : public Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02
{
public:

public:
};


// UnityEngine.Collider2D
struct  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityStandardAssets.Cameras.AbstractTargetFollower
struct  AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::m_Target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Target_4;
	// System.Boolean UnityStandardAssets.Cameras.AbstractTargetFollower::m_AutoTargetPlayer
	bool ___m_AutoTargetPlayer_5;
	// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType UnityStandardAssets.Cameras.AbstractTargetFollower::m_UpdateType
	int32_t ___m_UpdateType_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Cameras.AbstractTargetFollower::targetRigidbody
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___targetRigidbody_7;

public:
	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F, ___m_Target_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Target_4() const { return ___m_Target_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_AutoTargetPlayer_5() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F, ___m_AutoTargetPlayer_5)); }
	inline bool get_m_AutoTargetPlayer_5() const { return ___m_AutoTargetPlayer_5; }
	inline bool* get_address_of_m_AutoTargetPlayer_5() { return &___m_AutoTargetPlayer_5; }
	inline void set_m_AutoTargetPlayer_5(bool value)
	{
		___m_AutoTargetPlayer_5 = value;
	}

	inline static int32_t get_offset_of_m_UpdateType_6() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F, ___m_UpdateType_6)); }
	inline int32_t get_m_UpdateType_6() const { return ___m_UpdateType_6; }
	inline int32_t* get_address_of_m_UpdateType_6() { return &___m_UpdateType_6; }
	inline void set_m_UpdateType_6(int32_t value)
	{
		___m_UpdateType_6 = value;
	}

	inline static int32_t get_offset_of_targetRigidbody_7() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F, ___targetRigidbody_7)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_targetRigidbody_7() const { return ___targetRigidbody_7; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_targetRigidbody_7() { return &___targetRigidbody_7; }
	inline void set_targetRigidbody_7(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___targetRigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetRigidbody_7), (void*)value);
	}
};


// UnityStandardAssets._2D.Camera2DFollow
struct  Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform UnityStandardAssets._2D.Camera2DFollow::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_4;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::damping
	float ___damping_5;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadFactor
	float ___lookAheadFactor_6;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadReturnSpeed
	float ___lookAheadReturnSpeed_7;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadMoveThreshold
	float ___lookAheadMoveThreshold_8;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::m_OffsetZ
	float ___m_OffsetZ_9;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LastTargetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_LastTargetPosition_10;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_CurrentVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_CurrentVelocity_11;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LookAheadPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_LookAheadPos_12;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___target_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_4() const { return ___target_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_damping_5() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___damping_5)); }
	inline float get_damping_5() const { return ___damping_5; }
	inline float* get_address_of_damping_5() { return &___damping_5; }
	inline void set_damping_5(float value)
	{
		___damping_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadFactor_6() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___lookAheadFactor_6)); }
	inline float get_lookAheadFactor_6() const { return ___lookAheadFactor_6; }
	inline float* get_address_of_lookAheadFactor_6() { return &___lookAheadFactor_6; }
	inline void set_lookAheadFactor_6(float value)
	{
		___lookAheadFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadReturnSpeed_7() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___lookAheadReturnSpeed_7)); }
	inline float get_lookAheadReturnSpeed_7() const { return ___lookAheadReturnSpeed_7; }
	inline float* get_address_of_lookAheadReturnSpeed_7() { return &___lookAheadReturnSpeed_7; }
	inline void set_lookAheadReturnSpeed_7(float value)
	{
		___lookAheadReturnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadMoveThreshold_8() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___lookAheadMoveThreshold_8)); }
	inline float get_lookAheadMoveThreshold_8() const { return ___lookAheadMoveThreshold_8; }
	inline float* get_address_of_lookAheadMoveThreshold_8() { return &___lookAheadMoveThreshold_8; }
	inline void set_lookAheadMoveThreshold_8(float value)
	{
		___lookAheadMoveThreshold_8 = value;
	}

	inline static int32_t get_offset_of_m_OffsetZ_9() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___m_OffsetZ_9)); }
	inline float get_m_OffsetZ_9() const { return ___m_OffsetZ_9; }
	inline float* get_address_of_m_OffsetZ_9() { return &___m_OffsetZ_9; }
	inline void set_m_OffsetZ_9(float value)
	{
		___m_OffsetZ_9 = value;
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___m_LastTargetPosition_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_LastTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentVelocity_11() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___m_CurrentVelocity_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_CurrentVelocity_11() const { return ___m_CurrentVelocity_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_CurrentVelocity_11() { return &___m_CurrentVelocity_11; }
	inline void set_m_CurrentVelocity_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_CurrentVelocity_11 = value;
	}

	inline static int32_t get_offset_of_m_LookAheadPos_12() { return static_cast<int32_t>(offsetof(Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745, ___m_LookAheadPos_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_LookAheadPos_12() const { return ___m_LookAheadPos_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_LookAheadPos_12() { return &___m_LookAheadPos_12; }
	inline void set_m_LookAheadPos_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_LookAheadPos_12 = value;
	}
};


// CommonOnScreenControl
struct  CommonOnScreenControl_t0C28AB2E71F8BA8DB737F5DB21BCE523FEC4AECD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CnControls.Dpad
struct  Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CnControls.DpadAxis[] CnControls.Dpad::DpadAxis
	DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* ___DpadAxis_4;
	// UnityEngine.Camera CnControls.Dpad::<CurrentEventCamera>k__BackingField
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___U3CCurrentEventCameraU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_DpadAxis_4() { return static_cast<int32_t>(offsetof(Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75, ___DpadAxis_4)); }
	inline DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* get_DpadAxis_4() const { return ___DpadAxis_4; }
	inline DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11** get_address_of_DpadAxis_4() { return &___DpadAxis_4; }
	inline void set_DpadAxis_4(DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* value)
	{
		___DpadAxis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DpadAxis_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75, ___U3CCurrentEventCameraU3Ek__BackingField_5)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_U3CCurrentEventCameraU3Ek__BackingField_5() const { return ___U3CCurrentEventCameraU3Ek__BackingField_5; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_U3CCurrentEventCameraU3Ek__BackingField_5() { return &___U3CCurrentEventCameraU3Ek__BackingField_5; }
	inline void set_U3CCurrentEventCameraU3Ek__BackingField_5(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___U3CCurrentEventCameraU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentEventCameraU3Ek__BackingField_5), (void*)value);
	}
};


// CnControls.DpadAxis
struct  DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String CnControls.DpadAxis::AxisName
	String_t* ___AxisName_4;
	// System.Single CnControls.DpadAxis::AxisMultiplier
	float ___AxisMultiplier_5;
	// UnityEngine.RectTransform CnControls.DpadAxis::<RectTransform>k__BackingField
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___U3CRectTransformU3Ek__BackingField_6;
	// System.Int32 CnControls.DpadAxis::<LastFingerId>k__BackingField
	int32_t ___U3CLastFingerIdU3Ek__BackingField_7;
	// CnControls.VirtualAxis CnControls.DpadAxis::_virtualAxis
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ____virtualAxis_8;

public:
	inline static int32_t get_offset_of_AxisName_4() { return static_cast<int32_t>(offsetof(DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B, ___AxisName_4)); }
	inline String_t* get_AxisName_4() const { return ___AxisName_4; }
	inline String_t** get_address_of_AxisName_4() { return &___AxisName_4; }
	inline void set_AxisName_4(String_t* value)
	{
		___AxisName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AxisName_4), (void*)value);
	}

	inline static int32_t get_offset_of_AxisMultiplier_5() { return static_cast<int32_t>(offsetof(DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B, ___AxisMultiplier_5)); }
	inline float get_AxisMultiplier_5() const { return ___AxisMultiplier_5; }
	inline float* get_address_of_AxisMultiplier_5() { return &___AxisMultiplier_5; }
	inline void set_AxisMultiplier_5(float value)
	{
		___AxisMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_U3CRectTransformU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B, ___U3CRectTransformU3Ek__BackingField_6)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_U3CRectTransformU3Ek__BackingField_6() const { return ___U3CRectTransformU3Ek__BackingField_6; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_U3CRectTransformU3Ek__BackingField_6() { return &___U3CRectTransformU3Ek__BackingField_6; }
	inline void set_U3CRectTransformU3Ek__BackingField_6(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___U3CRectTransformU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRectTransformU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLastFingerIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B, ___U3CLastFingerIdU3Ek__BackingField_7)); }
	inline int32_t get_U3CLastFingerIdU3Ek__BackingField_7() const { return ___U3CLastFingerIdU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CLastFingerIdU3Ek__BackingField_7() { return &___U3CLastFingerIdU3Ek__BackingField_7; }
	inline void set_U3CLastFingerIdU3Ek__BackingField_7(int32_t value)
	{
		___U3CLastFingerIdU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__virtualAxis_8() { return static_cast<int32_t>(offsetof(DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B, ____virtualAxis_8)); }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * get__virtualAxis_8() const { return ____virtualAxis_8; }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD ** get_address_of__virtualAxis_8() { return &____virtualAxis_8; }
	inline void set__virtualAxis_8(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * value)
	{
		____virtualAxis_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____virtualAxis_8), (void*)value);
	}
};


// CustomJoystick.FourWayController
struct  FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3[] CustomJoystick.FourWayController::directionalVectors
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___directionalVectors_4;
	// UnityEngine.Transform CustomJoystick.FourWayController::_mainCameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____mainCameraTransform_5;

public:
	inline static int32_t get_offset_of_directionalVectors_4() { return static_cast<int32_t>(offsetof(FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291, ___directionalVectors_4)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_directionalVectors_4() const { return ___directionalVectors_4; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_directionalVectors_4() { return &___directionalVectors_4; }
	inline void set_directionalVectors_4(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___directionalVectors_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___directionalVectors_4), (void*)value);
	}

	inline static int32_t get_offset_of__mainCameraTransform_5() { return static_cast<int32_t>(offsetof(FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291, ____mainCameraTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__mainCameraTransform_5() const { return ____mainCameraTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__mainCameraTransform_5() { return &____mainCameraTransform_5; }
	inline void set__mainCameraTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____mainCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCameraTransform_5), (void*)value);
	}
};


// UnityStandardAssets.Copy._2D.Platformer2DUserControl
struct  Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityStandardAssets.Copy._2D.PlatformerCharacter2D UnityStandardAssets.Copy._2D.Platformer2DUserControl::m_Character
	PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * ___m_Character_4;
	// System.Boolean UnityStandardAssets.Copy._2D.Platformer2DUserControl::m_Jump
	bool ___m_Jump_5;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C, ___m_Character_4)); }
	inline PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * get_m_Character_4() const { return ___m_Character_4; }
	inline PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Character_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Jump_5() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C, ___m_Jump_5)); }
	inline bool get_m_Jump_5() const { return ___m_Jump_5; }
	inline bool* get_address_of_m_Jump_5() { return &___m_Jump_5; }
	inline void set_m_Jump_5(bool value)
	{
		___m_Jump_5 = value;
	}
};


// UnityStandardAssets.Copy._2D.PlatformerCharacter2D
struct  PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_MaxSpeed
	float ___m_MaxSpeed_4;
	// System.Single UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_JumpForce
	float ___m_JumpForce_5;
	// System.Boolean UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_AirControl
	bool ___m_AirControl_6;
	// UnityEngine.LayerMask UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_WhatIsGround
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_WhatIsGround_7;
	// UnityEngine.Transform UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_GroundCheck
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_GroundCheck_8;
	// System.Boolean UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_Grounded
	bool ___m_Grounded_10;
	// UnityEngine.Animator UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_Anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___m_Anim_11;
	// UnityEngine.Rigidbody2D UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_Rigidbody2D
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___m_Rigidbody2D_12;
	// System.Boolean UnityStandardAssets.Copy._2D.PlatformerCharacter2D::m_FacingRight
	bool ___m_FacingRight_13;

public:
	inline static int32_t get_offset_of_m_MaxSpeed_4() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_MaxSpeed_4)); }
	inline float get_m_MaxSpeed_4() const { return ___m_MaxSpeed_4; }
	inline float* get_address_of_m_MaxSpeed_4() { return &___m_MaxSpeed_4; }
	inline void set_m_MaxSpeed_4(float value)
	{
		___m_MaxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_JumpForce_5() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_JumpForce_5)); }
	inline float get_m_JumpForce_5() const { return ___m_JumpForce_5; }
	inline float* get_address_of_m_JumpForce_5() { return &___m_JumpForce_5; }
	inline void set_m_JumpForce_5(float value)
	{
		___m_JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_m_AirControl_6() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_AirControl_6)); }
	inline bool get_m_AirControl_6() const { return ___m_AirControl_6; }
	inline bool* get_address_of_m_AirControl_6() { return &___m_AirControl_6; }
	inline void set_m_AirControl_6(bool value)
	{
		___m_AirControl_6 = value;
	}

	inline static int32_t get_offset_of_m_WhatIsGround_7() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_WhatIsGround_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_WhatIsGround_7() const { return ___m_WhatIsGround_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_WhatIsGround_7() { return &___m_WhatIsGround_7; }
	inline void set_m_WhatIsGround_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_WhatIsGround_7 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheck_8() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_GroundCheck_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_GroundCheck_8() const { return ___m_GroundCheck_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_GroundCheck_8() { return &___m_GroundCheck_8; }
	inline void set_m_GroundCheck_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_GroundCheck_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GroundCheck_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Grounded_10() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_Grounded_10)); }
	inline bool get_m_Grounded_10() const { return ___m_Grounded_10; }
	inline bool* get_address_of_m_Grounded_10() { return &___m_Grounded_10; }
	inline void set_m_Grounded_10(bool value)
	{
		___m_Grounded_10 = value;
	}

	inline static int32_t get_offset_of_m_Anim_11() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_Anim_11)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_m_Anim_11() const { return ___m_Anim_11; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_m_Anim_11() { return &___m_Anim_11; }
	inline void set_m_Anim_11(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___m_Anim_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Anim_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Rigidbody2D_12() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_Rigidbody2D_12)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_m_Rigidbody2D_12() const { return ___m_Rigidbody2D_12; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_m_Rigidbody2D_12() { return &___m_Rigidbody2D_12; }
	inline void set_m_Rigidbody2D_12(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___m_Rigidbody2D_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rigidbody2D_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_FacingRight_13() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE, ___m_FacingRight_13)); }
	inline bool get_m_FacingRight_13() const { return ___m_FacingRight_13; }
	inline bool* get_address_of_m_FacingRight_13() { return &___m_FacingRight_13; }
	inline void set_m_FacingRight_13(bool value)
	{
		___m_FacingRight_13 = value;
	}
};


// UnityStandardAssets._2D.Restarter
struct  Restarter_tD2B91A87072A0549C4D60034C25AFC16223FA9ED  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Examples.Scenes.TouchpadCamera.RotateCamera
struct  RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Examples.Scenes.TouchpadCamera.RotateCamera::RotationSpeed
	float ___RotationSpeed_4;
	// UnityEngine.Transform Examples.Scenes.TouchpadCamera.RotateCamera::OriginTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___OriginTransform_5;

public:
	inline static int32_t get_offset_of_RotationSpeed_4() { return static_cast<int32_t>(offsetof(RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158, ___RotationSpeed_4)); }
	inline float get_RotationSpeed_4() const { return ___RotationSpeed_4; }
	inline float* get_address_of_RotationSpeed_4() { return &___RotationSpeed_4; }
	inline void set_RotationSpeed_4(float value)
	{
		___RotationSpeed_4 = value;
	}

	inline static int32_t get_offset_of_OriginTransform_5() { return static_cast<int32_t>(offsetof(RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158, ___OriginTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_OriginTransform_5() const { return ___OriginTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_OriginTransform_5() { return &___OriginTransform_5; }
	inline void set_OriginTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___OriginTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OriginTransform_5), (void*)value);
	}
};


// Examples.Scenes.TouchpadCamera.RotationConstraint
struct  RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Examples.Scenes.TouchpadCamera.RotationConstraint::Min
	float ___Min_4;
	// System.Single Examples.Scenes.TouchpadCamera.RotationConstraint::Max
	float ___Max_5;
	// UnityEngine.Transform Examples.Scenes.TouchpadCamera.RotationConstraint::_transformCache
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____transformCache_6;
	// UnityEngine.Quaternion Examples.Scenes.TouchpadCamera.RotationConstraint::_minQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____minQuaternion_7;
	// UnityEngine.Quaternion Examples.Scenes.TouchpadCamera.RotationConstraint::_maxQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____maxQuaternion_8;
	// UnityEngine.Vector3 Examples.Scenes.TouchpadCamera.RotationConstraint::_rotateAround
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____rotateAround_9;
	// System.Single Examples.Scenes.TouchpadCamera.RotationConstraint::_range
	float ____range_10;

public:
	inline static int32_t get_offset_of_Min_4() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ___Min_4)); }
	inline float get_Min_4() const { return ___Min_4; }
	inline float* get_address_of_Min_4() { return &___Min_4; }
	inline void set_Min_4(float value)
	{
		___Min_4 = value;
	}

	inline static int32_t get_offset_of_Max_5() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ___Max_5)); }
	inline float get_Max_5() const { return ___Max_5; }
	inline float* get_address_of_Max_5() { return &___Max_5; }
	inline void set_Max_5(float value)
	{
		___Max_5 = value;
	}

	inline static int32_t get_offset_of__transformCache_6() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ____transformCache_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__transformCache_6() const { return ____transformCache_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__transformCache_6() { return &____transformCache_6; }
	inline void set__transformCache_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____transformCache_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transformCache_6), (void*)value);
	}

	inline static int32_t get_offset_of__minQuaternion_7() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ____minQuaternion_7)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__minQuaternion_7() const { return ____minQuaternion_7; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__minQuaternion_7() { return &____minQuaternion_7; }
	inline void set__minQuaternion_7(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____minQuaternion_7 = value;
	}

	inline static int32_t get_offset_of__maxQuaternion_8() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ____maxQuaternion_8)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__maxQuaternion_8() const { return ____maxQuaternion_8; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__maxQuaternion_8() { return &____maxQuaternion_8; }
	inline void set__maxQuaternion_8(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____maxQuaternion_8 = value;
	}

	inline static int32_t get_offset_of__rotateAround_9() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ____rotateAround_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__rotateAround_9() const { return ____rotateAround_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__rotateAround_9() { return &____rotateAround_9; }
	inline void set__rotateAround_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____rotateAround_9 = value;
	}

	inline static int32_t get_offset_of__range_10() { return static_cast<int32_t>(offsetof(RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04, ____range_10)); }
	inline float get__range_10() const { return ____range_10; }
	inline float* get_address_of__range_10() { return &____range_10; }
	inline void set__range_10(float value)
	{
		____range_10 = value;
	}
};


// CnControls.SimpleButton
struct  SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String CnControls.SimpleButton::ButtonName
	String_t* ___ButtonName_4;
	// CnControls.VirtualButton CnControls.SimpleButton::_virtualButton
	VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ____virtualButton_5;

public:
	inline static int32_t get_offset_of_ButtonName_4() { return static_cast<int32_t>(offsetof(SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF, ___ButtonName_4)); }
	inline String_t* get_ButtonName_4() const { return ___ButtonName_4; }
	inline String_t** get_address_of_ButtonName_4() { return &___ButtonName_4; }
	inline void set_ButtonName_4(String_t* value)
	{
		___ButtonName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonName_4), (void*)value);
	}

	inline static int32_t get_offset_of__virtualButton_5() { return static_cast<int32_t>(offsetof(SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF, ____virtualButton_5)); }
	inline VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * get__virtualButton_5() const { return ____virtualButton_5; }
	inline VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F ** get_address_of__virtualButton_5() { return &____virtualButton_5; }
	inline void set__virtualButton_5(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * value)
	{
		____virtualButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____virtualButton_5), (void*)value);
	}
};


// CnControls.SimpleJoystick
struct  SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera CnControls.SimpleJoystick::<CurrentEventCamera>k__BackingField
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___U3CCurrentEventCameraU3Ek__BackingField_4;
	// System.Single CnControls.SimpleJoystick::MovementRange
	float ___MovementRange_5;
	// System.String CnControls.SimpleJoystick::HorizontalAxisName
	String_t* ___HorizontalAxisName_6;
	// System.String CnControls.SimpleJoystick::VerticalAxisName
	String_t* ___VerticalAxisName_7;
	// System.Boolean CnControls.SimpleJoystick::HideOnRelease
	bool ___HideOnRelease_8;
	// System.Boolean CnControls.SimpleJoystick::MoveBase
	bool ___MoveBase_9;
	// System.Boolean CnControls.SimpleJoystick::SnapsToFinger
	bool ___SnapsToFinger_10;
	// CnControls.ControlMovementDirection CnControls.SimpleJoystick::JoystickMoveAxis
	int32_t ___JoystickMoveAxis_11;
	// UnityEngine.UI.Image CnControls.SimpleJoystick::JoystickBase
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___JoystickBase_12;
	// UnityEngine.UI.Image CnControls.SimpleJoystick::Stick
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___Stick_13;
	// UnityEngine.RectTransform CnControls.SimpleJoystick::TouchZone
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___TouchZone_14;
	// UnityEngine.Vector2 CnControls.SimpleJoystick::_initialStickPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____initialStickPosition_15;
	// UnityEngine.Vector2 CnControls.SimpleJoystick::_intermediateStickPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____intermediateStickPosition_16;
	// UnityEngine.Vector2 CnControls.SimpleJoystick::_initialBasePosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____initialBasePosition_17;
	// UnityEngine.RectTransform CnControls.SimpleJoystick::_baseTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____baseTransform_18;
	// UnityEngine.RectTransform CnControls.SimpleJoystick::_stickTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____stickTransform_19;
	// System.Single CnControls.SimpleJoystick::_oneOverMovementRange
	float ____oneOverMovementRange_20;
	// CnControls.VirtualAxis CnControls.SimpleJoystick::HorizintalAxis
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___HorizintalAxis_21;
	// CnControls.VirtualAxis CnControls.SimpleJoystick::VerticalAxis
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___VerticalAxis_22;

public:
	inline static int32_t get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___U3CCurrentEventCameraU3Ek__BackingField_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_U3CCurrentEventCameraU3Ek__BackingField_4() const { return ___U3CCurrentEventCameraU3Ek__BackingField_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_U3CCurrentEventCameraU3Ek__BackingField_4() { return &___U3CCurrentEventCameraU3Ek__BackingField_4; }
	inline void set_U3CCurrentEventCameraU3Ek__BackingField_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___U3CCurrentEventCameraU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentEventCameraU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_MovementRange_5() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___MovementRange_5)); }
	inline float get_MovementRange_5() const { return ___MovementRange_5; }
	inline float* get_address_of_MovementRange_5() { return &___MovementRange_5; }
	inline void set_MovementRange_5(float value)
	{
		___MovementRange_5 = value;
	}

	inline static int32_t get_offset_of_HorizontalAxisName_6() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___HorizontalAxisName_6)); }
	inline String_t* get_HorizontalAxisName_6() const { return ___HorizontalAxisName_6; }
	inline String_t** get_address_of_HorizontalAxisName_6() { return &___HorizontalAxisName_6; }
	inline void set_HorizontalAxisName_6(String_t* value)
	{
		___HorizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HorizontalAxisName_6), (void*)value);
	}

	inline static int32_t get_offset_of_VerticalAxisName_7() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___VerticalAxisName_7)); }
	inline String_t* get_VerticalAxisName_7() const { return ___VerticalAxisName_7; }
	inline String_t** get_address_of_VerticalAxisName_7() { return &___VerticalAxisName_7; }
	inline void set_VerticalAxisName_7(String_t* value)
	{
		___VerticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalAxisName_7), (void*)value);
	}

	inline static int32_t get_offset_of_HideOnRelease_8() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___HideOnRelease_8)); }
	inline bool get_HideOnRelease_8() const { return ___HideOnRelease_8; }
	inline bool* get_address_of_HideOnRelease_8() { return &___HideOnRelease_8; }
	inline void set_HideOnRelease_8(bool value)
	{
		___HideOnRelease_8 = value;
	}

	inline static int32_t get_offset_of_MoveBase_9() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___MoveBase_9)); }
	inline bool get_MoveBase_9() const { return ___MoveBase_9; }
	inline bool* get_address_of_MoveBase_9() { return &___MoveBase_9; }
	inline void set_MoveBase_9(bool value)
	{
		___MoveBase_9 = value;
	}

	inline static int32_t get_offset_of_SnapsToFinger_10() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___SnapsToFinger_10)); }
	inline bool get_SnapsToFinger_10() const { return ___SnapsToFinger_10; }
	inline bool* get_address_of_SnapsToFinger_10() { return &___SnapsToFinger_10; }
	inline void set_SnapsToFinger_10(bool value)
	{
		___SnapsToFinger_10 = value;
	}

	inline static int32_t get_offset_of_JoystickMoveAxis_11() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___JoystickMoveAxis_11)); }
	inline int32_t get_JoystickMoveAxis_11() const { return ___JoystickMoveAxis_11; }
	inline int32_t* get_address_of_JoystickMoveAxis_11() { return &___JoystickMoveAxis_11; }
	inline void set_JoystickMoveAxis_11(int32_t value)
	{
		___JoystickMoveAxis_11 = value;
	}

	inline static int32_t get_offset_of_JoystickBase_12() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___JoystickBase_12)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_JoystickBase_12() const { return ___JoystickBase_12; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_JoystickBase_12() { return &___JoystickBase_12; }
	inline void set_JoystickBase_12(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___JoystickBase_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JoystickBase_12), (void*)value);
	}

	inline static int32_t get_offset_of_Stick_13() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___Stick_13)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_Stick_13() const { return ___Stick_13; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_Stick_13() { return &___Stick_13; }
	inline void set_Stick_13(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___Stick_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Stick_13), (void*)value);
	}

	inline static int32_t get_offset_of_TouchZone_14() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___TouchZone_14)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_TouchZone_14() const { return ___TouchZone_14; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_TouchZone_14() { return &___TouchZone_14; }
	inline void set_TouchZone_14(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___TouchZone_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TouchZone_14), (void*)value);
	}

	inline static int32_t get_offset_of__initialStickPosition_15() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____initialStickPosition_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__initialStickPosition_15() const { return ____initialStickPosition_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__initialStickPosition_15() { return &____initialStickPosition_15; }
	inline void set__initialStickPosition_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____initialStickPosition_15 = value;
	}

	inline static int32_t get_offset_of__intermediateStickPosition_16() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____intermediateStickPosition_16)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__intermediateStickPosition_16() const { return ____intermediateStickPosition_16; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__intermediateStickPosition_16() { return &____intermediateStickPosition_16; }
	inline void set__intermediateStickPosition_16(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____intermediateStickPosition_16 = value;
	}

	inline static int32_t get_offset_of__initialBasePosition_17() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____initialBasePosition_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__initialBasePosition_17() const { return ____initialBasePosition_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__initialBasePosition_17() { return &____initialBasePosition_17; }
	inline void set__initialBasePosition_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____initialBasePosition_17 = value;
	}

	inline static int32_t get_offset_of__baseTransform_18() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____baseTransform_18)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__baseTransform_18() const { return ____baseTransform_18; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__baseTransform_18() { return &____baseTransform_18; }
	inline void set__baseTransform_18(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____baseTransform_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____baseTransform_18), (void*)value);
	}

	inline static int32_t get_offset_of__stickTransform_19() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____stickTransform_19)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__stickTransform_19() const { return ____stickTransform_19; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__stickTransform_19() { return &____stickTransform_19; }
	inline void set__stickTransform_19(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____stickTransform_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stickTransform_19), (void*)value);
	}

	inline static int32_t get_offset_of__oneOverMovementRange_20() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ____oneOverMovementRange_20)); }
	inline float get__oneOverMovementRange_20() const { return ____oneOverMovementRange_20; }
	inline float* get_address_of__oneOverMovementRange_20() { return &____oneOverMovementRange_20; }
	inline void set__oneOverMovementRange_20(float value)
	{
		____oneOverMovementRange_20 = value;
	}

	inline static int32_t get_offset_of_HorizintalAxis_21() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___HorizintalAxis_21)); }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * get_HorizintalAxis_21() const { return ___HorizintalAxis_21; }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD ** get_address_of_HorizintalAxis_21() { return &___HorizintalAxis_21; }
	inline void set_HorizintalAxis_21(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * value)
	{
		___HorizintalAxis_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HorizintalAxis_21), (void*)value);
	}

	inline static int32_t get_offset_of_VerticalAxis_22() { return static_cast<int32_t>(offsetof(SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89, ___VerticalAxis_22)); }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * get_VerticalAxis_22() const { return ___VerticalAxis_22; }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD ** get_address_of_VerticalAxis_22() { return &___VerticalAxis_22; }
	inline void set_VerticalAxis_22(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * value)
	{
		___VerticalAxis_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalAxis_22), (void*)value);
	}
};


// ThidPersonExampleController
struct  ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ThidPersonExampleController::MovementSpeed
	float ___MovementSpeed_4;
	// UnityEngine.Transform ThidPersonExampleController::_mainCameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____mainCameraTransform_5;
	// UnityEngine.Transform ThidPersonExampleController::_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____transform_6;
	// UnityEngine.CharacterController ThidPersonExampleController::_characterController
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ____characterController_7;

public:
	inline static int32_t get_offset_of_MovementSpeed_4() { return static_cast<int32_t>(offsetof(ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40, ___MovementSpeed_4)); }
	inline float get_MovementSpeed_4() const { return ___MovementSpeed_4; }
	inline float* get_address_of_MovementSpeed_4() { return &___MovementSpeed_4; }
	inline void set_MovementSpeed_4(float value)
	{
		___MovementSpeed_4 = value;
	}

	inline static int32_t get_offset_of__mainCameraTransform_5() { return static_cast<int32_t>(offsetof(ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40, ____mainCameraTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__mainCameraTransform_5() const { return ____mainCameraTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__mainCameraTransform_5() { return &____mainCameraTransform_5; }
	inline void set__mainCameraTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____mainCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCameraTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of__transform_6() { return static_cast<int32_t>(offsetof(ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40, ____transform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__transform_6() const { return ____transform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__transform_6() { return &____transform_6; }
	inline void set__transform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____transform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transform_6), (void*)value);
	}

	inline static int32_t get_offset_of__characterController_7() { return static_cast<int32_t>(offsetof(ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40, ____characterController_7)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get__characterController_7() const { return ____characterController_7; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of__characterController_7() { return &____characterController_7; }
	inline void set__characterController_7(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		____characterController_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterController_7), (void*)value);
	}
};


// CnControls.Touchpad
struct  Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera CnControls.Touchpad::<CurrentEventCamera>k__BackingField
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___U3CCurrentEventCameraU3Ek__BackingField_4;
	// System.String CnControls.Touchpad::HorizontalAxisName
	String_t* ___HorizontalAxisName_5;
	// System.String CnControls.Touchpad::VerticalAxisName
	String_t* ___VerticalAxisName_6;
	// System.Boolean CnControls.Touchpad::PreserveInertia
	bool ___PreserveInertia_7;
	// System.Single CnControls.Touchpad::Friction
	float ___Friction_8;
	// CnControls.VirtualAxis CnControls.Touchpad::_horizintalAxis
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ____horizintalAxis_9;
	// CnControls.VirtualAxis CnControls.Touchpad::_verticalAxis
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ____verticalAxis_10;
	// System.Int32 CnControls.Touchpad::_lastDragFrameNumber
	int32_t ____lastDragFrameNumber_11;
	// System.Boolean CnControls.Touchpad::_isCurrentlyTweaking
	bool ____isCurrentlyTweaking_12;
	// CnControls.ControlMovementDirection CnControls.Touchpad::ControlMoveAxis
	int32_t ___ControlMoveAxis_13;

public:
	inline static int32_t get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___U3CCurrentEventCameraU3Ek__BackingField_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_U3CCurrentEventCameraU3Ek__BackingField_4() const { return ___U3CCurrentEventCameraU3Ek__BackingField_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_U3CCurrentEventCameraU3Ek__BackingField_4() { return &___U3CCurrentEventCameraU3Ek__BackingField_4; }
	inline void set_U3CCurrentEventCameraU3Ek__BackingField_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___U3CCurrentEventCameraU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentEventCameraU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_HorizontalAxisName_5() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___HorizontalAxisName_5)); }
	inline String_t* get_HorizontalAxisName_5() const { return ___HorizontalAxisName_5; }
	inline String_t** get_address_of_HorizontalAxisName_5() { return &___HorizontalAxisName_5; }
	inline void set_HorizontalAxisName_5(String_t* value)
	{
		___HorizontalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HorizontalAxisName_5), (void*)value);
	}

	inline static int32_t get_offset_of_VerticalAxisName_6() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___VerticalAxisName_6)); }
	inline String_t* get_VerticalAxisName_6() const { return ___VerticalAxisName_6; }
	inline String_t** get_address_of_VerticalAxisName_6() { return &___VerticalAxisName_6; }
	inline void set_VerticalAxisName_6(String_t* value)
	{
		___VerticalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalAxisName_6), (void*)value);
	}

	inline static int32_t get_offset_of_PreserveInertia_7() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___PreserveInertia_7)); }
	inline bool get_PreserveInertia_7() const { return ___PreserveInertia_7; }
	inline bool* get_address_of_PreserveInertia_7() { return &___PreserveInertia_7; }
	inline void set_PreserveInertia_7(bool value)
	{
		___PreserveInertia_7 = value;
	}

	inline static int32_t get_offset_of_Friction_8() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___Friction_8)); }
	inline float get_Friction_8() const { return ___Friction_8; }
	inline float* get_address_of_Friction_8() { return &___Friction_8; }
	inline void set_Friction_8(float value)
	{
		___Friction_8 = value;
	}

	inline static int32_t get_offset_of__horizintalAxis_9() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ____horizintalAxis_9)); }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * get__horizintalAxis_9() const { return ____horizintalAxis_9; }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD ** get_address_of__horizintalAxis_9() { return &____horizintalAxis_9; }
	inline void set__horizintalAxis_9(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * value)
	{
		____horizintalAxis_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____horizintalAxis_9), (void*)value);
	}

	inline static int32_t get_offset_of__verticalAxis_10() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ____verticalAxis_10)); }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * get__verticalAxis_10() const { return ____verticalAxis_10; }
	inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD ** get_address_of__verticalAxis_10() { return &____verticalAxis_10; }
	inline void set__verticalAxis_10(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * value)
	{
		____verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____verticalAxis_10), (void*)value);
	}

	inline static int32_t get_offset_of__lastDragFrameNumber_11() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ____lastDragFrameNumber_11)); }
	inline int32_t get__lastDragFrameNumber_11() const { return ____lastDragFrameNumber_11; }
	inline int32_t* get_address_of__lastDragFrameNumber_11() { return &____lastDragFrameNumber_11; }
	inline void set__lastDragFrameNumber_11(int32_t value)
	{
		____lastDragFrameNumber_11 = value;
	}

	inline static int32_t get_offset_of__isCurrentlyTweaking_12() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ____isCurrentlyTweaking_12)); }
	inline bool get__isCurrentlyTweaking_12() const { return ____isCurrentlyTweaking_12; }
	inline bool* get_address_of__isCurrentlyTweaking_12() { return &____isCurrentlyTweaking_12; }
	inline void set__isCurrentlyTweaking_12(bool value)
	{
		____isCurrentlyTweaking_12 = value;
	}

	inline static int32_t get_offset_of_ControlMoveAxis_13() { return static_cast<int32_t>(offsetof(Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5, ___ControlMoveAxis_13)); }
	inline int32_t get_ControlMoveAxis_13() const { return ___ControlMoveAxis_13; }
	inline int32_t* get_address_of_ControlMoveAxis_13() { return &___ControlMoveAxis_13; }
	inline void set_ControlMoveAxis_13(int32_t value)
	{
		___ControlMoveAxis_13 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct  PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51  : public AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Cam
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Cam_8;
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Pivot
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Pivot_9;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.PivotBasedCameraRig::m_LastTargetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_LastTargetPosition_10;

public:
	inline static int32_t get_offset_of_m_Cam_8() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51, ___m_Cam_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Cam_8() const { return ___m_Cam_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Cam_8() { return &___m_Cam_8; }
	inline void set_m_Cam_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Cam_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Cam_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Pivot_9() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51, ___m_Pivot_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Pivot_9() const { return ___m_Pivot_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Pivot_9() { return &___m_Pivot_9; }
	inline void set_m_Pivot_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Pivot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pivot_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51, ___m_LastTargetPosition_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_LastTargetPosition_10 = value;
	}
};


// CnControls.SensitiveJoystick
struct  SensitiveJoystick_t6BD7A696BA08155183A560BF113D5A06CDC99FFD  : public SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89
{
public:
	// UnityEngine.AnimationCurve CnControls.SensitiveJoystick::SensitivityCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___SensitivityCurve_23;

public:
	inline static int32_t get_offset_of_SensitivityCurve_23() { return static_cast<int32_t>(offsetof(SensitiveJoystick_t6BD7A696BA08155183A560BF113D5A06CDC99FFD, ___SensitivityCurve_23)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_SensitivityCurve_23() const { return ___SensitivityCurve_23; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_SensitivityCurve_23() { return &___SensitivityCurve_23; }
	inline void set_SensitivityCurve_23(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___SensitivityCurve_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SensitivityCurve_23), (void*)value);
	}
};


// UnityStandardAssets.Cameras.AutoCam
struct  AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84  : public PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51
{
public:
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_MoveSpeed
	float ___m_MoveSpeed_11;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeed
	float ___m_TurnSpeed_12;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_RollSpeed
	float ___m_RollSpeed_13;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowVelocity
	bool ___m_FollowVelocity_14;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowTilt
	bool ___m_FollowTilt_15;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SpinTurnLimit
	float ___m_SpinTurnLimit_16;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TargetVelocityLowerLimit
	float ___m_TargetVelocityLowerLimit_17;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SmoothTurnTime
	float ___m_SmoothTurnTime_18;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_LastFlatAngle
	float ___m_LastFlatAngle_19;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_CurrentTurnAmount
	float ___m_CurrentTurnAmount_20;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeedVelocityChange
	float ___m_TurnSpeedVelocityChange_21;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.AutoCam::m_RollUp
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_RollUp_22;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_11() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_MoveSpeed_11)); }
	inline float get_m_MoveSpeed_11() const { return ___m_MoveSpeed_11; }
	inline float* get_address_of_m_MoveSpeed_11() { return &___m_MoveSpeed_11; }
	inline void set_m_MoveSpeed_11(float value)
	{
		___m_MoveSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeed_12() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_TurnSpeed_12)); }
	inline float get_m_TurnSpeed_12() const { return ___m_TurnSpeed_12; }
	inline float* get_address_of_m_TurnSpeed_12() { return &___m_TurnSpeed_12; }
	inline void set_m_TurnSpeed_12(float value)
	{
		___m_TurnSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_RollSpeed_13() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_RollSpeed_13)); }
	inline float get_m_RollSpeed_13() const { return ___m_RollSpeed_13; }
	inline float* get_address_of_m_RollSpeed_13() { return &___m_RollSpeed_13; }
	inline void set_m_RollSpeed_13(float value)
	{
		___m_RollSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_14() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_FollowVelocity_14)); }
	inline bool get_m_FollowVelocity_14() const { return ___m_FollowVelocity_14; }
	inline bool* get_address_of_m_FollowVelocity_14() { return &___m_FollowVelocity_14; }
	inline void set_m_FollowVelocity_14(bool value)
	{
		___m_FollowVelocity_14 = value;
	}

	inline static int32_t get_offset_of_m_FollowTilt_15() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_FollowTilt_15)); }
	inline bool get_m_FollowTilt_15() const { return ___m_FollowTilt_15; }
	inline bool* get_address_of_m_FollowTilt_15() { return &___m_FollowTilt_15; }
	inline void set_m_FollowTilt_15(bool value)
	{
		___m_FollowTilt_15 = value;
	}

	inline static int32_t get_offset_of_m_SpinTurnLimit_16() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_SpinTurnLimit_16)); }
	inline float get_m_SpinTurnLimit_16() const { return ___m_SpinTurnLimit_16; }
	inline float* get_address_of_m_SpinTurnLimit_16() { return &___m_SpinTurnLimit_16; }
	inline void set_m_SpinTurnLimit_16(float value)
	{
		___m_SpinTurnLimit_16 = value;
	}

	inline static int32_t get_offset_of_m_TargetVelocityLowerLimit_17() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_TargetVelocityLowerLimit_17)); }
	inline float get_m_TargetVelocityLowerLimit_17() const { return ___m_TargetVelocityLowerLimit_17; }
	inline float* get_address_of_m_TargetVelocityLowerLimit_17() { return &___m_TargetVelocityLowerLimit_17; }
	inline void set_m_TargetVelocityLowerLimit_17(float value)
	{
		___m_TargetVelocityLowerLimit_17 = value;
	}

	inline static int32_t get_offset_of_m_SmoothTurnTime_18() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_SmoothTurnTime_18)); }
	inline float get_m_SmoothTurnTime_18() const { return ___m_SmoothTurnTime_18; }
	inline float* get_address_of_m_SmoothTurnTime_18() { return &___m_SmoothTurnTime_18; }
	inline void set_m_SmoothTurnTime_18(float value)
	{
		___m_SmoothTurnTime_18 = value;
	}

	inline static int32_t get_offset_of_m_LastFlatAngle_19() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_LastFlatAngle_19)); }
	inline float get_m_LastFlatAngle_19() const { return ___m_LastFlatAngle_19; }
	inline float* get_address_of_m_LastFlatAngle_19() { return &___m_LastFlatAngle_19; }
	inline void set_m_LastFlatAngle_19(float value)
	{
		___m_LastFlatAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTurnAmount_20() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_CurrentTurnAmount_20)); }
	inline float get_m_CurrentTurnAmount_20() const { return ___m_CurrentTurnAmount_20; }
	inline float* get_address_of_m_CurrentTurnAmount_20() { return &___m_CurrentTurnAmount_20; }
	inline void set_m_CurrentTurnAmount_20(float value)
	{
		___m_CurrentTurnAmount_20 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeedVelocityChange_21() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_TurnSpeedVelocityChange_21)); }
	inline float get_m_TurnSpeedVelocityChange_21() const { return ___m_TurnSpeedVelocityChange_21; }
	inline float* get_address_of_m_TurnSpeedVelocityChange_21() { return &___m_TurnSpeedVelocityChange_21; }
	inline void set_m_TurnSpeedVelocityChange_21(float value)
	{
		___m_TurnSpeedVelocityChange_21 = value;
	}

	inline static int32_t get_offset_of_m_RollUp_22() { return static_cast<int32_t>(offsetof(AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84, ___m_RollUp_22)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_RollUp_22() const { return ___m_RollUp_22; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_RollUp_22() { return &___m_RollUp_22; }
	inline void set_m_RollUp_22(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_RollUp_22 = value;
	}
};


// CnControls.EmptyGraphic
struct  EmptyGraphic_tEA7B1F28199F9AC58CB745C9706147B30E7A02A4  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:

public:
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// CnControls.DpadAxis[]
struct DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * m_Items[1];

public:
	inline DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * m_Items[1];

public:
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_mE6BF870B04922441F9F2760E782DEE6EE682615A_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);

// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (String_t* ___tag0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_SmoothDamp_m0B29D964FCB8460976BBE6BF56CBFDDC98EB5652 (float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE (float ___current0, float ___target1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B (float ___a0, float ___b1, float ___value2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upwards1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Slerp_mEDBE029B9D394258437E16D858F2C96D72A36B7B (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PivotBasedCameraRig__ctor_m3B61A8F501BD284A9B7C1EB30A5B585EBDBD904A (PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Sign(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB (float ___f0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_MoveTowards_mFB45EE30324E487925CA26EE6C001F0A3D257796 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::SmoothDamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// System.Void CnControls.CnInputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager__ctor_m4FF766F7ACC318C6B77AF8CF8EB7A8E60F87F6F8 (CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>::.ctor()
inline void Dictionary_2__ctor_m0531B1FE77B918C82A60F92F15467B240AC1CC8D (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>::.ctor()
inline void Dictionary_2__ctor_mA49EC9C8CBA40F297C88B5FAD6B88BF8AAD7B3A9 (Dictionary_2_t02D647864343068994385888E81A0778D3247896 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t02D647864343068994385888E81A0778D3247896 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6 (const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F (int32_t ___index0, const RuntimeMethod* method);
// System.Single CnControls.CnInputManager::GetAxis(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C (String_t* ___axisName0, bool ___isRaw1, const RuntimeMethod* method);
// System.Boolean CnControls.CnInputManager::AxisExists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_AxisExists_m0ED578BD6C0803B9FB25E5551BA91776583776C3 (String_t* ___axisName0, const RuntimeMethod* method);
// CnControls.CnInputManager CnControls.CnInputManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E (const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>::get_Item(!0)
inline List_1_tE5006B750AFF755696A7A223714798055FB561C7 * Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  List_1_tE5006B750AFF755696A7A223714798055FB561C7 * (*) (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.Single CnControls.CnInputManager::GetVirtualAxisValue(System.Collections.Generic.List`1<CnControls.VirtualAxis>,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetVirtualAxisValue_m3A649765E16DD2CBFF4C013525956B1F5EFACC9C (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * ___virtualAxisList0, String_t* ___axisName1, bool ___isRaw2, const RuntimeMethod* method);
// System.Boolean CnControls.CnInputManager::ButtonExists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64 (String_t* ___buttonName0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>::get_Item(!0)
inline List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749 (Dictionary_2_t02D647864343068994385888E81A0778D3247896 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * (*) (Dictionary_2_t02D647864343068994385888E81A0778D3247896 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButton(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B (String_t* ___axisName0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonDown(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButtonDown_m9A0048CA59A5964B864C20E17DFC2535A22C4138 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonUp(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButtonUp_m2A85153733F81918326833CDF305111DEB5D9990 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631 (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B (Dictionary_2_t02D647864343068994385888E81A0778D3247896 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t02D647864343068994385888E81A0778D3247896 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared)(__this, ___key0, method);
}
// System.String CnControls.VirtualAxis::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CnControls.VirtualAxis>::.ctor()
inline void List_1__ctor_mB7AB75528AD2F0CFC1A1DFCB24A168895566661F (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualAxis>>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m341F36867DC9FCF1CD5931753FF4BD4C9A516464 (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * __this, String_t* ___key0, List_1_tE5006B750AFF755696A7A223714798055FB561C7 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 *, String_t*, List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, const RuntimeMethod*))Dictionary_2_set_Item_mE6BF870B04922441F9F2760E782DEE6EE682615A_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.List`1<CnControls.VirtualAxis>::Add(!0)
inline void List_1_Add_m844F394B59F3D535F3493877B6D21EE1B04C943D (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * __this, VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<CnControls.VirtualAxis>::Remove(!0)
inline bool List_1_Remove_m84FC0E138EF115F91CC134E1DDD9C879DDFED9B4 (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * __this, VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String CnControls.VirtualButton::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CnControls.VirtualButton>::.ctor()
inline void List_1__ctor_mF8147BB302F6B4E2CFFB9AA338472C7789FDBFA8 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CnControls.VirtualButton>>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m116B6F471180B27F7AD111ACE39DEEF0559D7FF1 (Dictionary_2_t02D647864343068994385888E81A0778D3247896 * __this, String_t* ___key0, List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t02D647864343068994385888E81A0778D3247896 *, String_t*, List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, const RuntimeMethod*))Dictionary_2_set_Item_mE6BF870B04922441F9F2760E782DEE6EE682615A_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.List`1<CnControls.VirtualButton>::Add(!0)
inline void List_1_Add_m9C831679EEDF9F01998A14EA907429653D1BAE80 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * __this, VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<CnControls.VirtualButton>::Remove(!0)
inline bool List_1_Remove_m6AE3AC5DE2CDA479434C910C9B598E74A186F15D (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * __this, VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55 (float ___a0, float ___b1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<CnControls.VirtualAxis>::get_Item(System.Int32)
inline VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * List_1_get_Item_m72EB5FED66B0343A8FC5F20EFACA90F0AD39E92F_inline (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * (*) (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Single CnControls.VirtualAxis::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<CnControls.VirtualAxis>::get_Count()
inline int32_t List_1_get_Count_mAB7376FB6CC746A39504B77B71BA7572B16572EC_inline (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<CnControls.VirtualButton>::get_Item(System.Int32)
inline VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_inline (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * (*) (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Boolean CnControls.VirtualButton::get_GetButtonDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButtonDown_m1DAE11354C811678B39111E146200B23C6803E12 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<CnControls.VirtualButton>::get_Count()
inline int32_t List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_inline (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Boolean CnControls.VirtualButton::get_GetButtonUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButtonUp_mA53CCC28120C136AD5691AE8C98973DF460E6C86 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// System.Boolean CnControls.VirtualButton::get_GetButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButton_m5A784A059961193CE5B16ADB59B0D2C362DFBAF3 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.EventSystems.PointerEventData::get_pressEventCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * PointerEventData_get_pressEventCamera_m514C040A3C32E269345D0FC8B72BB2FE553FA448 (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method);
// UnityEngine.Camera CnControls.Dpad::get_CurrentEventCamera()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956_inline (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, const RuntimeMethod* method);
// System.Void CnControls.Dpad::set_CurrentEventCamera(UnityEngine.Camera)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379_inline (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method);
// UnityEngine.RectTransform CnControls.DpadAxis::get_RectTransform()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RectTransformUtility_RectangleContainsScreenPoint_m7D92A04D6DA6F4C7CC72439221C2EE46034A0595 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rect0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPoint1, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cam2, const RuntimeMethod* method);
// System.Int32 UnityEngine.EventSystems.PointerEventData::get_pointerId()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PointerEventData_get_pointerId_m50BE6AA34EE21DA6BE7AF07AAC9115CAB6B0636A_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method);
// System.Void CnControls.DpadAxis::Press(UnityEngine.Vector2,UnityEngine.Camera,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_Press_mD9BCDA57FCBADC57DDB5BB4A4439647EFBCBAD90 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPoint0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, int32_t ___pointerId2, const RuntimeMethod* method);
// System.Void CnControls.DpadAxis::TryRelease(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_TryRelease_mD494D94FAA2D11866BF18348679651CB57FDE322 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, int32_t ___pointerId0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void CnControls.DpadAxis::set_RectTransform(UnityEngine.RectTransform)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___value0, const RuntimeMethod* method);
// System.Void CnControls.VirtualAxis::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void CnControls.DpadAxis::set_LastFingerId(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void CnControls.CnInputManager::RegisterVirtualAxis(CnControls.VirtualAxis)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___virtualAxis0, const RuntimeMethod* method);
// System.Void CnControls.CnInputManager::UnregisterVirtualAxis(CnControls.VirtualAxis)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___virtualAxis0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Void CnControls.VirtualAxis::set_Value(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 CnControls.DpadAxis::get_LastFingerId()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_Clear_mBF3FB3CEA5153F8F72C74FFD6006A7AFF62C18BA (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Graphic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphic__ctor_m41CDFE33452C8382425A864410FB01D516C55D8F (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// System.Single CnControls.CnInputManager::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_back_mD521DF1A2C26E145578E07D618E1E4D08A1C6220 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5 (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Component_GetComponentInChildren_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mC2EEB227949FF6517A085ECC9E0FC1F8897A5546 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower__ctor_m729146B299728E3D434C1CEE3832FA6940994EA3 (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Copy._2D.PlatformerCharacter2D>()
inline PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * Component_GetComponent_TisPlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE_m07F4B2D7BABB71917777835CEADEA7C40DA07183 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_Move_mD91A6DDDA7DB352E4905785BFF0A7C75D800CD21 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, float ___move0, bool ___jump1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, String_t* ___n0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___mask0, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1* Physics2D_OverlapCircleAll_m1D3E8E59627D524F35EF6C67940DD1121CEE6E7C (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, float ___radius1, int32_t ___layerMask2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::GetBool(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_mB4754FC98ED65E5381854CDC858D12F0504FB3A2 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___force0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Component::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis0, float ___angle1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (float ___angle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lhs0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rhs1, const RuntimeMethod* method);
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Angle_m3BE44E43965BB9EDFD06DBC1E0985324A83327CF_inline (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3_set_Item_mF3E5D7FFAD5F81973283AE6C1D15C9B238AEE346 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnDrag_m315B6DA9C170A7106078F76A18F95C2398A94816 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54 (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// System.Void CnControls.SimpleJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick__ctor_mDDF1DAF26C35A567748C85CB911DD8B2A429311D (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method);
// System.Void CnControls.VirtualButton::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton__ctor_m33D71A415DA9EE486D63A4A6BF2F712F69EAC603 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void CnControls.CnInputManager::RegisterVirtualButton(CnControls.VirtualButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_RegisterVirtualButton_mE337F2AE2069EFC98A433950662B2AC92CB107F6 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___virtualButton0, const RuntimeMethod* method);
// System.Void CnControls.CnInputManager::UnregisterVirtualButton(CnControls.VirtualButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_UnregisterVirtualButton_mFE6F77977405E14E3E67716871D87C62A2D2FB7B (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___virtualButton0, const RuntimeMethod* method);
// System.Void CnControls.VirtualButton::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_Release_m5C7E5B88EC972BDCA47DDE42169C19B774CEBC68 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// System.Void CnControls.VirtualButton::Press()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_Press_m1944E262243474DF95B526DE21A08CAB0E39A2E9 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void CnControls.SimpleJoystick::Hide(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, bool ___isHidden0, const RuntimeMethod* method);
// UnityEngine.Camera CnControls.SimpleJoystick::get_CurrentEventCamera()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method);
// System.Void CnControls.SimpleJoystick::set_CurrentEventCamera(UnityEngine.Camera)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C_inline (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m76B10B1F8517DE72753AF7F61AE6E85722BF63E8 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rect0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPoint1, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cam2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___worldPoint3, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Division_m9E0ABD4CB731137B84249278B80D4C2624E58AC6_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Physics_get_gravity_m58D5D94276B1E7A04E9F7108EEAAB7AB786BA532 (const RuntimeMethod* method);
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60 (CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___motion0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_delta()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_delta_mCEECFB10CBB95E1C5FFD8A24B54A3989D926CA34_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Time::get_renderedFrameCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Time_get_renderedFrameCount_m97524F45A5996675DB60401A0211F700286D2B9A (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void CnControls.VirtualAxis::set_Name(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void CnControls.VirtualButton::set_Name(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean CnControls.VirtualButton::get_IsPressed()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method);
// System.Void CnControls.VirtualButton::set_IsPressed(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Time::get_frameCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Quaternion_Dot_m7F12C5843352AB2EA687923444CC987D51515F9A (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Quaternion::IsEqualUsingDot(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Quaternion_IsEqualUsingDot_mC57C44978B13AD1592750B1D523AAB4549BD5643 (float ___dot0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_Start_m3C2AFB5E22059519851F667C84F2B012A21195F7 (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_AutoTargetPlayer)
		bool L_0 = __this->get_m_AutoTargetPlayer_5();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// FindAndTargetPlayer();
		AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// if (m_Target == null) return;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_m_Target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		// if (m_Target == null) return;
		return;
	}

IL_001d:
	{
		// targetRigidbody = m_Target.GetComponent<Rigidbody>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_m_Target_4();
		NullCheck(L_3);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4;
		L_4 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(L_3, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		__this->set_targetRigidbody_7(L_4);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_FixedUpdate_m31CDC56D90DE22CC9B91B9D6DB8BD4E45043DEFE (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
		bool L_0 = __this->get_m_AutoTargetPlayer_5();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_m_Target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_m_Target_4();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002e;
		}
	}

IL_0028:
	{
		// FindAndTargetPlayer();
		AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// if (m_UpdateType == UpdateType.FixedUpdate)
		int32_t L_6 = __this->get_m_UpdateType_6();
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		// FollowTarget(Time.deltaTime);
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_0041:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_LateUpdate_m3B6E61E56F64E3782FA5C105D6176E547895BC4E (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
		bool L_0 = __this->get_m_AutoTargetPlayer_5();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_m_Target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_m_Target_4();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002e;
		}
	}

IL_0028:
	{
		// FindAndTargetPlayer();
		AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// if (m_UpdateType == UpdateType.LateUpdate)
		int32_t L_6 = __this->get_m_UpdateType_6();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0042;
		}
	}
	{
		// FollowTarget(Time.deltaTime);
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_ManualUpdate_m27219E307B25A44F405FD7C092527B2605C72BB5 (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
		bool L_0 = __this->get_m_AutoTargetPlayer_5();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_m_Target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_m_Target_4();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002e;
		}
	}

IL_0028:
	{
		// FindAndTargetPlayer();
		AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// if (m_UpdateType == UpdateType.ManualUpdate)
		int32_t L_6 = __this->get_m_UpdateType_6();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0042;
		}
	}
	{
		// FollowTarget(Time.deltaTime);
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_FindAndTargetPlayer_m9C83F6DFCF551CD445C9BEC8FD471F45E656F5DB (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		// var targetObj = GameObject.FindGameObjectWithTag("Player");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		V_0 = L_0;
		// if (targetObj)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// SetTarget(targetObj.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = V_0;
		NullCheck(L_3);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(6 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform) */, __this, L_4);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower_SetTarget_m09075B1BF3B7EADF99BC4E338A43C4E7781C7E82 (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___newTransform0, const RuntimeMethod* method)
{
	{
		// m_Target = newTransform;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___newTransform0;
		__this->set_m_Target_4(L_0);
		// }
		return;
	}
}
// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * AbstractTargetFollower_get_Target_mC4B781CBCC331D9CB9B6411D1E1B59AD9455BEDB (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Target; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_m_Target_4();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractTargetFollower__ctor_m729146B299728E3D434C1CEE3832FA6940994EA3 (AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private bool m_AutoTargetPlayer = true;  // Whether the rig should automatically target the player.
		__this->set_m_AutoTargetPlayer_5((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoCam_FollowTarget_mEFC2AFFB812961D49CE8EA44ED5711F55EE41F63 (AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84 * __this, float ___deltaTime0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float G_B13_0 = 0.0f;
	AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84 * G_B23_0 = NULL;
	AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84 * G_B22_0 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B24_0;
	memset((&G_B24_0), 0, sizeof(G_B24_0));
	AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84 * G_B24_1 = NULL;
	{
		// if (!(deltaTime > 0) || m_Target == null)
		float L_0 = ___deltaTime0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_m_Target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		// return;
		return;
	}

IL_0017:
	{
		// var targetForward = m_Target.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_m_Target_4();
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// var targetUp = m_Target.up;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_m_Target_4();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// if (m_FollowVelocity && Application.isPlaying)
		bool L_7 = __this->get_m_FollowVelocity_14();
		if (!L_7)
		{
			goto IL_00a2;
		}
	}
	{
		bool L_8;
		L_8 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a2;
		}
	}
	{
		// if (targetRigidbody.velocity.magnitude > m_TargetVelocityLowerLimit)
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_9 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_targetRigidbody_7();
		NullCheck(L_9);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11;
		L_11 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		float L_12 = __this->get_m_TargetVelocityLowerLimit_17();
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		// targetForward = targetRigidbody.velocity.normalized;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_13 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_targetRigidbody_7();
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		V_0 = L_15;
		// targetUp = Vector3.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		V_1 = L_16;
		// }
		goto IL_007b;
	}

IL_0075:
	{
		// targetUp = Vector3.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_007b:
	{
		// m_CurrentTurnAmount = Mathf.SmoothDamp(m_CurrentTurnAmount, 1, ref m_TurnSpeedVelocityChange, m_SmoothTurnTime);
		float L_18 = __this->get_m_CurrentTurnAmount_20();
		float* L_19 = __this->get_address_of_m_TurnSpeedVelocityChange_21();
		float L_20 = __this->get_m_SmoothTurnTime_18();
		float L_21;
		L_21 = Mathf_SmoothDamp_m0B29D964FCB8460976BBE6BF56CBFDDC98EB5652(L_18, (1.0f), (float*)L_19, L_20, /*hidden argument*/NULL);
		__this->set_m_CurrentTurnAmount_20(L_21);
		// }
		goto IL_0152;
	}

IL_00a2:
	{
		// var currentFlatAngle = Mathf.Atan2(targetForward.x, targetForward.z)*Mathf.Rad2Deg;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = V_0;
		float L_23 = L_22.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		float L_25 = L_24.get_z_4();
		float L_26;
		L_26 = atan2f(L_23, L_25);
		V_4 = ((float)il2cpp_codegen_multiply((float)L_26, (float)(57.2957802f)));
		// if (m_SpinTurnLimit > 0)
		float L_27 = __this->get_m_SpinTurnLimit_16();
		if ((!(((float)L_27) > ((float)(0.0f)))))
		{
			goto IL_013f;
		}
	}
	{
		// var targetSpinSpeed = Mathf.Abs(Mathf.DeltaAngle(m_LastFlatAngle, currentFlatAngle))/deltaTime;
		float L_28 = __this->get_m_LastFlatAngle_19();
		float L_29 = V_4;
		float L_30;
		L_30 = Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE(L_28, L_29, /*hidden argument*/NULL);
		float L_31;
		L_31 = fabsf(L_30);
		float L_32 = ___deltaTime0;
		V_5 = ((float)((float)L_31/(float)L_32));
		// var desiredTurnAmount = Mathf.InverseLerp(m_SpinTurnLimit, m_SpinTurnLimit*0.75f, targetSpinSpeed);
		float L_33 = __this->get_m_SpinTurnLimit_16();
		float L_34 = __this->get_m_SpinTurnLimit_16();
		float L_35 = V_5;
		float L_36;
		L_36 = Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B(L_33, ((float)il2cpp_codegen_multiply((float)L_34, (float)(0.75f))), L_35, /*hidden argument*/NULL);
		V_6 = L_36;
		// var turnReactSpeed = (m_CurrentTurnAmount > desiredTurnAmount ? .1f : 1f);
		float L_37 = __this->get_m_CurrentTurnAmount_20();
		float L_38 = V_6;
		if ((((float)L_37) > ((float)L_38)))
		{
			goto IL_010a;
		}
	}
	{
		G_B13_0 = (1.0f);
		goto IL_010f;
	}

IL_010a:
	{
		G_B13_0 = (0.100000001f);
	}

IL_010f:
	{
		V_7 = G_B13_0;
		// if (Application.isPlaying)
		bool L_39;
		L_39 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0135;
		}
	}
	{
		// m_CurrentTurnAmount = Mathf.SmoothDamp(m_CurrentTurnAmount, desiredTurnAmount,
		//                                      ref m_TurnSpeedVelocityChange, turnReactSpeed);
		float L_40 = __this->get_m_CurrentTurnAmount_20();
		float L_41 = V_6;
		float* L_42 = __this->get_address_of_m_TurnSpeedVelocityChange_21();
		float L_43 = V_7;
		float L_44;
		L_44 = Mathf_SmoothDamp_m0B29D964FCB8460976BBE6BF56CBFDDC98EB5652(L_40, L_41, (float*)L_42, L_43, /*hidden argument*/NULL);
		__this->set_m_CurrentTurnAmount_20(L_44);
		// }
		goto IL_014a;
	}

IL_0135:
	{
		// m_CurrentTurnAmount = desiredTurnAmount;
		float L_45 = V_6;
		__this->set_m_CurrentTurnAmount_20(L_45);
		// }
		goto IL_014a;
	}

IL_013f:
	{
		// m_CurrentTurnAmount = 1;
		__this->set_m_CurrentTurnAmount_20((1.0f));
	}

IL_014a:
	{
		// m_LastFlatAngle = currentFlatAngle;
		float L_46 = V_4;
		__this->set_m_LastFlatAngle_19(L_46);
	}

IL_0152:
	{
		// transform.position = Vector3.Lerp(transform.position, m_Target.position, deltaTime*m_MoveSpeed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47;
		L_47 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_48;
		L_48 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		L_49 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_48, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_50 = ((AbstractTargetFollower_t66C047C6C2A2F81EA5FADD6B22C2F4F5874E852F *)__this)->get_m_Target_4();
		NullCheck(L_50);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_50, /*hidden argument*/NULL);
		float L_52 = ___deltaTime0;
		float L_53 = __this->get_m_MoveSpeed_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54;
		L_54 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_49, L_51, ((float)il2cpp_codegen_multiply((float)L_52, (float)L_53)), /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_47, L_54, /*hidden argument*/NULL);
		// if (!m_FollowTilt)
		bool L_55 = __this->get_m_FollowTilt_15();
		if (L_55)
		{
			goto IL_01ae;
		}
	}
	{
		// targetForward.y = 0;
		(&V_0)->set_y_3((0.0f));
		// if (targetForward.sqrMagnitude < float.Epsilon)
		float L_56;
		L_56 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_56) < ((float)(1.40129846E-45f)))))
		{
			goto IL_01ae;
		}
	}
	{
		// targetForward = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_57;
		L_57 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		L_58 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_57, /*hidden argument*/NULL);
		V_0 = L_58;
	}

IL_01ae:
	{
		// var rollRotation = Quaternion.LookRotation(targetForward, m_RollUp);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60 = __this->get_m_RollUp_22();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_61;
		L_61 = Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1(L_59, L_60, /*hidden argument*/NULL);
		V_2 = L_61;
		// m_RollUp = m_RollSpeed > 0 ? Vector3.Slerp(m_RollUp, targetUp, m_RollSpeed*deltaTime) : Vector3.up;
		float L_62 = __this->get_m_RollSpeed_13();
		G_B22_0 = __this;
		if ((((float)L_62) > ((float)(0.0f))))
		{
			G_B23_0 = __this;
			goto IL_01d0;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63;
		L_63 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		G_B24_0 = L_63;
		G_B24_1 = G_B22_0;
		goto IL_01e4;
	}

IL_01d0:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_64 = __this->get_m_RollUp_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65 = V_1;
		float L_66 = __this->get_m_RollSpeed_13();
		float L_67 = ___deltaTime0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68;
		L_68 = Vector3_Slerp_mEDBE029B9D394258437E16D858F2C96D72A36B7B(L_64, L_65, ((float)il2cpp_codegen_multiply((float)L_66, (float)L_67)), /*hidden argument*/NULL);
		G_B24_0 = L_68;
		G_B24_1 = G_B23_0;
	}

IL_01e4:
	{
		NullCheck(G_B24_1);
		G_B24_1->set_m_RollUp_22(G_B24_0);
		// transform.rotation = Quaternion.Lerp(transform.rotation, rollRotation, m_TurnSpeed*m_CurrentTurnAmount*deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_69;
		L_69 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_70;
		L_70 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_71;
		L_71 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_70, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_72 = V_2;
		float L_73 = __this->get_m_TurnSpeed_12();
		float L_74 = __this->get_m_CurrentTurnAmount_20();
		float L_75 = ___deltaTime0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_76;
		L_76 = Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261(L_71, L_72, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_73, (float)L_74)), (float)L_75)), /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_69, L_76, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoCam__ctor_m573B4C3091E5E299C636B0AA28EB9C2BB3F9CF53 (AutoCam_t9540F629CAAA6D42F1297A5B0B73411CCE4ABC84 * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private float m_MoveSpeed = 3; // How fast the rig will move to keep up with target's position
		__this->set_m_MoveSpeed_11((3.0f));
		// [SerializeField] private float m_TurnSpeed = 1; // How fast the rig will turn to keep up with target's rotation
		__this->set_m_TurnSpeed_12((1.0f));
		// [SerializeField] private float m_RollSpeed = 0.2f;// How fast the rig will roll (around Z axis) to match target's roll.
		__this->set_m_RollSpeed_13((0.200000003f));
		// [SerializeField] private bool m_FollowTilt = true; // Whether the rig will tilt (around X axis) with the target.
		__this->set_m_FollowTilt_15((bool)1);
		// [SerializeField] private float m_SpinTurnLimit = 90;// The threshold beyond which the camera stops following the target's rotation. (used in situations where a car spins out, for example)
		__this->set_m_SpinTurnLimit_16((90.0f));
		// [SerializeField] private float m_TargetVelocityLowerLimit = 4f;// the minimum velocity above which the camera turns towards the object's velocity. Below this we use the object's forward direction.
		__this->set_m_TargetVelocityLowerLimit_17((4.0f));
		// [SerializeField] private float m_SmoothTurnTime = 0.2f; // the smoothing for the camera's rotation
		__this->set_m_SmoothTurnTime_18((0.200000003f));
		// private Vector3 m_RollUp = Vector3.up;// The roll of the camera around the z axis ( generally this will always just be up )
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		__this->set_m_RollUp_22(L_0);
		PivotBasedCameraRig__ctor_m3B61A8F501BD284A9B7C1EB30A5B585EBDBD904A(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera2DFollow_Start_m3227D33A9D35130293D1617A8D42EFCD9A2C3B0D (Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745 * __this, const RuntimeMethod* method)
{
	{
		// m_LastTargetPosition = target.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_target_4();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		__this->set_m_LastTargetPosition_10(L_1);
		// m_OffsetZ = (transform.position - target.position).z;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_target_4();
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_3, L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_z_4();
		__this->set_m_OffsetZ_9(L_7);
		// transform.parent = null;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_8, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera2DFollow_Update_m5CD7C2B523C3B074DFCA9FD9963439BEA5695D66 (Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float xMoveDelta = (target.position - m_LastTargetPosition).x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_target_4();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = __this->get_m_LastTargetPosition_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		V_0 = L_4;
		// bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
		float L_5 = V_0;
		float L_6;
		L_6 = fabsf(L_5);
		float L_7 = __this->get_lookAheadMoveThreshold_8();
		// if (updateLookAheadTarget)
		if (!((((float)L_6) > ((float)L_7))? 1 : 0))
		{
			goto IL_004f;
		}
	}
	{
		// m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
		float L_8 = __this->get_lookAheadFactor_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_8, L_9, /*hidden argument*/NULL);
		float L_11 = V_0;
		float L_12;
		L_12 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_11, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_10, L_12, /*hidden argument*/NULL);
		__this->set_m_LookAheadPos_12(L_13);
		// }
		goto IL_0071;
	}

IL_004f:
	{
		// m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = __this->get_m_LookAheadPos_12();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		float L_16;
		L_16 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_17 = __this->get_lookAheadReturnSpeed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_MoveTowards_mFB45EE30324E487925CA26EE6C001F0A3D257796(L_14, L_15, ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), /*hidden argument*/NULL);
		__this->set_m_LookAheadPos_12(L_18);
	}

IL_0071:
	{
		// Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19 = __this->get_target_4();
		NullCheck(L_19);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = __this->get_m_LookAheadPos_12();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_20, L_21, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		float L_24 = __this->get_m_OffsetZ_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_23, L_24, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_22, L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		// Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_30 = __this->get_address_of_m_CurrentVelocity_11();
		float L_31 = __this->get_damping_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627(L_28, L_29, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_30, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		// transform.position = newPos;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33;
		L_33 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34 = V_2;
		NullCheck(L_33);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_33, L_34, /*hidden argument*/NULL);
		// m_LastTargetPosition = target.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_35 = __this->get_target_4();
		NullCheck(L_35);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_35, /*hidden argument*/NULL);
		__this->set_m_LastTargetPosition_10(L_36);
		// }
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera2DFollow__ctor_m8A7C93E24F2CD212F136FCBDD3C32D5645E00CBE (Camera2DFollow_t72C29DC4980432640AE1EBB825AB9243F4E01745 * __this, const RuntimeMethod* method)
{
	{
		// public float damping = 1;
		__this->set_damping_5((1.0f));
		// public float lookAheadFactor = 3;
		__this->set_lookAheadFactor_6((3.0f));
		// public float lookAheadReturnSpeed = 0.5f;
		__this->set_lookAheadReturnSpeed_7((0.5f));
		// public float lookAheadMoveThreshold = 0.1f;
		__this->set_lookAheadMoveThreshold_8((0.100000001f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CnControls.CnInputManager CnControls.CnInputManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * G_B2_0 = NULL;
	CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * G_B1_0 = NULL;
	{
		// get { return _instance ?? (_instance = new CnInputManager()); }
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0 = ((CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_StaticFields*)il2cpp_codegen_static_fields_for(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_il2cpp_TypeInfo_var))->get__instance_0();
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0014;
		}
	}
	{
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_2 = (CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A *)il2cpp_codegen_object_new(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_il2cpp_TypeInfo_var);
		CnInputManager__ctor_m4FF766F7ACC318C6B77AF8CF8EB7A8E60F87F6F8(L_2, /*hidden argument*/NULL);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_3 = L_2;
		((CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_StaticFields*)il2cpp_codegen_static_fields_for(CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A_il2cpp_TypeInfo_var))->set__instance_0(L_3);
		G_B2_0 = L_3;
	}

IL_0014:
	{
		return G_B2_0;
	}
}
// System.Void CnControls.CnInputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager__ctor_m4FF766F7ACC318C6B77AF8CF8EB7A8E60F87F6F8 (CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m0531B1FE77B918C82A60F92F15467B240AC1CC8D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA49EC9C8CBA40F297C88B5FAD6B88BF8AAD7B3A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t02D647864343068994385888E81A0778D3247896_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Dictionary<string, List<VirtualAxis>> _virtualAxisDictionary =
		//     new Dictionary<string, List<VirtualAxis>>();
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_0 = (Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 *)il2cpp_codegen_object_new(Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m0531B1FE77B918C82A60F92F15467B240AC1CC8D(L_0, /*hidden argument*/Dictionary_2__ctor_m0531B1FE77B918C82A60F92F15467B240AC1CC8D_RuntimeMethod_var);
		__this->set__virtualAxisDictionary_1(L_0);
		// private Dictionary<string, List<VirtualButton>> _virtualButtonsDictionary =
		//     new Dictionary<string, List<VirtualButton>>();
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_1 = (Dictionary_2_t02D647864343068994385888E81A0778D3247896 *)il2cpp_codegen_object_new(Dictionary_2_t02D647864343068994385888E81A0778D3247896_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mA49EC9C8CBA40F297C88B5FAD6B88BF8AAD7B3A9(L_1, /*hidden argument*/Dictionary_2__ctor_mA49EC9C8CBA40F297C88B5FAD6B88BF8AAD7B3A9_RuntimeMethod_var);
		__this->set__virtualButtonsDictionary_2(L_1);
		// private CnInputManager() { }
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// private CnInputManager() { }
		return;
	}
}
// System.Int32 CnControls.CnInputManager::get_TouchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CnInputManager_get_TouchCount_mA59758D8D293E1F02ECF559A596A7040CC0E1782 (const RuntimeMethod* method)
{
	{
		// return Input.touchCount;
		int32_t L_0;
		L_0 = Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6(/*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Touch CnControls.CnInputManager::GetTouch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  CnInputManager_GetTouch_m8B17A871490DAC8716F88916E9B513BF18D67842 (int32_t ___touchIndex0, const RuntimeMethod* method)
{
	{
		// return Input.GetTouch(touchIndex);
		int32_t L_0 = ___touchIndex0;
		Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  L_1;
		L_1 = Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B (String_t* ___axisName0, const RuntimeMethod* method)
{
	{
		// return GetAxis(axisName, false);
		String_t* L_0 = ___axisName0;
		float L_1;
		L_1 = CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C(L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxisRaw(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetAxisRaw_mC341ECB544726B2F36DA83F51F718A2E93380C2E (String_t* ___axisName0, const RuntimeMethod* method)
{
	{
		// return GetAxis(axisName, true);
		String_t* L_0 = ___axisName0;
		float L_1;
		L_1 = CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C(L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxis(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetAxis_m81C94D95D32ADCEFE015C0874222FC9B2175689C (String_t* ___axisName0, bool ___isRaw1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (AxisExists(axisName))
		String_t* L_0 = ___axisName0;
		bool L_1;
		L_1 = CnInputManager_AxisExists_m0ED578BD6C0803B9FB25E5551BA91776583776C3(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		// return GetVirtualAxisValue(Instance._virtualAxisDictionary[axisName], axisName, isRaw);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_2;
		L_2 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_2);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_3 = L_2->get__virtualAxisDictionary_1();
		String_t* L_4 = ___axisName0;
		NullCheck(L_3);
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_5;
		L_5 = Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		String_t* L_6 = ___axisName0;
		bool L_7 = ___isRaw1;
		float L_8;
		L_8 = CnInputManager_GetVirtualAxisValue_m3A649765E16DD2CBFF4C013525956B1F5EFACC9C(L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0020:
	{
		// if (ButtonExists(axisName))
		String_t* L_9 = ___axisName0;
		bool L_10;
		L_10 = CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0045;
		}
	}
	{
		// var anyButtonIsPressed = GetAnyVirtualButton(Instance._virtualButtonsDictionary[axisName]);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_11;
		L_11 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_12 = L_11->get__virtualButtonsDictionary_2();
		String_t* L_13 = ___axisName0;
		NullCheck(L_12);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_14;
		L_14 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_12, L_13, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		bool L_15;
		L_15 = CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F(L_14, /*hidden argument*/NULL);
		// if (anyButtonIsPressed)
		if (!L_15)
		{
			goto IL_0045;
		}
	}
	{
		// return 1f;
		return (1.0f);
	}

IL_0045:
	{
		// return isRaw ? Input.GetAxisRaw(axisName) : Input.GetAxis(axisName);
		bool L_16 = ___isRaw1;
		if (L_16)
		{
			goto IL_004f;
		}
	}
	{
		String_t* L_17 = ___axisName0;
		float L_18;
		L_18 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_004f:
	{
		String_t* L_19 = ___axisName0;
		float L_20;
		L_20 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean CnControls.CnInputManager::GetButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetButton_mC823D5A0CC24751793A0CAD17C97AA35A05490F4 (String_t* ___buttonName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var standardInputButtonState = Input.GetButton(buttonName);
		String_t* L_0 = ___buttonName0;
		bool L_1;
		L_1 = Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C(L_0, /*hidden argument*/NULL);
		// if (standardInputButtonState == true) return true;
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		// if (standardInputButtonState == true) return true;
		return (bool)1;
	}

IL_000a:
	{
		// if (ButtonExists(buttonName))
		String_t* L_2 = ___buttonName0;
		bool L_3;
		L_3 = CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		// return GetAnyVirtualButton(Instance._virtualButtonsDictionary[buttonName]);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_4;
		L_4 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_5 = L_4->get__virtualButtonsDictionary_2();
		String_t* L_6 = ___buttonName0;
		NullCheck(L_5);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_7;
		L_7 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		bool L_8;
		L_8 = CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0028:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetButtonDown_m465EE8E01A82D8BA2D0AED4017B26A7991BF9DF7 (String_t* ___buttonName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var standardInputButtonState = Input.GetButtonDown(buttonName);
		String_t* L_0 = ___buttonName0;
		bool L_1;
		L_1 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(L_0, /*hidden argument*/NULL);
		// if (standardInputButtonState == true) return true;
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		// if (standardInputButtonState == true) return true;
		return (bool)1;
	}

IL_000a:
	{
		// if (ButtonExists(buttonName))
		String_t* L_2 = ___buttonName0;
		bool L_3;
		L_3 = CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		// return GetAnyVirtualButtonDown(Instance._virtualButtonsDictionary[buttonName]);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_4;
		L_4 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_5 = L_4->get__virtualButtonsDictionary_2();
		String_t* L_6 = ___buttonName0;
		NullCheck(L_5);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_7;
		L_7 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		bool L_8;
		L_8 = CnInputManager_GetAnyVirtualButtonDown_m9A0048CA59A5964B864C20E17DFC2535A22C4138(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0028:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetButtonUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetButtonUp_m8809729C18402C8F7B142F6C255FDCE42659543E (String_t* ___buttonName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var standardInputButtonState = Input.GetButtonUp(buttonName);
		String_t* L_0 = ___buttonName0;
		bool L_1;
		L_1 = Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD(L_0, /*hidden argument*/NULL);
		// if (standardInputButtonState == true) return true;
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		// if (standardInputButtonState == true) return true;
		return (bool)1;
	}

IL_000a:
	{
		// if (ButtonExists(buttonName))
		String_t* L_2 = ___buttonName0;
		bool L_3;
		L_3 = CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		// return GetAnyVirtualButtonUp(Instance._virtualButtonsDictionary[buttonName]);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_4;
		L_4 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_5 = L_4->get__virtualButtonsDictionary_2();
		String_t* L_6 = ___buttonName0;
		NullCheck(L_5);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_7;
		L_7 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		bool L_8;
		L_8 = CnInputManager_GetAnyVirtualButtonUp_m2A85153733F81918326833CDF305111DEB5D9990(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0028:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::AxisExists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_AxisExists_m0ED578BD6C0803B9FB25E5551BA91776583776C3 (String_t* ___axisName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Instance._virtualAxisDictionary.ContainsKey(axisName);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_1 = L_0->get__virtualAxisDictionary_1();
		String_t* L_2 = ___axisName0;
		NullCheck(L_1);
		bool L_3;
		L_3 = Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		return L_3;
	}
}
// System.Boolean CnControls.CnInputManager::ButtonExists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_ButtonExists_m48710C0AA46284999989B6600CFEC6729A8E6B64 (String_t* ___buttonName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Instance._virtualButtonsDictionary.ContainsKey(buttonName);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_1 = L_0->get__virtualButtonsDictionary_2();
		String_t* L_2 = ___buttonName0;
		NullCheck(L_1);
		bool L_3;
		L_3 = Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		return L_3;
	}
}
// System.Void CnControls.CnInputManager::RegisterVirtualAxis(CnControls.VirtualAxis)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___virtualAxis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m341F36867DC9FCF1CD5931753FF4BD4C9A516464_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m844F394B59F3D535F3493877B6D21EE1B04C943D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB7AB75528AD2F0CFC1A1DFCB24A168895566661F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tE5006B750AFF755696A7A223714798055FB561C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Instance._virtualAxisDictionary.ContainsKey(virtualAxis.Name))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_1 = L_0->get__virtualAxisDictionary_1();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_2 = ___virtualAxis0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		// Instance._virtualAxisDictionary[virtualAxis.Name] = new List<VirtualAxis>();
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_5;
		L_5 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_6 = L_5->get__virtualAxisDictionary_1();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_7 = ___virtualAxis0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_7, /*hidden argument*/NULL);
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_9 = (List_1_tE5006B750AFF755696A7A223714798055FB561C7 *)il2cpp_codegen_object_new(List_1_tE5006B750AFF755696A7A223714798055FB561C7_il2cpp_TypeInfo_var);
		List_1__ctor_mB7AB75528AD2F0CFC1A1DFCB24A168895566661F(L_9, /*hidden argument*/List_1__ctor_mB7AB75528AD2F0CFC1A1DFCB24A168895566661F_RuntimeMethod_var);
		NullCheck(L_6);
		Dictionary_2_set_Item_m341F36867DC9FCF1CD5931753FF4BD4C9A516464(L_6, L_8, L_9, /*hidden argument*/Dictionary_2_set_Item_m341F36867DC9FCF1CD5931753FF4BD4C9A516464_RuntimeMethod_var);
	}

IL_0031:
	{
		// Instance._virtualAxisDictionary[virtualAxis.Name].Add(virtualAxis);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_10;
		L_10 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_11 = L_10->get__virtualAxisDictionary_1();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_12 = ___virtualAxis0;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_14;
		L_14 = Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D(L_11, L_13, /*hidden argument*/Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_15 = ___virtualAxis0;
		NullCheck(L_14);
		List_1_Add_m844F394B59F3D535F3493877B6D21EE1B04C943D(L_14, L_15, /*hidden argument*/List_1_Add_m844F394B59F3D535F3493877B6D21EE1B04C943D_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void CnControls.CnInputManager::UnregisterVirtualAxis(CnControls.VirtualAxis)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * ___virtualAxis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m84FC0E138EF115F91CC134E1DDD9C879DDFED9B4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral41C37D6140239C1EEC97F656CE8CF405635536E8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4FF6F93CCEBCA7FFB7AD642F8D2A1568D2339BE5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6976943C2BFBCFE85CB5EBBE8F087A90B277059C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral96F1E4C380B7193666F533294DF99B093CF11038);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance._virtualAxisDictionary.ContainsKey(virtualAxis.Name))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_1 = L_0->get__virtualAxisDictionary_1();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_2 = ___virtualAxis0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m4EF5C43E3C425961766A4CB7D7377C6F9910C631_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		// if (!Instance._virtualAxisDictionary[virtualAxis.Name].Remove(virtualAxis))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_5;
		L_5 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_tCD4E65DAB408C1683AE5E042DA8C0E5FE3A41948 * L_6 = L_5->get__virtualAxisDictionary_1();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_7 = ___virtualAxis0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_9;
		L_9 = Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m903A99F0A0CF4A1D0FDD1EBE246EAE8AE4D1C73D_RuntimeMethod_var);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_10 = ___virtualAxis0;
		NullCheck(L_9);
		bool L_11;
		L_11 = List_1_Remove_m84FC0E138EF115F91CC134E1DDD9C879DDFED9B4(L_9, L_10, /*hidden argument*/List_1_Remove_m84FC0E138EF115F91CC134E1DDD9C879DDFED9B4_RuntimeMethod_var);
		if (L_11)
		{
			goto IL_0069;
		}
	}
	{
		// Debug.LogError("Requested axis " + virtualAxis.Name + " exists, but there's no such virtual axis that you're trying to unregister");
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_12 = ___virtualAxis0;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_12, /*hidden argument*/NULL);
		String_t* L_14;
		L_14 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral41C37D6140239C1EEC97F656CE8CF405635536E8, L_13, _stringLiteral4FF6F93CCEBCA7FFB7AD642F8D2A1568D2339BE5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_14, /*hidden argument*/NULL);
		// }
		return;
	}

IL_004f:
	{
		// Debug.LogError("Trying to unregister an axis " + virtualAxis.Name + " that was never registered");
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_15 = ___virtualAxis0;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline(L_15, /*hidden argument*/NULL);
		String_t* L_17;
		L_17 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral96F1E4C380B7193666F533294DF99B093CF11038, L_16, _stringLiteral6976943C2BFBCFE85CB5EBBE8F087A90B277059C, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_17, /*hidden argument*/NULL);
	}

IL_0069:
	{
		// }
		return;
	}
}
// System.Void CnControls.CnInputManager::RegisterVirtualButton(CnControls.VirtualButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_RegisterVirtualButton_mE337F2AE2069EFC98A433950662B2AC92CB107F6 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___virtualButton0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m116B6F471180B27F7AD111ACE39DEEF0559D7FF1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m9C831679EEDF9F01998A14EA907429653D1BAE80_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8147BB302F6B4E2CFFB9AA338472C7789FDBFA8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Instance._virtualButtonsDictionary.ContainsKey(virtualButton.Name))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_1 = L_0->get__virtualButtonsDictionary_2();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_2 = ___virtualButton0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		// Instance._virtualButtonsDictionary[virtualButton.Name] = new List<VirtualButton>();
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_5;
		L_5 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_6 = L_5->get__virtualButtonsDictionary_2();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_7 = ___virtualButton0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline(L_7, /*hidden argument*/NULL);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_9 = (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 *)il2cpp_codegen_object_new(List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59_il2cpp_TypeInfo_var);
		List_1__ctor_mF8147BB302F6B4E2CFFB9AA338472C7789FDBFA8(L_9, /*hidden argument*/List_1__ctor_mF8147BB302F6B4E2CFFB9AA338472C7789FDBFA8_RuntimeMethod_var);
		NullCheck(L_6);
		Dictionary_2_set_Item_m116B6F471180B27F7AD111ACE39DEEF0559D7FF1(L_6, L_8, L_9, /*hidden argument*/Dictionary_2_set_Item_m116B6F471180B27F7AD111ACE39DEEF0559D7FF1_RuntimeMethod_var);
	}

IL_0031:
	{
		// Instance._virtualButtonsDictionary[virtualButton.Name].Add(virtualButton);
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_10;
		L_10 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_11 = L_10->get__virtualButtonsDictionary_2();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_12 = ___virtualButton0;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_14;
		L_14 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_11, L_13, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_15 = ___virtualButton0;
		NullCheck(L_14);
		List_1_Add_m9C831679EEDF9F01998A14EA907429653D1BAE80(L_14, L_15, /*hidden argument*/List_1_Add_m9C831679EEDF9F01998A14EA907429653D1BAE80_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void CnControls.CnInputManager::UnregisterVirtualButton(CnControls.VirtualButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CnInputManager_UnregisterVirtualButton_mFE6F77977405E14E3E67716871D87C62A2D2FB7B (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * ___virtualButton0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m6AE3AC5DE2CDA479434C910C9B598E74A186F15D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral65E0026258A51B468CEA87545210A330EECBA556);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1CC7ED8198324B9A3ACFCF175E153FC25D05999);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance._virtualButtonsDictionary.ContainsKey(virtualButton.Name))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_0;
		L_0 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_1 = L_0->get__virtualButtonsDictionary_2();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_2 = ___virtualButton0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m542778948B1F0A60900B73B821D3C0E277F8466B_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		// if (!Instance._virtualButtonsDictionary[virtualButton.Name].Remove(virtualButton))
		CnInputManager_t3749A5383D4AF3097BD5988D3278B0413FE5906A * L_5;
		L_5 = CnInputManager_get_Instance_mF9CD6378E60E082C63B9C7CE495144B502A4F48E(/*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t02D647864343068994385888E81A0778D3247896 * L_6 = L_5->get__virtualButtonsDictionary_2();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_7 = ___virtualButton0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_9;
		L_9 = Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_mD784A284BAEAAFADB1C3808E433DADD8A7BD2749_RuntimeMethod_var);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_10 = ___virtualButton0;
		NullCheck(L_9);
		bool L_11;
		L_11 = List_1_Remove_m6AE3AC5DE2CDA479434C910C9B598E74A186F15D(L_9, L_10, /*hidden argument*/List_1_Remove_m6AE3AC5DE2CDA479434C910C9B598E74A186F15D_RuntimeMethod_var);
		if (L_11)
		{
			goto IL_0049;
		}
	}
	{
		// Debug.LogError("Requested button axis exists, but there's no such virtual button that you're trying to unregister");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteralB1CC7ED8198324B9A3ACFCF175E153FC25D05999, /*hidden argument*/NULL);
		// }
		return;
	}

IL_003f:
	{
		// Debug.LogError("Trying to unregister a button that was never registered");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral65E0026258A51B468CEA87545210A330EECBA556, /*hidden argument*/NULL);
	}

IL_0049:
	{
		// }
		return;
	}
}
// System.Single CnControls.CnInputManager::GetVirtualAxisValue(System.Collections.Generic.List`1<CnControls.VirtualAxis>,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CnInputManager_GetVirtualAxisValue_m3A649765E16DD2CBFF4C013525956B1F5EFACC9C (List_1_tE5006B750AFF755696A7A223714798055FB561C7 * ___virtualAxisList0, String_t* ___axisName1, bool ___isRaw2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mAB7376FB6CC746A39504B77B71BA7572B16572EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m72EB5FED66B0343A8FC5F20EFACA90F0AD39E92F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		// float axisValue = isRaw ? Input.GetAxisRaw(axisName) : Input.GetAxis(axisName);
		bool L_0 = ___isRaw2;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		String_t* L_1 = ___axisName1;
		float L_2;
		L_2 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0011;
	}

IL_000b:
	{
		String_t* L_3 = ___axisName1;
		float L_4;
		L_4 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0011:
	{
		V_0 = G_B3_0;
		// if (!Mathf.Approximately(axisValue, 0f))
		float L_5 = V_0;
		bool L_6;
		L_6 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_5, (0.0f), /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0021;
		}
	}
	{
		// return axisValue;
		float L_7 = V_0;
		return L_7;
	}

IL_0021:
	{
		// for (int i = 0; i < virtualAxisList.Count; i++)
		V_1 = 0;
		goto IL_0045;
	}

IL_0025:
	{
		// var currentAxisValue = virtualAxisList[i].Value;
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_8 = ___virtualAxisList0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_10;
		L_10 = List_1_get_Item_m72EB5FED66B0343A8FC5F20EFACA90F0AD39E92F_inline(L_8, L_9, /*hidden argument*/List_1_get_Item_m72EB5FED66B0343A8FC5F20EFACA90F0AD39E92F_RuntimeMethod_var);
		NullCheck(L_10);
		float L_11;
		L_11 = VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		// if (!Mathf.Approximately(currentAxisValue, 0f))
		float L_12 = V_2;
		bool L_13;
		L_13 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_12, (0.0f), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0041;
		}
	}
	{
		// return currentAxisValue;
		float L_14 = V_2;
		return L_14;
	}

IL_0041:
	{
		// for (int i = 0; i < virtualAxisList.Count; i++)
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0045:
	{
		// for (int i = 0; i < virtualAxisList.Count; i++)
		int32_t L_16 = V_1;
		List_1_tE5006B750AFF755696A7A223714798055FB561C7 * L_17 = ___virtualAxisList0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = List_1_get_Count_mAB7376FB6CC746A39504B77B71BA7572B16572EC_inline(L_17, /*hidden argument*/List_1_get_Count_mAB7376FB6CC746A39504B77B71BA7572B16572EC_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0025;
		}
	}
	{
		// return 0f;
		return (0.0f);
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonDown(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButtonDown_m9A0048CA59A5964B864C20E17DFC2535A22C4138 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		V_0 = 0;
		goto IL_0018;
	}

IL_0004:
	{
		// if (virtualButtons[i].GetButtonDown) return true;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_2;
		L_2 = List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3;
		L_3 = VirtualButton_get_GetButtonDown_m1DAE11354C811678B39111E146200B23C6803E12(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		// if (virtualButtons[i].GetButtonDown) return true;
		return (bool)1;
	}

IL_0014:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0018:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_5 = V_0;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_inline(L_6, /*hidden argument*/List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonUp(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButtonUp_m2A85153733F81918326833CDF305111DEB5D9990 (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		V_0 = 0;
		goto IL_0018;
	}

IL_0004:
	{
		// if (virtualButtons[i].GetButtonUp) return true;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_2;
		L_2 = List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3;
		L_3 = VirtualButton_get_GetButtonUp_mA53CCC28120C136AD5691AE8C98973DF460E6C86(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		// if (virtualButtons[i].GetButtonUp) return true;
		return (bool)1;
	}

IL_0014:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0018:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_5 = V_0;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_inline(L_6, /*hidden argument*/List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButton(System.Collections.Generic.List`1<CnControls.VirtualButton>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CnInputManager_GetAnyVirtualButton_mA98A92CB1839BE736CA34985CE24AF93BAB8723F (List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * ___virtualButtons0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		V_0 = 0;
		goto IL_0018;
	}

IL_0004:
	{
		// if (virtualButtons[i].GetButton) return true;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_2;
		L_2 = List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mCE44D07FE4D7D441C05E20378AD2274B8A9BA630_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3;
		L_3 = VirtualButton_get_GetButton_m5A784A059961193CE5B16ADB59B0D2C362DFBAF3(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		// if (virtualButtons[i].GetButton) return true;
		return (bool)1;
	}

IL_0014:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0018:
	{
		// for (int i = 0; i < virtualButtons.Count; i++)
		int32_t L_5 = V_0;
		List_1_t96C5E47591FBFF8A29606CB4CC56A0748069FD59 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_inline(L_6, /*hidden argument*/List_1_get_Count_m8F7B693C412D8D2B981296445045F888AB921E23_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CommonOnScreenControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonOnScreenControl__ctor_m0EDAD36310415FB96E7C848A3824BE488964EF43 (CommonOnScreenControl_t0C28AB2E71F8BA8DB737F5DB21BCE523FEC4AECD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Camera CnControls.Dpad::get_CurrentEventCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956 (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void CnControls.Dpad::set_CurrentEventCamera(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379 (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void CnControls.Dpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dpad_OnPointerDown_mF82EB7B90302B22E359009913D5BA754FCE42C35 (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* V_0 = NULL;
	int32_t V_1 = 0;
	DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * V_2 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B2_0 = NULL;
	Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * G_B2_1 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B1_0 = NULL;
	Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * G_B1_1 = NULL;
	{
		// CurrentEventCamera = eventData.pressEventCamera ?? CurrentEventCamera;
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_0 = ___eventData0;
		NullCheck(L_0);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1;
		L_1 = PointerEventData_get_pressEventCamera_m514C040A3C32E269345D0FC8B72BB2FE553FA448(L_0, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_0011;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956_inline(__this, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0011:
	{
		NullCheck(G_B2_1);
		Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379_inline(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		// foreach (var dpadAxis in DpadAxis)
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_4 = __this->get_DpadAxis_4();
		V_0 = L_4;
		V_1 = 0;
		goto IL_005a;
	}

IL_0021:
	{
		// foreach (var dpadAxis in DpadAxis)
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		// if (RectTransformUtility.RectangleContainsScreenPoint(dpadAxis.RectTransform, eventData.position,
		//     CurrentEventCamera))
		DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * L_9 = V_2;
		NullCheck(L_9);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_10;
		L_10 = DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2_inline(L_9, /*hidden argument*/NULL);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_11 = ___eventData0;
		NullCheck(L_11);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12;
		L_12 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_11, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_13;
		L_13 = Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = RectTransformUtility_RectangleContainsScreenPoint_m7D92A04D6DA6F4C7CC72439221C2EE46034A0595(L_10, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0056;
		}
	}
	{
		// dpadAxis.Press(eventData.position, CurrentEventCamera, eventData.pointerId);
		DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * L_15 = V_2;
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_16 = ___eventData0;
		NullCheck(L_16);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_17;
		L_17 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_16, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_18;
		L_18 = Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956_inline(__this, /*hidden argument*/NULL);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_19 = ___eventData0;
		NullCheck(L_19);
		int32_t L_20;
		L_20 = PointerEventData_get_pointerId_m50BE6AA34EE21DA6BE7AF07AAC9115CAB6B0636A_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		DpadAxis_Press_mD9BCDA57FCBADC57DDB5BB4A4439647EFBCBAD90(L_15, L_17, L_18, L_20, /*hidden argument*/NULL);
	}

IL_0056:
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_005a:
	{
		// foreach (var dpadAxis in DpadAxis)
		int32_t L_22 = V_1;
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_0021;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CnControls.Dpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dpad_OnPointerUp_m29099F9D826939398203D027C9951BB7C241F535 (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// foreach (var dpadAxis in DpadAxis)
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_0 = __this->get_DpadAxis_4();
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000b:
	{
		// foreach (var dpadAxis in DpadAxis)
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		// dpadAxis.TryRelease(eventData.pointerId);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_5 = ___eventData0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = PointerEventData_get_pointerId_m50BE6AA34EE21DA6BE7AF07AAC9115CAB6B0636A_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DpadAxis_TryRelease_mD494D94FAA2D11866BF18348679651CB57FDE322(L_4, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_001d:
	{
		// foreach (var dpadAxis in DpadAxis)
		int32_t L_8 = V_1;
		DpadAxisU5BU5D_t2E85A8D78D43D6381DD5F6D09F8FCE0CDBA93F11* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CnControls.Dpad::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dpad__ctor_m04228D51365F267F1B691E37B763E8C724AD663C (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.RectTransform CnControls.DpadAxis::get_RectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		// public RectTransform RectTransform { get; private set; }
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_U3CRectTransformU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void CnControls.DpadAxis::set_RectTransform(UnityEngine.RectTransform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___value0, const RuntimeMethod* method)
{
	{
		// public RectTransform RectTransform { get; private set; }
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = ___value0;
		__this->set_U3CRectTransformU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Int32 CnControls.DpadAxis::get_LastFingerId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		// public int LastFingerId { get; set; }
		int32_t L_0 = __this->get_U3CLastFingerIdU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void CnControls.DpadAxis::set_LastFingerId(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastFingerId { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastFingerIdU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void CnControls.DpadAxis::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_Awake_m08F451D554C54B2DE8EAD3859C209EE21616AB11 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// RectTransform = GetComponent<RectTransform>();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0;
		L_0 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.DpadAxis::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_OnEnable_m64D8939B835CD0E93488DD219E7EE90D684DDCC8 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B2_0 = NULL;
	DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * G_B2_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B1_0 = NULL;
	DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * G_B1_1 = NULL;
	{
		// _virtualAxis = _virtualAxis ?? new VirtualAxis(AxisName);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get__virtualAxis_8();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = __this->get_AxisName_4();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_3 = (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *)il2cpp_codegen_object_new(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__virtualAxis_8(G_B2_0);
		// LastFingerId = -1;
		DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C_inline(__this, (-1), /*hidden argument*/NULL);
		// CnInputManager.RegisterVirtualAxis(_virtualAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_4 = __this->get__virtualAxis_8();
		CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.DpadAxis::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_OnDisable_mFF33785DDED5388FE8264131DE795079DB0FB546 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		// CnInputManager.UnregisterVirtualAxis(_virtualAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get__virtualAxis_8();
		CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.DpadAxis::Press(UnityEngine.Vector2,UnityEngine.Camera,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_Press_mD9BCDA57FCBADC57DDB5BB4A4439647EFBCBAD90 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPoint0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, int32_t ___pointerId2, const RuntimeMethod* method)
{
	{
		// _virtualAxis.Value = Mathf.Clamp(AxisMultiplier, -1f, 1f);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get__virtualAxis_8();
		float L_1 = __this->get_AxisMultiplier_5();
		float L_2;
		L_2 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_1, (-1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_0, L_2, /*hidden argument*/NULL);
		// LastFingerId = pointerId;
		int32_t L_3 = ___pointerId2;
		DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C_inline(__this, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.DpadAxis::TryRelease(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis_TryRelease_mD494D94FAA2D11866BF18348679651CB57FDE322 (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, int32_t ___pointerId0, const RuntimeMethod* method)
{
	{
		// if (LastFingerId == pointerId)
		int32_t L_0;
		L_0 = DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___pointerId0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0020;
		}
	}
	{
		// _virtualAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_2 = __this->get__virtualAxis_8();
		NullCheck(L_2);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_2, (0.0f), /*hidden argument*/NULL);
		// LastFingerId = -1;
		DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C_inline(__this, (-1), /*hidden argument*/NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void CnControls.DpadAxis::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DpadAxis__ctor_m7DE7D60F8FF168B2131EB768A69CC64C0F7118DE (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CnControls.EmptyGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmptyGraphic_OnPopulateMesh_m641C20ACAB49C8E8D264ED1AF6A8448DA06E91BD (EmptyGraphic_tEA7B1F28199F9AC58CB745C9706147B30E7A02A4 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, const RuntimeMethod* method)
{
	{
		// vh.Clear();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_0 = ___vh0;
		NullCheck(L_0);
		VertexHelper_Clear_mBF3FB3CEA5153F8F72C74FFD6006A7AFF62C18BA(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.EmptyGraphic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmptyGraphic__ctor_mAE371F9CE4D9C24BB2D82C0585EFDECEB3440F56 (EmptyGraphic_tEA7B1F28199F9AC58CB745C9706147B30E7A02A4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var);
		Graphic__ctor_m41CDFE33452C8382425A864410FB01D516C55D8F(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CustomJoystick.FourWayController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FourWayController_Awake_m2111C25BCDC49D679BF07F91D90255389C220666 (FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291 * __this, const RuntimeMethod* method)
{
	{
		// _mainCameraTransform = Camera.main.transform;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		__this->set__mainCameraTransform_5(L_1);
		// }
		return;
	}
}
// System.Void CustomJoystick.FourWayController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FourWayController_Update_m12783631ECF15B350164B57C0417F869B5777D41 (FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	{
		// var movementVector = new Vector3(CnInputManager.GetAxis("Horizontal"), 0f, CnInputManager.GetAxis("Vertical"));
		float L_0;
		L_0 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		float L_1;
		L_1 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), L_0, (0.0f), L_1, /*hidden argument*/NULL);
		// if (movementVector.sqrMagnitude < 0.00001f) return;
		float L_2;
		L_2 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)(9.99999975E-06f)))))
		{
			goto IL_002f;
		}
	}
	{
		// if (movementVector.sqrMagnitude < 0.00001f) return;
		return;
	}

IL_002f:
	{
		// Vector3 closestDirectionVector = directionalVectors[0];
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = __this->get_directionalVectors_4();
		NullCheck(L_3);
		int32_t L_4 = 0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		// float closestDot = Vector3.Dot(movementVector, closestDirectionVector);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_1;
		float L_8;
		L_8 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		// for (int i = 1; i < directionalVectors.Length; i++)
		V_4 = 1;
		goto IL_007a;
	}

IL_0049:
	{
		// float dot = Vector3.Dot(movementVector, directionalVectors[i]);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = V_0;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_10 = __this->get_directionalVectors_4();
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		float L_14;
		L_14 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_9, L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		// if (dot < closestDot)
		float L_15 = V_5;
		float L_16 = V_2;
		if ((!(((float)L_15) < ((float)L_16))))
		{
			goto IL_0074;
		}
	}
	{
		// closestDirectionVector = directionalVectors[i];
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_17 = __this->get_directionalVectors_4();
		int32_t L_18 = V_4;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = L_20;
		// closestDot = dot;
		float L_21 = V_5;
		V_2 = L_21;
	}

IL_0074:
	{
		// for (int i = 1; i < directionalVectors.Length; i++)
		int32_t L_22 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_007a:
	{
		// for (int i = 1; i < directionalVectors.Length; i++)
		int32_t L_23 = V_4;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_24 = __this->get_directionalVectors_4();
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_0049;
		}
	}
	{
		// var transformedDirection = _mainCameraTransform.InverseTransformDirection(closestDirectionVector);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25 = __this->get__mainCameraTransform_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_1;
		NullCheck(L_25);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696(L_25, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		// transformedDirection.y = 0f;
		(&V_3)->set_y_3((0.0f));
		// transformedDirection.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		// transform.position += transformedDirection * Time.deltaTime;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29 = L_28;
		NullCheck(L_29);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_29, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = V_3;
		float L_32;
		L_32 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_31, L_32, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_30, L_33, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_29, L_34, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CustomJoystick.FourWayController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FourWayController__ctor_m116F69B4CAF6BA3E5B5FA479A1AF701977C6C7BB (FourWayController_tD4E9B6E0B030D534F078DCF34C4DC0105D2CC291 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Vector3[] directionalVectors = { Vector3.forward, Vector3.back, Vector3.right, Vector3.left };
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_0 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_1 = L_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_2);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_back_mD521DF1A2C26E145578E07D618E1E4D08A1C6220(/*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_4);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_5 = L_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_6);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_7 = L_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_8);
		__this->set_directionalVectors_4(L_7);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PivotBasedCameraRig_Awake_m26BD1EC124D79E38642DAC0C7A61AF260CBD247C (PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mC2EEB227949FF6517A085ECC9E0FC1F8897A5546_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Cam = GetComponentInChildren<Camera>().transform;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Component_GetComponentInChildren_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mC2EEB227949FF6517A085ECC9E0FC1F8897A5546(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mC2EEB227949FF6517A085ECC9E0FC1F8897A5546_RuntimeMethod_var);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		__this->set_m_Cam_8(L_1);
		// m_Pivot = m_Cam.parent;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_m_Cam_8();
		NullCheck(L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_2, /*hidden argument*/NULL);
		__this->set_m_Pivot_9(L_3);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PivotBasedCameraRig__ctor_m3B61A8F501BD284A9B7C1EB30A5B585EBDBD904A (PivotBasedCameraRig_t13A4E973234D190F056550A73CDCAD10D8F1DD51 * __this, const RuntimeMethod* method)
{
	{
		AbstractTargetFollower__ctor_m729146B299728E3D434C1CEE3832FA6940994EA3(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Platformer2DUserControl_Awake_mABD0B580E89C5468D99D2CD54D7FFF4FC0B61487 (Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE_m07F4B2D7BABB71917777835CEADEA7C40DA07183_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Character = GetComponent<PlatformerCharacter2D>();
		PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * L_0;
		L_0 = Component_GetComponent_TisPlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE_m07F4B2D7BABB71917777835CEADEA7C40DA07183(__this, /*hidden argument*/Component_GetComponent_TisPlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE_m07F4B2D7BABB71917777835CEADEA7C40DA07183_RuntimeMethod_var);
		__this->set_m_Character_4(L_0);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Platformer2DUserControl_Update_mB04800ADEDB9749C86C36286AF963264F2B6D6FE (Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!m_Jump)
		bool L_0 = __this->get_m_Jump_5();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		// m_Jump = CnInputManager.GetAxis("Jump") > 0f;
		float L_1;
		L_1 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, /*hidden argument*/NULL);
		__this->set_m_Jump_5((bool)((((float)L_1) > ((float)(0.0f)))? 1 : 0));
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Platformer2DUserControl_FixedUpdate_mAD35FD5F54D275CAAC7E3D0C76E17A99A223ED90 (Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float h = CnInputManager.GetAxis("Horizontal");
		float L_0;
		L_0 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		V_0 = L_0;
		// m_Character.Move(h, m_Jump);
		PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * L_1 = __this->get_m_Character_4();
		float L_2 = V_0;
		bool L_3 = __this->get_m_Jump_5();
		NullCheck(L_1);
		PlatformerCharacter2D_Move_mD91A6DDDA7DB352E4905785BFF0A7C75D800CD21(L_1, L_2, L_3, /*hidden argument*/NULL);
		// m_Jump = false;
		__this->set_m_Jump_5((bool)0);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Platformer2DUserControl__ctor_m2CFB7DEA8CC9EE1E9CD72EABA68F144512AB3E9F (Platformer2DUserControl_t569073AB53E7391376DCF23689E6B7610A08018C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_Awake_m2C234BD7760D8A1BA3E4509F10C01E15F77C2D7C (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7E39FF2E25E7C626101199E5389D85A5D4DF1D47);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_GroundCheck = transform.Find("GroundCheck");
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1(L_0, _stringLiteral7E39FF2E25E7C626101199E5389D85A5D4DF1D47, /*hidden argument*/NULL);
		__this->set_m_GroundCheck_8(L_1);
		// m_Anim = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2;
		L_2 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set_m_Anim_11(L_2);
		// m_Rigidbody2D = GetComponent<Rigidbody2D>();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_3;
		L_3 = Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		__this->set_m_Rigidbody2D_12(L_3);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_FixedUpdate_m36E4B99B14AF11152920DC79BC413A0C7BBA8211 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Physics2D_t1C1ECE6BA2F958C5C1440DDB9E9A5DAAA8F86D92_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A);
		s_Il2CppMethodInitialized = true;
	}
	Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// m_Grounded = false;
		__this->set_m_Grounded_10((bool)0);
		// Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_m_GroundCheck_8();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		L_2 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_1, /*hidden argument*/NULL);
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_3 = __this->get_m_WhatIsGround_7();
		int32_t L_4;
		L_4 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1C1ECE6BA2F958C5C1440DDB9E9A5DAAA8F86D92_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1* L_5;
		L_5 = Physics2D_OverlapCircleAll_m1D3E8E59627D524F35EF6C67940DD1121CEE6E7C(L_2, (0.200000003f), L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// for (int i = 0; i < colliders.Length; i++)
		V_1 = 0;
		goto IL_0051;
	}

IL_0031:
	{
		// if (colliders[i].gameObject != gameObject)
		Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_9, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004d;
		}
	}
	{
		// m_Grounded = true;
		__this->set_m_Grounded_10((bool)1);
	}

IL_004d:
	{
		// for (int i = 0; i < colliders.Length; i++)
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0051:
	{
		// for (int i = 0; i < colliders.Length; i++)
		int32_t L_14 = V_1;
		Collider2DU5BU5D_t00DF4453C28C5F1D2EE97FAE6CF865E53DE189D1* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length))))))
		{
			goto IL_0031;
		}
	}
	{
		// m_Anim.SetBool("Ground", m_Grounded);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_16 = __this->get_m_Anim_11();
		bool L_17 = __this->get_m_Grounded_10();
		NullCheck(L_16);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_16, _stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A, L_17, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_Move_mD91A6DDDA7DB352E4905785BFF0A7C75D800CD21 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, float ___move0, bool ___jump1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Grounded || m_AirControl)
		bool L_0 = __this->get_m_Grounded_10();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = __this->get_m_AirControl_6();
		if (!L_1)
		{
			goto IL_007c;
		}
	}

IL_0010:
	{
		// m_Anim.SetFloat("Speed", Mathf.Abs(move));
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = __this->get_m_Anim_11();
		float L_3 = ___move0;
		float L_4;
		L_4 = fabsf(L_3);
		NullCheck(L_2);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_2, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_4, /*hidden argument*/NULL);
		// m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_5 = __this->get_m_Rigidbody2D_12();
		float L_6 = ___move0;
		float L_7 = __this->get_m_MaxSpeed_4();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_8 = __this->get_m_Rigidbody2D_12();
		NullCheck(L_8);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9;
		L_9 = Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_11), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)), L_10, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_5, L_11, /*hidden argument*/NULL);
		// if (move > 0 && !m_FacingRight)
		float L_12 = ___move0;
		if ((!(((float)L_12) > ((float)(0.0f)))))
		{
			goto IL_0066;
		}
	}
	{
		bool L_13 = __this->get_m_FacingRight_13();
		if (L_13)
		{
			goto IL_0066;
		}
	}
	{
		// Flip();
		PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62(__this, /*hidden argument*/NULL);
		// }
		goto IL_007c;
	}

IL_0066:
	{
		// else if (move < 0 && m_FacingRight)
		float L_14 = ___move0;
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = __this->get_m_FacingRight_13();
		if (!L_15)
		{
			goto IL_007c;
		}
	}
	{
		// Flip();
		PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62(__this, /*hidden argument*/NULL);
	}

IL_007c:
	{
		// if (m_Grounded && jump && m_Anim.GetBool("Ground"))
		bool L_16 = __this->get_m_Grounded_10();
		bool L_17 = ___jump1;
		if (!((int32_t)((int32_t)L_16&(int32_t)L_17)))
		{
			goto IL_00cb;
		}
	}
	{
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_18 = __this->get_m_Anim_11();
		NullCheck(L_18);
		bool L_19;
		L_19 = Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A(L_18, _stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00cb;
		}
	}
	{
		// m_Grounded = false;
		__this->set_m_Grounded_10((bool)0);
		// m_Anim.SetBool("Ground", false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_20 = __this->get_m_Anim_11();
		NullCheck(L_20);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_20, _stringLiteral7A65EFC5BB52048E35B0D3E2214BCF8CE116440A, (bool)0, /*hidden argument*/NULL);
		// m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_21 = __this->get_m_Rigidbody2D_12();
		float L_22 = __this->get_m_JumpForce_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_23), (0.0f), L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody2D_AddForce_mB4754FC98ED65E5381854CDC858D12F0504FB3A2(L_21, L_23, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D_Flip_mCE698DD5E444E327F18B4B75446BB1EF7907DB62 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// m_FacingRight = !m_FacingRight;
		bool L_0 = __this->get_m_FacingRight_13();
		__this->set_m_FacingRight_13((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// Vector3 theScale = transform.localScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// theScale.x *= -1;
		float* L_3 = (&V_0)->get_address_of_x_2();
		float* L_4 = L_3;
		float L_5 = *((float*)L_4);
		*((float*)L_4) = (float)((float)il2cpp_codegen_multiply((float)L_5, (float)(-1.0f)));
		// transform.localScale = theScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		NullCheck(L_6);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformerCharacter2D__ctor_mD4485148574D5910D0694CA518875CE200C1B592 (PlatformerCharacter2D_t152AD8CB5E57E1376425D8BEFE31DEC793709FAE * __this, const RuntimeMethod* method)
{
	{
		// private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
		__this->set_m_MaxSpeed_4((10.0f));
		// private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
		__this->set_m_JumpForce_5((400.0f));
		// private bool m_FacingRight = true;  // For determining which way the player is currently facing.
		__this->set_m_FacingRight_13((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Restarter_OnTriggerEnter2D_m8BB6E3D53D95807B777A981678EE7BCCDFD35220 (Restarter_tD2B91A87072A0549C4D60034C25AFC16223FA9ED * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (other.tag == "Player")
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		// SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_3;
		L_3 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4;
		L_4 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092(L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void UnityStandardAssets._2D.Restarter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Restarter__ctor_mB1248092F111834B403FBD6AD3FA097D01B7DB20 (Restarter_tD2B91A87072A0549C4D60034C25AFC16223FA9ED * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotateCamera_Update_m4645E8E651D7268AA58DECC2FDDCD22A48B92724 (RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// var horizontalMovement = CnInputManager.GetAxis("Horizontal");
		float L_0;
		L_0 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		V_0 = L_0;
		// OriginTransform.Rotate(Vector3.up, horizontalMovement * Time.deltaTime * RotationSpeed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_OriginTransform_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_3 = V_0;
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_5 = __this->get_RotationSpeed_4();
		NullCheck(L_1);
		Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC(L_1, L_2, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)), (float)L_5)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotateCamera__ctor_mDEE2D7C98ED57BF92A36CAC9A54A7791117B120F (RotateCamera_t9869BF67C82EF585D1C278E6BFFBB15C5D853158 * __this, const RuntimeMethod* method)
{
	{
		// public float RotationSpeed = 15f;
		__this->set_RotationSpeed_4((15.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationConstraint_Awake_m5FFE5E61328AB477064AC23BAA0A26BF35F9AA8A (RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04 * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// _transformCache = transform;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		__this->set__transformCache_6(L_0);
		// _rotateAround = Vector3.right;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		__this->set__rotateAround_9(L_1);
		// var axisRotation = Quaternion.AngleAxis(_transformCache.localRotation.eulerAngles[0], _rotateAround);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get__transformCache_6();
		NullCheck(L_2);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3;
		L_3 = Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5;
		L_5 = Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), 0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get__rotateAround_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_7;
		L_7 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// _minQuaternion = axisRotation * Quaternion.AngleAxis(Min, _rotateAround);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_8 = V_0;
		float L_9 = __this->get_Min_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = __this->get__rotateAround_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_11;
		L_11 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(L_9, L_10, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_12;
		L_12 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_8, L_11, /*hidden argument*/NULL);
		__this->set__minQuaternion_7(L_12);
		// _maxQuaternion = axisRotation * Quaternion.AngleAxis(Max, _rotateAround);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_13 = V_0;
		float L_14 = __this->get_Max_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = __this->get__rotateAround_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_16;
		L_16 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(L_14, L_15, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_17;
		L_17 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_13, L_16, /*hidden argument*/NULL);
		__this->set__maxQuaternion_8(L_17);
		// _range = Max - Min;
		float L_18 = __this->get_Max_5();
		float L_19 = __this->get_Min_4();
		__this->set__range_10(((float)il2cpp_codegen_subtract((float)L_18, (float)L_19)));
		// }
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationConstraint_LateUpdate_mD64E7AB56C7F672D22D3E96D72D5FC9A5BF9BCCB (RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04 * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// var localRotation = _transformCache.localRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get__transformCache_6();
		NullCheck(L_0);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1;
		L_1 = Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var axisRotation = Quaternion.AngleAxis(localRotation.eulerAngles[0], _rotateAround);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3;
		L_3 = Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), 0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = __this->get__rotateAround_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_5;
		L_5 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(L_3, L_4, /*hidden argument*/NULL);
		// var angleFromMin = Quaternion.Angle(axisRotation, _minQuaternion);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = L_5;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_7 = __this->get__minQuaternion_7();
		float L_8;
		L_8 = Quaternion_Angle_m3BE44E43965BB9EDFD06DBC1E0985324A83327CF_inline(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// var angleFromMax = Quaternion.Angle(axisRotation, _maxQuaternion);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9 = __this->get__maxQuaternion_8();
		float L_10;
		L_10 = Quaternion_Angle_m3BE44E43965BB9EDFD06DBC1E0985324A83327CF_inline(L_6, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		// if (angleFromMin <= _range && angleFromMax <= _range)
		float L_11 = V_1;
		float L_12 = __this->get__range_10();
		if ((!(((float)L_11) <= ((float)L_12))))
		{
			goto IL_0053;
		}
	}
	{
		float L_13 = V_2;
		float L_14 = __this->get__range_10();
		if ((!(((float)L_13) <= ((float)L_14))))
		{
			goto IL_0053;
		}
	}
	{
		// return; // within range
		return;
	}

IL_0053:
	{
		// var euler = localRotation.eulerAngles;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), /*hidden argument*/NULL);
		V_4 = L_15;
		// if (angleFromMin > angleFromMax)
		float L_16 = V_1;
		float L_17 = V_2;
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_007e;
		}
	}
	{
		// euler[0] = _maxQuaternion.eulerAngles[0];
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * L_18 = __this->get_address_of__maxQuaternion_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20;
		L_20 = Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), 0, /*hidden argument*/NULL);
		Vector3_set_Item_mF3E5D7FFAD5F81973283AE6C1D15C9B238AEE346((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_4), 0, L_20, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_007e:
	{
		// euler[0] = _minQuaternion.eulerAngles[0];
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * L_21 = __this->get_address_of__minQuaternion_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23;
		L_23 = Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), 0, /*hidden argument*/NULL);
		Vector3_set_Item_mF3E5D7FFAD5F81973283AE6C1D15C9B238AEE346((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_4), 0, L_23, /*hidden argument*/NULL);
	}

IL_009a:
	{
		// _transformCache.localEulerAngles = euler;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24 = __this->get__transformCache_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25 = V_4;
		NullCheck(L_24);
		Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B(L_24, L_25, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationConstraint__ctor_m72B29DC3691BD50A46BF00F86F45A99B58D26781 (RotationConstraint_t2AA0DB1C981D7364A3FF32E4B11267FAC9EEEE04 * __this, const RuntimeMethod* method)
{
	{
		// public float Min = -15f;
		__this->set_Min_4((-15.0f));
		// public float Max = 15f;
		__this->set_Max_5((15.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CnControls.SensitiveJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensitiveJoystick_OnDrag_mEF3AFC37A7F67D138F9E5A03C823B899CEAEB9EB (SensitiveJoystick_t6BD7A696BA08155183A560BF113D5A06CDC99FFD * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// base.OnDrag(eventData);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_0 = ___eventData0;
		SimpleJoystick_OnDrag_m315B6DA9C170A7106078F76A18F95C2398A94816(__this, L_0, /*hidden argument*/NULL);
		// var linearHorizontalValue = HorizintalAxis.Value;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = ((SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 *)__this)->get_HorizintalAxis_21();
		NullCheck(L_1);
		float L_2;
		L_2 = VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// var linearVecticalValue = VerticalAxis.Value;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_3 = ((SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 *)__this)->get_VerticalAxis_22();
		NullCheck(L_3);
		float L_4;
		L_4 = VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// var horizontalSign = Mathf.Sign(linearHorizontalValue);
		float L_5 = V_0;
		float L_6;
		L_6 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// var verticalSign = Mathf.Sign(linearVecticalValue);
		float L_7 = V_1;
		float L_8;
		L_8 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		// HorizintalAxis.Value = horizontalSign * SensitivityCurve.Evaluate(horizontalSign * linearHorizontalValue);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_9 = ((SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 *)__this)->get_HorizintalAxis_21();
		float L_10 = V_2;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get_SensitivityCurve_23();
		float L_12 = V_2;
		float L_13 = V_0;
		NullCheck(L_11);
		float L_14;
		L_14 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_11, ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_9, ((float)il2cpp_codegen_multiply((float)L_10, (float)L_14)), /*hidden argument*/NULL);
		// VerticalAxis.Value = verticalSign * SensitivityCurve.Evaluate(verticalSign * linearVecticalValue);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_15 = ((SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 *)__this)->get_VerticalAxis_22();
		float L_16 = V_3;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_17 = __this->get_SensitivityCurve_23();
		float L_18 = V_3;
		float L_19 = V_1;
		NullCheck(L_17);
		float L_20;
		L_20 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_17, ((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)), /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_15, ((float)il2cpp_codegen_multiply((float)L_16, (float)L_20)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SensitiveJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensitiveJoystick__ctor_m04B2E748AA080D3C64EB2BF5105E5C8E350289E5 (SensitiveJoystick_t6BD7A696BA08155183A560BF113D5A06CDC99FFD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AnimationCurve SensitivityCurve = new AnimationCurve(
		//     new Keyframe(0f, 0f, 1f, 1f),
		//     new Keyframe(1f, 1f, 1f, 1f));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54((&L_2), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54((&L_4), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_SensitivityCurve_23(L_5);
		SimpleJoystick__ctor_mDDF1DAF26C35A567748C85CB911DD8B2A429311D(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CnControls.SimpleButton::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleButton_OnEnable_m57FF5CB5CBDE58B890755AF99140E4EE225813E7 (SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * G_B2_0 = NULL;
	SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * G_B2_1 = NULL;
	VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * G_B1_0 = NULL;
	SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * G_B1_1 = NULL;
	{
		// _virtualButton = _virtualButton ?? new VirtualButton(ButtonName);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_0 = __this->get__virtualButton_5();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = __this->get_ButtonName_4();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_3 = (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F *)il2cpp_codegen_object_new(VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F_il2cpp_TypeInfo_var);
		VirtualButton__ctor_m33D71A415DA9EE486D63A4A6BF2F712F69EAC603(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__virtualButton_5(G_B2_0);
		// CnInputManager.RegisterVirtualButton(_virtualButton);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_4 = __this->get__virtualButton_5();
		CnInputManager_RegisterVirtualButton_mE337F2AE2069EFC98A433950662B2AC92CB107F6(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleButton::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleButton_OnDisable_m92FF8AE5F15DEA4AE042456EC6BCEBA761902C65 (SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * __this, const RuntimeMethod* method)
{
	{
		// CnInputManager.UnregisterVirtualButton(_virtualButton);
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_0 = __this->get__virtualButton_5();
		CnInputManager_UnregisterVirtualButton_mFE6F77977405E14E3E67716871D87C62A2D2FB7B(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleButton_OnPointerUp_mB01978646F7323AE9F95A2015BABC4D722B097A1 (SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// _virtualButton.Release();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_0 = __this->get__virtualButton_5();
		NullCheck(L_0);
		VirtualButton_Release_m5C7E5B88EC972BDCA47DDE42169C19B774CEBC68(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleButton_OnPointerDown_mCAED6359F7B3E616334761061255876421E5879F (SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// _virtualButton.Press();
		VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * L_0 = __this->get__virtualButton_5();
		NullCheck(L_0);
		VirtualButton_Press_m1944E262243474DF95B526DE21A08CAB0E39A2E9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleButton__ctor_mDF932CB6F180F0900F92D4B3241A4D41ED402178 (SimpleButton_t1BA17467187442FB6515AF11CCA60A39DAD46EBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string ButtonName = "Jump";
		__this->set_ButtonName_4(_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Camera CnControls.SimpleJoystick::get_CurrentEventCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void CnControls.SimpleJoystick::set_CurrentEventCamera(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_Awake_m4A668E3B6AA0C1779D5B5D1B2637B9559B08CC72 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _stickTransform = Stick.GetComponent<RectTransform>();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_Stick_13();
		NullCheck(L_0);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1;
		L_1 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(L_0, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		__this->set__stickTransform_19(L_1);
		// _baseTransform = JoystickBase.GetComponent<RectTransform>();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_2 = __this->get_JoystickBase_12();
		NullCheck(L_2);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_3;
		L_3 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(L_2, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		__this->set__baseTransform_18(L_3);
		// _initialStickPosition = _stickTransform.anchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4 = __this->get__stickTransform_19();
		NullCheck(L_4);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_4, /*hidden argument*/NULL);
		__this->set__initialStickPosition_15(L_5);
		// _intermediateStickPosition = _initialStickPosition;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = __this->get__initialStickPosition_15();
		__this->set__intermediateStickPosition_16(L_6);
		// _initialBasePosition = _baseTransform.anchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_7 = __this->get__baseTransform_18();
		NullCheck(L_7);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_7, /*hidden argument*/NULL);
		__this->set__initialBasePosition_17(L_8);
		// _stickTransform.anchoredPosition = _initialStickPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_9 = __this->get__stickTransform_19();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10 = __this->get__initialStickPosition_15();
		NullCheck(L_9);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_9, L_10, /*hidden argument*/NULL);
		// _baseTransform.anchoredPosition = _initialBasePosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_11 = __this->get__baseTransform_18();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = __this->get__initialBasePosition_17();
		NullCheck(L_11);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_11, L_12, /*hidden argument*/NULL);
		// _oneOverMovementRange = 1f / MovementRange;
		float L_13 = __this->get_MovementRange_5();
		__this->set__oneOverMovementRange_20(((float)((float)(1.0f)/(float)L_13)));
		// if (HideOnRelease)
		bool L_14 = __this->get_HideOnRelease_8();
		if (!L_14)
		{
			goto IL_0093;
		}
	}
	{
		// Hide(true);
		SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0093:
	{
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnEnable_mF783F82863C089C43C34585A262414AE20E9E250 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B2_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B2_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B1_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B1_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B4_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B4_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B3_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B3_1 = NULL;
	{
		// HorizintalAxis = HorizintalAxis ?? new VirtualAxis(HorizontalAxisName);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get_HorizintalAxis_21();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = __this->get_HorizontalAxisName_6();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_3 = (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *)il2cpp_codegen_object_new(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_HorizintalAxis_21(G_B2_0);
		// VerticalAxis = VerticalAxis ?? new VirtualAxis(VerticalAxisName);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_4 = __this->get_VerticalAxis_22();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_5 = L_4;
		G_B3_0 = L_5;
		G_B3_1 = __this;
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = __this;
			goto IL_0031;
		}
	}
	{
		String_t* L_6 = __this->get_VerticalAxisName_7();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_7 = (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *)il2cpp_codegen_object_new(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED(L_7, L_6, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_1;
	}

IL_0031:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_VerticalAxis_22(G_B4_0);
		// CnInputManager.RegisterVirtualAxis(HorizintalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_8 = __this->get_HorizintalAxis_21();
		CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2(L_8, /*hidden argument*/NULL);
		// CnInputManager.RegisterVirtualAxis(VerticalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_9 = __this->get_VerticalAxis_22();
		CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2(L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnDisable_m585C6E6A10806341B0B2D865B869BABBB0115C4B (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	{
		// CnInputManager.UnregisterVirtualAxis(HorizintalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get_HorizintalAxis_21();
		CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46(L_0, /*hidden argument*/NULL);
		// CnInputManager.UnregisterVirtualAxis(VerticalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = __this->get_VerticalAxis_22();
		CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnDrag_m315B6DA9C170A7106078F76A18F95C2398A94816 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_5;
	memset((&V_5), 0, sizeof(V_5));
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B2_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B2_1 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B1_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B1_1 = NULL;
	{
		// CurrentEventCamera = eventData.pressEventCamera ?? CurrentEventCamera;
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_0 = ___eventData0;
		NullCheck(L_0);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1;
		L_1 = PointerEventData_get_pressEventCamera_m514C040A3C32E269345D0FC8B72BB2FE553FA448(L_0, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_0011;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline(__this, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0011:
	{
		NullCheck(G_B2_1);
		SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C_inline(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToWorldPointInRectangle(_stickTransform, eventData.position,
		//     CurrentEventCamera, out worldJoystickPosition);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4 = __this->get__stickTransform_19();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_5 = ___eventData0;
		NullCheck(L_5);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_5, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_7;
		L_7 = SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m76B10B1F8517DE72753AF7F61AE6E85722BF63E8(L_4, L_6, L_7, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		// _stickTransform.position = worldJoystickPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_9 = __this->get__stickTransform_19();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		NullCheck(L_9);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_9, L_10, /*hidden argument*/NULL);
		// var stickAnchoredPosition = _stickTransform.anchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_11 = __this->get__stickTransform_19();
		NullCheck(L_11);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12;
		L_12 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// if ((JoystickMoveAxis & ControlMovementDirection.Horizontal) == 0)
		int32_t L_13 = __this->get_JoystickMoveAxis_11();
		if (((int32_t)((int32_t)L_13&(int32_t)1)))
		{
			goto IL_0064;
		}
	}
	{
		// stickAnchoredPosition.x = _intermediateStickPosition.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_14 = __this->get_address_of__intermediateStickPosition_16();
		float L_15 = L_14->get_x_0();
		(&V_1)->set_x_0(L_15);
	}

IL_0064:
	{
		// if ((JoystickMoveAxis & ControlMovementDirection.Vertical) == 0)
		int32_t L_16 = __this->get_JoystickMoveAxis_11();
		if (((int32_t)((int32_t)L_16&(int32_t)2)))
		{
			goto IL_0080;
		}
	}
	{
		// stickAnchoredPosition.y = _intermediateStickPosition.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_17 = __this->get_address_of__intermediateStickPosition_16();
		float L_18 = L_17->get_y_1();
		(&V_1)->set_y_1(L_18);
	}

IL_0080:
	{
		// _stickTransform.anchoredPosition = stickAnchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_19 = __this->get__stickTransform_19();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_20 = V_1;
		NullCheck(L_19);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_19, L_20, /*hidden argument*/NULL);
		// Vector2 difference = new Vector2(stickAnchoredPosition.x, stickAnchoredPosition.y) - _intermediateStickPosition;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21 = V_1;
		float L_22 = L_21.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_23 = V_1;
		float L_24 = L_23.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_25), L_22, L_24, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_26 = __this->get__intermediateStickPosition_16();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_27;
		L_27 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_25, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		// var diffMagnitude = difference.magnitude;
		float L_28;
		L_28 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_28;
		// var normalizedDifference = difference / diffMagnitude;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_29 = V_2;
		float L_30 = V_3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_31;
		L_31 = Vector2_op_Division_m9E0ABD4CB731137B84249278B80D4C2624E58AC6_inline(L_29, L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		// if (diffMagnitude > MovementRange)
		float L_32 = V_3;
		float L_33 = __this->get_MovementRange_5();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_013e;
		}
	}
	{
		// if (MoveBase && SnapsToFinger)
		bool L_34 = __this->get_MoveBase_9();
		if (!L_34)
		{
			goto IL_011b;
		}
	}
	{
		bool L_35 = __this->get_SnapsToFinger_10();
		if (!L_35)
		{
			goto IL_011b;
		}
	}
	{
		// var baseMovementDifference = difference.magnitude - MovementRange;
		float L_36;
		L_36 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_2), /*hidden argument*/NULL);
		float L_37 = __this->get_MovementRange_5();
		V_8 = ((float)il2cpp_codegen_subtract((float)L_36, (float)L_37));
		// var addition = normalizedDifference * baseMovementDifference;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_38 = V_4;
		float L_39 = V_8;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_40;
		L_40 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_38, L_39, /*hidden argument*/NULL);
		V_9 = L_40;
		// _baseTransform.anchoredPosition += addition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_41 = __this->get__baseTransform_18();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_42 = L_41;
		NullCheck(L_42);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_43;
		L_43 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_42, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_44 = V_9;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_45;
		L_45 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_43, L_44, /*hidden argument*/NULL);
		NullCheck(L_42);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_42, L_45, /*hidden argument*/NULL);
		// _intermediateStickPosition += addition;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_46 = __this->get__intermediateStickPosition_16();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_47 = V_9;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_48;
		L_48 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_46, L_47, /*hidden argument*/NULL);
		__this->set__intermediateStickPosition_16(L_48);
		// }
		goto IL_013e;
	}

IL_011b:
	{
		// _stickTransform.anchoredPosition = _intermediateStickPosition + normalizedDifference * MovementRange;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_49 = __this->get__stickTransform_19();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_50 = __this->get__intermediateStickPosition_16();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_51 = V_4;
		float L_52 = __this->get_MovementRange_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_53;
		L_53 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_51, L_52, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_54;
		L_54 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_50, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_49, L_54, /*hidden argument*/NULL);
	}

IL_013e:
	{
		// var finalStickAnchoredPosition = _stickTransform.anchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_55 = __this->get__stickTransform_19();
		NullCheck(L_55);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_56;
		L_56 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_55, /*hidden argument*/NULL);
		V_5 = L_56;
		// Vector2 finalDifference = new Vector2(finalStickAnchoredPosition.x, finalStickAnchoredPosition.y) - _intermediateStickPosition;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_57 = V_5;
		float L_58 = L_57.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_59 = V_5;
		float L_60 = L_59.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_61;
		memset((&L_61), 0, sizeof(L_61));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_61), L_58, L_60, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_62 = __this->get__intermediateStickPosition_16();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_63;
		L_63 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_61, L_62, /*hidden argument*/NULL);
		// var horizontalValue = Mathf.Clamp(finalDifference.x * _oneOverMovementRange, -1f, 1f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_64 = L_63;
		float L_65 = L_64.get_x_0();
		float L_66 = __this->get__oneOverMovementRange_20();
		float L_67;
		L_67 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(((float)il2cpp_codegen_multiply((float)L_65, (float)L_66)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_6 = L_67;
		// var verticalValue = Mathf.Clamp(finalDifference.y * _oneOverMovementRange, -1f, 1f);
		float L_68 = L_64.get_y_1();
		float L_69 = __this->get__oneOverMovementRange_20();
		float L_70;
		L_70 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(((float)il2cpp_codegen_multiply((float)L_68, (float)L_69)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_7 = L_70;
		// HorizintalAxis.Value = horizontalValue;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_71 = __this->get_HorizintalAxis_21();
		float L_72 = V_6;
		NullCheck(L_71);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_71, L_72, /*hidden argument*/NULL);
		// VerticalAxis.Value = verticalValue;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_73 = __this->get_VerticalAxis_22();
		float L_74 = V_7;
		NullCheck(L_73);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_73, L_74, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnPointerUp_m1BBC47BB0B9F02B873794D073BAD8BD0BF1BD7D1 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// _baseTransform.anchoredPosition = _initialBasePosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get__baseTransform_18();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = __this->get__initialBasePosition_17();
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_0, L_1, /*hidden argument*/NULL);
		// _stickTransform.anchoredPosition = _initialStickPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_2 = __this->get__stickTransform_19();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = __this->get__initialStickPosition_15();
		NullCheck(L_2);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_2, L_3, /*hidden argument*/NULL);
		// _intermediateStickPosition = _initialStickPosition;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = __this->get__initialStickPosition_15();
		__this->set__intermediateStickPosition_16(L_4);
		// HorizintalAxis.Value = VerticalAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_5 = __this->get_HorizintalAxis_21();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_6 = __this->get_VerticalAxis_22();
		float L_7 = (0.0f);
		V_0 = L_7;
		NullCheck(L_6);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_6, L_7, /*hidden argument*/NULL);
		float L_8 = V_0;
		NullCheck(L_5);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_5, L_8, /*hidden argument*/NULL);
		// if (HideOnRelease)
		bool L_9 = __this->get_HideOnRelease_8();
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		// Hide(true);
		SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_OnPointerDown_m228FDDFF072D62F4F531BE48AFE562E5D591EF13 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B5_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B5_1 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B4_0 = NULL;
	SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * G_B4_1 = NULL;
	{
		// if (HideOnRelease)
		bool L_0 = __this->get_HideOnRelease_8();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		// Hide(false);
		SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_000f:
	{
		// if (SnapsToFinger)
		bool L_1 = __this->get_SnapsToFinger_10();
		if (!L_1)
		{
			goto IL_008b;
		}
	}
	{
		// CurrentEventCamera = eventData.pressEventCamera ?? CurrentEventCamera;
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_2 = ___eventData0;
		NullCheck(L_2);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = PointerEventData_get_pressEventCamera_m514C040A3C32E269345D0FC8B72BB2FE553FA448(L_2, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_4 = L_3;
		G_B4_0 = L_4;
		G_B4_1 = __this;
		if (L_4)
		{
			G_B5_0 = L_4;
			G_B5_1 = __this;
			goto IL_0028;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_5;
		L_5 = SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline(__this, /*hidden argument*/NULL);
		G_B5_0 = L_5;
		G_B5_1 = G_B4_1;
	}

IL_0028:
	{
		NullCheck(G_B5_1);
		SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C_inline(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToWorldPointInRectangle(_stickTransform, eventData.position,
		//     CurrentEventCamera, out localStickPosition);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_6 = __this->get__stickTransform_19();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_7 = ___eventData0;
		NullCheck(L_7);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_7, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_9;
		L_9 = SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m76B10B1F8517DE72753AF7F61AE6E85722BF63E8(L_6, L_8, L_9, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		// RectTransformUtility.ScreenPointToWorldPointInRectangle(_baseTransform, eventData.position,
		//     CurrentEventCamera, out localBasePosition);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_11 = __this->get__baseTransform_18();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_12 = ___eventData0;
		NullCheck(L_12);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline(L_12, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_14;
		L_14 = SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline(__this, /*hidden argument*/NULL);
		bool L_15;
		L_15 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m76B10B1F8517DE72753AF7F61AE6E85722BF63E8(L_11, L_13, L_14, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		// _baseTransform.position = localBasePosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_16 = __this->get__baseTransform_18();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_1;
		NullCheck(L_16);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_16, L_17, /*hidden argument*/NULL);
		// _stickTransform.position = localStickPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_18 = __this->get__stickTransform_19();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = V_0;
		NullCheck(L_18);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_18, L_19, /*hidden argument*/NULL);
		// _intermediateStickPosition = _stickTransform.anchoredPosition;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_20 = __this->get__stickTransform_19();
		NullCheck(L_20);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21;
		L_21 = RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975(L_20, /*hidden argument*/NULL);
		__this->set__intermediateStickPosition_16(L_21);
		// }
		return;
	}

IL_008b:
	{
		// OnDrag(eventData);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_22 = ___eventData0;
		VirtActionInvoker1< PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * >::Invoke(7 /* System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_22);
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::Hide(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick_Hide_m42407C0921BE1E3C98BCBD6FA2B19F8CCAA342E9 (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, bool ___isHidden0, const RuntimeMethod* method)
{
	{
		// JoystickBase.gameObject.SetActive(!isHidden);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_JoystickBase_12();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		bool L_2 = ___isHidden0;
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// Stick.gameObject.SetActive(!isHidden);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3 = __this->get_Stick_13();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		bool L_5 = ___isHidden0;
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.SimpleJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleJoystick__ctor_mDDF1DAF26C35A567748C85CB911DD8B2A429311D (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float MovementRange = 50f;
		__this->set_MovementRange_5((50.0f));
		// public string HorizontalAxisName = "Horizontal";
		__this->set_HorizontalAxisName_6(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		// public string VerticalAxisName = "Vertical";
		__this->set_VerticalAxisName_7(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		// public bool MoveBase = true;
		__this->set_MoveBase_9((bool)1);
		// public bool SnapsToFinger = true;
		__this->set_SnapsToFinger_10((bool)1);
		// public ControlMovementDirection JoystickMoveAxis = ControlMovementDirection.Both;
		__this->set_JoystickMoveAxis_11(3);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ThidPersonExampleController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThidPersonExampleController_OnEnable_mB2992A2594EDC4250F065E13583A0C8521340334 (ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _mainCameraTransform = Camera.main.GetComponent<Transform>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213(L_0, /*hidden argument*/Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213_RuntimeMethod_var);
		__this->set__mainCameraTransform_5(L_1);
		// _characterController = GetComponent<CharacterController>();
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_2;
		L_2 = Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		__this->set__characterController_7(L_2);
		// _transform = GetComponent<Transform>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213(__this, /*hidden argument*/Component_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mE2EA0E48C8C0EAFA09C6FAD2003105EACAC85213_RuntimeMethod_var);
		__this->set__transform_6(L_3);
		// }
		return;
	}
}
// System.Void ThidPersonExampleController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThidPersonExampleController_Update_mEB0EB920E5FD84D02E3289893C024E6B23A68805 (ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
		float L_0;
		L_0 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		float L_1;
		L_1 = CnInputManager_GetAxis_m95B2310659014A79AABB0021735454CB96C2EE7B(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), L_0, L_1, /*hidden argument*/NULL);
		// Vector3 movementVector = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_1 = L_2;
		// if (inputVector.sqrMagnitude > 0.001f)
		float L_3;
		L_3 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)(0.00100000005f)))))
		{
			goto IL_005b;
		}
	}
	{
		// movementVector = _mainCameraTransform.TransformDirection(inputVector);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get__mainCameraTransform_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// movementVector.y = 0f;
		(&V_1)->set_y_3((0.0f));
		// movementVector.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		// _transform.forward = movementVector;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = __this->get__transform_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_1;
		NullCheck(L_7);
		Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D(L_7, L_8, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// movementVector += Physics.gravity;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Physics_get_gravity_m58D5D94276B1E7A04E9F7108EEAAB7AB786BA532(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		// _characterController.Move(movementVector * Time.deltaTime);
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_12 = __this->get__characterController_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_1;
		float L_14;
		L_14 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_16;
		L_16 = CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60(L_12, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ThidPersonExampleController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThidPersonExampleController__ctor_m6B7054DE640F21560C86BF017E8368F9EA232101 (ThidPersonExampleController_tB8A4671F14AD7B6728197E4C66E3A1196D64DC40 * __this, const RuntimeMethod* method)
{
	{
		// public float MovementSpeed = 10f;
		__this->set_MovementSpeed_4((10.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Camera CnControls.Touchpad::get_CurrentEventCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Touchpad_get_CurrentEventCamera_m77CEFA791EFA8662F3F8C5B6E6FEC11E1A3139E4 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void CnControls.Touchpad::set_CurrentEventCamera(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_set_CurrentEventCamera_m37E4B76190F2765D52ED9F71CF67EBE6CE5BAA92 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void CnControls.Touchpad::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_OnEnable_m434A35BC9F950BF4EDC68E6DB4F93F013D5BBD3B (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B2_0 = NULL;
	Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * G_B2_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B1_0 = NULL;
	Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * G_B1_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B4_0 = NULL;
	Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * G_B4_1 = NULL;
	VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * G_B3_0 = NULL;
	Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * G_B3_1 = NULL;
	{
		// _horizintalAxis = _horizintalAxis ?? new VirtualAxis(HorizontalAxisName);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get__horizintalAxis_9();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = __this->get_HorizontalAxisName_5();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_3 = (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *)il2cpp_codegen_object_new(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__horizintalAxis_9(G_B2_0);
		// _verticalAxis = _verticalAxis ?? new VirtualAxis(VerticalAxisName);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_4 = __this->get__verticalAxis_10();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_5 = L_4;
		G_B3_0 = L_5;
		G_B3_1 = __this;
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = __this;
			goto IL_0031;
		}
	}
	{
		String_t* L_6 = __this->get_VerticalAxisName_6();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_7 = (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD *)il2cpp_codegen_object_new(VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED(L_7, L_6, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_1;
	}

IL_0031:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__verticalAxis_10(G_B4_0);
		// CnInputManager.RegisterVirtualAxis(_horizintalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_8 = __this->get__horizintalAxis_9();
		CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2(L_8, /*hidden argument*/NULL);
		// CnInputManager.RegisterVirtualAxis(_verticalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_9 = __this->get__verticalAxis_10();
		CnInputManager_RegisterVirtualAxis_m09AFDE704C851E3820D22A32E7E40EAA010FE3E2(L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_OnDisable_mB50AA2BED48EA529DB36AF516D143FAD1E5F0F90 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, const RuntimeMethod* method)
{
	{
		// CnInputManager.UnregisterVirtualAxis(_horizintalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_0 = __this->get__horizintalAxis_9();
		CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46(L_0, /*hidden argument*/NULL);
		// CnInputManager.UnregisterVirtualAxis(_verticalAxis);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = __this->get__verticalAxis_10();
		CnInputManager_UnregisterVirtualAxis_mF7EFBE767BAEDBC5C21278F72D08BB75C4C2CB46(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_OnDrag_m668AFF004B7734F4C376FC7C31D928DC84AD72C7 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// if ((ControlMoveAxis & ControlMovementDirection.Horizontal) != 0)
		int32_t L_0 = __this->get_ControlMoveAxis_13();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		// _horizintalAxis.Value = eventData.delta.x;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = __this->get__horizintalAxis_9();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_2 = ___eventData0;
		NullCheck(L_2);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = PointerEventData_get_delta_mCEECFB10CBB95E1C5FFD8A24B54A3989D926CA34_inline(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_x_0();
		NullCheck(L_1);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_1, L_4, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// if ((ControlMoveAxis & ControlMovementDirection.Vertical) != 0)
		int32_t L_5 = __this->get_ControlMoveAxis_13();
		if (!((int32_t)((int32_t)L_5&(int32_t)2)))
		{
			goto IL_0040;
		}
	}
	{
		// _verticalAxis.Value = eventData.delta.y;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_6 = __this->get__verticalAxis_10();
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_7 = ___eventData0;
		NullCheck(L_7);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = PointerEventData_get_delta_mCEECFB10CBB95E1C5FFD8A24B54A3989D926CA34_inline(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_1();
		NullCheck(L_6);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_6, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		// _lastDragFrameNumber = Time.renderedFrameCount;
		int32_t L_10;
		L_10 = Time_get_renderedFrameCount_m97524F45A5996675DB60401A0211F700286D2B9A(/*hidden argument*/NULL);
		__this->set__lastDragFrameNumber_11(L_10);
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_OnPointerUp_mC05E90519E111D361ED3C8E09D92269A0CFDC989 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// _isCurrentlyTweaking = false;
		__this->set__isCurrentlyTweaking_12((bool)0);
		// if (!PreserveInertia)
		bool L_0 = __this->get_PreserveInertia_7();
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		// _horizintalAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_1 = __this->get__horizintalAxis_9();
		NullCheck(L_1);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_1, (0.0f), /*hidden argument*/NULL);
		// _verticalAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_2 = __this->get__verticalAxis_10();
		NullCheck(L_2);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_2, (0.0f), /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_OnPointerDown_m1BEBD7E307303F93B24D1B79A7F78354BF11717F (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___eventData0, const RuntimeMethod* method)
{
	{
		// _isCurrentlyTweaking = true;
		__this->set__isCurrentlyTweaking_12((bool)1);
		// OnDrag(eventData);
		PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * L_0 = ___eventData0;
		VirtActionInvoker1< PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * >::Invoke(7 /* System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_0);
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad_Update_mF7762CE57B2BAB8EED9E379E152F372A0D65523C (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, const RuntimeMethod* method)
{
	{
		// if (_isCurrentlyTweaking && _lastDragFrameNumber < Time.renderedFrameCount - 2)
		bool L_0 = __this->get__isCurrentlyTweaking_12();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_1 = __this->get__lastDragFrameNumber_11();
		int32_t L_2;
		L_2 = Time_get_renderedFrameCount_m97524F45A5996675DB60401A0211F700286D2B9A(/*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)2)))))
		{
			goto IL_0037;
		}
	}
	{
		// _horizintalAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_3 = __this->get__horizintalAxis_9();
		NullCheck(L_3);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_3, (0.0f), /*hidden argument*/NULL);
		// _verticalAxis.Value = 0f;
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_4 = __this->get__verticalAxis_10();
		NullCheck(L_4);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_4, (0.0f), /*hidden argument*/NULL);
	}

IL_0037:
	{
		// if (PreserveInertia && !_isCurrentlyTweaking)
		bool L_5 = __this->get_PreserveInertia_7();
		if (!L_5)
		{
			goto IL_009f;
		}
	}
	{
		bool L_6 = __this->get__isCurrentlyTweaking_12();
		if (L_6)
		{
			goto IL_009f;
		}
	}
	{
		// _horizintalAxis.Value = Mathf.Lerp(_horizintalAxis.Value, 0f, Friction * Time.deltaTime);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_7 = __this->get__horizintalAxis_9();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_8 = __this->get__horizintalAxis_9();
		NullCheck(L_8);
		float L_9;
		L_9 = VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_Friction_8();
		float L_11;
		L_11 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_12;
		L_12 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_9, (0.0f), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_7, L_12, /*hidden argument*/NULL);
		// _verticalAxis.Value = Mathf.Lerp(_verticalAxis.Value, 0f, Friction * Time.deltaTime);
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_13 = __this->get__verticalAxis_10();
		VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * L_14 = __this->get__verticalAxis_10();
		NullCheck(L_14);
		float L_15;
		L_15 = VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_Friction_8();
		float L_17;
		L_17 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_18;
		L_18 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_15, (0.0f), ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline(L_13, L_18, /*hidden argument*/NULL);
	}

IL_009f:
	{
		// }
		return;
	}
}
// System.Void CnControls.Touchpad::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Touchpad__ctor_mCADCA3DB1B6248C43CDD46EE1A5DDB2C08C77537 (Touchpad_tEA4053F1392EE3F9F86F763165BED42BEE823FF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string HorizontalAxisName = "Horizontal";
		__this->set_HorizontalAxisName_5(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		// public string VerticalAxisName = "Vertical";
		__this->set_VerticalAxisName_6(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		// public bool PreserveInertia = true;
		__this->set_PreserveInertia_7((bool)1);
		// public float Friction = 3f;
		__this->set_Friction_8((3.0f));
		// public ControlMovementDirection ControlMoveAxis = ControlMovementDirection.Both;
		__this->set_ControlMoveAxis_13(3);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String CnControls.VirtualAxis::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CnControls.VirtualAxis::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single CnControls.VirtualAxis::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130 (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method)
{
	{
		// public float Value { get; set; }
		float L_0 = __this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CnControls.VirtualAxis::set_Value(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Value { get; set; }
		float L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void CnControls.VirtualAxis::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualAxis__ctor_m50C0FC5C709003F4A8B62FCA7BA026B07E1702ED (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		// public VirtualAxis(string name)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// Name = name;
		String_t* L_0 = ___name0;
		VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String CnControls.VirtualButton::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CnControls.VirtualButton::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean CnControls.VirtualButton::get_IsPressed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// public bool IsPressed { get; private set; }
		bool L_0 = __this->get_U3CIsPressedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CnControls.VirtualButton::set_IsPressed(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsPressed { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CIsPressedU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void CnControls.VirtualButton::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton__ctor_m33D71A415DA9EE486D63A4A6BF2F712F69EAC603 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		// private int _lastPressedFrame = -1;
		__this->set__lastPressedFrame_2((-1));
		// private int _lastReleasedFrame = -1;
		__this->set__lastReleasedFrame_3((-1));
		// public VirtualButton(string name)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// Name = name;
		String_t* L_0 = ___name0;
		VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CnControls.VirtualButton::Press()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_Press_m1944E262243474DF95B526DE21A08CAB0E39A2E9 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// if (IsPressed)
		bool L_0;
		L_0 = VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// IsPressed = true;
		VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB_inline(__this, (bool)1, /*hidden argument*/NULL);
		// _lastPressedFrame = Time.frameCount;
		int32_t L_1;
		L_1 = Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0(/*hidden argument*/NULL);
		__this->set__lastPressedFrame_2(L_1);
		// }
		return;
	}
}
// System.Void CnControls.VirtualButton::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VirtualButton_Release_m5C7E5B88EC972BDCA47DDE42169C19B774CEBC68 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// IsPressed = false;
		VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB_inline(__this, (bool)0, /*hidden argument*/NULL);
		// _lastReleasedFrame = Time.frameCount;
		int32_t L_0;
		L_0 = Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0(/*hidden argument*/NULL);
		__this->set__lastReleasedFrame_3(L_0);
		// }
		return;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButton_m5A784A059961193CE5B16ADB59B0D2C362DFBAF3 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// get { return IsPressed; }
		bool L_0;
		L_0 = VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD_inline(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButtonDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButtonDown_m1DAE11354C811678B39111E146200B23C6803E12 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// return _lastPressedFrame != -1 && _lastPressedFrame - Time.frameCount == -1;
		int32_t L_0 = __this->get__lastPressedFrame_2();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = __this->get__lastPressedFrame_2();
		int32_t L_2;
		L_2 = Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0(/*hidden argument*/NULL);
		return (bool)((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_2))) == ((int32_t)(-1)))? 1 : 0);
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButtonUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VirtualButton_get_GetButtonUp_mA53CCC28120C136AD5691AE8C98973DF460E6C86 (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// return _lastReleasedFrame != -1 && _lastReleasedFrame == Time.frameCount - 1;
		int32_t L_0 = __this->get__lastReleasedFrame_3();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = __this->get__lastReleasedFrame_3();
		int32_t L_2;
		L_2 = Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0(/*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1))))? 1 : 0);
	}

IL_0019:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VirtualAxis_get_Name_m5ED91F91E531454F15E845A494A46DCB715E2777_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VirtualButton_get_Name_m580D12FA0DFE5A3E3B78C4E0C855B8FC09D0D5DA_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float VirtualAxis_get_Value_m9C5F15E4F8A77A4FB75260526A96AA92576AF130_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, const RuntimeMethod* method)
{
	{
		// public float Value { get; set; }
		float L_0 = __this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Dpad_get_CurrentEventCamera_m5C03F772D192CC261C7E15EB6D33A6EE06DB7956_inline (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Dpad_set_CurrentEventCamera_mFB9A227A876B0CED7DC257D50C42B10E56F95379_inline (Dpad_t42BE805A8E3239000EC1F71AC33F2F91AF908B75 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * DpadAxis_get_RectTransform_mC2B6CC2920F1D0203A2650F25C56B9CE24AB23E2_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		// public RectTransform RectTransform { get; private set; }
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_U3CRectTransformU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_position_mE65C1CF448C935678F7C2A6265B4F3906FD9D651_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 position { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_U3CpositionU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PointerEventData_get_pointerId_m50BE6AA34EE21DA6BE7AF07AAC9115CAB6B0636A_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method)
{
	{
		// public int pointerId { get; set; }
		int32_t L_0 = __this->get_U3CpointerIdU3Ek__BackingField_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DpadAxis_set_RectTransform_m2910C24D387EACDEA73FBA7FC931C50BA0ECC163_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___value0, const RuntimeMethod* method)
{
	{
		// public RectTransform RectTransform { get; private set; }
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = ___value0;
		__this->set_U3CRectTransformU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DpadAxis_set_LastFingerId_mF0CD5E809F4B0CF54B815A41AB82FCF5C533CF6C_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastFingerId { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastFingerIdU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualAxis_set_Value_mAB356EAA8AF174A3F202C6476F77323F4A3C9AAD_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Value { get; set; }
		float L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DpadAxis_get_LastFingerId_m841F4715A1FF0715C15593B5E5C489B8A11A3CD4_inline (DpadAxis_t290D12249295C195F4AED23ACD6D6018518C515B * __this, const RuntimeMethod* method)
{
	{
		// public int LastFingerId { get; set; }
		int32_t L_0 = __this->get_U3CLastFingerIdU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___lhs0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___rhs1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___lhs0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___rhs1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___lhs0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___rhs1;
		float L_11 = L_10.get_z_4();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Angle_m3BE44E43965BB9EDFD06DBC1E0985324A83327CF_inline (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0 = ___a0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1 = ___b1;
		float L_2;
		L_2 = Quaternion_Dot_m7F12C5843352AB2EA687923444CC987D51515F9A(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		bool L_4;
		L_4 = Quaternion_IsEqualUsingDot_mC57C44978B13AD1592750B1D523AAB4549BD5643(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		float L_5 = V_0;
		float L_6;
		L_6 = fabsf(L_5);
		float L_7;
		L_7 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_6, (1.0f), /*hidden argument*/NULL);
		float L_8;
		L_8 = acosf(L_7);
		G_B3_0 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_8, (float)(2.0f))), (float)(57.2957802f)));
		goto IL_0039;
	}

IL_0034:
	{
		G_B3_0 = (0.0f);
	}

IL_0039:
	{
		V_1 = G_B3_0;
		goto IL_003c;
	}

IL_003c:
	{
		float L_9 = V_1;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * SimpleJoystick_get_CurrentEventCamera_m124917EB489CAFF1DCAD89C38D2A86C8DEF312A1_inline (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SimpleJoystick_set_CurrentEventCamera_m2E2864EFB44799B0FCA6BB0549B5BA098891551C_inline (SimpleJoystick_tAE2610BC21903FF07726E76C0AB48FABEB1C8E89 * __this, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___value0, const RuntimeMethod* method)
{
	{
		// public Camera CurrentEventCamera { get; set; }
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Division_m9E0ABD4CB731137B84249278B80D4C2624E58AC6_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		__this->set_z_4((0.0f));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PointerEventData_get_delta_mCEECFB10CBB95E1C5FFD8A24B54A3989D926CA34_inline (PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 delta { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_U3CdeltaU3Ek__BackingField_14();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualAxis_set_Name_m866A9E5ACE9B5FEADE252F98C1AFC7298CC1DEDB_inline (VirtualAxis_tA4600EB53CF231FDA6D8DEAD152DAEC13232F9FD * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualButton_set_Name_m3542069555B7F55163213562D4A0BA6E9C4BCDB6_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Name { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool VirtualButton_get_IsPressed_m3E6884E53F30A2FA27088FE1C45E7B6E01F136BD_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, const RuntimeMethod* method)
{
	{
		// public bool IsPressed { get; private set; }
		bool L_0 = __this->get_U3CIsPressedU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VirtualButton_set_IsPressed_m3F4AEB1243183ED02323AC5F40BB645013DFD9EB_inline (VirtualButton_tB78A0170434B1D10291E410B35C045F3CCD1593F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsPressed { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CIsPressedU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
