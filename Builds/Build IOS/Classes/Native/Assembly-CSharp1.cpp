﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// ButtonZfix
struct ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// Controll
struct Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// OnTextLayer
struct OnTextLayer_tB9AFE4C1846C1E96D19887DE46A3C98471C4A931;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// ParticlesBase
struct ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// Rotate
struct Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// ScaleZfromZero
struct ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686;
// Shkala
struct Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058;
// SkinCollector
struct SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.TextMesh
struct TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UpgradePrice
struct UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// WeekReward
struct WeekReward_t03D10DBBCBB5EDCAD14B1EC679997776BBB46CE2;
// YourPlace
struct YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3;
// guiRankSystem
struct guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15;
// textCountPet
struct textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5;
// translateLanguages
struct translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D;
// wwwBlock
struct wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529;
// CFX_AutoDestructShuriken/<CheckIfAlive>d__2
struct U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral073138638D196EB8FEC8AFDE609B8EFA374C5446;
IL2CPP_EXTERN_C String_t* _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626;
IL2CPP_EXTERN_C String_t* _stringLiteral0D0221001FCE79003404D8010C7056484DCFC6A3;
IL2CPP_EXTERN_C String_t* _stringLiteral172BBFD23CD800C2FFE73DF068B95520FE05A383;
IL2CPP_EXTERN_C String_t* _stringLiteral2EC66281BFC74AA5FE28EA5424D8A08C9CC2A8E0;
IL2CPP_EXTERN_C String_t* _stringLiteral3070995B7FCD12FF8158CDD4C8DF16E4878D533D;
IL2CPP_EXTERN_C String_t* _stringLiteral34ACEF827EC0C7AA8CAF48045E67A5546F64AB4B;
IL2CPP_EXTERN_C String_t* _stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0;
IL2CPP_EXTERN_C String_t* _stringLiteral5AC799D9AAAF31CC04AC171870B3550DDBF029C0;
IL2CPP_EXTERN_C String_t* _stringLiteral5BDF37FF2609B4D78AB0AE5F9E4897F84727A58E;
IL2CPP_EXTERN_C String_t* _stringLiteral61728EAE200F766A4FA68673634F3C01D677726E;
IL2CPP_EXTERN_C String_t* _stringLiteral777E4B7B2E7904F4FB07ABF0FDCAADD0A4734A8A;
IL2CPP_EXTERN_C String_t* _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51;
IL2CPP_EXTERN_C String_t* _stringLiteral8786B3C8228E5D838EA3026028F99E8CD2F3BC7A;
IL2CPP_EXTERN_C String_t* _stringLiteralB59F0EFCC2B82196C428CFD6D80A08F8B47573FC;
IL2CPP_EXTERN_C String_t* _stringLiteralCE18B047107AA23D1AA9B2ED32D316148E02655F;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE280D065A824A791F8305234D3E093FC9A5A90C7;
IL2CPP_EXTERN_C String_t* _stringLiteralFA57B623302DFD4D4C2F3E547EC3AFE5B3A705FB;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_m91CE0171326B90198E69EAFA60F45473CAC6E0C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB_m8DD0FC4A6D5411A3D20F2FE044585D557A302FDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mEF16B074432DE98F427DAA1007C84C6ADD3B6E6F_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// CFX_AutoDestructShuriken/<CheckIfAlive>d__2
struct  U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272  : public RuntimeObject
{
public:
	// System.Int32 CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CFX_AutoDestructShuriken CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>4__this
	CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * ___U3CU3E4__this_2;
	// UnityEngine.ParticleSystem CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<ps>5__2
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___U3CpsU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272, ___U3CU3E4__this_2)); }
	inline CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpsU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272, ___U3CpsU3E5__2_3)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_U3CpsU3E5__2_3() const { return ___U3CpsU3E5__2_3; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_U3CpsU3E5__2_3() { return &___U3CpsU3E5__2_3; }
	inline void set_U3CpsU3E5__2_3(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___U3CpsU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpsU3E5__2_3), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.SystemLanguage
struct  SystemLanguage_tF8A9C86102588DE9A5041719609C2693D283B3A6 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemLanguage_tF8A9C86102588DE9A5041719609C2693D283B3A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.ParticleSystem
struct  ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.TextMesh
struct  TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MeshRenderer
struct  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// ButtonZfix
struct  ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ButtonZfix::sc
	float ___sc_4;
	// System.Single ButtonZfix::startSc
	float ___startSc_5;
	// System.Int32 ButtonZfix::step
	int32_t ___step_6;
	// System.Single ButtonZfix::scale
	float ___scale_7;
	// System.Single ButtonZfix::speed
	float ___speed_8;

public:
	inline static int32_t get_offset_of_sc_4() { return static_cast<int32_t>(offsetof(ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB, ___sc_4)); }
	inline float get_sc_4() const { return ___sc_4; }
	inline float* get_address_of_sc_4() { return &___sc_4; }
	inline void set_sc_4(float value)
	{
		___sc_4 = value;
	}

	inline static int32_t get_offset_of_startSc_5() { return static_cast<int32_t>(offsetof(ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB, ___startSc_5)); }
	inline float get_startSc_5() const { return ___startSc_5; }
	inline float* get_address_of_startSc_5() { return &___startSc_5; }
	inline void set_startSc_5(float value)
	{
		___startSc_5 = value;
	}

	inline static int32_t get_offset_of_step_6() { return static_cast<int32_t>(offsetof(ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB, ___step_6)); }
	inline int32_t get_step_6() const { return ___step_6; }
	inline int32_t* get_address_of_step_6() { return &___step_6; }
	inline void set_step_6(int32_t value)
	{
		___step_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB, ___scale_7)); }
	inline float get_scale_7() const { return ___scale_7; }
	inline float* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(float value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}
};


// CFX_AutoDestructShuriken
struct  CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean CFX_AutoDestructShuriken::OnlyDeactivate
	bool ___OnlyDeactivate_4;

public:
	inline static int32_t get_offset_of_OnlyDeactivate_4() { return static_cast<int32_t>(offsetof(CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA, ___OnlyDeactivate_4)); }
	inline bool get_OnlyDeactivate_4() const { return ___OnlyDeactivate_4; }
	inline bool* get_address_of_OnlyDeactivate_4() { return &___OnlyDeactivate_4; }
	inline void set_OnlyDeactivate_4(bool value)
	{
		___OnlyDeactivate_4 = value;
	}
};


// Controll
struct  Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Controll::minionPrabaf
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___minionPrabaf_4;
	// System.Int32 Controll::maxMinions
	int32_t ___maxMinions_5;
	// System.String Controll::names
	String_t* ___names_6;
	// System.Boolean Controll::bot
	bool ___bot_7;
	// System.Int32 Controll::score
	int32_t ___score_8;
	// System.Int32 Controll::scoreAll
	int32_t ___scoreAll_9;
	// UnityEngine.GameObject Controll::JoystickBase
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___JoystickBase_10;
	// UnityEngine.GameObject Controll::Stick
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Stick_11;
	// System.Single Controll::speed
	float ___speed_12;
	// System.Single Controll::speedBonus
	float ___speedBonus_13;
	// System.Single Controll::iSpeed
	float ___iSpeed_14;
	// System.Single Controll::iShield
	float ___iShield_15;
	// System.Int32 Controll::wohooCount
	int32_t ___wohooCount_16;
	// System.Single Controll::voiceTimer
	float ___voiceTimer_17;
	// System.Single Controll::timer
	float ___timer_18;
	// UnityEngine.GameObject Controll::MainSystem
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MainSystem_19;
	// System.Int32 Controll::countPet
	int32_t ___countPet_20;
	// System.Single Controll::timer2
	float ___timer2_21;
	// System.Int32 Controll::gradkaCount
	int32_t ___gradkaCount_22;
	// UnityEngine.Vector3 Controll::target
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target_23;
	// UnityEngine.GameObject Controll::Hero
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Hero_24;
	// System.Boolean Controll::CreateMinionUpd
	bool ___CreateMinionUpd_25;
	// System.Int32 Controll::countPetTemp
	int32_t ___countPetTemp_26;
	// UnityEngine.GameObject Controll::countPetsBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___countPetsBox_27;
	// UnityEngine.GameObject Controll::score_box
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___score_box_28;
	// UnityEngine.GameObject Controll::text
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___text_29;
	// UnityEngine.GameObject Controll::textName
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___textName_30;
	// UnityEngine.GameObject Controll::boostIco
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___boostIco_31;
	// UnityEngine.GameObject Controll::shieldEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___shieldEffect_32;
	// UnityEngine.GameObject Controll::minionLoad
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___minionLoad_33;
	// UnityEngine.GameObject Controll::newPetEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___newPetEffect_34;
	// UnityEngine.GameObject Controll::killEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___killEffect_35;
	// System.Int32 Controll::countRank
	int32_t ___countRank_36;
	// System.Int32 Controll::chanceDouble
	int32_t ___chanceDouble_37;
	// UnityEngine.Color Controll::colorPlayer
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___colorPlayer_38;
	// System.Boolean Controll::StartedGame
	bool ___StartedGame_39;
	// UnityEngine.GameObject Controll::skinnn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___skinnn_40;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Controll::allMinions
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___allMinions_41;
	// System.Int32 Controll::numMinion
	int32_t ___numMinion_42;
	// System.Boolean Controll::clonned
	bool ___clonned_43;

public:
	inline static int32_t get_offset_of_minionPrabaf_4() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___minionPrabaf_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_minionPrabaf_4() const { return ___minionPrabaf_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_minionPrabaf_4() { return &___minionPrabaf_4; }
	inline void set_minionPrabaf_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___minionPrabaf_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___minionPrabaf_4), (void*)value);
	}

	inline static int32_t get_offset_of_maxMinions_5() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___maxMinions_5)); }
	inline int32_t get_maxMinions_5() const { return ___maxMinions_5; }
	inline int32_t* get_address_of_maxMinions_5() { return &___maxMinions_5; }
	inline void set_maxMinions_5(int32_t value)
	{
		___maxMinions_5 = value;
	}

	inline static int32_t get_offset_of_names_6() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___names_6)); }
	inline String_t* get_names_6() const { return ___names_6; }
	inline String_t** get_address_of_names_6() { return &___names_6; }
	inline void set_names_6(String_t* value)
	{
		___names_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___names_6), (void*)value);
	}

	inline static int32_t get_offset_of_bot_7() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___bot_7)); }
	inline bool get_bot_7() const { return ___bot_7; }
	inline bool* get_address_of_bot_7() { return &___bot_7; }
	inline void set_bot_7(bool value)
	{
		___bot_7 = value;
	}

	inline static int32_t get_offset_of_score_8() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___score_8)); }
	inline int32_t get_score_8() const { return ___score_8; }
	inline int32_t* get_address_of_score_8() { return &___score_8; }
	inline void set_score_8(int32_t value)
	{
		___score_8 = value;
	}

	inline static int32_t get_offset_of_scoreAll_9() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___scoreAll_9)); }
	inline int32_t get_scoreAll_9() const { return ___scoreAll_9; }
	inline int32_t* get_address_of_scoreAll_9() { return &___scoreAll_9; }
	inline void set_scoreAll_9(int32_t value)
	{
		___scoreAll_9 = value;
	}

	inline static int32_t get_offset_of_JoystickBase_10() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___JoystickBase_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_JoystickBase_10() const { return ___JoystickBase_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_JoystickBase_10() { return &___JoystickBase_10; }
	inline void set_JoystickBase_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___JoystickBase_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JoystickBase_10), (void*)value);
	}

	inline static int32_t get_offset_of_Stick_11() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___Stick_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Stick_11() const { return ___Stick_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Stick_11() { return &___Stick_11; }
	inline void set_Stick_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Stick_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Stick_11), (void*)value);
	}

	inline static int32_t get_offset_of_speed_12() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___speed_12)); }
	inline float get_speed_12() const { return ___speed_12; }
	inline float* get_address_of_speed_12() { return &___speed_12; }
	inline void set_speed_12(float value)
	{
		___speed_12 = value;
	}

	inline static int32_t get_offset_of_speedBonus_13() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___speedBonus_13)); }
	inline float get_speedBonus_13() const { return ___speedBonus_13; }
	inline float* get_address_of_speedBonus_13() { return &___speedBonus_13; }
	inline void set_speedBonus_13(float value)
	{
		___speedBonus_13 = value;
	}

	inline static int32_t get_offset_of_iSpeed_14() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___iSpeed_14)); }
	inline float get_iSpeed_14() const { return ___iSpeed_14; }
	inline float* get_address_of_iSpeed_14() { return &___iSpeed_14; }
	inline void set_iSpeed_14(float value)
	{
		___iSpeed_14 = value;
	}

	inline static int32_t get_offset_of_iShield_15() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___iShield_15)); }
	inline float get_iShield_15() const { return ___iShield_15; }
	inline float* get_address_of_iShield_15() { return &___iShield_15; }
	inline void set_iShield_15(float value)
	{
		___iShield_15 = value;
	}

	inline static int32_t get_offset_of_wohooCount_16() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___wohooCount_16)); }
	inline int32_t get_wohooCount_16() const { return ___wohooCount_16; }
	inline int32_t* get_address_of_wohooCount_16() { return &___wohooCount_16; }
	inline void set_wohooCount_16(int32_t value)
	{
		___wohooCount_16 = value;
	}

	inline static int32_t get_offset_of_voiceTimer_17() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___voiceTimer_17)); }
	inline float get_voiceTimer_17() const { return ___voiceTimer_17; }
	inline float* get_address_of_voiceTimer_17() { return &___voiceTimer_17; }
	inline void set_voiceTimer_17(float value)
	{
		___voiceTimer_17 = value;
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___timer_18)); }
	inline float get_timer_18() const { return ___timer_18; }
	inline float* get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(float value)
	{
		___timer_18 = value;
	}

	inline static int32_t get_offset_of_MainSystem_19() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___MainSystem_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MainSystem_19() const { return ___MainSystem_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MainSystem_19() { return &___MainSystem_19; }
	inline void set_MainSystem_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MainSystem_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainSystem_19), (void*)value);
	}

	inline static int32_t get_offset_of_countPet_20() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___countPet_20)); }
	inline int32_t get_countPet_20() const { return ___countPet_20; }
	inline int32_t* get_address_of_countPet_20() { return &___countPet_20; }
	inline void set_countPet_20(int32_t value)
	{
		___countPet_20 = value;
	}

	inline static int32_t get_offset_of_timer2_21() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___timer2_21)); }
	inline float get_timer2_21() const { return ___timer2_21; }
	inline float* get_address_of_timer2_21() { return &___timer2_21; }
	inline void set_timer2_21(float value)
	{
		___timer2_21 = value;
	}

	inline static int32_t get_offset_of_gradkaCount_22() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___gradkaCount_22)); }
	inline int32_t get_gradkaCount_22() const { return ___gradkaCount_22; }
	inline int32_t* get_address_of_gradkaCount_22() { return &___gradkaCount_22; }
	inline void set_gradkaCount_22(int32_t value)
	{
		___gradkaCount_22 = value;
	}

	inline static int32_t get_offset_of_target_23() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___target_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_target_23() const { return ___target_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_target_23() { return &___target_23; }
	inline void set_target_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___target_23 = value;
	}

	inline static int32_t get_offset_of_Hero_24() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___Hero_24)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Hero_24() const { return ___Hero_24; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Hero_24() { return &___Hero_24; }
	inline void set_Hero_24(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Hero_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Hero_24), (void*)value);
	}

	inline static int32_t get_offset_of_CreateMinionUpd_25() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___CreateMinionUpd_25)); }
	inline bool get_CreateMinionUpd_25() const { return ___CreateMinionUpd_25; }
	inline bool* get_address_of_CreateMinionUpd_25() { return &___CreateMinionUpd_25; }
	inline void set_CreateMinionUpd_25(bool value)
	{
		___CreateMinionUpd_25 = value;
	}

	inline static int32_t get_offset_of_countPetTemp_26() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___countPetTemp_26)); }
	inline int32_t get_countPetTemp_26() const { return ___countPetTemp_26; }
	inline int32_t* get_address_of_countPetTemp_26() { return &___countPetTemp_26; }
	inline void set_countPetTemp_26(int32_t value)
	{
		___countPetTemp_26 = value;
	}

	inline static int32_t get_offset_of_countPetsBox_27() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___countPetsBox_27)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_countPetsBox_27() const { return ___countPetsBox_27; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_countPetsBox_27() { return &___countPetsBox_27; }
	inline void set_countPetsBox_27(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___countPetsBox_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___countPetsBox_27), (void*)value);
	}

	inline static int32_t get_offset_of_score_box_28() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___score_box_28)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_score_box_28() const { return ___score_box_28; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_score_box_28() { return &___score_box_28; }
	inline void set_score_box_28(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___score_box_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___score_box_28), (void*)value);
	}

	inline static int32_t get_offset_of_text_29() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___text_29)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_text_29() const { return ___text_29; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_text_29() { return &___text_29; }
	inline void set_text_29(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___text_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_29), (void*)value);
	}

	inline static int32_t get_offset_of_textName_30() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___textName_30)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_textName_30() const { return ___textName_30; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_textName_30() { return &___textName_30; }
	inline void set_textName_30(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___textName_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textName_30), (void*)value);
	}

	inline static int32_t get_offset_of_boostIco_31() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___boostIco_31)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_boostIco_31() const { return ___boostIco_31; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_boostIco_31() { return &___boostIco_31; }
	inline void set_boostIco_31(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___boostIco_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boostIco_31), (void*)value);
	}

	inline static int32_t get_offset_of_shieldEffect_32() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___shieldEffect_32)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_shieldEffect_32() const { return ___shieldEffect_32; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_shieldEffect_32() { return &___shieldEffect_32; }
	inline void set_shieldEffect_32(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___shieldEffect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shieldEffect_32), (void*)value);
	}

	inline static int32_t get_offset_of_minionLoad_33() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___minionLoad_33)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_minionLoad_33() const { return ___minionLoad_33; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_minionLoad_33() { return &___minionLoad_33; }
	inline void set_minionLoad_33(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___minionLoad_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___minionLoad_33), (void*)value);
	}

	inline static int32_t get_offset_of_newPetEffect_34() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___newPetEffect_34)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_newPetEffect_34() const { return ___newPetEffect_34; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_newPetEffect_34() { return &___newPetEffect_34; }
	inline void set_newPetEffect_34(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___newPetEffect_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newPetEffect_34), (void*)value);
	}

	inline static int32_t get_offset_of_killEffect_35() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___killEffect_35)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_killEffect_35() const { return ___killEffect_35; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_killEffect_35() { return &___killEffect_35; }
	inline void set_killEffect_35(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___killEffect_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___killEffect_35), (void*)value);
	}

	inline static int32_t get_offset_of_countRank_36() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___countRank_36)); }
	inline int32_t get_countRank_36() const { return ___countRank_36; }
	inline int32_t* get_address_of_countRank_36() { return &___countRank_36; }
	inline void set_countRank_36(int32_t value)
	{
		___countRank_36 = value;
	}

	inline static int32_t get_offset_of_chanceDouble_37() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___chanceDouble_37)); }
	inline int32_t get_chanceDouble_37() const { return ___chanceDouble_37; }
	inline int32_t* get_address_of_chanceDouble_37() { return &___chanceDouble_37; }
	inline void set_chanceDouble_37(int32_t value)
	{
		___chanceDouble_37 = value;
	}

	inline static int32_t get_offset_of_colorPlayer_38() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___colorPlayer_38)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_colorPlayer_38() const { return ___colorPlayer_38; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_colorPlayer_38() { return &___colorPlayer_38; }
	inline void set_colorPlayer_38(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___colorPlayer_38 = value;
	}

	inline static int32_t get_offset_of_StartedGame_39() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___StartedGame_39)); }
	inline bool get_StartedGame_39() const { return ___StartedGame_39; }
	inline bool* get_address_of_StartedGame_39() { return &___StartedGame_39; }
	inline void set_StartedGame_39(bool value)
	{
		___StartedGame_39 = value;
	}

	inline static int32_t get_offset_of_skinnn_40() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___skinnn_40)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_skinnn_40() const { return ___skinnn_40; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_skinnn_40() { return &___skinnn_40; }
	inline void set_skinnn_40(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___skinnn_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinnn_40), (void*)value);
	}

	inline static int32_t get_offset_of_allMinions_41() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___allMinions_41)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_allMinions_41() const { return ___allMinions_41; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_allMinions_41() { return &___allMinions_41; }
	inline void set_allMinions_41(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___allMinions_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allMinions_41), (void*)value);
	}

	inline static int32_t get_offset_of_numMinion_42() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___numMinion_42)); }
	inline int32_t get_numMinion_42() const { return ___numMinion_42; }
	inline int32_t* get_address_of_numMinion_42() { return &___numMinion_42; }
	inline void set_numMinion_42(int32_t value)
	{
		___numMinion_42 = value;
	}

	inline static int32_t get_offset_of_clonned_43() { return static_cast<int32_t>(offsetof(Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09, ___clonned_43)); }
	inline bool get_clonned_43() const { return ___clonned_43; }
	inline bool* get_address_of_clonned_43() { return &___clonned_43; }
	inline void set_clonned_43(bool value)
	{
		___clonned_43 = value;
	}
};


// OnTextLayer
struct  OnTextLayer_tB9AFE4C1846C1E96D19887DE46A3C98471C4A931  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 OnTextLayer::Order
	int32_t ___Order_4;

public:
	inline static int32_t get_offset_of_Order_4() { return static_cast<int32_t>(offsetof(OnTextLayer_tB9AFE4C1846C1E96D19887DE46A3C98471C4A931, ___Order_4)); }
	inline int32_t get_Order_4() const { return ___Order_4; }
	inline int32_t* get_address_of_Order_4() { return &___Order_4; }
	inline void set_Order_4(int32_t value)
	{
		___Order_4 = value;
	}
};


// ParticlesBase
struct  ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] ParticlesBase::particles
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___particles_4;
	// UnityEngine.GameObject ParticlesBase::paren
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___paren_5;
	// UnityEngine.Vector3 ParticlesBase::pos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos_6;

public:
	inline static int32_t get_offset_of_particles_4() { return static_cast<int32_t>(offsetof(ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438, ___particles_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_particles_4() const { return ___particles_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_particles_4() { return &___particles_4; }
	inline void set_particles_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___particles_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particles_4), (void*)value);
	}

	inline static int32_t get_offset_of_paren_5() { return static_cast<int32_t>(offsetof(ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438, ___paren_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_paren_5() const { return ___paren_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_paren_5() { return &___paren_5; }
	inline void set_paren_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___paren_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paren_5), (void*)value);
	}

	inline static int32_t get_offset_of_pos_6() { return static_cast<int32_t>(offsetof(ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438, ___pos_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_pos_6() const { return ___pos_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_pos_6() { return &___pos_6; }
	inline void set_pos_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___pos_6 = value;
	}
};


// Rotate
struct  Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Rotate::RotVector3
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___RotVector3_4;

public:
	inline static int32_t get_offset_of_RotVector3_4() { return static_cast<int32_t>(offsetof(Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789, ___RotVector3_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_RotVector3_4() const { return ___RotVector3_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_RotVector3_4() { return &___RotVector3_4; }
	inline void set_RotVector3_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___RotVector3_4 = value;
	}
};


// ScaleZfromZero
struct  ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ScaleZfromZero::sc
	float ___sc_4;
	// System.Single ScaleZfromZero::startSc
	float ___startSc_5;
	// System.Int32 ScaleZfromZero::step
	int32_t ___step_6;
	// System.Single ScaleZfromZero::scale
	float ___scale_7;
	// System.Single ScaleZfromZero::zoomSpeed
	float ___zoomSpeed_8;
	// System.Single ScaleZfromZero::startSpeed
	float ___startSpeed_9;
	// System.Single ScaleZfromZero::speed
	float ___speed_10;
	// System.Int32 ScaleZfromZero::trig
	int32_t ___trig_11;

public:
	inline static int32_t get_offset_of_sc_4() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___sc_4)); }
	inline float get_sc_4() const { return ___sc_4; }
	inline float* get_address_of_sc_4() { return &___sc_4; }
	inline void set_sc_4(float value)
	{
		___sc_4 = value;
	}

	inline static int32_t get_offset_of_startSc_5() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___startSc_5)); }
	inline float get_startSc_5() const { return ___startSc_5; }
	inline float* get_address_of_startSc_5() { return &___startSc_5; }
	inline void set_startSc_5(float value)
	{
		___startSc_5 = value;
	}

	inline static int32_t get_offset_of_step_6() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___step_6)); }
	inline int32_t get_step_6() const { return ___step_6; }
	inline int32_t* get_address_of_step_6() { return &___step_6; }
	inline void set_step_6(int32_t value)
	{
		___step_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___scale_7)); }
	inline float get_scale_7() const { return ___scale_7; }
	inline float* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(float value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_8() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___zoomSpeed_8)); }
	inline float get_zoomSpeed_8() const { return ___zoomSpeed_8; }
	inline float* get_address_of_zoomSpeed_8() { return &___zoomSpeed_8; }
	inline void set_zoomSpeed_8(float value)
	{
		___zoomSpeed_8 = value;
	}

	inline static int32_t get_offset_of_startSpeed_9() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___startSpeed_9)); }
	inline float get_startSpeed_9() const { return ___startSpeed_9; }
	inline float* get_address_of_startSpeed_9() { return &___startSpeed_9; }
	inline void set_startSpeed_9(float value)
	{
		___startSpeed_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_trig_11() { return static_cast<int32_t>(offsetof(ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686, ___trig_11)); }
	inline int32_t get_trig_11() const { return ___trig_11; }
	inline int32_t* get_address_of_trig_11() { return &___trig_11; }
	inline void set_trig_11(int32_t value)
	{
		___trig_11 = value;
	}
};


// Shkala
struct  Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Shkala::fixS
	float ___fixS_4;
	// System.Single Shkala::timer
	float ___timer_5;

public:
	inline static int32_t get_offset_of_fixS_4() { return static_cast<int32_t>(offsetof(Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058, ___fixS_4)); }
	inline float get_fixS_4() const { return ___fixS_4; }
	inline float* get_address_of_fixS_4() { return &___fixS_4; }
	inline void set_fixS_4(float value)
	{
		___fixS_4 = value;
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058, ___timer_5)); }
	inline float get_timer_5() const { return ___timer_5; }
	inline float* get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(float value)
	{
		___timer_5 = value;
	}
};


// SkinCollector
struct  SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] SkinCollector::skins
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___skins_4;

public:
	inline static int32_t get_offset_of_skins_4() { return static_cast<int32_t>(offsetof(SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939, ___skins_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_skins_4() const { return ___skins_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_skins_4() { return &___skins_4; }
	inline void set_skins_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___skins_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skins_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UpgradePrice
struct  UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String UpgradePrice::prefsName
	String_t* ___prefsName_4;

public:
	inline static int32_t get_offset_of_prefsName_4() { return static_cast<int32_t>(offsetof(UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9, ___prefsName_4)); }
	inline String_t* get_prefsName_4() const { return ___prefsName_4; }
	inline String_t** get_address_of_prefsName_4() { return &___prefsName_4; }
	inline void set_prefsName_4(String_t* value)
	{
		___prefsName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefsName_4), (void*)value);
	}
};


// WeekReward
struct  WeekReward_t03D10DBBCBB5EDCAD14B1EC679997776BBB46CE2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// YourPlace
struct  YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject YourPlace::Hero
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Hero_4;
	// System.String YourPlace::ss
	String_t* ___ss_5;

public:
	inline static int32_t get_offset_of_Hero_4() { return static_cast<int32_t>(offsetof(YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3, ___Hero_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Hero_4() const { return ___Hero_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Hero_4() { return &___Hero_4; }
	inline void set_Hero_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Hero_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Hero_4), (void*)value);
	}

	inline static int32_t get_offset_of_ss_5() { return static_cast<int32_t>(offsetof(YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3, ___ss_5)); }
	inline String_t* get_ss_5() const { return ___ss_5; }
	inline String_t** get_address_of_ss_5() { return &___ss_5; }
	inline void set_ss_5(String_t* value)
	{
		___ss_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ss_5), (void*)value);
	}
};


// guiRankSystem
struct  guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject guiRankSystem::crown
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___crown_4;
	// UnityEngine.GameObject[] guiRankSystem::unitsGUI
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___unitsGUI_5;
	// UnityEngine.GameObject[] guiRankSystem::units
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___units_6;
	// UnityEngine.GameObject[] guiRankSystem::unitsTemp
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___unitsTemp_7;
	// System.Int32[] guiRankSystem::unitsScore
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___unitsScore_8;
	// System.Int32 guiRankSystem::tempScore
	int32_t ___tempScore_9;
	// System.Int32 guiRankSystem::tempUnit
	int32_t ___tempUnit_10;
	// System.Single guiRankSystem::timer
	float ___timer_11;
	// System.Boolean guiRankSystem::colSet
	bool ___colSet_12;

public:
	inline static int32_t get_offset_of_crown_4() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___crown_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_crown_4() const { return ___crown_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_crown_4() { return &___crown_4; }
	inline void set_crown_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___crown_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crown_4), (void*)value);
	}

	inline static int32_t get_offset_of_unitsGUI_5() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___unitsGUI_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_unitsGUI_5() const { return ___unitsGUI_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_unitsGUI_5() { return &___unitsGUI_5; }
	inline void set_unitsGUI_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___unitsGUI_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unitsGUI_5), (void*)value);
	}

	inline static int32_t get_offset_of_units_6() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___units_6)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_units_6() const { return ___units_6; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_units_6() { return &___units_6; }
	inline void set_units_6(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___units_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___units_6), (void*)value);
	}

	inline static int32_t get_offset_of_unitsTemp_7() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___unitsTemp_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_unitsTemp_7() const { return ___unitsTemp_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_unitsTemp_7() { return &___unitsTemp_7; }
	inline void set_unitsTemp_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___unitsTemp_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unitsTemp_7), (void*)value);
	}

	inline static int32_t get_offset_of_unitsScore_8() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___unitsScore_8)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_unitsScore_8() const { return ___unitsScore_8; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_unitsScore_8() { return &___unitsScore_8; }
	inline void set_unitsScore_8(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___unitsScore_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unitsScore_8), (void*)value);
	}

	inline static int32_t get_offset_of_tempScore_9() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___tempScore_9)); }
	inline int32_t get_tempScore_9() const { return ___tempScore_9; }
	inline int32_t* get_address_of_tempScore_9() { return &___tempScore_9; }
	inline void set_tempScore_9(int32_t value)
	{
		___tempScore_9 = value;
	}

	inline static int32_t get_offset_of_tempUnit_10() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___tempUnit_10)); }
	inline int32_t get_tempUnit_10() const { return ___tempUnit_10; }
	inline int32_t* get_address_of_tempUnit_10() { return &___tempUnit_10; }
	inline void set_tempUnit_10(int32_t value)
	{
		___tempUnit_10 = value;
	}

	inline static int32_t get_offset_of_timer_11() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___timer_11)); }
	inline float get_timer_11() const { return ___timer_11; }
	inline float* get_address_of_timer_11() { return &___timer_11; }
	inline void set_timer_11(float value)
	{
		___timer_11 = value;
	}

	inline static int32_t get_offset_of_colSet_12() { return static_cast<int32_t>(offsetof(guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15, ___colSet_12)); }
	inline bool get_colSet_12() const { return ___colSet_12; }
	inline bool* get_address_of_colSet_12() { return &___colSet_12; }
	inline void set_colSet_12(bool value)
	{
		___colSet_12 = value;
	}
};


// textCountPet
struct  textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single textCountPet::timer
	float ___timer_4;
	// System.Boolean textCountPet::textGo
	bool ___textGo_5;

public:
	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}

	inline static int32_t get_offset_of_textGo_5() { return static_cast<int32_t>(offsetof(textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5, ___textGo_5)); }
	inline bool get_textGo_5() const { return ___textGo_5; }
	inline bool* get_address_of_textGo_5() { return &___textGo_5; }
	inline void set_textGo_5(bool value)
	{
		___textGo_5 = value;
	}
};


// translateLanguages
struct  translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String translateLanguages::rus
	String_t* ___rus_4;
	// System.String translateLanguages::Oman
	String_t* ___Oman_5;
	// System.String translateLanguages::France
	String_t* ___France_6;
	// System.String translateLanguages::Vietnam
	String_t* ___Vietnam_7;
	// System.String translateLanguages::Brazil
	String_t* ___Brazil_8;
	// System.Boolean translateLanguages::canvass
	bool ___canvass_9;

public:
	inline static int32_t get_offset_of_rus_4() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___rus_4)); }
	inline String_t* get_rus_4() const { return ___rus_4; }
	inline String_t** get_address_of_rus_4() { return &___rus_4; }
	inline void set_rus_4(String_t* value)
	{
		___rus_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rus_4), (void*)value);
	}

	inline static int32_t get_offset_of_Oman_5() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___Oman_5)); }
	inline String_t* get_Oman_5() const { return ___Oman_5; }
	inline String_t** get_address_of_Oman_5() { return &___Oman_5; }
	inline void set_Oman_5(String_t* value)
	{
		___Oman_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Oman_5), (void*)value);
	}

	inline static int32_t get_offset_of_France_6() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___France_6)); }
	inline String_t* get_France_6() const { return ___France_6; }
	inline String_t** get_address_of_France_6() { return &___France_6; }
	inline void set_France_6(String_t* value)
	{
		___France_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___France_6), (void*)value);
	}

	inline static int32_t get_offset_of_Vietnam_7() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___Vietnam_7)); }
	inline String_t* get_Vietnam_7() const { return ___Vietnam_7; }
	inline String_t** get_address_of_Vietnam_7() { return &___Vietnam_7; }
	inline void set_Vietnam_7(String_t* value)
	{
		___Vietnam_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Vietnam_7), (void*)value);
	}

	inline static int32_t get_offset_of_Brazil_8() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___Brazil_8)); }
	inline String_t* get_Brazil_8() const { return ___Brazil_8; }
	inline String_t** get_address_of_Brazil_8() { return &___Brazil_8; }
	inline void set_Brazil_8(String_t* value)
	{
		___Brazil_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Brazil_8), (void*)value);
	}

	inline static int32_t get_offset_of_canvass_9() { return static_cast<int32_t>(offsetof(translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D, ___canvass_9)); }
	inline bool get_canvass_9() const { return ___canvass_9; }
	inline bool* get_address_of_canvass_9() { return &___canvass_9; }
	inline void set_canvass_9(bool value)
	{
		___canvass_9 = value;
	}
};


// wwwBlock
struct  wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single wwwBlock::timer
	float ___timer_4;

public:
	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);

// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulers0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84 (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986 (String_t* ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04 (TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<ButtonZfix>()
inline ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB * GameObject_AddComponent_TisButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB_m8DD0FC4A6D5411A3D20F2FE044585D557A302FDB (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.String System.Char::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8 (Il2CppChar* __this, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C (String_t* ___s0, const RuntimeMethod* method);
// System.Void UnityEngine.Component::set_tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_set_tag_m6E921BD86BD4A0B5114725FFF0CCD62F26BD7E81 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, String_t* ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B (String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<Controll>()
inline Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09 * GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.TextMesh>()
inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void guiRankSystem::SetColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_SetColor_mB823FA5562A471E47BFED28419C3F67A01A48FBC (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method);
// System.Void guiRankSystem::GetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_GetData_m56CE67AF7D07080B8AE97225C670278F59937BA9 (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_color_m276CE9AB9F88B34C0A9C6DD5300FC1123102D3DE (TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045 (const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.String UnityEngine.TextMesh::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B (TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * __this, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * Component_GetComponent_TisParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_m91CE0171326B90198E69EAFA60F45473CAC6E0C3 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OnTextLayer::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnTextLayer_Start_mF7F2FB5B8FF1EAC58BA2D3D9AB1B09972E9B841A (OnTextLayer_tB9AFE4C1846C1E96D19887DE46A3C98471C4A931 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Order == 0) Order = 600;
		int32_t L_0 = __this->get_Order_4();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// if (Order == 0) Order = 600;
		__this->set_Order_4(((int32_t)600));
	}

IL_0013:
	{
		// gameObject.GetComponent<MeshRenderer> ().sortingOrder = Order;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_2;
		L_2 = GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B(L_1, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		int32_t L_3 = __this->get_Order_4();
		NullCheck(L_2);
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void OnTextLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnTextLayer__ctor_mF03A2A242AFA619EDCB1023C8485A32F23F2D622 (OnTextLayer_tB9AFE4C1846C1E96D19887DE46A3C98471C4A931 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ParticlesBase::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticlesBase_Start_m5654D41E720DBD1D377501ADF028F2CC22540BF5 (ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ParticlesBase::SetPos(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticlesBase_SetPos_mF86860F03EDC7FD68B932FE486391928D61D19B1 (ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___p0, const RuntimeMethod* method)
{
	{
		// pos = p;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___p0;
		__this->set_pos_6(L_0);
		// }
		return;
	}
}
// System.Void ParticlesBase::SetParent(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticlesBase_SetParent_mF083317FF3617951BB5117E332B93A3837E9D01B (ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___p0, const RuntimeMethod* method)
{
	{
		// paren = p;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___p0;
		__this->set_paren_5(L_0);
		// }
		return;
	}
}
// System.Void ParticlesBase::ClonePart(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticlesBase_ClonePart_mD8FC8031DF075AC05E66171EC7F0110D193245A3 (ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		// var gg = Instantiate(particles[v], pos, particles[v].transform.rotation);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_particles_4();
		int32_t L_1 = ___v0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = __this->get_pos_6();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_5 = __this->get_particles_4();
		int32_t L_6 = ___v0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_10;
		L_10 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_3, L_4, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		V_0 = L_11;
		// if (paren != null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_paren_5();
		bool L_13;
		L_13 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_12, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_004a;
		}
	}
	{
		// gg.transform.parent = paren.transform;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = V_0;
		NullCheck(L_14);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_14, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = __this->get_paren_5();
		NullCheck(L_16);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_15, L_17, /*hidden argument*/NULL);
	}

IL_004a:
	{
		// paren = null;
		__this->set_paren_5((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
		// }
		return;
	}
}
// System.Void ParticlesBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticlesBase__ctor_m5AE7611AB30E112A5D5114EBC97FF3C7B1966255 (ParticlesBase_t6EAF318C62D810D54F053684A32C793DD4D4D438 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Rotate::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_Start_m260F3DA22D8BEE3571FB61596065ECC238B60968 (Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Rotate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_Update_m66D07F3686A017A63E869D8294C08E68F4A2FBAB (Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789 * __this, const RuntimeMethod* method)
{
	{
		// transform.Rotate(RotVector3*Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_RotVector3_4();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7(L_0, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate__ctor_mAB2884DA9234D7A6485C5662D97205C92CA9B9C4 (Rotate_tC69B3ABBABA1DB1DFD96A92633DF3743CBB57789 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ScaleZfromZero::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScaleZfromZero_Start_m94FD92CA50BC7F929373F7847EAB4FA924CA8C7E (ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686 * __this, const RuntimeMethod* method)
{
	{
		// sc = transform.localScale.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_2();
		__this->set_sc_4(L_2);
		// startSc = sc;
		float L_3 = __this->get_sc_4();
		__this->set_startSc_5(L_3);
		// speed = startSpeed;
		float L_4 = __this->get_startSpeed_9();
		__this->set_speed_10(L_4);
		// if (speed == 0) speed = 0.3f;
		float L_5 = __this->get_speed_10();
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			goto IL_0046;
		}
	}
	{
		// if (speed == 0) speed = 0.3f;
		__this->set_speed_10((0.300000012f));
	}

IL_0046:
	{
		// if (scale == 0) scale = 1.2f;
		float L_6 = __this->get_scale_7();
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		// if (scale == 0) scale = 1.2f;
		__this->set_scale_7((1.20000005f));
	}

IL_005e:
	{
		// step = 1;
		__this->set_step_6(1);
		// sc = 0.01f;
		__this->set_sc_4((0.00999999978f));
		// transform.localScale = Vector3.zero;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ScaleZfromZero::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScaleZfromZero_Update_mFA24A4D046131464927D650A2C85419327188E4B (ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686 * __this, const RuntimeMethod* method)
{
	{
		// if (step == 0)
		int32_t L_0 = __this->get_step_6();
		if (L_0)
		{
			goto IL_005f;
		}
	}
	{
		// sc += Time.unscaledDeltaTime * speed;
		float L_1 = __this->get_sc_4();
		float L_2;
		L_2 = Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84(/*hidden argument*/NULL);
		float L_3 = __this->get_speed_10();
		__this->set_sc_4(((float)il2cpp_codegen_add((float)L_1, (float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)))));
		// transform.localScale = new Vector3(sc,sc,1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_sc_4();
		float L_6 = __this->get_sc_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_7), L_5, L_6, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_4, L_7, /*hidden argument*/NULL);
		// if (sc > startSc * scale){
		float L_8 = __this->get_sc_4();
		float L_9 = __this->get_startSc_5();
		float L_10 = __this->get_scale_7();
		if ((!(((float)L_8) > ((float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_10))))))
		{
			goto IL_00d1;
		}
	}
	{
		// step = 1;
		__this->set_step_6(1);
		// }
		return;
	}

IL_005f:
	{
		// sc -= Time.unscaledDeltaTime * speed;
		float L_11 = __this->get_sc_4();
		float L_12;
		L_12 = Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84(/*hidden argument*/NULL);
		float L_13 = __this->get_speed_10();
		__this->set_sc_4(((float)il2cpp_codegen_subtract((float)L_11, (float)((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)))));
		// transform.localScale = new Vector3(sc,sc,1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_sc_4();
		float L_16 = __this->get_sc_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		memset((&L_17), 0, sizeof(L_17));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_17), L_15, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_14, L_17, /*hidden argument*/NULL);
		// if (sc < startSc){
		float L_18 = __this->get_sc_4();
		float L_19 = __this->get_startSc_5();
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00d1;
		}
	}
	{
		// step = 0;
		__this->set_step_6(0);
		// trig++;
		int32_t L_20 = __this->get_trig_11();
		__this->set_trig_11(((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)));
		// if (trig >1) speed = zoomSpeed;
		int32_t L_21 = __this->get_trig_11();
		if ((((int32_t)L_21) <= ((int32_t)1)))
		{
			goto IL_00d1;
		}
	}
	{
		// if (trig >1) speed = zoomSpeed;
		float L_22 = __this->get_zoomSpeed_8();
		__this->set_speed_10(L_22);
	}

IL_00d1:
	{
		// }
		return;
	}
}
// System.Void ScaleZfromZero::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScaleZfromZero__ctor_mD7FAFBFDBE482A430F986C2F0484B6C7D6AC3365 (ScaleZfromZero_t066056B0BC9F48B366CE10DEC03BA4AFEA1F7686 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Shkala::SetScore(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shkala_SetScore_mAF61E2CEAD6676FB739C2C5E907DBD1202446A52 (Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058 * __this, int32_t ___s0, const RuntimeMethod* method)
{
	{
		// fixS = s / 100f;
		int32_t L_0 = ___s0;
		__this->set_fixS_4(((float)((float)((float)((float)L_0))/(float)(100.0f))));
		// timer = 0;
		__this->set_timer_5((0.0f));
		// }
		return;
	}
}
// System.Void Shkala::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shkala_Update_m238314D88236D379D17637641B3893B42AFD1E26 (Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058 * __this, const RuntimeMethod* method)
{
	{
		// if (timer < 3)
		float L_0 = __this->get_timer_5();
		if ((!(((float)L_0) < ((float)(3.0f)))))
		{
			goto IL_005a;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_1 = __this->get_timer_5();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_5(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(fixS, 1, 1), Time.deltaTime * 10);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_fixS_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_7), L_6, (1.0f), (1.0f), /*hidden argument*/NULL);
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_5, L_7, ((float)il2cpp_codegen_multiply((float)L_8, (float)(10.0f))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_3, L_9, /*hidden argument*/NULL);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Shkala::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shkala__ctor_m6B103A6508D0A28B8800DBE55914EC6E099AF16E (Shkala_t1DA3C57C59836E50699F3AAD43F8A761FA25C058 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SkinCollector::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinCollector_Start_m1FFAA068442F683F6C0AED646159AFD52D1A1415 (SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void SkinCollector::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinCollector_Update_mE89154F3BDAEDF5763F8E0C9CC9908BF280B8FA7 (SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void SkinCollector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinCollector__ctor_m5D3F23077A752A37702C6400D3B8BDE4902EEFEB (SkinCollector_tDBD3306C0297A25F96E71C595FA258397B915939 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UpgradePrice::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpgradePrice_Start_mCF58BD0B9804E35AACD8C11E5C1E951EBB728AED (UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* G_B7_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B7_1 = NULL;
	String_t* G_B6_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B6_1 = NULL;
	{
		// pr = 1200;
		V_0 = ((int32_t)1200);
		// for(int i = 0; i < PlayerPrefs.GetInt (prefsName)-1; i++)
		V_1 = 0;
		goto IL_0020;
	}

IL_000a:
	{
		// if (PlayerPrefs.GetInt (prefsName) > 1) pr *= 2;
		String_t* L_0 = __this->get_prefsName_4();
		int32_t L_1;
		L_1 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		// if (PlayerPrefs.GetInt (prefsName) > 1) pr *= 2;
		int32_t L_2 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_2, (int32_t)2));
	}

IL_001c:
	{
		// for(int i = 0; i < PlayerPrefs.GetInt (prefsName)-1; i++)
		int32_t L_3 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0020:
	{
		// for(int i = 0; i < PlayerPrefs.GetInt (prefsName)-1; i++)
		int32_t L_4 = V_1;
		String_t* L_5 = __this->get_prefsName_4();
		int32_t L_6;
		L_6 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)))))
		{
			goto IL_000a;
		}
	}
	{
		// GetComponent<TextMesh>().text = pr + "";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_7;
		L_7 = Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_9 = L_8;
		G_B6_0 = L_9;
		G_B6_1 = L_7;
		if (L_9)
		{
			G_B7_0 = L_9;
			G_B7_1 = L_7;
			goto IL_0046;
		}
	}
	{
		G_B7_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B7_1 = G_B6_1;
	}

IL_0046:
	{
		NullCheck(G_B7_1);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B7_1, G_B7_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UpgradePrice::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpgradePrice_Update_mEB4135A78509C3F45DAC05FD964735C7B5317B2C (UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UpgradePrice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpgradePrice__ctor_mC1DDBB48094E06C4F804C7CEE9C340E47E9164FB (UpgradePrice_t9F5C86875C2B6589E7C735CDD2DE5E24EF805BB9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeekReward::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeekReward_Start_m97316E0634CEF5BF4D2F82DADE625D9414E8C2FB (WeekReward_t03D10DBBCBB5EDCAD14B1EC679997776BBB46CE2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB_m8DD0FC4A6D5411A3D20F2FE044585D557A302FDB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0D0221001FCE79003404D8010C7056484DCFC6A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8786B3C8228E5D838EA3026028F99E8CD2F3BC7A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB59F0EFCC2B82196C428CFD6D80A08F8B47573FC);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	{
		// if (name == "b_day" + PlayerPrefs.GetInt("weekDay"))
		String_t* L_0;
		L_0 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(__this, /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral8786B3C8228E5D838EA3026028F99E8CD2F3BC7A, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2;
		L_2 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_3;
		L_3 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral0D0221001FCE79003404D8010C7056484DCFC6A3, L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_0, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		// GetComponent<SpriteRenderer>().color = new Color(0,1,0,1f);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_5;
		L_5 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_6), (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_5, L_6, /*hidden argument*/NULL);
		// gameObject.AddComponent<ButtonZfix>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		ButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB * L_8;
		L_8 = GameObject_AddComponent_TisButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB_m8DD0FC4A6D5411A3D20F2FE044585D557A302FDB(L_7, /*hidden argument*/GameObject_AddComponent_TisButtonZfix_t8927E2F5D36E0D249B63D89BA8E11DBD887B2ECB_m8DD0FC4A6D5411A3D20F2FE044585D557A302FDB_RuntimeMethod_var);
		// }
		return;
	}

IL_005a:
	{
		// GetComponent<SpriteRenderer>().color = new Color(1,1,1,0.3f);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_9;
		L_9 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_10), (1.0f), (1.0f), (1.0f), (0.300000012f), /*hidden argument*/NULL);
		NullCheck(L_9);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_9, L_10, /*hidden argument*/NULL);
		// if (int.Parse(name[5].ToString()) < PlayerPrefs.GetInt("weekDay"))
		String_t* L_11;
		L_11 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Il2CppChar L_12;
		L_12 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_11, 5, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13;
		L_13 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)(&V_1), /*hidden argument*/NULL);
		int32_t L_14;
		L_14 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_13, /*hidden argument*/NULL);
		int32_t L_15;
		L_15 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral8786B3C8228E5D838EA3026028F99E8CD2F3BC7A, /*hidden argument*/NULL);
		if ((((int32_t)L_14) >= ((int32_t)L_15)))
		{
			goto IL_00c7;
		}
	}
	{
		// GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.3f);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_16;
		L_16 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_17;
		memset((&L_17), 0, sizeof(L_17));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_17), (0.0f), (0.0f), (0.0f), (0.300000012f), /*hidden argument*/NULL);
		NullCheck(L_16);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		// tag = "Untagged";
		Component_set_tag_m6E921BD86BD4A0B5114725FFF0CCD62F26BD7E81(__this, _stringLiteralB59F0EFCC2B82196C428CFD6D80A08F8B47573FC, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WeekReward::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeekReward_Update_mA6EF8049FFA73A62524785C4912C2A771A88A23D (WeekReward_t03D10DBBCBB5EDCAD14B1EC679997776BBB46CE2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void WeekReward::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeekReward__ctor_mD44273D9E21AED7C1513A9B6D8B28C81275374B5 (WeekReward_t03D10DBBCBB5EDCAD14B1EC679997776BBB46CE2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void YourPlace::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YourPlace_Awake_m96E4A9E1757509F60DDD030AE9E5AB89956D32E3 (YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void YourPlace::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YourPlace_Update_m321AC7BE750DB85C875132054CA34F98FC01B2B0 (YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral073138638D196EB8FEC8AFDE609B8EFA374C5446);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2EC66281BFC74AA5FE28EA5424D8A08C9CC2A8E0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3070995B7FCD12FF8158CDD4C8DF16E4878D533D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral34ACEF827EC0C7AA8CAF48045E67A5546F64AB4B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5AC799D9AAAF31CC04AC171870B3550DDBF029C0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5BDF37FF2609B4D78AB0AE5F9E4897F84727A58E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral61728EAE200F766A4FA68673634F3C01D677726E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral777E4B7B2E7904F4FB07ABF0FDCAADD0A4734A8A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA57B623302DFD4D4C2F3E547EC3AFE5B3A705FB);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float pos = GameObject.Find("numHero").transform.localPosition.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B(_stringLiteral2EC66281BFC74AA5FE28EA5424D8A08C9CC2A8E0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_y_3();
		V_0 = L_3;
		// ss = "-";
		__this->set_ss_5(_stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0);
		// if (GameObject.Find("Hero"))
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B(_stringLiteral61728EAE200F766A4FA68673634F3C01D677726E, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00ce;
		}
	}
	{
		// if (pos == -2.5)
		float L_6 = V_0;
		if ((!(((double)((double)((double)L_6))) == ((double)(-2.5)))))
		{
			goto IL_005b;
		}
	}
	{
		// ss = "6th";
		__this->set_ss_5(_stringLiteralFA57B623302DFD4D4C2F3E547EC3AFE5B3A705FB);
		// Debug.Log("ASFASF");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral5AC799D9AAAF31CC04AC171870B3550DDBF029C0, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// if (pos == -2)
		float L_7 = V_0;
		if ((!(((float)L_7) == ((float)(-2.0f)))))
		{
			goto IL_0078;
		}
	}
	{
		// ss = "5th";
		__this->set_ss_5(_stringLiteral3070995B7FCD12FF8158CDD4C8DF16E4878D533D);
		// Debug.Log("ASFASF");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral5AC799D9AAAF31CC04AC171870B3550DDBF029C0, /*hidden argument*/NULL);
	}

IL_0078:
	{
		// if (pos == -1.5)
		float L_8 = V_0;
		if ((!(((double)((double)((double)L_8))) == ((double)(-1.5)))))
		{
			goto IL_0090;
		}
	}
	{
		// ss = "4th";
		__this->set_ss_5(_stringLiteral777E4B7B2E7904F4FB07ABF0FDCAADD0A4734A8A);
	}

IL_0090:
	{
		// if (pos == -1)
		float L_9 = V_0;
		if ((!(((float)L_9) == ((float)(-1.0f)))))
		{
			goto IL_00a3;
		}
	}
	{
		// ss = "3rd";
		__this->set_ss_5(_stringLiteral5BDF37FF2609B4D78AB0AE5F9E4897F84727A58E);
	}

IL_00a3:
	{
		// if (pos == -0.5)
		float L_10 = V_0;
		if ((!(((double)((double)((double)L_10))) == ((double)(-0.5)))))
		{
			goto IL_00bb;
		}
	}
	{
		// ss = "2nd";
		__this->set_ss_5(_stringLiteral34ACEF827EC0C7AA8CAF48045E67A5546F64AB4B);
	}

IL_00bb:
	{
		// if (pos == 0)
		float L_11 = V_0;
		if ((!(((float)L_11) == ((float)(0.0f)))))
		{
			goto IL_00ce;
		}
	}
	{
		// ss = "1st";
		__this->set_ss_5(_stringLiteral073138638D196EB8FEC8AFDE609B8EFA374C5446);
	}

IL_00ce:
	{
		// GetComponent<TextMesh>().text = ss;
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_12;
		L_12 = Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		String_t* L_13 = __this->get_ss_5();
		NullCheck(L_12);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_12, L_13, /*hidden argument*/NULL);
		// Destroy((this));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void YourPlace::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YourPlace__ctor_mE2938F71984B009DE0678C3126F1D84E10F09CA9 (YourPlace_t534A25E038448606C37EC18CCA57ED42B60F4EF3 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void guiRankSystem::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_Start_mC851CCB3C96B4C69BFD1581B7DE1BB1D0705EDB1 (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	{
		// colSet = false;
		__this->set_colSet_12((bool)0);
		// }
		return;
	}
}
// System.Void guiRankSystem::SetColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_SetColor_mB823FA5562A471E47BFED28419C3F67A01A48FBC (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral172BBFD23CD800C2FFE73DF068B95520FE05A383);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE18B047107AA23D1AA9B2ED32D316148E02655F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	String_t* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// for(int i = 0; i < 10; i++)
		V_0 = 0;
		goto IL_00ce;
	}

IL_0007:
	{
		// foreach (Transform child in unitsGUI[i].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_unitsGUI_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject* L_5;
		L_5 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a9;
		}

IL_001f:
		{
			// foreach (Transform child in unitsGUI[i].transform)
			RuntimeObject* L_6 = V_1;
			NullCheck(L_6);
			RuntimeObject * L_7;
			L_7 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_6);
			V_2 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_7, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "bgCount") child.gameObject.GetComponent<SpriteRenderer>().color = units[i].GetComponent<Controll>().colorPlayer;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8 = V_2;
			NullCheck(L_8);
			String_t* L_9;
			L_9 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_8, /*hidden argument*/NULL);
			bool L_10;
			L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_9, _stringLiteral172BBFD23CD800C2FFE73DF068B95520FE05A383, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_005f;
			}
		}

IL_003d:
		{
			// if (child.name == "bgCount") child.gameObject.GetComponent<SpriteRenderer>().color = units[i].GetComponent<Controll>().colorPlayer;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = V_2;
			NullCheck(L_11);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
			L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_13;
			L_13 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_12, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
			GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_14 = __this->get_units_6();
			int32_t L_15 = V_0;
			NullCheck(L_14);
			int32_t L_16 = L_15;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
			NullCheck(L_17);
			Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09 * L_18;
			L_18 = GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683(L_17, /*hidden argument*/GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var);
			NullCheck(L_18);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_19 = L_18->get_colorPlayer_38();
			NullCheck(L_13);
			SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_13, L_19, /*hidden argument*/NULL);
		}

IL_005f:
		{
			// if (child.name == "name"){
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20 = V_2;
			NullCheck(L_20);
			String_t* L_21;
			L_21 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_20, /*hidden argument*/NULL);
			bool L_22;
			L_22 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_21, _stringLiteralCE18B047107AA23D1AA9B2ED32D316148E02655F, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_00a9;
			}
		}

IL_0071:
		{
			// string sss = units[i].GetComponent<Controll>().names;
			GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_23 = __this->get_units_6();
			int32_t L_24 = V_0;
			NullCheck(L_23);
			int32_t L_25 = L_24;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
			NullCheck(L_26);
			Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09 * L_27;
			L_27 = GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683(L_26, /*hidden argument*/GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var);
			NullCheck(L_27);
			String_t* L_28 = L_27->get_names_6();
			V_3 = L_28;
			// if (sss.Length > 10){
			String_t* L_29 = V_3;
			NullCheck(L_29);
			int32_t L_30;
			L_30 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_29, /*hidden argument*/NULL);
			if ((((int32_t)L_30) <= ((int32_t)((int32_t)10))))
			{
				goto IL_0098;
			}
		}

IL_008e:
		{
			// sss = sss.Substring(0,11);
			String_t* L_31 = V_3;
			NullCheck(L_31);
			String_t* L_32;
			L_32 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_31, 0, ((int32_t)11), /*hidden argument*/NULL);
			V_3 = L_32;
		}

IL_0098:
		{
			// child.gameObject.GetComponent<TextMesh>().text = sss;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33 = V_2;
			NullCheck(L_33);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34;
			L_34 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_33, /*hidden argument*/NULL);
			NullCheck(L_34);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_35;
			L_35 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_34, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			String_t* L_36 = V_3;
			NullCheck(L_35);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_35, L_36, /*hidden argument*/NULL);
		}

IL_00a9:
		{
			// foreach (Transform child in unitsGUI[i].transform)
			RuntimeObject* L_37 = V_1;
			NullCheck(L_37);
			bool L_38;
			L_38 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_001f;
			}
		}

IL_00b4:
		{
			IL2CPP_LEAVE(0xCA, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_39 = V_1;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_39, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_40 = V_4;
			if (!L_40)
			{
				goto IL_00c9;
			}
		}

IL_00c2:
		{
			RuntimeObject* L_41 = V_4;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_41);
		}

IL_00c9:
		{
			IL2CPP_END_FINALLY(182)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xCA, IL_00ca)
	}

IL_00ca:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_42 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00ce:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_43 = V_0;
		if ((((int32_t)L_43) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void guiRankSystem::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_FixedUpdate_m164FE1B89D696B0C9CB3CC2A9CD962CF62E13764 (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (unitsTemp[0]) crown.transform.position = unitsTemp[0].transform.position;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_unitsTemp_7();
		NullCheck(L_0);
		int32_t L_1 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		// if (unitsTemp[0]) crown.transform.position = unitsTemp[0].transform.position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_crown_4();
		NullCheck(L_4);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_4, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_6 = __this->get_unitsTemp_7();
		NullCheck(L_6);
		int32_t L_7 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_5, L_10, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void guiRankSystem::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_Update_m860BBA2C2ED0F08A745AB405BE1EE56D741D6D53 (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	{
		// timer += Time.deltaTime;
		float L_0 = __this->get_timer_11();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_11(((float)il2cpp_codegen_add((float)L_0, (float)L_1)));
		// if (timer > 1)
		float L_2 = __this->get_timer_11();
		if ((!(((float)L_2) > ((float)(1.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		// timer = 0;
		__this->set_timer_11((0.0f));
		// if (colSet == false)
		bool L_3 = __this->get_colSet_12();
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		// colSet = true;
		__this->set_colSet_12((bool)1);
		// SetColor();
		guiRankSystem_SetColor_mB823FA5562A471E47BFED28419C3F67A01A48FBC(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// GetData();
		guiRankSystem_GetData_m56CE67AF7D07080B8AE97225C670278F59937BA9(__this, /*hidden argument*/NULL);
	}

IL_0045:
	{
		// }
		return;
	}
}
// System.Void guiRankSystem::GetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem_GetData_m56CE67AF7D07080B8AE97225C670278F59937BA9 (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral07624473F417C06C74D59C64840A1532FCE2C626);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	int32_t V_5 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_6 = NULL;
	int32_t V_7 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_8 = NULL;
	int32_t V_9 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_10 = NULL;
	int32_t V_11 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_12 = NULL;
	int32_t V_13 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_14 = NULL;
	int32_t V_15 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_16 = NULL;
	int32_t V_17 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_18 = NULL;
	int32_t V_19 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_20 = NULL;
	int32_t V_21 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_22 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 10> __leave_targets;
	String_t* G_B18_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B18_1 = NULL;
	String_t* G_B17_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B17_1 = NULL;
	String_t* G_B36_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B36_1 = NULL;
	String_t* G_B35_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B35_1 = NULL;
	String_t* G_B55_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B55_1 = NULL;
	String_t* G_B54_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B54_1 = NULL;
	String_t* G_B75_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B75_1 = NULL;
	String_t* G_B74_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B74_1 = NULL;
	String_t* G_B96_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B96_1 = NULL;
	String_t* G_B95_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B95_1 = NULL;
	String_t* G_B118_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B118_1 = NULL;
	String_t* G_B117_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B117_1 = NULL;
	String_t* G_B141_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B141_1 = NULL;
	String_t* G_B140_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B140_1 = NULL;
	String_t* G_B165_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B165_1 = NULL;
	String_t* G_B164_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B164_1 = NULL;
	String_t* G_B190_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B190_1 = NULL;
	String_t* G_B189_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B189_1 = NULL;
	String_t* G_B216_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B216_1 = NULL;
	String_t* G_B215_0 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B215_1 = NULL;
	{
		// for(int i = 0; i < 10; i++)
		V_0 = 0;
		goto IL_0058;
	}

IL_0004:
	{
		// unitsScore[i] = 0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = __this->get_unitsScore_8();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)0);
		// if (units[i]){ unitsScore[i] = units[i].GetComponent<Controll>().countPet;}
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = __this->get_units_6();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		// if (units[i]){ unitsScore[i] = units[i].GetComponent<Controll>().countPet;}
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_7 = __this->get_unitsScore_8();
		int32_t L_8 = V_0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_9 = __this->get_units_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Controll_t76017577F87821B56EDB803283D60EF4AB4CBA09 * L_13;
		L_13 = GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683(L_12, /*hidden argument*/GameObject_GetComponent_TisControll_t76017577F87821B56EDB803283D60EF4AB4CBA09_m7AE3FD06CB3986F7F4BE44FAD20BA62E60AA2683_RuntimeMethod_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_countPet_20();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_14);
		// if (units[i]){ unitsScore[i] = units[i].GetComponent<Controll>().countPet;}
		goto IL_0054;
	}

IL_0038:
	{
		// if (unitsGUI[i]) Destroy(unitsGUI[i]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_15 = __this->get_unitsGUI_5();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_19;
		L_19 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0054;
		}
	}
	{
		// if (unitsGUI[i]) Destroy(unitsGUI[i]);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_20 = __this->get_unitsGUI_5();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_23, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_24 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_0058:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) < ((int32_t)((int32_t)10))))
		{
			goto IL_0004;
		}
	}
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_1 = 0;
		goto IL_00a1;
	}

IL_0068:
	{
		// if (tempScore < unitsScore[i])
		int32_t L_26 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = __this->get_unitsScore_8();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		int32_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		if ((((int32_t)L_26) >= ((int32_t)L_30)))
		{
			goto IL_009d;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = __this->get_unitsScore_8();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		int32_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		__this->set_tempScore_9(L_34);
		// unitsTemp[0] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_35 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_36 = __this->get_units_6();
		int32_t L_37 = V_1;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_39);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_39);
		// tempUnit = i;
		int32_t L_40 = V_1;
		__this->set_tempUnit_10(L_40);
	}

IL_009d:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_41 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00a1:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_42 = V_1;
		if ((((int32_t)L_42) < ((int32_t)((int32_t)10))))
		{
			goto IL_0068;
		}
	}
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_43 = __this->get_unitsGUI_5();
		int32_t L_44 = __this->get_tempUnit_10();
		NullCheck(L_43);
		int32_t L_45 = L_44;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_47;
		L_47 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01cb;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,0,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_48 = __this->get_unitsGUI_5();
		int32_t L_49 = __this->get_tempUnit_10();
		NullCheck(L_48);
		int32_t L_50 = L_49;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_51);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_52;
		L_52 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_51, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		memset((&L_53), 0, sizeof(L_53));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_53), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_52, L_53, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_54 = __this->get_unitsGUI_5();
		int32_t L_55 = __this->get_tempUnit_10();
		NullCheck(L_54);
		int32_t L_56 = L_55;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_58;
		L_58 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		RuntimeObject* L_59;
		L_59 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_58, /*hidden argument*/NULL);
		V_2 = L_59;
	}

IL_0100:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0144;
		}

IL_0102:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_60 = V_2;
			NullCheck(L_60);
			RuntimeObject * L_61;
			L_61 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_60);
			V_3 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_61, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_62 = V_3;
			NullCheck(L_62);
			String_t* L_63;
			L_63 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_62, /*hidden argument*/NULL);
			bool L_64;
			L_64 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_63, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_64)
			{
				goto IL_0144;
			}
		}

IL_0120:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_65 = V_3;
			NullCheck(L_65);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_66;
			L_66 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_65, /*hidden argument*/NULL);
			NullCheck(L_66);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_67;
			L_67 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_66, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_68 = __this->get_address_of_tempScore_9();
			String_t* L_69;
			L_69 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_68, /*hidden argument*/NULL);
			String_t* L_70 = L_69;
			G_B17_0 = L_70;
			G_B17_1 = L_67;
			if (L_70)
			{
				G_B18_0 = L_70;
				G_B18_1 = L_67;
				goto IL_013f;
			}
		}

IL_0139:
		{
			G_B18_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B18_1 = G_B17_1;
		}

IL_013f:
		{
			NullCheck(G_B18_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B18_1, G_B18_0, /*hidden argument*/NULL);
		}

IL_0144:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_71 = V_2;
			NullCheck(L_71);
			bool L_72;
			L_72 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_71);
			if (L_72)
			{
				goto IL_0102;
			}
		}

IL_014c:
		{
			IL2CPP_LEAVE(0x162, FINALLY_014e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_014e;
	}

FINALLY_014e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_73 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_73, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_74 = V_4;
			if (!L_74)
			{
				goto IL_0161;
			}
		}

IL_015a:
		{
			RuntimeObject* L_75 = V_4;
			NullCheck(L_75);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_75);
		}

IL_0161:
		{
			IL2CPP_END_FINALLY(334)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(334)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x162, IL_0162)
	}

IL_0162:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_5 = 0;
		goto IL_01c5;
	}

IL_016e:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0])
		int32_t L_76 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_77 = __this->get_unitsScore_8();
		int32_t L_78 = V_5;
		NullCheck(L_77);
		int32_t L_79 = L_78;
		int32_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		if ((((int32_t)L_76) >= ((int32_t)L_80)))
		{
			goto IL_01bf;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_81 = __this->get_units_6();
		int32_t L_82 = V_5;
		NullCheck(L_81);
		int32_t L_83 = L_82;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_85 = __this->get_unitsTemp_7();
		NullCheck(L_85);
		int32_t L_86 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_87 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_88;
		L_88 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_84, L_87, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_01bf;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_89 = __this->get_unitsScore_8();
		int32_t L_90 = V_5;
		NullCheck(L_89);
		int32_t L_91 = L_90;
		int32_t L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		__this->set_tempScore_9(L_92);
		// unitsTemp[1] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_93 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_94 = __this->get_units_6();
		int32_t L_95 = V_5;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		NullCheck(L_93);
		ArrayElementTypeCheck (L_93, L_97);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(1), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_97);
		// tempUnit = i;
		int32_t L_98 = V_5;
		__this->set_tempUnit_10(L_98);
	}

IL_01bf:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_99 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_99, (int32_t)1));
	}

IL_01c5:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_100 = V_5;
		if ((((int32_t)L_100) < ((int32_t)((int32_t)10))))
		{
			goto IL_016e;
		}
	}

IL_01cb:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_101 = __this->get_unitsGUI_5();
		int32_t L_102 = __this->get_tempUnit_10();
		NullCheck(L_101);
		int32_t L_103 = L_102;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_104 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_105;
		L_105 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_030b;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-0.5f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_106 = __this->get_unitsGUI_5();
		int32_t L_107 = __this->get_tempUnit_10();
		NullCheck(L_106);
		int32_t L_108 = L_107;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		NullCheck(L_109);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_110;
		L_110 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_109, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_111;
		memset((&L_111), 0, sizeof(L_111));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_111), (0.0f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_110);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_110, L_111, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_112 = __this->get_unitsGUI_5();
		int32_t L_113 = __this->get_tempUnit_10();
		NullCheck(L_112);
		int32_t L_114 = L_113;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_115 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_114));
		NullCheck(L_115);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_116;
		L_116 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		RuntimeObject* L_117;
		L_117 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_116, /*hidden argument*/NULL);
		V_2 = L_117;
	}

IL_0225:
	try
	{ // begin try (depth: 1)
		{
			goto IL_026c;
		}

IL_0227:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_118 = V_2;
			NullCheck(L_118);
			RuntimeObject * L_119;
			L_119 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_118);
			V_6 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_119, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_120 = V_6;
			NullCheck(L_120);
			String_t* L_121;
			L_121 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_120, /*hidden argument*/NULL);
			bool L_122;
			L_122 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_121, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_122)
			{
				goto IL_026c;
			}
		}

IL_0247:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_123 = V_6;
			NullCheck(L_123);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_124;
			L_124 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_123, /*hidden argument*/NULL);
			NullCheck(L_124);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_125;
			L_125 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_124, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_126 = __this->get_address_of_tempScore_9();
			String_t* L_127;
			L_127 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_126, /*hidden argument*/NULL);
			String_t* L_128 = L_127;
			G_B35_0 = L_128;
			G_B35_1 = L_125;
			if (L_128)
			{
				G_B36_0 = L_128;
				G_B36_1 = L_125;
				goto IL_0267;
			}
		}

IL_0261:
		{
			G_B36_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B36_1 = G_B35_1;
		}

IL_0267:
		{
			NullCheck(G_B36_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B36_1, G_B36_0, /*hidden argument*/NULL);
		}

IL_026c:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_129 = V_2;
			NullCheck(L_129);
			bool L_130;
			L_130 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_129);
			if (L_130)
			{
				goto IL_0227;
			}
		}

IL_0274:
		{
			IL2CPP_LEAVE(0x28A, FINALLY_0276);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0276;
	}

FINALLY_0276:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_131 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_131, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_132 = V_4;
			if (!L_132)
			{
				goto IL_0289;
			}
		}

IL_0282:
		{
			RuntimeObject* L_133 = V_4;
			NullCheck(L_133);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_133);
		}

IL_0289:
		{
			IL2CPP_END_FINALLY(630)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(630)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x28A, IL_028a)
	}

IL_028a:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_7 = 0;
		goto IL_0305;
	}

IL_0296:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1])
		int32_t L_134 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_135 = __this->get_unitsScore_8();
		int32_t L_136 = V_7;
		NullCheck(L_135);
		int32_t L_137 = L_136;
		int32_t L_138 = (L_135)->GetAt(static_cast<il2cpp_array_size_t>(L_137));
		if ((((int32_t)L_134) >= ((int32_t)L_138)))
		{
			goto IL_02ff;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_139 = __this->get_units_6();
		int32_t L_140 = V_7;
		NullCheck(L_139);
		int32_t L_141 = L_140;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_142 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_143 = __this->get_unitsTemp_7();
		NullCheck(L_143);
		int32_t L_144 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_145 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_146;
		L_146 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_142, L_145, /*hidden argument*/NULL);
		if (!L_146)
		{
			goto IL_02ff;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_147 = __this->get_units_6();
		int32_t L_148 = V_7;
		NullCheck(L_147);
		int32_t L_149 = L_148;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_150 = (L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_151 = __this->get_unitsTemp_7();
		NullCheck(L_151);
		int32_t L_152 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_153 = (L_151)->GetAt(static_cast<il2cpp_array_size_t>(L_152));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_154;
		L_154 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_150, L_153, /*hidden argument*/NULL);
		if (!L_154)
		{
			goto IL_02ff;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_155 = __this->get_unitsScore_8();
		int32_t L_156 = V_7;
		NullCheck(L_155);
		int32_t L_157 = L_156;
		int32_t L_158 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		__this->set_tempScore_9(L_158);
		// unitsTemp[2] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_159 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_160 = __this->get_units_6();
		int32_t L_161 = V_7;
		NullCheck(L_160);
		int32_t L_162 = L_161;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_163 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_162));
		NullCheck(L_159);
		ArrayElementTypeCheck (L_159, L_163);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(2), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_163);
		// tempUnit = i;
		int32_t L_164 = V_7;
		__this->set_tempUnit_10(L_164);
	}

IL_02ff:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_165 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_165, (int32_t)1));
	}

IL_0305:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_166 = V_7;
		if ((((int32_t)L_166) < ((int32_t)((int32_t)10))))
		{
			goto IL_0296;
		}
	}

IL_030b:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_167 = __this->get_unitsGUI_5();
		int32_t L_168 = __this->get_tempUnit_10();
		NullCheck(L_167);
		int32_t L_169 = L_168;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_171;
		L_171 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_170, /*hidden argument*/NULL);
		if (!L_171)
		{
			goto IL_0469;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-1,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_172 = __this->get_unitsGUI_5();
		int32_t L_173 = __this->get_tempUnit_10();
		NullCheck(L_172);
		int32_t L_174 = L_173;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_175 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		NullCheck(L_175);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_176;
		L_176 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_175, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_177;
		memset((&L_177), 0, sizeof(L_177));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_177), (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_176);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_176, L_177, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_178 = __this->get_unitsGUI_5();
		int32_t L_179 = __this->get_tempUnit_10();
		NullCheck(L_178);
		int32_t L_180 = L_179;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_181 = (L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_180));
		NullCheck(L_181);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_182;
		L_182 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_181, /*hidden argument*/NULL);
		NullCheck(L_182);
		RuntimeObject* L_183;
		L_183 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_182, /*hidden argument*/NULL);
		V_2 = L_183;
	}

IL_0365:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03ac;
		}

IL_0367:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_184 = V_2;
			NullCheck(L_184);
			RuntimeObject * L_185;
			L_185 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_184);
			V_8 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_185, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_186 = V_8;
			NullCheck(L_186);
			String_t* L_187;
			L_187 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_186, /*hidden argument*/NULL);
			bool L_188;
			L_188 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_187, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_188)
			{
				goto IL_03ac;
			}
		}

IL_0387:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_189 = V_8;
			NullCheck(L_189);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_190;
			L_190 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_189, /*hidden argument*/NULL);
			NullCheck(L_190);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_191;
			L_191 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_190, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_192 = __this->get_address_of_tempScore_9();
			String_t* L_193;
			L_193 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_192, /*hidden argument*/NULL);
			String_t* L_194 = L_193;
			G_B54_0 = L_194;
			G_B54_1 = L_191;
			if (L_194)
			{
				G_B55_0 = L_194;
				G_B55_1 = L_191;
				goto IL_03a7;
			}
		}

IL_03a1:
		{
			G_B55_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B55_1 = G_B54_1;
		}

IL_03a7:
		{
			NullCheck(G_B55_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B55_1, G_B55_0, /*hidden argument*/NULL);
		}

IL_03ac:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_195 = V_2;
			NullCheck(L_195);
			bool L_196;
			L_196 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_195);
			if (L_196)
			{
				goto IL_0367;
			}
		}

IL_03b4:
		{
			IL2CPP_LEAVE(0x3CA, FINALLY_03b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_03b6;
	}

FINALLY_03b6:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_197 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_197, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_198 = V_4;
			if (!L_198)
			{
				goto IL_03c9;
			}
		}

IL_03c2:
		{
			RuntimeObject* L_199 = V_4;
			NullCheck(L_199);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_199);
		}

IL_03c9:
		{
			IL2CPP_END_FINALLY(950)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(950)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3CA, IL_03ca)
	}

IL_03ca:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_9 = 0;
		goto IL_0460;
	}

IL_03d9:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2])
		int32_t L_200 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_201 = __this->get_unitsScore_8();
		int32_t L_202 = V_9;
		NullCheck(L_201);
		int32_t L_203 = L_202;
		int32_t L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		if ((((int32_t)L_200) >= ((int32_t)L_204)))
		{
			goto IL_045a;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_205 = __this->get_units_6();
		int32_t L_206 = V_9;
		NullCheck(L_205);
		int32_t L_207 = L_206;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_208 = (L_205)->GetAt(static_cast<il2cpp_array_size_t>(L_207));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_209 = __this->get_unitsTemp_7();
		NullCheck(L_209);
		int32_t L_210 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_211 = (L_209)->GetAt(static_cast<il2cpp_array_size_t>(L_210));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_212;
		L_212 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_208, L_211, /*hidden argument*/NULL);
		if (!L_212)
		{
			goto IL_045a;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_213 = __this->get_units_6();
		int32_t L_214 = V_9;
		NullCheck(L_213);
		int32_t L_215 = L_214;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_216 = (L_213)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_217 = __this->get_unitsTemp_7();
		NullCheck(L_217);
		int32_t L_218 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_219 = (L_217)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_220;
		L_220 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_216, L_219, /*hidden argument*/NULL);
		if (!L_220)
		{
			goto IL_045a;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_221 = __this->get_units_6();
		int32_t L_222 = V_9;
		NullCheck(L_221);
		int32_t L_223 = L_222;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_224 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_225 = __this->get_unitsTemp_7();
		NullCheck(L_225);
		int32_t L_226 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_227 = (L_225)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_228;
		L_228 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_224, L_227, /*hidden argument*/NULL);
		if (!L_228)
		{
			goto IL_045a;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_229 = __this->get_unitsScore_8();
		int32_t L_230 = V_9;
		NullCheck(L_229);
		int32_t L_231 = L_230;
		int32_t L_232 = (L_229)->GetAt(static_cast<il2cpp_array_size_t>(L_231));
		__this->set_tempScore_9(L_232);
		// unitsTemp[3] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_233 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_234 = __this->get_units_6();
		int32_t L_235 = V_9;
		NullCheck(L_234);
		int32_t L_236 = L_235;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_237 = (L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_236));
		NullCheck(L_233);
		ArrayElementTypeCheck (L_233, L_237);
		(L_233)->SetAt(static_cast<il2cpp_array_size_t>(3), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_237);
		// tempUnit = i;
		int32_t L_238 = V_9;
		__this->set_tempUnit_10(L_238);
	}

IL_045a:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_239 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_239, (int32_t)1));
	}

IL_0460:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_240 = V_9;
		if ((((int32_t)L_240) < ((int32_t)((int32_t)10))))
		{
			goto IL_03d9;
		}
	}

IL_0469:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_241 = __this->get_unitsGUI_5();
		int32_t L_242 = __this->get_tempUnit_10();
		NullCheck(L_241);
		int32_t L_243 = L_242;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_244 = (L_241)->GetAt(static_cast<il2cpp_array_size_t>(L_243));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_245;
		L_245 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_244, /*hidden argument*/NULL);
		if (!L_245)
		{
			goto IL_05e2;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-1.5f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_246 = __this->get_unitsGUI_5();
		int32_t L_247 = __this->get_tempUnit_10();
		NullCheck(L_246);
		int32_t L_248 = L_247;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_249 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		NullCheck(L_249);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_250;
		L_250 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_249, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_251;
		memset((&L_251), 0, sizeof(L_251));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_251), (0.0f), (-1.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_250);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_250, L_251, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_252 = __this->get_unitsGUI_5();
		int32_t L_253 = __this->get_tempUnit_10();
		NullCheck(L_252);
		int32_t L_254 = L_253;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_255 = (L_252)->GetAt(static_cast<il2cpp_array_size_t>(L_254));
		NullCheck(L_255);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_256;
		L_256 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_255, /*hidden argument*/NULL);
		NullCheck(L_256);
		RuntimeObject* L_257;
		L_257 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_256, /*hidden argument*/NULL);
		V_2 = L_257;
	}

IL_04c3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_050a;
		}

IL_04c5:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_258 = V_2;
			NullCheck(L_258);
			RuntimeObject * L_259;
			L_259 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_258);
			V_10 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_259, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_260 = V_10;
			NullCheck(L_260);
			String_t* L_261;
			L_261 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_260, /*hidden argument*/NULL);
			bool L_262;
			L_262 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_261, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_262)
			{
				goto IL_050a;
			}
		}

IL_04e5:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_263 = V_10;
			NullCheck(L_263);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_264;
			L_264 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_263, /*hidden argument*/NULL);
			NullCheck(L_264);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_265;
			L_265 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_264, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_266 = __this->get_address_of_tempScore_9();
			String_t* L_267;
			L_267 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_266, /*hidden argument*/NULL);
			String_t* L_268 = L_267;
			G_B74_0 = L_268;
			G_B74_1 = L_265;
			if (L_268)
			{
				G_B75_0 = L_268;
				G_B75_1 = L_265;
				goto IL_0505;
			}
		}

IL_04ff:
		{
			G_B75_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B75_1 = G_B74_1;
		}

IL_0505:
		{
			NullCheck(G_B75_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B75_1, G_B75_0, /*hidden argument*/NULL);
		}

IL_050a:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_269 = V_2;
			NullCheck(L_269);
			bool L_270;
			L_270 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_269);
			if (L_270)
			{
				goto IL_04c5;
			}
		}

IL_0512:
		{
			IL2CPP_LEAVE(0x528, FINALLY_0514);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0514;
	}

FINALLY_0514:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_271 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_271, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_272 = V_4;
			if (!L_272)
			{
				goto IL_0527;
			}
		}

IL_0520:
		{
			RuntimeObject* L_273 = V_4;
			NullCheck(L_273);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_273);
		}

IL_0527:
		{
			IL2CPP_END_FINALLY(1300)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1300)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x528, IL_0528)
	}

IL_0528:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_11 = 0;
		goto IL_05d9;
	}

IL_0537:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3])
		int32_t L_274 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_275 = __this->get_unitsScore_8();
		int32_t L_276 = V_11;
		NullCheck(L_275);
		int32_t L_277 = L_276;
		int32_t L_278 = (L_275)->GetAt(static_cast<il2cpp_array_size_t>(L_277));
		if ((((int32_t)L_274) >= ((int32_t)L_278)))
		{
			goto IL_05d3;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_279 = __this->get_units_6();
		int32_t L_280 = V_11;
		NullCheck(L_279);
		int32_t L_281 = L_280;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_282 = (L_279)->GetAt(static_cast<il2cpp_array_size_t>(L_281));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_283 = __this->get_unitsTemp_7();
		NullCheck(L_283);
		int32_t L_284 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_285 = (L_283)->GetAt(static_cast<il2cpp_array_size_t>(L_284));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_286;
		L_286 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_282, L_285, /*hidden argument*/NULL);
		if (!L_286)
		{
			goto IL_05d3;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_287 = __this->get_units_6();
		int32_t L_288 = V_11;
		NullCheck(L_287);
		int32_t L_289 = L_288;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_290 = (L_287)->GetAt(static_cast<il2cpp_array_size_t>(L_289));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_291 = __this->get_unitsTemp_7();
		NullCheck(L_291);
		int32_t L_292 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_293 = (L_291)->GetAt(static_cast<il2cpp_array_size_t>(L_292));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_294;
		L_294 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_290, L_293, /*hidden argument*/NULL);
		if (!L_294)
		{
			goto IL_05d3;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_295 = __this->get_units_6();
		int32_t L_296 = V_11;
		NullCheck(L_295);
		int32_t L_297 = L_296;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_298 = (L_295)->GetAt(static_cast<il2cpp_array_size_t>(L_297));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_299 = __this->get_unitsTemp_7();
		NullCheck(L_299);
		int32_t L_300 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_301 = (L_299)->GetAt(static_cast<il2cpp_array_size_t>(L_300));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_302;
		L_302 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_298, L_301, /*hidden argument*/NULL);
		if (!L_302)
		{
			goto IL_05d3;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_303 = __this->get_units_6();
		int32_t L_304 = V_11;
		NullCheck(L_303);
		int32_t L_305 = L_304;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_306 = (L_303)->GetAt(static_cast<il2cpp_array_size_t>(L_305));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_307 = __this->get_unitsTemp_7();
		NullCheck(L_307);
		int32_t L_308 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_309 = (L_307)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_310;
		L_310 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_306, L_309, /*hidden argument*/NULL);
		if (!L_310)
		{
			goto IL_05d3;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_311 = __this->get_unitsScore_8();
		int32_t L_312 = V_11;
		NullCheck(L_311);
		int32_t L_313 = L_312;
		int32_t L_314 = (L_311)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		__this->set_tempScore_9(L_314);
		// unitsTemp[4] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_315 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_316 = __this->get_units_6();
		int32_t L_317 = V_11;
		NullCheck(L_316);
		int32_t L_318 = L_317;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_319 = (L_316)->GetAt(static_cast<il2cpp_array_size_t>(L_318));
		NullCheck(L_315);
		ArrayElementTypeCheck (L_315, L_319);
		(L_315)->SetAt(static_cast<il2cpp_array_size_t>(4), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_319);
		// tempUnit = i;
		int32_t L_320 = V_11;
		__this->set_tempUnit_10(L_320);
	}

IL_05d3:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_321 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_321, (int32_t)1));
	}

IL_05d9:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_322 = V_11;
		if ((((int32_t)L_322) < ((int32_t)((int32_t)10))))
		{
			goto IL_0537;
		}
	}

IL_05e2:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_323 = __this->get_unitsGUI_5();
		int32_t L_324 = __this->get_tempUnit_10();
		NullCheck(L_323);
		int32_t L_325 = L_324;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_326 = (L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_325));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_327;
		L_327 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_326, /*hidden argument*/NULL);
		if (!L_327)
		{
			goto IL_0776;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-2,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_328 = __this->get_unitsGUI_5();
		int32_t L_329 = __this->get_tempUnit_10();
		NullCheck(L_328);
		int32_t L_330 = L_329;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_331 = (L_328)->GetAt(static_cast<il2cpp_array_size_t>(L_330));
		NullCheck(L_331);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_332;
		L_332 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_331, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_333;
		memset((&L_333), 0, sizeof(L_333));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_333), (0.0f), (-2.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_332);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_332, L_333, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_334 = __this->get_unitsGUI_5();
		int32_t L_335 = __this->get_tempUnit_10();
		NullCheck(L_334);
		int32_t L_336 = L_335;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_337 = (L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336));
		NullCheck(L_337);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_338;
		L_338 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_337, /*hidden argument*/NULL);
		NullCheck(L_338);
		RuntimeObject* L_339;
		L_339 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_338, /*hidden argument*/NULL);
		V_2 = L_339;
	}

IL_063c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0683;
		}

IL_063e:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_340 = V_2;
			NullCheck(L_340);
			RuntimeObject * L_341;
			L_341 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_340);
			V_12 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_341, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_342 = V_12;
			NullCheck(L_342);
			String_t* L_343;
			L_343 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_342, /*hidden argument*/NULL);
			bool L_344;
			L_344 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_343, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_344)
			{
				goto IL_0683;
			}
		}

IL_065e:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_345 = V_12;
			NullCheck(L_345);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_346;
			L_346 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_345, /*hidden argument*/NULL);
			NullCheck(L_346);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_347;
			L_347 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_346, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_348 = __this->get_address_of_tempScore_9();
			String_t* L_349;
			L_349 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_348, /*hidden argument*/NULL);
			String_t* L_350 = L_349;
			G_B95_0 = L_350;
			G_B95_1 = L_347;
			if (L_350)
			{
				G_B96_0 = L_350;
				G_B96_1 = L_347;
				goto IL_067e;
			}
		}

IL_0678:
		{
			G_B96_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B96_1 = G_B95_1;
		}

IL_067e:
		{
			NullCheck(G_B96_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B96_1, G_B96_0, /*hidden argument*/NULL);
		}

IL_0683:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_351 = V_2;
			NullCheck(L_351);
			bool L_352;
			L_352 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_351);
			if (L_352)
			{
				goto IL_063e;
			}
		}

IL_068b:
		{
			IL2CPP_LEAVE(0x6A1, FINALLY_068d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_068d;
	}

FINALLY_068d:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_353 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_353, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_354 = V_4;
			if (!L_354)
			{
				goto IL_06a0;
			}
		}

IL_0699:
		{
			RuntimeObject* L_355 = V_4;
			NullCheck(L_355);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_355);
		}

IL_06a0:
		{
			IL2CPP_END_FINALLY(1677)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1677)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6A1, IL_06a1)
	}

IL_06a1:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_13 = 0;
		goto IL_076d;
	}

IL_06b0:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3] && units[i] != unitsTemp[4])
		int32_t L_356 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_357 = __this->get_unitsScore_8();
		int32_t L_358 = V_13;
		NullCheck(L_357);
		int32_t L_359 = L_358;
		int32_t L_360 = (L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_359));
		if ((((int32_t)L_356) >= ((int32_t)L_360)))
		{
			goto IL_0767;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_361 = __this->get_units_6();
		int32_t L_362 = V_13;
		NullCheck(L_361);
		int32_t L_363 = L_362;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_364 = (L_361)->GetAt(static_cast<il2cpp_array_size_t>(L_363));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_365 = __this->get_unitsTemp_7();
		NullCheck(L_365);
		int32_t L_366 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_367 = (L_365)->GetAt(static_cast<il2cpp_array_size_t>(L_366));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_368;
		L_368 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_364, L_367, /*hidden argument*/NULL);
		if (!L_368)
		{
			goto IL_0767;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_369 = __this->get_units_6();
		int32_t L_370 = V_13;
		NullCheck(L_369);
		int32_t L_371 = L_370;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_372 = (L_369)->GetAt(static_cast<il2cpp_array_size_t>(L_371));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_373 = __this->get_unitsTemp_7();
		NullCheck(L_373);
		int32_t L_374 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_375 = (L_373)->GetAt(static_cast<il2cpp_array_size_t>(L_374));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_376;
		L_376 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_372, L_375, /*hidden argument*/NULL);
		if (!L_376)
		{
			goto IL_0767;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_377 = __this->get_units_6();
		int32_t L_378 = V_13;
		NullCheck(L_377);
		int32_t L_379 = L_378;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_380 = (L_377)->GetAt(static_cast<il2cpp_array_size_t>(L_379));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_381 = __this->get_unitsTemp_7();
		NullCheck(L_381);
		int32_t L_382 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_383 = (L_381)->GetAt(static_cast<il2cpp_array_size_t>(L_382));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_384;
		L_384 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_380, L_383, /*hidden argument*/NULL);
		if (!L_384)
		{
			goto IL_0767;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_385 = __this->get_units_6();
		int32_t L_386 = V_13;
		NullCheck(L_385);
		int32_t L_387 = L_386;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_388 = (L_385)->GetAt(static_cast<il2cpp_array_size_t>(L_387));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_389 = __this->get_unitsTemp_7();
		NullCheck(L_389);
		int32_t L_390 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_391 = (L_389)->GetAt(static_cast<il2cpp_array_size_t>(L_390));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_392;
		L_392 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_388, L_391, /*hidden argument*/NULL);
		if (!L_392)
		{
			goto IL_0767;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_393 = __this->get_units_6();
		int32_t L_394 = V_13;
		NullCheck(L_393);
		int32_t L_395 = L_394;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_396 = (L_393)->GetAt(static_cast<il2cpp_array_size_t>(L_395));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_397 = __this->get_unitsTemp_7();
		NullCheck(L_397);
		int32_t L_398 = 4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_399 = (L_397)->GetAt(static_cast<il2cpp_array_size_t>(L_398));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_400;
		L_400 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_396, L_399, /*hidden argument*/NULL);
		if (!L_400)
		{
			goto IL_0767;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_401 = __this->get_unitsScore_8();
		int32_t L_402 = V_13;
		NullCheck(L_401);
		int32_t L_403 = L_402;
		int32_t L_404 = (L_401)->GetAt(static_cast<il2cpp_array_size_t>(L_403));
		__this->set_tempScore_9(L_404);
		// unitsTemp[5] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_405 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_406 = __this->get_units_6();
		int32_t L_407 = V_13;
		NullCheck(L_406);
		int32_t L_408 = L_407;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_409 = (L_406)->GetAt(static_cast<il2cpp_array_size_t>(L_408));
		NullCheck(L_405);
		ArrayElementTypeCheck (L_405, L_409);
		(L_405)->SetAt(static_cast<il2cpp_array_size_t>(5), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_409);
		// tempUnit = i;
		int32_t L_410 = V_13;
		__this->set_tempUnit_10(L_410);
	}

IL_0767:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_411 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_411, (int32_t)1));
	}

IL_076d:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_412 = V_13;
		if ((((int32_t)L_412) < ((int32_t)((int32_t)10))))
		{
			goto IL_06b0;
		}
	}

IL_0776:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_413 = __this->get_unitsGUI_5();
		int32_t L_414 = __this->get_tempUnit_10();
		NullCheck(L_413);
		int32_t L_415 = L_414;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_416 = (L_413)->GetAt(static_cast<il2cpp_array_size_t>(L_415));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_417;
		L_417 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_416, /*hidden argument*/NULL);
		if (!L_417)
		{
			goto IL_0925;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-2.5f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_418 = __this->get_unitsGUI_5();
		int32_t L_419 = __this->get_tempUnit_10();
		NullCheck(L_418);
		int32_t L_420 = L_419;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_421 = (L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_420));
		NullCheck(L_421);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_422;
		L_422 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_421, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_423;
		memset((&L_423), 0, sizeof(L_423));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_423), (0.0f), (-2.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_422);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_422, L_423, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_424 = __this->get_unitsGUI_5();
		int32_t L_425 = __this->get_tempUnit_10();
		NullCheck(L_424);
		int32_t L_426 = L_425;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_427 = (L_424)->GetAt(static_cast<il2cpp_array_size_t>(L_426));
		NullCheck(L_427);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_428;
		L_428 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_427, /*hidden argument*/NULL);
		NullCheck(L_428);
		RuntimeObject* L_429;
		L_429 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_428, /*hidden argument*/NULL);
		V_2 = L_429;
	}

IL_07d0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0817;
		}

IL_07d2:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_430 = V_2;
			NullCheck(L_430);
			RuntimeObject * L_431;
			L_431 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_430);
			V_14 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_431, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_432 = V_14;
			NullCheck(L_432);
			String_t* L_433;
			L_433 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_432, /*hidden argument*/NULL);
			bool L_434;
			L_434 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_433, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_434)
			{
				goto IL_0817;
			}
		}

IL_07f2:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_435 = V_14;
			NullCheck(L_435);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_436;
			L_436 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_435, /*hidden argument*/NULL);
			NullCheck(L_436);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_437;
			L_437 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_436, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_438 = __this->get_address_of_tempScore_9();
			String_t* L_439;
			L_439 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_438, /*hidden argument*/NULL);
			String_t* L_440 = L_439;
			G_B117_0 = L_440;
			G_B117_1 = L_437;
			if (L_440)
			{
				G_B118_0 = L_440;
				G_B118_1 = L_437;
				goto IL_0812;
			}
		}

IL_080c:
		{
			G_B118_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B118_1 = G_B117_1;
		}

IL_0812:
		{
			NullCheck(G_B118_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B118_1, G_B118_0, /*hidden argument*/NULL);
		}

IL_0817:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_441 = V_2;
			NullCheck(L_441);
			bool L_442;
			L_442 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_441);
			if (L_442)
			{
				goto IL_07d2;
			}
		}

IL_081f:
		{
			IL2CPP_LEAVE(0x835, FINALLY_0821);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0821;
	}

FINALLY_0821:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_443 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_443, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_444 = V_4;
			if (!L_444)
			{
				goto IL_0834;
			}
		}

IL_082d:
		{
			RuntimeObject* L_445 = V_4;
			NullCheck(L_445);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_445);
		}

IL_0834:
		{
			IL2CPP_END_FINALLY(2081)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(2081)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x835, IL_0835)
	}

IL_0835:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_15 = 0;
		goto IL_091c;
	}

IL_0844:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3] && units[i] != unitsTemp[4] && units[i] != unitsTemp[5])
		int32_t L_446 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_447 = __this->get_unitsScore_8();
		int32_t L_448 = V_15;
		NullCheck(L_447);
		int32_t L_449 = L_448;
		int32_t L_450 = (L_447)->GetAt(static_cast<il2cpp_array_size_t>(L_449));
		if ((((int32_t)L_446) >= ((int32_t)L_450)))
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_451 = __this->get_units_6();
		int32_t L_452 = V_15;
		NullCheck(L_451);
		int32_t L_453 = L_452;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_454 = (L_451)->GetAt(static_cast<il2cpp_array_size_t>(L_453));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_455 = __this->get_unitsTemp_7();
		NullCheck(L_455);
		int32_t L_456 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_457 = (L_455)->GetAt(static_cast<il2cpp_array_size_t>(L_456));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_458;
		L_458 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_454, L_457, /*hidden argument*/NULL);
		if (!L_458)
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_459 = __this->get_units_6();
		int32_t L_460 = V_15;
		NullCheck(L_459);
		int32_t L_461 = L_460;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_462 = (L_459)->GetAt(static_cast<il2cpp_array_size_t>(L_461));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_463 = __this->get_unitsTemp_7();
		NullCheck(L_463);
		int32_t L_464 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_465 = (L_463)->GetAt(static_cast<il2cpp_array_size_t>(L_464));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_466;
		L_466 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_462, L_465, /*hidden argument*/NULL);
		if (!L_466)
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_467 = __this->get_units_6();
		int32_t L_468 = V_15;
		NullCheck(L_467);
		int32_t L_469 = L_468;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_470 = (L_467)->GetAt(static_cast<il2cpp_array_size_t>(L_469));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_471 = __this->get_unitsTemp_7();
		NullCheck(L_471);
		int32_t L_472 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_473 = (L_471)->GetAt(static_cast<il2cpp_array_size_t>(L_472));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_474;
		L_474 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_470, L_473, /*hidden argument*/NULL);
		if (!L_474)
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_475 = __this->get_units_6();
		int32_t L_476 = V_15;
		NullCheck(L_475);
		int32_t L_477 = L_476;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_478 = (L_475)->GetAt(static_cast<il2cpp_array_size_t>(L_477));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_479 = __this->get_unitsTemp_7();
		NullCheck(L_479);
		int32_t L_480 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_481 = (L_479)->GetAt(static_cast<il2cpp_array_size_t>(L_480));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_482;
		L_482 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_478, L_481, /*hidden argument*/NULL);
		if (!L_482)
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_483 = __this->get_units_6();
		int32_t L_484 = V_15;
		NullCheck(L_483);
		int32_t L_485 = L_484;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_486 = (L_483)->GetAt(static_cast<il2cpp_array_size_t>(L_485));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_487 = __this->get_unitsTemp_7();
		NullCheck(L_487);
		int32_t L_488 = 4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_489 = (L_487)->GetAt(static_cast<il2cpp_array_size_t>(L_488));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_490;
		L_490 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_486, L_489, /*hidden argument*/NULL);
		if (!L_490)
		{
			goto IL_0916;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_491 = __this->get_units_6();
		int32_t L_492 = V_15;
		NullCheck(L_491);
		int32_t L_493 = L_492;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_494 = (L_491)->GetAt(static_cast<il2cpp_array_size_t>(L_493));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_495 = __this->get_unitsTemp_7();
		NullCheck(L_495);
		int32_t L_496 = 5;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_497 = (L_495)->GetAt(static_cast<il2cpp_array_size_t>(L_496));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_498;
		L_498 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_494, L_497, /*hidden argument*/NULL);
		if (!L_498)
		{
			goto IL_0916;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_499 = __this->get_unitsScore_8();
		int32_t L_500 = V_15;
		NullCheck(L_499);
		int32_t L_501 = L_500;
		int32_t L_502 = (L_499)->GetAt(static_cast<il2cpp_array_size_t>(L_501));
		__this->set_tempScore_9(L_502);
		// unitsTemp[6] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_503 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_504 = __this->get_units_6();
		int32_t L_505 = V_15;
		NullCheck(L_504);
		int32_t L_506 = L_505;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_507 = (L_504)->GetAt(static_cast<il2cpp_array_size_t>(L_506));
		NullCheck(L_503);
		ArrayElementTypeCheck (L_503, L_507);
		(L_503)->SetAt(static_cast<il2cpp_array_size_t>(6), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_507);
		// tempUnit = i;
		int32_t L_508 = V_15;
		__this->set_tempUnit_10(L_508);
	}

IL_0916:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_509 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_509, (int32_t)1));
	}

IL_091c:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_510 = V_15;
		if ((((int32_t)L_510) < ((int32_t)((int32_t)10))))
		{
			goto IL_0844;
		}
	}

IL_0925:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_511 = __this->get_unitsGUI_5();
		int32_t L_512 = __this->get_tempUnit_10();
		NullCheck(L_511);
		int32_t L_513 = L_512;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_514 = (L_511)->GetAt(static_cast<il2cpp_array_size_t>(L_513));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_515;
		L_515 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_514, /*hidden argument*/NULL);
		if (!L_515)
		{
			goto IL_0aef;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-3f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_516 = __this->get_unitsGUI_5();
		int32_t L_517 = __this->get_tempUnit_10();
		NullCheck(L_516);
		int32_t L_518 = L_517;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_519 = (L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518));
		NullCheck(L_519);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_520;
		L_520 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_519, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_521;
		memset((&L_521), 0, sizeof(L_521));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_521), (0.0f), (-3.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_520);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_520, L_521, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_522 = __this->get_unitsGUI_5();
		int32_t L_523 = __this->get_tempUnit_10();
		NullCheck(L_522);
		int32_t L_524 = L_523;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_525 = (L_522)->GetAt(static_cast<il2cpp_array_size_t>(L_524));
		NullCheck(L_525);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_526;
		L_526 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_525, /*hidden argument*/NULL);
		NullCheck(L_526);
		RuntimeObject* L_527;
		L_527 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_526, /*hidden argument*/NULL);
		V_2 = L_527;
	}

IL_097f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_09c6;
		}

IL_0981:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_528 = V_2;
			NullCheck(L_528);
			RuntimeObject * L_529;
			L_529 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_528);
			V_16 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_529, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_530 = V_16;
			NullCheck(L_530);
			String_t* L_531;
			L_531 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_530, /*hidden argument*/NULL);
			bool L_532;
			L_532 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_531, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_532)
			{
				goto IL_09c6;
			}
		}

IL_09a1:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_533 = V_16;
			NullCheck(L_533);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_534;
			L_534 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_533, /*hidden argument*/NULL);
			NullCheck(L_534);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_535;
			L_535 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_534, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_536 = __this->get_address_of_tempScore_9();
			String_t* L_537;
			L_537 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_536, /*hidden argument*/NULL);
			String_t* L_538 = L_537;
			G_B140_0 = L_538;
			G_B140_1 = L_535;
			if (L_538)
			{
				G_B141_0 = L_538;
				G_B141_1 = L_535;
				goto IL_09c1;
			}
		}

IL_09bb:
		{
			G_B141_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B141_1 = G_B140_1;
		}

IL_09c1:
		{
			NullCheck(G_B141_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B141_1, G_B141_0, /*hidden argument*/NULL);
		}

IL_09c6:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_539 = V_2;
			NullCheck(L_539);
			bool L_540;
			L_540 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_539);
			if (L_540)
			{
				goto IL_0981;
			}
		}

IL_09ce:
		{
			IL2CPP_LEAVE(0x9E4, FINALLY_09d0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_09d0;
	}

FINALLY_09d0:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_541 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_541, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_542 = V_4;
			if (!L_542)
			{
				goto IL_09e3;
			}
		}

IL_09dc:
		{
			RuntimeObject* L_543 = V_4;
			NullCheck(L_543);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_543);
		}

IL_09e3:
		{
			IL2CPP_END_FINALLY(2512)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(2512)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x9E4, IL_09e4)
	}

IL_09e4:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_17 = 0;
		goto IL_0ae6;
	}

IL_09f3:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3] && units[i] != unitsTemp[4] && units[i] != unitsTemp[5] && units[i] != unitsTemp[6])
		int32_t L_544 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_545 = __this->get_unitsScore_8();
		int32_t L_546 = V_17;
		NullCheck(L_545);
		int32_t L_547 = L_546;
		int32_t L_548 = (L_545)->GetAt(static_cast<il2cpp_array_size_t>(L_547));
		if ((((int32_t)L_544) >= ((int32_t)L_548)))
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_549 = __this->get_units_6();
		int32_t L_550 = V_17;
		NullCheck(L_549);
		int32_t L_551 = L_550;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_552 = (L_549)->GetAt(static_cast<il2cpp_array_size_t>(L_551));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_553 = __this->get_unitsTemp_7();
		NullCheck(L_553);
		int32_t L_554 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_555 = (L_553)->GetAt(static_cast<il2cpp_array_size_t>(L_554));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_556;
		L_556 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_552, L_555, /*hidden argument*/NULL);
		if (!L_556)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_557 = __this->get_units_6();
		int32_t L_558 = V_17;
		NullCheck(L_557);
		int32_t L_559 = L_558;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_560 = (L_557)->GetAt(static_cast<il2cpp_array_size_t>(L_559));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_561 = __this->get_unitsTemp_7();
		NullCheck(L_561);
		int32_t L_562 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_563 = (L_561)->GetAt(static_cast<il2cpp_array_size_t>(L_562));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_564;
		L_564 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_560, L_563, /*hidden argument*/NULL);
		if (!L_564)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_565 = __this->get_units_6();
		int32_t L_566 = V_17;
		NullCheck(L_565);
		int32_t L_567 = L_566;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_568 = (L_565)->GetAt(static_cast<il2cpp_array_size_t>(L_567));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_569 = __this->get_unitsTemp_7();
		NullCheck(L_569);
		int32_t L_570 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_571 = (L_569)->GetAt(static_cast<il2cpp_array_size_t>(L_570));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_572;
		L_572 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_568, L_571, /*hidden argument*/NULL);
		if (!L_572)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_573 = __this->get_units_6();
		int32_t L_574 = V_17;
		NullCheck(L_573);
		int32_t L_575 = L_574;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_576 = (L_573)->GetAt(static_cast<il2cpp_array_size_t>(L_575));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_577 = __this->get_unitsTemp_7();
		NullCheck(L_577);
		int32_t L_578 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_579 = (L_577)->GetAt(static_cast<il2cpp_array_size_t>(L_578));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_580;
		L_580 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_576, L_579, /*hidden argument*/NULL);
		if (!L_580)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_581 = __this->get_units_6();
		int32_t L_582 = V_17;
		NullCheck(L_581);
		int32_t L_583 = L_582;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_584 = (L_581)->GetAt(static_cast<il2cpp_array_size_t>(L_583));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_585 = __this->get_unitsTemp_7();
		NullCheck(L_585);
		int32_t L_586 = 4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_587 = (L_585)->GetAt(static_cast<il2cpp_array_size_t>(L_586));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_588;
		L_588 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_584, L_587, /*hidden argument*/NULL);
		if (!L_588)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_589 = __this->get_units_6();
		int32_t L_590 = V_17;
		NullCheck(L_589);
		int32_t L_591 = L_590;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_592 = (L_589)->GetAt(static_cast<il2cpp_array_size_t>(L_591));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_593 = __this->get_unitsTemp_7();
		NullCheck(L_593);
		int32_t L_594 = 5;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_595 = (L_593)->GetAt(static_cast<il2cpp_array_size_t>(L_594));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_596;
		L_596 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_592, L_595, /*hidden argument*/NULL);
		if (!L_596)
		{
			goto IL_0ae0;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_597 = __this->get_units_6();
		int32_t L_598 = V_17;
		NullCheck(L_597);
		int32_t L_599 = L_598;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_600 = (L_597)->GetAt(static_cast<il2cpp_array_size_t>(L_599));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_601 = __this->get_unitsTemp_7();
		NullCheck(L_601);
		int32_t L_602 = 6;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_603 = (L_601)->GetAt(static_cast<il2cpp_array_size_t>(L_602));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_604;
		L_604 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_600, L_603, /*hidden argument*/NULL);
		if (!L_604)
		{
			goto IL_0ae0;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_605 = __this->get_unitsScore_8();
		int32_t L_606 = V_17;
		NullCheck(L_605);
		int32_t L_607 = L_606;
		int32_t L_608 = (L_605)->GetAt(static_cast<il2cpp_array_size_t>(L_607));
		__this->set_tempScore_9(L_608);
		// unitsTemp[7] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_609 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_610 = __this->get_units_6();
		int32_t L_611 = V_17;
		NullCheck(L_610);
		int32_t L_612 = L_611;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_613 = (L_610)->GetAt(static_cast<il2cpp_array_size_t>(L_612));
		NullCheck(L_609);
		ArrayElementTypeCheck (L_609, L_613);
		(L_609)->SetAt(static_cast<il2cpp_array_size_t>(7), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_613);
		// tempUnit = i;
		int32_t L_614 = V_17;
		__this->set_tempUnit_10(L_614);
	}

IL_0ae0:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_615 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_615, (int32_t)1));
	}

IL_0ae6:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_616 = V_17;
		if ((((int32_t)L_616) < ((int32_t)((int32_t)10))))
		{
			goto IL_09f3;
		}
	}

IL_0aef:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_617 = __this->get_unitsGUI_5();
		int32_t L_618 = __this->get_tempUnit_10();
		NullCheck(L_617);
		int32_t L_619 = L_618;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_620 = (L_617)->GetAt(static_cast<il2cpp_array_size_t>(L_619));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_621;
		L_621 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_620, /*hidden argument*/NULL);
		if (!L_621)
		{
			goto IL_0cd4;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-3.5f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_622 = __this->get_unitsGUI_5();
		int32_t L_623 = __this->get_tempUnit_10();
		NullCheck(L_622);
		int32_t L_624 = L_623;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_625 = (L_622)->GetAt(static_cast<il2cpp_array_size_t>(L_624));
		NullCheck(L_625);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_626;
		L_626 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_625, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_627;
		memset((&L_627), 0, sizeof(L_627));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_627), (0.0f), (-3.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_626);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_626, L_627, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_628 = __this->get_unitsGUI_5();
		int32_t L_629 = __this->get_tempUnit_10();
		NullCheck(L_628);
		int32_t L_630 = L_629;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_631 = (L_628)->GetAt(static_cast<il2cpp_array_size_t>(L_630));
		NullCheck(L_631);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_632;
		L_632 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_631, /*hidden argument*/NULL);
		NullCheck(L_632);
		RuntimeObject* L_633;
		L_633 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_632, /*hidden argument*/NULL);
		V_2 = L_633;
	}

IL_0b49:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0b90;
		}

IL_0b4b:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_634 = V_2;
			NullCheck(L_634);
			RuntimeObject * L_635;
			L_635 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_634);
			V_18 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_635, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_636 = V_18;
			NullCheck(L_636);
			String_t* L_637;
			L_637 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_636, /*hidden argument*/NULL);
			bool L_638;
			L_638 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_637, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_638)
			{
				goto IL_0b90;
			}
		}

IL_0b6b:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_639 = V_18;
			NullCheck(L_639);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_640;
			L_640 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_639, /*hidden argument*/NULL);
			NullCheck(L_640);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_641;
			L_641 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_640, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_642 = __this->get_address_of_tempScore_9();
			String_t* L_643;
			L_643 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_642, /*hidden argument*/NULL);
			String_t* L_644 = L_643;
			G_B164_0 = L_644;
			G_B164_1 = L_641;
			if (L_644)
			{
				G_B165_0 = L_644;
				G_B165_1 = L_641;
				goto IL_0b8b;
			}
		}

IL_0b85:
		{
			G_B165_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B165_1 = G_B164_1;
		}

IL_0b8b:
		{
			NullCheck(G_B165_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B165_1, G_B165_0, /*hidden argument*/NULL);
		}

IL_0b90:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_645 = V_2;
			NullCheck(L_645);
			bool L_646;
			L_646 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_645);
			if (L_646)
			{
				goto IL_0b4b;
			}
		}

IL_0b98:
		{
			IL2CPP_LEAVE(0xBAE, FINALLY_0b9a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0b9a;
	}

FINALLY_0b9a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_647 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_647, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_648 = V_4;
			if (!L_648)
			{
				goto IL_0bad;
			}
		}

IL_0ba6:
		{
			RuntimeObject* L_649 = V_4;
			NullCheck(L_649);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_649);
		}

IL_0bad:
		{
			IL2CPP_END_FINALLY(2970)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(2970)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xBAE, IL_0bae)
	}

IL_0bae:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_19 = 0;
		goto IL_0ccb;
	}

IL_0bbd:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3] && units[i] != unitsTemp[4] && units[i] != unitsTemp[5] && units[i] != unitsTemp[6] && units[i] != unitsTemp[7])
		int32_t L_650 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_651 = __this->get_unitsScore_8();
		int32_t L_652 = V_19;
		NullCheck(L_651);
		int32_t L_653 = L_652;
		int32_t L_654 = (L_651)->GetAt(static_cast<il2cpp_array_size_t>(L_653));
		if ((((int32_t)L_650) >= ((int32_t)L_654)))
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_655 = __this->get_units_6();
		int32_t L_656 = V_19;
		NullCheck(L_655);
		int32_t L_657 = L_656;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_658 = (L_655)->GetAt(static_cast<il2cpp_array_size_t>(L_657));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_659 = __this->get_unitsTemp_7();
		NullCheck(L_659);
		int32_t L_660 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_661 = (L_659)->GetAt(static_cast<il2cpp_array_size_t>(L_660));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_662;
		L_662 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_658, L_661, /*hidden argument*/NULL);
		if (!L_662)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_663 = __this->get_units_6();
		int32_t L_664 = V_19;
		NullCheck(L_663);
		int32_t L_665 = L_664;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_666 = (L_663)->GetAt(static_cast<il2cpp_array_size_t>(L_665));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_667 = __this->get_unitsTemp_7();
		NullCheck(L_667);
		int32_t L_668 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_669 = (L_667)->GetAt(static_cast<il2cpp_array_size_t>(L_668));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_670;
		L_670 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_666, L_669, /*hidden argument*/NULL);
		if (!L_670)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_671 = __this->get_units_6();
		int32_t L_672 = V_19;
		NullCheck(L_671);
		int32_t L_673 = L_672;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_674 = (L_671)->GetAt(static_cast<il2cpp_array_size_t>(L_673));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_675 = __this->get_unitsTemp_7();
		NullCheck(L_675);
		int32_t L_676 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_677 = (L_675)->GetAt(static_cast<il2cpp_array_size_t>(L_676));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_678;
		L_678 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_674, L_677, /*hidden argument*/NULL);
		if (!L_678)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_679 = __this->get_units_6();
		int32_t L_680 = V_19;
		NullCheck(L_679);
		int32_t L_681 = L_680;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_682 = (L_679)->GetAt(static_cast<il2cpp_array_size_t>(L_681));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_683 = __this->get_unitsTemp_7();
		NullCheck(L_683);
		int32_t L_684 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_685 = (L_683)->GetAt(static_cast<il2cpp_array_size_t>(L_684));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_686;
		L_686 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_682, L_685, /*hidden argument*/NULL);
		if (!L_686)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_687 = __this->get_units_6();
		int32_t L_688 = V_19;
		NullCheck(L_687);
		int32_t L_689 = L_688;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_690 = (L_687)->GetAt(static_cast<il2cpp_array_size_t>(L_689));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_691 = __this->get_unitsTemp_7();
		NullCheck(L_691);
		int32_t L_692 = 4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_693 = (L_691)->GetAt(static_cast<il2cpp_array_size_t>(L_692));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_694;
		L_694 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_690, L_693, /*hidden argument*/NULL);
		if (!L_694)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_695 = __this->get_units_6();
		int32_t L_696 = V_19;
		NullCheck(L_695);
		int32_t L_697 = L_696;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_698 = (L_695)->GetAt(static_cast<il2cpp_array_size_t>(L_697));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_699 = __this->get_unitsTemp_7();
		NullCheck(L_699);
		int32_t L_700 = 5;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_701 = (L_699)->GetAt(static_cast<il2cpp_array_size_t>(L_700));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_702;
		L_702 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_698, L_701, /*hidden argument*/NULL);
		if (!L_702)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_703 = __this->get_units_6();
		int32_t L_704 = V_19;
		NullCheck(L_703);
		int32_t L_705 = L_704;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_706 = (L_703)->GetAt(static_cast<il2cpp_array_size_t>(L_705));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_707 = __this->get_unitsTemp_7();
		NullCheck(L_707);
		int32_t L_708 = 6;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_709 = (L_707)->GetAt(static_cast<il2cpp_array_size_t>(L_708));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_710;
		L_710 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_706, L_709, /*hidden argument*/NULL);
		if (!L_710)
		{
			goto IL_0cc5;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_711 = __this->get_units_6();
		int32_t L_712 = V_19;
		NullCheck(L_711);
		int32_t L_713 = L_712;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_714 = (L_711)->GetAt(static_cast<il2cpp_array_size_t>(L_713));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_715 = __this->get_unitsTemp_7();
		NullCheck(L_715);
		int32_t L_716 = 7;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_717 = (L_715)->GetAt(static_cast<il2cpp_array_size_t>(L_716));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_718;
		L_718 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_714, L_717, /*hidden argument*/NULL);
		if (!L_718)
		{
			goto IL_0cc5;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_719 = __this->get_unitsScore_8();
		int32_t L_720 = V_19;
		NullCheck(L_719);
		int32_t L_721 = L_720;
		int32_t L_722 = (L_719)->GetAt(static_cast<il2cpp_array_size_t>(L_721));
		__this->set_tempScore_9(L_722);
		// unitsTemp[8] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_723 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_724 = __this->get_units_6();
		int32_t L_725 = V_19;
		NullCheck(L_724);
		int32_t L_726 = L_725;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_727 = (L_724)->GetAt(static_cast<il2cpp_array_size_t>(L_726));
		NullCheck(L_723);
		ArrayElementTypeCheck (L_723, L_727);
		(L_723)->SetAt(static_cast<il2cpp_array_size_t>(8), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_727);
		// tempUnit = i;
		int32_t L_728 = V_19;
		__this->set_tempUnit_10(L_728);
	}

IL_0cc5:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_729 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_729, (int32_t)1));
	}

IL_0ccb:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_730 = V_19;
		if ((((int32_t)L_730) < ((int32_t)((int32_t)10))))
		{
			goto IL_0bbd;
		}
	}

IL_0cd4:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_731 = __this->get_unitsGUI_5();
		int32_t L_732 = __this->get_tempUnit_10();
		NullCheck(L_731);
		int32_t L_733 = L_732;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_734 = (L_731)->GetAt(static_cast<il2cpp_array_size_t>(L_733));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_735;
		L_735 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_734, /*hidden argument*/NULL);
		if (!L_735)
		{
			goto IL_0ed5;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-4f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_736 = __this->get_unitsGUI_5();
		int32_t L_737 = __this->get_tempUnit_10();
		NullCheck(L_736);
		int32_t L_738 = L_737;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_739 = (L_736)->GetAt(static_cast<il2cpp_array_size_t>(L_738));
		NullCheck(L_739);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_740;
		L_740 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_739, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_741;
		memset((&L_741), 0, sizeof(L_741));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_741), (0.0f), (-4.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_740);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_740, L_741, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_742 = __this->get_unitsGUI_5();
		int32_t L_743 = __this->get_tempUnit_10();
		NullCheck(L_742);
		int32_t L_744 = L_743;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_745 = (L_742)->GetAt(static_cast<il2cpp_array_size_t>(L_744));
		NullCheck(L_745);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_746;
		L_746 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_745, /*hidden argument*/NULL);
		NullCheck(L_746);
		RuntimeObject* L_747;
		L_747 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_746, /*hidden argument*/NULL);
		V_2 = L_747;
	}

IL_0d2e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0d75;
		}

IL_0d30:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_748 = V_2;
			NullCheck(L_748);
			RuntimeObject * L_749;
			L_749 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_748);
			V_20 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_749, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_750 = V_20;
			NullCheck(L_750);
			String_t* L_751;
			L_751 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_750, /*hidden argument*/NULL);
			bool L_752;
			L_752 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_751, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_752)
			{
				goto IL_0d75;
			}
		}

IL_0d50:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_753 = V_20;
			NullCheck(L_753);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_754;
			L_754 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_753, /*hidden argument*/NULL);
			NullCheck(L_754);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_755;
			L_755 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_754, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_756 = __this->get_address_of_tempScore_9();
			String_t* L_757;
			L_757 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_756, /*hidden argument*/NULL);
			String_t* L_758 = L_757;
			G_B189_0 = L_758;
			G_B189_1 = L_755;
			if (L_758)
			{
				G_B190_0 = L_758;
				G_B190_1 = L_755;
				goto IL_0d70;
			}
		}

IL_0d6a:
		{
			G_B190_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B190_1 = G_B189_1;
		}

IL_0d70:
		{
			NullCheck(G_B190_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B190_1, G_B190_0, /*hidden argument*/NULL);
		}

IL_0d75:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_759 = V_2;
			NullCheck(L_759);
			bool L_760;
			L_760 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_759);
			if (L_760)
			{
				goto IL_0d30;
			}
		}

IL_0d7d:
		{
			IL2CPP_LEAVE(0xD93, FINALLY_0d7f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0d7f;
	}

FINALLY_0d7f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_761 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_761, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_762 = V_4;
			if (!L_762)
			{
				goto IL_0d92;
			}
		}

IL_0d8b:
		{
			RuntimeObject* L_763 = V_4;
			NullCheck(L_763);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_763);
		}

IL_0d92:
		{
			IL2CPP_END_FINALLY(3455)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(3455)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xD93, IL_0d93)
	}

IL_0d93:
	{
		// tempScore = -1;
		__this->set_tempScore_9((-1));
		// for(int i = 0; i < 10; i++)
		V_21 = 0;
		goto IL_0ecc;
	}

IL_0da2:
	{
		// if (tempScore < unitsScore[i] && units[i] != unitsTemp[0] && units[i] != unitsTemp[1] && units[i] != unitsTemp[2] && units[i] != unitsTemp[3] && units[i] != unitsTemp[4] && units[i] != unitsTemp[5] && units[i] != unitsTemp[6] && units[i] != unitsTemp[7] && units[i] != unitsTemp[8])
		int32_t L_764 = __this->get_tempScore_9();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_765 = __this->get_unitsScore_8();
		int32_t L_766 = V_21;
		NullCheck(L_765);
		int32_t L_767 = L_766;
		int32_t L_768 = (L_765)->GetAt(static_cast<il2cpp_array_size_t>(L_767));
		if ((((int32_t)L_764) >= ((int32_t)L_768)))
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_769 = __this->get_units_6();
		int32_t L_770 = V_21;
		NullCheck(L_769);
		int32_t L_771 = L_770;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_772 = (L_769)->GetAt(static_cast<il2cpp_array_size_t>(L_771));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_773 = __this->get_unitsTemp_7();
		NullCheck(L_773);
		int32_t L_774 = 0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_775 = (L_773)->GetAt(static_cast<il2cpp_array_size_t>(L_774));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_776;
		L_776 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_772, L_775, /*hidden argument*/NULL);
		if (!L_776)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_777 = __this->get_units_6();
		int32_t L_778 = V_21;
		NullCheck(L_777);
		int32_t L_779 = L_778;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_780 = (L_777)->GetAt(static_cast<il2cpp_array_size_t>(L_779));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_781 = __this->get_unitsTemp_7();
		NullCheck(L_781);
		int32_t L_782 = 1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_783 = (L_781)->GetAt(static_cast<il2cpp_array_size_t>(L_782));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_784;
		L_784 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_780, L_783, /*hidden argument*/NULL);
		if (!L_784)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_785 = __this->get_units_6();
		int32_t L_786 = V_21;
		NullCheck(L_785);
		int32_t L_787 = L_786;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_788 = (L_785)->GetAt(static_cast<il2cpp_array_size_t>(L_787));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_789 = __this->get_unitsTemp_7();
		NullCheck(L_789);
		int32_t L_790 = 2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_791 = (L_789)->GetAt(static_cast<il2cpp_array_size_t>(L_790));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_792;
		L_792 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_788, L_791, /*hidden argument*/NULL);
		if (!L_792)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_793 = __this->get_units_6();
		int32_t L_794 = V_21;
		NullCheck(L_793);
		int32_t L_795 = L_794;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_796 = (L_793)->GetAt(static_cast<il2cpp_array_size_t>(L_795));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_797 = __this->get_unitsTemp_7();
		NullCheck(L_797);
		int32_t L_798 = 3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_799 = (L_797)->GetAt(static_cast<il2cpp_array_size_t>(L_798));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_800;
		L_800 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_796, L_799, /*hidden argument*/NULL);
		if (!L_800)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_801 = __this->get_units_6();
		int32_t L_802 = V_21;
		NullCheck(L_801);
		int32_t L_803 = L_802;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_804 = (L_801)->GetAt(static_cast<il2cpp_array_size_t>(L_803));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_805 = __this->get_unitsTemp_7();
		NullCheck(L_805);
		int32_t L_806 = 4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_807 = (L_805)->GetAt(static_cast<il2cpp_array_size_t>(L_806));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_808;
		L_808 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_804, L_807, /*hidden argument*/NULL);
		if (!L_808)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_809 = __this->get_units_6();
		int32_t L_810 = V_21;
		NullCheck(L_809);
		int32_t L_811 = L_810;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_812 = (L_809)->GetAt(static_cast<il2cpp_array_size_t>(L_811));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_813 = __this->get_unitsTemp_7();
		NullCheck(L_813);
		int32_t L_814 = 5;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_815 = (L_813)->GetAt(static_cast<il2cpp_array_size_t>(L_814));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_816;
		L_816 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_812, L_815, /*hidden argument*/NULL);
		if (!L_816)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_817 = __this->get_units_6();
		int32_t L_818 = V_21;
		NullCheck(L_817);
		int32_t L_819 = L_818;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_820 = (L_817)->GetAt(static_cast<il2cpp_array_size_t>(L_819));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_821 = __this->get_unitsTemp_7();
		NullCheck(L_821);
		int32_t L_822 = 6;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_823 = (L_821)->GetAt(static_cast<il2cpp_array_size_t>(L_822));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_824;
		L_824 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_820, L_823, /*hidden argument*/NULL);
		if (!L_824)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_825 = __this->get_units_6();
		int32_t L_826 = V_21;
		NullCheck(L_825);
		int32_t L_827 = L_826;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_828 = (L_825)->GetAt(static_cast<il2cpp_array_size_t>(L_827));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_829 = __this->get_unitsTemp_7();
		NullCheck(L_829);
		int32_t L_830 = 7;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_831 = (L_829)->GetAt(static_cast<il2cpp_array_size_t>(L_830));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_832;
		L_832 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_828, L_831, /*hidden argument*/NULL);
		if (!L_832)
		{
			goto IL_0ec6;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_833 = __this->get_units_6();
		int32_t L_834 = V_21;
		NullCheck(L_833);
		int32_t L_835 = L_834;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_836 = (L_833)->GetAt(static_cast<il2cpp_array_size_t>(L_835));
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_837 = __this->get_unitsTemp_7();
		NullCheck(L_837);
		int32_t L_838 = 8;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_839 = (L_837)->GetAt(static_cast<il2cpp_array_size_t>(L_838));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_840;
		L_840 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_836, L_839, /*hidden argument*/NULL);
		if (!L_840)
		{
			goto IL_0ec6;
		}
	}
	{
		// tempScore = unitsScore[i];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_841 = __this->get_unitsScore_8();
		int32_t L_842 = V_21;
		NullCheck(L_841);
		int32_t L_843 = L_842;
		int32_t L_844 = (L_841)->GetAt(static_cast<il2cpp_array_size_t>(L_843));
		__this->set_tempScore_9(L_844);
		// unitsTemp[9] = units[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_845 = __this->get_unitsTemp_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_846 = __this->get_units_6();
		int32_t L_847 = V_21;
		NullCheck(L_846);
		int32_t L_848 = L_847;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_849 = (L_846)->GetAt(static_cast<il2cpp_array_size_t>(L_848));
		NullCheck(L_845);
		ArrayElementTypeCheck (L_845, L_849);
		(L_845)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_849);
		// tempUnit = i;
		int32_t L_850 = V_21;
		__this->set_tempUnit_10(L_850);
	}

IL_0ec6:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_851 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_851, (int32_t)1));
	}

IL_0ecc:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_852 = V_21;
		if ((((int32_t)L_852) < ((int32_t)((int32_t)10))))
		{
			goto IL_0da2;
		}
	}

IL_0ed5:
	{
		// if (unitsGUI[tempUnit]){
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_853 = __this->get_unitsGUI_5();
		int32_t L_854 = __this->get_tempUnit_10();
		NullCheck(L_853);
		int32_t L_855 = L_854;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_856 = (L_853)->GetAt(static_cast<il2cpp_array_size_t>(L_855));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_857;
		L_857 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_856, /*hidden argument*/NULL);
		if (!L_857)
		{
			goto IL_0f94;
		}
	}
	{
		// unitsGUI[tempUnit].transform.localPosition = new Vector3(0,-4.5f,0);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_858 = __this->get_unitsGUI_5();
		int32_t L_859 = __this->get_tempUnit_10();
		NullCheck(L_858);
		int32_t L_860 = L_859;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_861 = (L_858)->GetAt(static_cast<il2cpp_array_size_t>(L_860));
		NullCheck(L_861);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_862;
		L_862 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_861, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_863;
		memset((&L_863), 0, sizeof(L_863));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_863), (0.0f), (-4.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_862);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_862, L_863, /*hidden argument*/NULL);
		// foreach (Transform child in unitsGUI[tempUnit].transform)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_864 = __this->get_unitsGUI_5();
		int32_t L_865 = __this->get_tempUnit_10();
		NullCheck(L_864);
		int32_t L_866 = L_865;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_867 = (L_864)->GetAt(static_cast<il2cpp_array_size_t>(L_866));
		NullCheck(L_867);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_868;
		L_868 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_867, /*hidden argument*/NULL);
		NullCheck(L_868);
		RuntimeObject* L_869;
		L_869 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_868, /*hidden argument*/NULL);
		V_2 = L_869;
	}

IL_0f2f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0f76;
		}

IL_0f31:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_870 = V_2;
			NullCheck(L_870);
			RuntimeObject * L_871;
			L_871 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_870);
			V_22 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_871, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_872 = V_22;
			NullCheck(L_872);
			String_t* L_873;
			L_873 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_872, /*hidden argument*/NULL);
			bool L_874;
			L_874 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_873, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
			if (!L_874)
			{
				goto IL_0f76;
			}
		}

IL_0f51:
		{
			// if (child.name == "count") child.gameObject.GetComponent<TextMesh>().text = tempScore+"";
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_875 = V_22;
			NullCheck(L_875);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_876;
			L_876 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_875, /*hidden argument*/NULL);
			NullCheck(L_876);
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_877;
			L_877 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_876, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
			int32_t* L_878 = __this->get_address_of_tempScore_9();
			String_t* L_879;
			L_879 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_878, /*hidden argument*/NULL);
			String_t* L_880 = L_879;
			G_B215_0 = L_880;
			G_B215_1 = L_877;
			if (L_880)
			{
				G_B216_0 = L_880;
				G_B216_1 = L_877;
				goto IL_0f71;
			}
		}

IL_0f6b:
		{
			G_B216_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B216_1 = G_B215_1;
		}

IL_0f71:
		{
			NullCheck(G_B216_1);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B216_1, G_B216_0, /*hidden argument*/NULL);
		}

IL_0f76:
		{
			// foreach (Transform child in unitsGUI[tempUnit].transform)
			RuntimeObject* L_881 = V_2;
			NullCheck(L_881);
			bool L_882;
			L_882 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_881);
			if (L_882)
			{
				goto IL_0f31;
			}
		}

IL_0f7e:
		{
			IL2CPP_LEAVE(0xF94, FINALLY_0f80);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0f80;
	}

FINALLY_0f80:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_883 = V_2;
			V_4 = ((RuntimeObject*)IsInst((RuntimeObject*)L_883, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_884 = V_4;
			if (!L_884)
			{
				goto IL_0f93;
			}
		}

IL_0f8c:
		{
			RuntimeObject* L_885 = V_4;
			NullCheck(L_885);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_885);
		}

IL_0f93:
		{
			IL2CPP_END_FINALLY(3968)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(3968)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xF94, IL_0f94)
	}

IL_0f94:
	{
		// }
		return;
	}
}
// System.Void guiRankSystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void guiRankSystem__ctor_mA51632BE792772E8519B90F90364F025222ED2BE (guiRankSystem_t97A43D634F8131AB448F00704C8B46C3E92B5A15 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void textCountPet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void textCountPet_Start_m65E746456D2B48D724B9189AC747969BB28F2DE6 (textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void textCountPet::startText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void textCountPet_startText_m6519369B424DAFC7AF3C4E64C442B3DAC5333250 (textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5 * __this, String_t* ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<TextMesh>().text = t;
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_0;
		L_0 = Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		String_t* L_1 = ___t0;
		NullCheck(L_0);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_0, L_1, /*hidden argument*/NULL);
		// textGo = true;
		__this->set_textGo_5((bool)1);
		// timer = 1.3f;
		__this->set_timer_4((1.29999995f));
		// }
		return;
	}
}
// System.Void textCountPet::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void textCountPet_Update_m9D3CA3B9DA878FF1A398656A1A71ADACD11E4097 (textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (textGo)
		bool L_0 = __this->get_textGo_5();
		if (!L_0)
		{
			goto IL_0065;
		}
	}
	{
		// timer -= Time.deltaTime;
		float L_1 = __this->get_timer_4();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_4(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		// if (timer < 0)
		float L_3 = __this->get_timer_4();
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		// Destroy(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_4, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0033:
	{
		// if (timer < 1) GetComponent<TextMesh>().color = new Color(1f, 0.7f, 0, timer);
		float L_5 = __this->get_timer_4();
		if ((!(((float)L_5) < ((float)(1.0f)))))
		{
			goto IL_0065;
		}
	}
	{
		// if (timer < 1) GetComponent<TextMesh>().color = new Color(1f, 0.7f, 0, timer);
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_6;
		L_6 = Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		float L_7 = __this->get_timer_4();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_8), (1.0f), (0.699999988f), (0.0f), L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		TextMesh_set_color_m276CE9AB9F88B34C0A9C6DD5300FC1123102D3DE(L_6, L_8, /*hidden argument*/NULL);
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void textCountPet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void textCountPet__ctor_m4FE7C5E3CEBADF0A7908261DA894D12E1EB85F36 (textCountPet_t222F43AB6BC6939A62B0045DCD143B11CF87F8A5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void translateLanguages::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void translateLanguages_Awake_mE9B9BDF764535E4C43865C5EF9D56EFDC2B012B1 (translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE280D065A824A791F8305234D3E093FC9A5A90C7);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B11_0 = 0;
	{
		// var lang = Application.systemLanguage;
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		// string newtext = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// if (lang == SystemLanguage.Russian && rus != "") newtext = rus;
		int32_t L_1 = L_0;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)30)))))
		{
			G_B3_0 = L_1;
			goto IL_0029;
		}
	}
	{
		String_t* L_2 = __this->get_rus_4();
		bool L_3;
		L_3 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_2, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0029;
		}
	}
	{
		// if (lang == SystemLanguage.Russian && rus != "") newtext = rus;
		String_t* L_4 = __this->get_rus_4();
		V_0 = L_4;
		G_B3_0 = G_B2_0;
	}

IL_0029:
	{
		// if (lang == SystemLanguage.Arabic && Oman != "") newtext = Oman;
		int32_t L_5 = G_B3_0;
		G_B4_0 = L_5;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B6_0 = L_5;
			goto IL_0046;
		}
	}
	{
		String_t* L_6 = __this->get_Oman_5();
		bool L_7;
		L_7 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_6, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		G_B5_0 = G_B4_0;
		if (!L_7)
		{
			G_B6_0 = G_B4_0;
			goto IL_0046;
		}
	}
	{
		// if (lang == SystemLanguage.Arabic && Oman != "") newtext = Oman;
		String_t* L_8 = __this->get_Oman_5();
		V_0 = L_8;
		G_B6_0 = G_B5_0;
	}

IL_0046:
	{
		// if (lang == SystemLanguage.French && France != "") newtext = France;
		int32_t L_9 = G_B6_0;
		G_B7_0 = L_9;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)14)))))
		{
			G_B9_0 = L_9;
			goto IL_0064;
		}
	}
	{
		String_t* L_10 = __this->get_France_6();
		bool L_11;
		L_11 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_10, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if (!L_11)
		{
			G_B9_0 = G_B7_0;
			goto IL_0064;
		}
	}
	{
		// if (lang == SystemLanguage.French && France != "") newtext = France;
		String_t* L_12 = __this->get_France_6();
		V_0 = L_12;
		G_B9_0 = G_B8_0;
	}

IL_0064:
	{
		// if (lang == SystemLanguage.Vietnamese && Vietnam != "") newtext = Vietnam;
		int32_t L_13 = G_B9_0;
		G_B10_0 = L_13;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)39)))))
		{
			G_B12_0 = L_13;
			goto IL_0082;
		}
	}
	{
		String_t* L_14 = __this->get_Vietnam_7();
		bool L_15;
		L_15 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_14, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		G_B11_0 = G_B10_0;
		if (!L_15)
		{
			G_B12_0 = G_B10_0;
			goto IL_0082;
		}
	}
	{
		// if (lang == SystemLanguage.Vietnamese && Vietnam != "") newtext = Vietnam;
		String_t* L_16 = __this->get_Vietnam_7();
		V_0 = L_16;
		G_B12_0 = G_B11_0;
	}

IL_0082:
	{
		// if (lang == SystemLanguage.Portuguese && Brazil != "") newtext = Brazil;
		if ((!(((uint32_t)G_B12_0) == ((uint32_t)((int32_t)28)))))
		{
			goto IL_009f;
		}
	}
	{
		String_t* L_17 = __this->get_Brazil_8();
		bool L_18;
		L_18 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_17, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_009f;
		}
	}
	{
		// if (lang == SystemLanguage.Portuguese && Brazil != "") newtext = Brazil;
		String_t* L_19 = __this->get_Brazil_8();
		V_0 = L_19;
	}

IL_009f:
	{
		// if (newtext != "")
		String_t* L_20 = V_0;
		bool L_21;
		L_21 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_20, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_014a;
		}
	}
	{
		// if (canvass == false)
		bool L_22 = __this->get_canvass_9();
		if (L_22)
		{
			goto IL_0101;
		}
	}
	{
		// GetComponent<TextMesh>().text = newtext;
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_23;
		L_23 = Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_mCC9FD2C0BE9B8D38A7FAA28AD8C4228AC43D4860_RuntimeMethod_var);
		String_t* L_24 = V_0;
		NullCheck(L_23);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_23, L_24, /*hidden argument*/NULL);
		// var TextScript = gameObject.GetComponent<TextMesh>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25;
		L_25 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_26;
		L_26 = GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00(L_25, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273_m81EC88132D7EDC335BD0C2BE0287BC44F468FC00_RuntimeMethod_var);
		// var texts = TextScript.text.Replace("\\n","\n");
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_27 = L_26;
		NullCheck(L_27);
		String_t* L_28;
		L_28 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29;
		L_29 = String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D(L_28, _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		V_1 = L_29;
		// texts = TextScript.text.Replace("*","\n");
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_30 = L_27;
		NullCheck(L_30);
		String_t* L_31;
		L_31 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_32;
		L_32 = String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D(L_31, _stringLiteralE280D065A824A791F8305234D3E093FC9A5A90C7, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		V_1 = L_32;
		// TextScript.text = texts;
		String_t* L_33 = V_1;
		NullCheck(L_30);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_30, L_33, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0101:
	{
		// GetComponent<Text>().text = newtext;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_34;
		L_34 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(__this, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		String_t* L_35 = V_0;
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, L_35);
		// var TextScript = gameObject.GetComponent<Text>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36;
		L_36 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_37;
		L_37 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_36, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		// var texts = TextScript.text.Replace("\\n","\n");
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_38 = L_37;
		NullCheck(L_38);
		String_t* L_39;
		L_39 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_38);
		NullCheck(L_39);
		String_t* L_40;
		L_40 = String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D(L_39, _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		V_2 = L_40;
		// texts = TextScript.text.Replace("*","\n");
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_41 = L_38;
		NullCheck(L_41);
		String_t* L_42;
		L_42 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_41);
		NullCheck(L_42);
		String_t* L_43;
		L_43 = String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D(L_42, _stringLiteralE280D065A824A791F8305234D3E093FC9A5A90C7, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		V_2 = L_43;
		// TextScript.text = texts;
		String_t* L_44 = V_2;
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_44);
	}

IL_014a:
	{
		// }
		return;
	}
}
// System.Void translateLanguages::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void translateLanguages__ctor_mB6854D5A6353DB164F09F51D9F0E7AE14ED4D104 (translateLanguages_t0E56AB9599070ADCD6E6D1DE9A97CFBE17338E8D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void wwwBlock::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void wwwBlock_Start_m8757300AF42ED16FD1C7280530231534CA179CC5 (wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void wwwBlock::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void wwwBlock_Update_m22CA0459B98BA43B4C6103608E6820525F903045 (wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void wwwBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void wwwBlock__ctor_m3E2FF37683CBF51ACE0C4A6B3EB18CFB0D5A396B (wwwBlock_t1D1C83DF12E3ABABF6F0FE253266E9078A4C4529 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckIfAliveU3Ed__2__ctor_m9CFF1B8CDF82583983832BA103A4BA18C983D3E2 (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_m07DEC90B24059ACF5EB13FFD45A081B3F7366B53 (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCheckIfAliveU3Ed__2_MoveNext_mFF6D5E6297855647BEB70161FE8043152EBFE4BC (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_m91CE0171326B90198E69EAFA60F45473CAC6E0C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// ParticleSystem ps = this.GetComponent<ParticleSystem>();
		CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * L_4 = V_1;
		NullCheck(L_4);
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_5;
		L_5 = Component_GetComponent_TisParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_m91CE0171326B90198E69EAFA60F45473CAC6E0C3(L_4, /*hidden argument*/Component_GetComponent_TisParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_m91CE0171326B90198E69EAFA60F45473CAC6E0C3_RuntimeMethod_var);
		__this->set_U3CpsU3E5__2_3(L_5);
		goto IL_007d;
	}

IL_002c:
	{
		// yield return new WaitForSeconds(0.5f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_6 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_6, (0.5f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0045:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if(!ps.IsAlive(true))
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_7 = __this->get_U3CpsU3E5__2_3();
		NullCheck(L_7);
		bool L_8;
		L_8 = ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9(L_7, (bool)1, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007d;
		}
	}
	{
		// if(OnlyDeactivate)
		CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * L_9 = V_1;
		NullCheck(L_9);
		bool L_10 = L_9->get_OnlyDeactivate_4();
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		// this.gameObject.SetActive(false);
		CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * L_11 = V_1;
		NullCheck(L_11);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_12, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_008b;
	}

IL_0070:
	{
		// GameObject.Destroy(this.gameObject);
		CFX_AutoDestructShuriken_t0CCD98B6AC0F30A880B129CB89476F24817440DA * L_13 = V_1;
		NullCheck(L_13);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_14, /*hidden argument*/NULL);
		// break;
		goto IL_008b;
	}

IL_007d:
	{
		// while(true && ps != null)
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_15 = __this->get_U3CpsU3E5__2_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_15, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_002c;
		}
	}

IL_008b:
	{
		// }
		return (bool)0;
	}
}
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43BF71BDE8530EE51B7418043E96F489B3B8C38B (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mEF16B074432DE98F427DAA1007C84C6ADD3B6E6F (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mEF16B074432DE98F427DAA1007C84C6ADD3B6E6F_RuntimeMethod_var)));
	}
}
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mF25D234AC7B0F5FCA156FA8210CBBB703217EB2D (U3CCheckIfAliveU3Ed__2_tF58339866D5A2C73DBFBEFB905F8C77641412272 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
